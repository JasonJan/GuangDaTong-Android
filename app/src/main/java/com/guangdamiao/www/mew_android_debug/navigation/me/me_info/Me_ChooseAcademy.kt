package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.*
import es.dmoral.toasty.Toasty

class Me_ChooseAcademy : Activity() {

    private var mListView: ListView? = null//从别人得到暂用
    private var items: ArrayList<String> = ArrayList<String>()//从别人得到暂用
    private var choose_academy_back_btn: Button?=null
    private var resultFromServer: String? = null
    private var bundle:Bundle?=null
    private val TAG= Constant.Choose_Academy_TAG
    private var choose_academy_status:ImageView?=null
    private var old_college:String=""
    private var loading_first:Loading_view?=null


    public override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        fullscreen()
        bundle=intent.extras//接收别人传过来的bundle
        if(bundle!!.getString("me_info_college")!=null){
            old_college=bundle!!.getString("me_info_college")
        }
        if(NetUtil.checkNetWork(this)){
            loading_first=Loading_view(this@Me_ChooseAcademy,R.style.CustomDialog)
            loading_first!!.show()
            myServerRequest()
        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }


        setContentView(R.layout.activity_choose_academy)//加载主布局
        mListView = findViewById(R.id.choose_academy_listview) as ListView
        choose_academy_back_btn=findViewById(R.id.choose_academy_back_btn) as Button
        LogUtils.d_debugprint(TAG,items.toString())
        LogUtils.d_debugprint("bundle此刻为：",bundle.toString())
        addSomeListener()

    }

    fun addSomeListener(){
        val itemClickListener= AdapterView.OnItemClickListener { parent, view, position, id ->

            LogUtils.d_debugprint(TAG,"第"+position+"个学院")
            LogUtils.d_debugprint(TAG,"学院名称："+items[position])
            bundle!!.putString("college",items[position])
            bundle!!.putInt("academyPosition",position)
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseAcademy,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        mListView!!.choiceMode=ListView.CHOICE_MODE_SINGLE
        mListView!!.setOnItemClickListener(itemClickListener)
        choose_academy_back_btn!!.setOnClickListener{
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseAcademy,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        choose_academy_back_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                choose_academy_back_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                choose_academy_back_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                choose_academy_back_btn!!.setBackgroundColor(Color.TRANSPARENT)
                choose_academy_back_btn!!.alpha=1.0f
            }
            false
        }
    }

    /**
     * 向服务器请求学院数据
     */
    fun myServerRequest(){
        val pageSize= Constant.PAGESIZE
        val pageIndex=0
        val school= Constant.SCHOOLDEFULT
        val sign:String
        val urlPath:String
        if(LogUtils.APP_IS_DEBUG){
            sign= MD5Util.md5(Constant.SALTFORTEST)
            urlPath= Constant.BASEURLFORTEST + Constant.OrganizingData_Academy
        }else{
            sign= MD5Util.md5(Constant.SALTFORRELEASE)
            urlPath= Constant.BASEURLFORRELEASE + Constant.OrganizingData_Academy
        }
        val params= mapOf<String,Any?>("pageSize" to pageSize,"pageIndex" to pageIndex,"school" to school,"sign" to sign)
        val encode=Constant.ENCODE
        val handler_result=object: Handler(){
            override fun handleMessage(msg: Message) {
                if (msg.what == 0) {
                    LogUtils.d_debugprint(TAG, resultFromServer!!)
                    var getCode: String = ""
                    getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!)
                    if (Constant.RIGHTCODE.equals(getCode)) {
                        var result: String = ""
                        var colleges: String = ""
                        var list: List<String>?
                        result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServer!!)
                        colleges = JsonUtil.get_key_string("colleges", result)
                        list = JsonUtil.getList("colleges", result)
                        for (i in list.indices) {
                            items!!.add(list.get(i))
                        }
                        mListView!!.adapter = CustomListAdapter(applicationContext)//加载getView
                        LogUtils.d_debugprint(TAG,items.toString())
                        if(loading_first!=null){
                            loading_first!!.cancel()
                        }
                    } else {
                        Toasty.error(this@Me_ChooseAcademy,Constant.OrginizingData_No_Academy).show()
                    }
                }
                super.handleMessage(msg)
            }
        }
        Thread(Runnable{
            resultFromServer= HttpUtil.httpPost(urlPath,params,encode)
            LogUtils.d_debugprint(TAG,"服务器返回数据："+resultFromServer!!)
            handler_result.sendEmptyMessage(0)
        }).start()
    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    internal inner class CustomListAdapter(context: Context) : BaseAdapter() {
        private val mInflater: LayoutInflater//从别人得到 自己用
        private var mContext: Context? = null//从别人得到 自己用
        init {
            mContext = context
            mInflater = LayoutInflater.from(mContext)
        }
        override fun getItem(arg0: Int): Any {

            return items!![arg0]
        }
        override fun getItemId(position: Int): Long {

            return position.toLong()
        }
        override fun getCount(): Int {

            return items!!.size
        }
        override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
            var convertView = convertView
            val indexText: TextView
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_coustom, null)
            }
            indexText = convertView!!.findViewById(R.id.choose_academy_listitem) as TextView
            indexText.text = items!![position]
            choose_academy_status=convertView.findViewById(R.id.choose_academy_status) as ImageView
            if(indexText.text.equals(old_college)){
                choose_academy_status!!.visibility=View.VISIBLE
            }else{
                choose_academy_status!!.visibility=View.GONE
            }
            indexText.setTextColor(Color.BLACK)
            return convertView
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseAcademy,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
