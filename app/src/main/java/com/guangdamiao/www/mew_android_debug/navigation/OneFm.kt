package com.guangdamiao.www.mew_android_debug.navigation


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.banner.CycleViewPager
import com.guangdamiao.www.mew_android_debug.banner.ViewFactory
import com.guangdamiao.www.mew_android_debug.bean.HTab
import com.guangdamiao.www.mew_android_debug.bean.HTabDb
import com.guangdamiao.www.mew_android_debug.bean.MyImage
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.school.*
import com.guangdamiao.www.mew_android_debug.navigation.school.message.*
import com.guangdamiao.www.mew_android_debug.utils.HttpUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.util.*


class OneFm : Fragment(), ViewPager.OnPageChangeListener {

    private var view_: View? = null
    private var rg_: RadioGroup? = null
    private var vp_: ViewPager? = null
    private var hv_: HorizontalScrollView? = null
    private val newsList = ArrayList<Fragment>()
    private var adapter: SchoolFmAdapter? = null
    private var hTabs: List<HTab>? = null//=====处理4个小分页
    var parent:ViewGroup?=null
    private val TAG = Constant.School_TAG
    private var getAdfromServer: String? = null
    private val handler_result1= Handler()
    private var jpush_type=""

    //广告栏
    private val views = ArrayList<ImageView>()
    private var cycleViewPager: CycleViewPager? = null
    private var caption: TextView? = null
    private var main_setting:ImageView?=null
    private var main_right_btn:Button?=null
    private var main_right_msg_btn:Button?=null
    private var caption_msg: ImageView? = null
    private var caption_ask_Iv:ImageView?=null
    private var caption_msg_point: TextView? = null
    private var main_ask:TextView?=null
    private var myImageList=arrayListOf<MyImage>()
    private var myImageOverAD:ImageView?=null
    var msg_all=0


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        initCaption()
        if (view_ == null) {
            view_ = inflater!!.inflate(R.layout.fm_one, container, false)//初始化view
            rg_ = view_!!.findViewById(R.id.one_rg) as RadioGroup//分页顶部的单选按钮
            vp_ = view_!!.findViewById(R.id.one_view) as ViewPager//每一个小分页
            hv_ = view_!!.findViewById(R.id.one_hv) as HorizontalScrollView//分页顶部的那条线
            myImageOverAD=view_!!.findViewById(R.id.oneFm_fragment_cycle_viewpager_content_over) as ImageView
            rg_!!.setOnCheckedChangeListener { group, id -> vp_!!.currentItem = id }
            if(NetUtil.checkNetWork(context)){
                getADfromServer()//请求服务器=====广告的地址和链接
            }else{
                // 要是没有网，就默认一张图即可
                Toasty.info(context,Constant.NONETWORK).show()
            }

            configImageLoader()
            initTab(inflater)
            initView()//初始化viewpager
            addSomeListener()

        }
        if(view_!!.parent!=null){
            parent=view_!!.parent as ViewGroup
            parent!!.removeView(view_)
        }
        return view_

    }

    override fun onStart() {
        getMsg()

        super.onStart()
    }

    override fun onResume(){
       super.onResume()
        JAnalyticsInterface.onPageStart(context,"首页主页面")
        if(msg_all>0){
            caption_msg_point!!.visibility=View.VISIBLE
        }else{
            caption_msg_point!!.visibility=View.GONE
        }
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(context,"首页主页面")
        super.onPause()
    }


   fun getMsg(){

       var msg_num=0
       val message1_unread=query1NowAll()
       val message2_unread=query2NowAll()
       val message3_unread=query3NowAll()

       val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
       msg_all = read.getInt("msg_all", 0)
       LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
       if(msg_all>0){
           LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
       }else{
           caption_msg_point!!.visibility=View.GONE
       }

       LogUtils.d_debugprint(Constant.Message_TAG,"\n\n服务器没请求前共有$msg_num 条新的消息！！!\n\n " +
               "本地message1有 $message1_unread 条未读的消息\n"+
               "本地message2有 $message2_unread 条未读的消息\n"+
               "本地message3有 $message3_unread 条未读的消息\n"+
               "所有未读的消息有 ${message1_unread+message2_unread+message3_unread+msg_num} 条\n\n")

       if(message1_unread+message2_unread+message3_unread!=0){
           val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
           editor.putInt("msg_all",message1_unread+message2_unread+message3_unread)
           editor.commit()
           val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
           val msg_all = read.getInt("msg_all", 0)
           LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
           if(msg_all>0){
               LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")

           }else{
               caption_msg_point!!.visibility=View.GONE
           }
       }

        Message_Request.getMsgNum(activity,context,Constant.Message_List_LeaveWords,Constant.Message_TAG,object:IDataRequestListener2String{
            override fun loadSuccess(response_string: String?) {
                if(response_string!=null){
                    msg_num+=response_string!!.toInt()
                    if(msg_num!=0){
                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                        editor.putInt("msg_all",message1_unread+message2_unread+message3_unread+msg_num)
                        editor.commit()
                        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                        val msg_all = read.getInt("msg_all", 0)
                        LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
                        if(msg_all>0){
                            LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
                        }else{
                            caption_msg_point!!.visibility=View.GONE
                        }
                    }
                }
                Message_Request.getMsgNum(activity,context,Constant.Message_List_AskMsg,Constant.Message_TAG,object:IDataRequestListener2String{
                    override fun loadSuccess(respsonse_string: String?) {
                        if(response_string!=null){
                            msg_num+=response_string!!.toInt()
                            if(msg_num!=0) {
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                                editor.putInt("msg_all",message1_unread+message2_unread+message3_unread+msg_num)
                                editor.commit()
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val msg_all = read.getInt("msg_all", 0)
                                LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
                                if(msg_all>0){
                                    LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
                                }else{
                                    caption_msg_point!!.visibility=View.GONE
                                }
                            }
                        }
                        Message_Request.getMsgNum(activity,context,Constant.Message_List_Notices,Constant.Message_TAG,object:IDataRequestListener2String{
                            override fun loadSuccess(response_string: String?) {
                                if(respsonse_string!=null) msg_num+=response_string!!.toInt()
                                if(msg_num!=0){
                                    val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                                    editor.putInt("msg_all",message1_unread+message2_unread+message3_unread+msg_num)
                                    editor.commit()
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val msg_all = read.getInt("msg_all", 0)
                                    LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
                                    if(msg_all>0){
                                        LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
                                    }else{
                                        caption_msg_point!!.visibility=View.GONE
                                    }

                                }else{
                                    LogUtils.d_debugprint(Constant.Message_TAG,"\n\n服务器没请求后共有$msg_num 条新的消息！！!\n\n " +
                                            "本地message1有 $message1_unread 条未读的消息\n"+
                                            "本地message2有 $message2_unread 条未读的消息\n"+
                                            "本地message3有 $message3_unread 条未读的消息\n"+
                                            "所有未读的消息有 ${message1_unread+message2_unread+message3_unread+msg_num} 条\n\n")

                                    if((message1_unread+message2_unread+message3_unread+msg_num)==0){
                                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                                        editor.putInt("msg_all",0)
                                        editor.commit()
                                        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                        val msg_all = read.getInt("msg_all", 0)
                                        LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
                                        if(msg_all>0){
                                            LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
                                        }else{
                                            caption_msg_point!!.visibility=View.GONE
                                        }
                                    }else{
                                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                                        editor.putInt("msg_all",message1_unread+message2_unread+message3_unread+msg_num)
                                        editor.commit()
                                        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                        val msg_all = read.getInt("msg_all", 0)
                                        LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart外部时，msg_all为：$msg_all \n\n")
                                        if(msg_all>0){
                                            LogUtils.d_debugprint(Constant.Message_TAG,"这里onStart时，msg_all为：$msg_all \n\n")
                                        }else{
                                            caption_msg_point!!.visibility=View.GONE
                                        }
                                    }
                                }
                            }
                        })
                    }
                })
            }
        })
   }

    private fun query1NowAll():Int{
        val sql="select * from "+ Message1SQLiteOpenHelper.M1_Name+" where isRead='0'"
        val helper1=Message1SQLiteOpenHelper(context)
        val cursor=helper1!!.readableDatabase.rawQuery(sql,null)
        var message1_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message1_unread++
            }
        }
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message1有多少条未读的：$message1_unread\n\n")
        return message1_unread

    }

    private fun query2NowAll():Int{
        val sql="select * from "+ Message2SQLiteOpenHelper.M2_Name+" where isRead='0'"
        val helper2=Message2SQLiteOpenHelper(context)
        val cursor= helper2!!.readableDatabase.rawQuery(sql,null)
        var message2_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message2_unread++
            }
        }

        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2有多少条未读的：$message2_unread\n\n")
        return message2_unread
    }

    private fun query3NowAll():Int{
        val sql="select * from "+ Message3SQLiteOpenHelper.M3_Name+" where isRead='0'"
        val helper3=Message3SQLiteOpenHelper(context)
        val cursor= helper3!!.readableDatabase.rawQuery(sql,null)
        var message3_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message3_unread++
            }
        }

        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message3有多少条未读的：$message3_unread\n\n")
        return message3_unread
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser外部时，msg_all为： \n\n")
        if(!hidden){
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val msg_all = read.getInt("msg_all", 0)
            LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser外部时，msg_all为：$msg_all \n\n")
            if(msg_all>0){
                LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser时，msg_all为：$msg_all \n\n")
                caption_msg_point!!.visibility=View.VISIBLE
            }else{
                caption_msg_point!!.visibility=View.GONE
            }

            JAnalyticsInterface.onPageStart(context,"首页Tab")
        }else{
            caption_msg_point!!.visibility=View.GONE
            JAnalyticsInterface.onPageEnd(context,"首页Tab")
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser外部时，msg_all为： \n\n")
        if(isVisibleToUser){
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val msg_all = read.getInt("msg_all", 0)
            LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser外部时，msg_all为：$msg_all \n\n")
            if(msg_all>0){
                LogUtils.d_debugprint(Constant.Message_TAG,"这里isVisibleToUser时，msg_all为：$msg_all \n\n")
                caption_msg_point!!.visibility=View.VISIBLE
            }else{
                caption_msg_point!!.visibility=View.GONE
            }

            JAnalyticsInterface.onPageStart(context,"首页Tab")
        }else{
            caption_msg_point!!.visibility=View.GONE
            JAnalyticsInterface.onPageEnd(context,"首页Tab")
        }
    }

    private fun addSomeListener(){
        main_right_msg_btn!!.setOnClickListener{
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(activity,Message::class.java)
                startActivity(intent)
            }else{
                val intent:Intent=Intent(context,LoginMain::class.java)
                startActivity(intent)
            }
        }

        main_right_msg_btn!!.setOnTouchListener { v, event ->
            if(event.action==MotionEvent.ACTION_DOWN){
                caption_msg!!.alpha=0.618f
                main_right_msg_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else if(event.action==MotionEvent.ACTION_UP){
                caption_msg!!.alpha=1.0f
                main_right_msg_btn!!.setBackgroundColor(Color.TRANSPARENT)

            }
            false
        }

    }

    //设置标题
    private fun initCaption() {
        caption = activity.findViewById(R.id.main_txt1) as TextView
        caption_msg = activity.findViewById(R.id.main_msg) as ImageView
        main_setting=activity.findViewById(R.id.main_setting) as ImageView
        main_right_btn=activity.findViewById(R.id.main_right_btn) as Button
        main_right_msg_btn=activity.findViewById(R.id.main_right_msg_btn) as Button
        caption_msg_point = activity.findViewById(R.id.main_unread_msg) as TextView
        caption_ask_Iv=activity.findViewById(R.id.ask_down_Iv) as ImageView
        main_ask=activity.findViewById(R.id.main_ask) as TextView
        main_ask!!.visibility=View.GONE
        main_setting!!.visibility=View.GONE
        caption_ask_Iv!!.visibility=View.GONE
        main_right_btn!!.visibility=View.GONE
        main_right_msg_btn!!.visibility=View.VISIBLE
        caption_msg!!.setImageResource(R.mipmap.navi_msg)
        caption_msg!!.visibility = View.GONE
        caption_msg_point!!.visibility = View.GONE
        caption!!.text = "首页"
    }
    
    //设置广告栏
    @SuppressLint("NewApi")
    private fun initialize() {
        myImageOverAD!!.visibility=View.GONE
        cycleViewPager = activity.fragmentManager.findFragmentById(R.id.oneFm_fragment_cycle_viewpager_content) as CycleViewPager

        views.add(ViewFactory.getImageView(context, myImageList[myImageList.size - 1].image)) // 将最后一个ImageView添加进来
        for (i in myImageList.indices) {
            views.add(ViewFactory.getImageView(context, myImageList[i].image))
        }
        views.add(ViewFactory.getImageView(context, myImageList[0].image)) // 将第一个ImageView添加进来
        cycleViewPager!!.isCycle = true // 设置循环，在调用setData方法前调用
        cycleViewPager!!.setData(views, myImageList, mAdCycleViewListener) // 在加载数据前设置是否循环
        cycleViewPager!!.isWheel = true //设置轮播
        cycleViewPager!!.setTime(5000) // 设置轮播时间，默认5000ms
        cycleViewPager!!.setIndicatorCenter() //设置圆点指示图标组居中显示，默认靠右
    }

    /***
     * 初始化头部导航栏
     * @param inflater
     */
    private fun initTab(inflater: LayoutInflater) {
        val hTabs = HTabDb.getSelected()
        for (i in hTabs.indices) {
            //设置头部项布局初始化数据
            val rbButton = inflater.inflate(R.layout.tab_top1, null) as RadioButton
            rbButton.id = i
            rbButton.text = hTabs[i].name

            val dm = DisplayMetrics()
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(dm)
            val screenWidth = dm.widthPixels
            rbButton.width = screenWidth / 4

            val params = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT)
            //加入RadioGroup
            rg_!!.addView(rbButton, params)
        }
        //默认点击
        rg_!!.check(0)
    }

    /***
     * 初始化viewpager
     */
    private fun initView() {
        hTabs = HTabDb.getSelected()
        OneFm_FirstTab()
        OneFm_SecondTab()
        OneFm_ThirdTab()
        OneFm_FourthTab()
        //设置viewpager适配器
        adapter = SchoolFmAdapter(activity.supportFragmentManager, newsList)
        vp_!!.adapter = adapter
        //两个viewpager切换不重新加载
        vp_!!.offscreenPageLimit = 3
        //设置默认
        vp_!!.currentItem = 0
        //设置viewpager监听事件
        vp_!!.setOnPageChangeListener(this)
    }

    private fun OneFm_FirstTab() {
        val fm1 = SchoolFm1()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)//首页4个小碎片的list
    }

    private fun OneFm_SecondTab() {
        val fm1 = SchoolFm2()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)//首页4个小碎片的list
    }

    private fun OneFm_ThirdTab() {
        val fm1 = SchoolFm3()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)//首页4个小碎片的list
    }

    private fun OneFm_FourthTab() {
        val fm1 = SchoolFm4()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)//首页4个小碎片的list
    }

    //添加监听器
    private val mAdCycleViewListener = CycleViewPager.ImageCycleViewListener { info, position, imageView ->
        var position = position
        if (cycleViewPager!!.isCycle) {
            position = position - 1
            //Toasty.info(context,"标题："+info.title+ "\n链接：" + info.link).show()

            val bundle=Bundle()
            bundle.putString("title",info.title)
            bundle.putString("link",info.link)
            val intent:Intent=Intent(context,ADWebView::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            activity.overridePendingTransition(0,R.anim.slide_left_out)
        }
    }

    /**
     * 配置ImageLoder
     */
    private fun configImageLoader() {
        // 初始化ImageLoader
        val options = DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.icon_empty) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.icon_empty) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.icon_error) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // 设置下载的图片是否缓存在SD卡中
                // .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片
                .build() // 创建配置过得DisplayImageOption对象

        val config = ImageLoaderConfiguration.Builder(context).defaultDisplayImageOptions(options)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build()

        ImageLoader.getInstance().init(config)
    }

    override fun onPageScrollStateChanged(arg0: Int) {}

    override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

    override fun onPageSelected(id: Int) {
        setTab(id)
    }

    /***
     * 页面跳转切换头部偏移设置
     * @param id
     */
    private fun setTab(id: Int) {
        val rbButton = rg_!!.getChildAt(id) as RadioButton
        //设置标题被点击
        rbButton.isChecked = true
        //偏移设置
        val left = rbButton.left
        val width = rbButton.measuredWidth
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        val screenWidth = metrics.widthPixels//屏幕宽度
        //移动距离= 左边的位置 + button宽度的一半 - 屏幕宽度的一半
        val len = left + width / 2 - screenWidth / 2
        //移动
        hv_!!.smoothScrollTo(len, 0)
    }

    private fun getADfromServer() {
        var urlPath = ""
        var sign = ""
        val encode = Constant.ENCODE
        val school = Constant.SCHOOLDEFULT
        if (LogUtils.APP_IS_DEBUG) {
            urlPath = Constant.BASEURLFORTEST + Constant.School_AD
            sign = MD5Util.md5(Constant.SALTFORTEST)
        } else {
            urlPath = Constant.BASEURLFORRELEASE + Constant.School_AD
            sign = MD5Util.md5(Constant.SALTFORRELEASE)
        }
        val params = mapOf<String, Any?>("school" to school,"sign" to sign)
        LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的map=" + params.toString())
        Thread(Runnable {
            getAdfromServer= HttpUtil.httpPost(urlPath,params,encode)
           // LogUtils.d_debugprint(TAG,Constant.GETDATAFROMSERVER+getAdfromServer)
            val getCode:String
            var msg:String?=null
            var result:String
            var getData:List<Map<String,Any?>>
            getCode= JsonUtil.get_key_string(Constant.Server_Code,getAdfromServer!!)
            if(Constant.RIGHTCODE.equals(getCode)) {
                handler_result1.post {
                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getAdfromServer!!)
                    result = JsonUtil.get_key_string(Constant.Server_Result, getAdfromServer!!)
                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString())
                    if (getData != null&&getData.size>0) {
                        for (i in getData.indices) {
                            val myImage=MyImage()
                            if(getData[i].getValue("link").toString()!=null&&getData[i].getValue("image").toString()!=null&&getData[i].getValue("title").toString()!=null) {
                                myImage.link = getData[i].getValue("link").toString()
                                myImage.image = getData[i].getValue("image").toString()
                                myImage.title = getData[i].getValue("title").toString()
                                myImageList.add(myImage)//=====这里保存了所有广告信息
                            }else{
                                Toasty.error(context,Constant.SERVERBROKEN).show()
                            }
                        }
                        initialize() //初始化顶部导航栏
                    }else{
                        val myImage=MyImage()
                        myImage.link=""
                        myImage.image=""
                        myImage.title=""
                        myImageList.add(myImage)
                        //Toasty.error(context,Constant.SERVERBROKEN).show()
                    }
                }
            }
        }).start()
    }

}
