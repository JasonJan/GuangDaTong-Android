package com.guangdamiao.www.mew_android_debug.navigation

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import java.util.*

class GuideActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    private var vp: ViewPager? = null
    private var imageIdArray: IntArray? = null//图片资源的数组
    private var viewList: MutableList<View>? = null//图片资源的集合
    private var vg: ViewGroup? = null//放置圆点

    //实例化原点View
    private var iv_point: ImageView? = null
    private var ivPointArray: Array<ImageView?>? = null

    //最后一页的按钮
    private var ib_start: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_guide)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        ib_start = findViewById(R.id.guide_ib_start) as Button
        ib_start!!.setOnClickListener {
            val intent:Intent=Intent(this@GuideActivity,NavigationMainActivity::class.java)
            intent.putExtra("openAPP",true)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            startActivity(intent)
            finish()
        }

        //加载ViewPager
        initViewPager()

        //加载底部圆点
        initPoint()
    }


    /**
     * 加载底部圆点
     */
    private fun initPoint() {
        //这里实例化LinearLayout
        vg = findViewById(R.id.guide_ll_point) as ViewGroup
        //根据ViewPager的item数量实例化数组
        ivPointArray = arrayOfNulls(viewList!!.size)
        //循环新建底部圆点ImageView，将生成的ImageView保存到数组中
        val size = viewList!!.size
        for (i in 0..size - 1) {
            iv_point = ImageView(this)
            iv_point!!.layoutParams = ViewGroup.LayoutParams(50, 50)
            iv_point!!.setPadding(20, 0, 20, 150)//left,top,right,bottom
            ivPointArray!![i] = iv_point!!
            //第一个页面需要设置为选中状态，这里采用两张不同的图片
            if (i == 0) {
                iv_point!!.setBackgroundResource(R.mipmap.icon_point_pre)
            } else {
                iv_point!!.setBackgroundResource(R.mipmap.icon_point)
            }
            //将数组中的ImageView加入到ViewGroup
            vg!!.addView(ivPointArray!![i])
        }


    }

    /**
     * 加载图片ViewPager
     */
    private fun initViewPager() {
        vp = findViewById(R.id.guide_vp) as ViewPager
        //实例化图片资源
        imageIdArray = intArrayOf(R.mipmap.intro_school, R.mipmap.intro_discovery_1, R.mipmap.intro_discovery_2
        ,R.mipmap.intro_discovery_3,R.mipmap.intro_ask_1,R.mipmap.intro_ask_2,R.mipmap.intro_ask_3,R.mipmap.intro_mew_coin)
        viewList = ArrayList()
        //获取一个Layout参数，设置为全屏


        //循环创建View并加入到集合中
        val len = imageIdArray!!.size
        for (i in 0..len - 1) {
            //new ImageView并设置全屏和图片资源
            val imageView = ImageView(this)
          /*  val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            imageView.layoutParams = params
            imageView.setBackgroundResource(imageIdArray!![i])*/

            //将ImageView加入到集合中
            viewList!!.add(imageView)
        }

        //View集合初始化好后，设置Adapter
        vp!!.adapter = GuidePageAdapter(viewList)
        vp!!.offscreenPageLimit=9
        //设置滑动监听
        vp!!.setOnPageChangeListener(this)
    }


    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    /**
     * 滑动后的监听
     * @param position
     */
    override fun onPageSelected(position: Int) {
        //循环设置当前页的标记图
        val length = imageIdArray!!.size
        for (i in 0..length - 1) {
            ivPointArray!![position]!!.setBackgroundResource(R.mipmap.icon_point_pre)
            if (position != i) {
                ivPointArray!![i]!!.setBackgroundResource(R.mipmap.icon_point)
            }
        }

        //判断是否是最后一页，若是则显示按钮
        if (position == imageIdArray!!.size - 1) {
            ib_start!!.visibility = View.VISIBLE
        } else {
            ib_start!!.visibility = View.GONE
        }
    }


    override fun onPageScrollStateChanged(state: Int) {

    }
}
