package com.guangdamiao.www.mew_android_debug.navigation.me.me_setting

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.*
import es.dmoral.toasty.Toasty

class Me_Modify_Password : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var modify_password:Button?=null
    private var modify_password_old:EditText?=null
    private var modify_password_new1:EditText?=null
    private var modify_password_new2:EditText?=null
    private var resultFromServer:String=""

    private var account:String?=""
    private var password:String?=""
    private var token:String=""
    private var TAG= Constant.Me_TAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_modify_password)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        initView()
        getSharedFile()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Me_Modify_Password,"设置-修改密码")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Me_Modify_Password,"设置-修改密码")
        super.onPause()
    }

    private fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text="修改密码"
        title_center_textview!!.visibility= View.VISIBLE
        modify_password_old=findViewById(R.id.modify_password1) as EditText
        modify_password_new1=findViewById(R.id.modify_password2) as EditText
        modify_password_new2=findViewById(R.id.modify_password3) as EditText
        modify_password=findViewById(R.id.modify_password) as Button
        
    }

    private fun getSharedFile() {
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        account = read.getString("account", "")
        password = read.getString("password", "")
        token = read.getString("token", "")
        LogUtils.d_debugprint(TAG, "account=" + account + "\npassword=" + password + "\ncollege=" + "\ntoken=" + token)
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        modify_password!!.setOnClickListener{
            if(islegal()){
                if(NetUtil.checkNetWork(this)){
                    val pDialog=SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Loading");
                    pDialog.setCancelable(false);
                    pDialog.show()
                    requestFromServer(pDialog)
                }else{
                    Toasty.info(this,Constant.NONETWORK).show()
                }
            }
        }
    }

    private fun islegal():Boolean{
        if(token==""||token==null){
            Toasty.warning(this,"亲，请登录后再尝试！").show()
            return false
        }else if(!modify_password_old!!.text.toString().equals(password)){
            Toasty.warning(this,"亲，旧密码不正确！").show()
            return false
        }else if(!modify_password_new1!!.text.toString().equals(modify_password_new2!!.text.toString())){
            Toasty.warning(this,"亲，两次输入密码不匹配！").show()
            return false
        }else if(modify_password_new1!!.text.toString().length<6){
            Toasty.warning(this,"亲，密码不能低于6位！").show()
            return false
        }else{
            return true
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@Me_Modify_Password.currentFocus != null) {
                if (this@Me_Modify_Password.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@Me_Modify_Password.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    private fun requestFromServer(pDialog: SweetAlertDialog){
        val password_new=modify_password_new1!!.text.toString()
        if (password == null || password==""|| password_new==null||password_new=="") {
            Toasty.error(this,Constant.UNKNOWNERROR).show()
        }
        else{
            var newPwd: String? = ""
            var oldPwd: String? = ""
            var sign: String? = ""
            val salt:String
            val urlPath:String
            if(LogUtils.APP_IS_DEBUG){
                salt = Constant.SALTFORTEST
                urlPath = Constant.BASEURLFORTEST + Constant.Me_AlterPassword
            }else{
                salt = Constant.SALTFORRELEASE
                urlPath = Constant.BASEURLFORRELEASE + Constant.Me_AlterPassword
            }
            newPwd=password_new
            oldPwd=password
            sign = MD5Util.md5(salt + token)
            LogUtils.d_debugprint(TAG,"salt:"+salt+"\ntoken:"+token)
            var encode = Constant.ENCODE
            var params = mapOf<String, Any?>("newPwd" to newPwd, "oldPwd" to oldPwd, "sign" to sign)
            LogUtils.d_debugprint(TAG,"向服务器传递的数据\n"+urlPath+"\n"+params.toString()+"\n"+encode)
            val hander_result_register = object : Handler() {
                override fun handleMessage(msg: Message?) {
                    if (msg!!.what == 0) {
                        LogUtils.d_debugprint(TAG,resultFromServer!!)
                        var getCode: String = ""
                        getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!)
                        if (Constant.RIGHTCODE.equals(getCode)) {
                            pDialog.cancel()
                            val pSuccess=SweetAlertDialog(this@Me_Modify_Password, SweetAlertDialog.SUCCESS_TYPE)
                                    pSuccess.setTitleText("亲~")
                                    pSuccess.setContentText("修改密码成功")
                                    pSuccess.setConfirmClickListener {
                                        pSuccess.cancel()
                                        finish()
                                        overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
                                    }
                                    pSuccess.show()

                            val handler= Handler()
                            handler.postDelayed({
                                if(pSuccess!=null){
                                    pSuccess.cancel()
                                    finish()
                                    overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
                                }
                            },1236)

                            val editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit();
                            editor.putString("password",password)//在全局文件中更新密码
                            editor.commit()
                        } else{
                            pDialog.cancel()
                            SweetAlertDialog(this@Me_Modify_Password, SweetAlertDialog.ERROR_TYPE).setTitleText("亲~").setContentText("修改密码失败").show()
                        }
                    }
                    super.handleMessage(msg)
                }
            }
            Thread(Runnable {
                resultFromServer = HttpUtil.httpPost(urlPath, params, encode)
                hander_result_register.sendEmptyMessage(0)
            }).start()
        }
    }
}
