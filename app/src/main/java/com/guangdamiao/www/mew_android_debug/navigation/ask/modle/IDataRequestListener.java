package com.guangdamiao.www.mew_android_debug.navigation.ask.modle;
/**
 * 
* @ClassName: IDataRequestListener 
* @Description: 请求后台数据服务器响应后的回调 
* @author yiw
* @date 2017-8-1
*
 */
public interface IDataRequestListener {

	void loadSuccess(Object object);

}
