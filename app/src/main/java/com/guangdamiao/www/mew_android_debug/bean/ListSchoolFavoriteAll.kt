package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class ListSchoolFavoriteAll {

    var favorID:String?=""
    var title: String?=""
    var createTime: String?= ""
    var id:String?=""
    var link:String?=""
    var readCount:String?=""
    var zan_:String?=""
    var isZan:String?=""
    var urlEnd:String?=""
    var isShow=false
    var isChecked=false

    constructor():super()
    constructor(isShow:Boolean,isChecked:Boolean,favorID:String,urlEnd:String,createTime: String,title:String,id:String,link:String,readCount:String,zan_:String,isZan:String) : super() {
        this.favorID=favorID
        this.createTime = createTime
        this.title=title
        this.id=id
        this.link=link
        this.readCount=readCount
        this.zan_=zan_
        this.isZan=isZan
        this.urlEnd=urlEnd
        this.isShow=isShow
        this.isChecked=isChecked
    }

    override fun toString(): String {
        return "ListSchoolAll[isShow=$isShow,isChecked=$isShow,title=$title,objectID=$id,urlEnd=$urlEnd,createTime=$createTime,link=$link,readCount=$readCount,zan_=$zan_,isZan=$isZan]"
    }
}
