package com.guangdamiao.www.mew_android_debug.navigation

import android.R.attr.path
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.KeyEvent
import android.widget.Button
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import uk.co.senab.photoview.PhotoView
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


class ImageSingleActivity : Activity() {

    private var imageSingle_pv:PhotoView?=null
    private var imageSingle_btn: Button?=null
    private var imgUrl:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        setContentView(R.layout.activity_image_single)

        val imgUrl = intent.getStringExtra(INTENT_IMGURL)
        initview()
        getIcon(imgUrl!!,imageSingle_pv!!)
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@ImageSingleActivity,"查看单张图片")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@ImageSingleActivity,"查看单张图片")
        super.onPause()
    }

    fun initview(){
        imageSingle_pv=findViewById(R.id.imageSingle_pv) as PhotoView
        imageSingle_btn=findViewById(R.id.imageSingle_btn) as Button
    }


    fun getIcon(imgUrl: String,imageSingle_pv:PhotoView){
        ImageLoader.getInstance().displayImage(imgUrl,imageSingle_pv)
    }

    fun addSomeListener(){
        imageSingle_pv!!.setOnPhotoTapListener { view, x, y ->
            finish()
            this@ImageSingleActivity.overridePendingTransition(0,R.anim.zoom_exit)
        }

        imageSingle_btn!!.setOnClickListener{
            val bitmapDrawable=imageSingle_pv!!.drawable as BitmapDrawable
            val bm=bitmapDrawable.bitmap
            saveImageToGallery(this@ImageSingleActivity,bm)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            finish()
            this@ImageSingleActivity.overridePendingTransition(0,R.anim.zoom_exit)
        }
        return super.onKeyDown(keyCode, event)
    }

    fun saveImageToGallery(context: Context, bmp: Bitmap) {
        // 首先保存图片
        val appDir = File(Environment.getExternalStorageDirectory(), "mew")
        if (!appDir.exists()) {
            appDir.mkdir()
        }
        val fileName = System.currentTimeMillis().toString() + ".jpg"
        val file = File(appDir, fileName)
        try {
            val fos = FileOutputStream(file)
            bmp.compress(CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.contentResolver,
                    file.absolutePath, fileName, null)

        } catch (e: FileNotFoundException) {
            //Toasty.info(context,"亲，保存失败3！保存路径：$fileName").show()
            e.printStackTrace()
        }

        // 最后通知图库更新
        context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)))
        Toasty.info(context,"亲，保存成功！保存路径：$appDir").show()
    }

    companion object {
        val INTENT_IMGURL = "imgurl"

        fun startImageSingleActivity(context: Context, imgUrl: String) {
            val intent = Intent(context, ImageSingleActivity::class.java)
            intent.putExtra(INTENT_IMGURL, imgUrl)
            context.startActivity(intent)
        }
    }


}
