package com.guangdamiao.www.mew_android_debug.bean.ask_bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyAsk {

    var commentCount:String?=null
    var id: String? = null
    var createTime:String?=null
    var publisherID:String?=null
    var nickname:String?=null
    var icon:String?=null
    var gender:String?=null
    var reward:String?=null
    var zan:String?=null
    var commentID:String?=null
    var type:String?=null
    var label:String?=null
    var content:String?=null
    var link:String?=null
    var image:ArrayList<String>?=null
    var thumbnailImage:ArrayList<String>?=null
    var zanIs:String?=null
    var favoriteIs:String?=null
    var title:String?=null


    constructor():super()
    constructor(id:String,createTime:String,publisherID:String,nickname:String,icon:String,reward:String,zan:String,type:String,label:String,content:String,link:String,isZan:String,isFavorite:String,commentCount:String,title:String){
        this.id=id
        this.title=title
        this.commentCount=commentCount
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.zan=zan
        this.type=type
        this.label=label
        this.content=content
        this.link=link
        this.zanIs=isZan
        this.favoriteIs=isFavorite
    }
    constructor(title:String,id:String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,zan:String,commentID:String,type:String,label:String,content:String,link:String,image:ArrayList<String>,thumbnailImage:ArrayList<String>,isZan:String,isFavorite:String,commentCount:String){
        this.id=id
        this.title=title
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.zan=zan
        this.commentID=commentID
        this.type=type
        this.label=label
        this.content=content
        this.link=link
        this.zanIs=isZan
        this.favoriteIs=isFavorite
        this.gender=gender
        this.image=image
        this.thumbnailImage=thumbnailImage
        this.commentCount=commentCount

    }

    override fun toString(): String {
        return "\n\n\nMyAsk[\nid=$id,\ntitle=$title,\ncreateTime=$createTime,\npublisherID=$publisherID,\nnickname=$nickname,\nicon=$icon,\nreward=$reward,\nzan=$zan,\ntype=$type,\nlabel=$label,\ncontent=$content,\nlink=$link,\nisZan=$zanIs,\nisFavorite=$favoriteIs,\ncommentCount=$commentCount,\nimage=$image,\nthumImage=$thumbnailImage \n]"
    }
}
