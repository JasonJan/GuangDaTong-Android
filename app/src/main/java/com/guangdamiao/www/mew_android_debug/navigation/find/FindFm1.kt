package com.guangdamiao.www.mew_android_debug.navigation.find


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.bulong.rudeness.RudenessScreenHelper
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find1SQLiteOpenHelper
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import java.io.File
import java.util.*

class FindFm1 : Fragment() {


    private var name: String? = null
    //栏目列表
    private val listItems = ArrayList<ListFindAll>()
    private var mlistView: ListView? = null
    private var adapter: FindPartTimeAdpter? = null
    private val TAG = Constant.Find_TAG
    private var pageNow = 0
    private val orderBy = "createTime"
    private val order = "desc"
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()

    var flag=0

    private var helper1: Find1SQLiteOpenHelper?=null

    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        initImageLoader()
        val view = inflater!!.inflate(R.layout.fragment_find_all, container, false)
        RudenessScreenHelper(activity.application,750.0f)
        helper1= Find1SQLiteOpenHelper(context)
        
        mlistView = view.findViewById(R.id.list_find_new) as ListView
        ptrClassicFrameLayout = view!!.findViewById(R.id.list_find_frame) as PtrClassicFrameLayout


        adapter = FindPartTimeAdpter(context, listItems)
        mlistView!!.setAdapter(adapter!!)//关键代码
        initData()

        return view
    }

    override fun onStart() {
        if(flag==0){
            flag++
            queryNowAll(Find1SQLiteOpenHelper.F1_Name,listItems)
            adapter!!.notifyDataSetChanged()
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
        }

        val read = context.getSharedPreferences("update_find1", Context.MODE_PRIVATE)
        val update_position = read.getInt("position", 0)
        val update_id=read.getString("id","")
        val update_isZan=read.getString("isZan","")
        val update_readCount=read.getString("readCount","")
        val update_zan=read.getString("zan","")
        if(listItems.size>0&&listItems.size-1>=update_position){
            if(listItems[update_position]!=null&&listItems[update_position].id!!.equals(update_id)){
                listItems[update_position].zan_=update_zan
                listItems[update_position].isZan=update_isZan
                listItems[update_position].readCount=update_readCount

                adapter!!.notifyDataSetChanged()
            }
        }

        super.onStart()
    }

    private fun initData() {

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 100)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                //ptrClassicFrameLayout!!.loadMoreComplete(true)
                RequestServerList(++pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                if (pageNow === 1) {
                    //set load more disable
                    ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                }
            }, 100)
        })
    }


    private fun RequestServerList(pageNow: Int, orderBy: String, order: String,adapter: FindPartTimeAdpter,ptrClassicFrameLayout: PtrClassicFrameLayout) {
        SameListRequest.getResultFromServer(context, TAG, Constant.Find_PartTimeJob, pageNow, orderBy, order, listItems,adapter,ptrClassicFrameLayout)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            JAnalyticsInterface.onPageStart(context,"栏目-兼职")
        }else{
            JAnalyticsInterface.onPageEnd(context,"栏目-兼职")
        }
    }

    //数据库查询
    private fun queryNowAll(table:String,listItems:ArrayList<ListFindAll>){


        val sql="select * from "+ Find1SQLiteOpenHelper.F1_Name+" order by id asc"
        listItems.clear()
        val cursor= helper1!!.readableDatabase.rawQuery(sql,null)

        while(cursor.moveToNext()){
            val find= ListFindAll()
            find.id=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Sid))
            find.icon=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Icon))
            find.title=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Title))
            find.createTime=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_CreateTime))
            find.position=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Position))
            find.fee=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Fee))
            find.description=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Description))
            find.link=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Link))
            find.readCount=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_ReadCount))
            find.zan_=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_Zan))
            find.isZan=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_IsZan))
            find.isFavorite=cursor.getString(cursor.getColumnIndex(Find1SQLiteOpenHelper.F1_IsFavorite))

            listItems.add(find)
        }
        LogUtils.d_debugprint(Constant.Find_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }

}
