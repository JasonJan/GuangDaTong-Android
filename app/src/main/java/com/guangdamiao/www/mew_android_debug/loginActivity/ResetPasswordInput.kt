package com.guangdamiao.www.mew_android_debug.loginActivity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

class ResetPasswordInput : BaseActivity() {

    private var title_left_imageview: ImageView? = null
    private var title_center_textview: TextView? = null
    private var title_right_imageview:ImageView?=null
    private var TAG:String= Constant.Register_Tag
    private var but1: Button? = null
    private var password1: EditText? = null
    private var password2: EditText? = null
    private var resultFromServerReset:String?=null
    private var bundle_from_reset: Bundle?=null
    private var phone_:String?=null
    private var result_:String?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password_input)
        MainApplication.getInstance().addActivity(this)
        LoginCacheActivity.addActivity(this)
        fullscreen()
        bundle_from_reset = intent.extras
        initView()
        addSomeListener()
    }

    fun addSomeListener(){
        title_left_imageview!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        but1!!.setOnClickListener{
            if(samePassword()){
                RegisterOverFromServer()
            }
        }
    }

    fun initView(){
        but1 = findViewById(R.id.reset_password_but1) as Button
        password1 = findViewById(R.id.reset_password_edt1) as EditText
        password2 = findViewById(R.id.reset_password_edt2) as EditText
        title_left_imageview = findViewById(R.id.title_left_imageview) as ImageView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text="输入新密码"
        title_right_imageview!!.visibility= View.GONE
        title_center_textview!!.visibility=View.VISIBLE

        phone_=bundle_from_reset!!.getString("phone")
        result_=bundle_from_reset!!.getString("result")
    }

    fun RegisterOverFromServer(){
        if(NetUtil.checkNetWork(this)){
            var result: String? = ""
            var phone: String? = ""
            var password: String? = ""
            var sign: String? = ""
            var verifyToken: String? =""
            val salt:String
            val urlPath:String
            if(LogUtils.APP_IS_DEBUG){
                salt = Constant.SALTFORTEST
                urlPath = Constant.BASEURLFORTEST + Constant.Reset_Password
            }else{
                salt = Constant.SALTFORRELEASE
                urlPath = Constant.BASEURLFORRELEASE + Constant.Reset_Password
            }
            result = result_
            phone = phone_
            password = password1!!.text.toString()
            verifyToken = JsonUtil.get_key_string("verifyToken", result!!)
            sign = MD5Util.md5(salt + verifyToken)

            val jsonObject=JSONObject()
            jsonObject.put("account",phone)
            jsonObject.put("password",password)
            jsonObject.put("sign",sign)

            val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
            LogUtils.d_debugprint(TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())
            val client= AsyncHttpClient(true,80,443)
            client.post(this@ResetPasswordInput,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if(responseBody!=null){
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response=JSONObject(resultDate)
                        resultFromServerReset=response.toString()
                        LogUtils.d_debugprint(TAG,resultFromServerReset!!)
                        resultFromServerReset=response.toString()
                        var getCode: String = ""
                        getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServerReset!!)
                        if (Constant.RIGHTCODE.equals(getCode)) {

                            var account: String = ""
                            var password: String = ""
                            account = bundle_from_reset!!.getString("phone")
                            password = password1!!.text.toString()

                            //用bundle_from_reset记录服务器返回数据
                            var dataFromServer: Bundle = Bundle()
                            dataFromServer.putString("account", account)
                            dataFromServer.putString("password", password)
                            //添加到全局文件中，方便所有页面的调用
                            val editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                            editor.putString("account",account)
                            editor.putString("password",password)
                            editor.commit()

                            SweetAlertDialog(this@ResetPasswordInput, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("提示")
                                    .setContentText("亲，修改密码成功！")
                                    .setConfirmClickListener {
                                        /*直接跳登录页面*/
                                        val intent = Intent(this@ResetPasswordInput, LoginMain::class.java)
                                        startActivity(intent)
                                        overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out)
                                    }
                                    .show()

                        } else {
                            var msg: String = ""
                            msg = JsonUtil.get_key_string(Constant.Server_Msg, resultFromServerReset!!)
                            SweetAlertDialog(this@ResetPasswordInput, SweetAlertDialog.ERROR_TYPE).setTitleText("提示").setContentText("修改密码失败\n"+msg).show()
                        }
                     }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                      Toasty.error(this@ResetPasswordInput,Constant.REQUESTFAIL).show()
                }
            })


        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }
    }

    fun samePassword():Boolean{
        if(!TextUtils.isEmpty(password1!!.text.toString().trim{ it<=' '})){
            if(password1!!.text.toString().trim{it<=' '}.length<6){
                Toasty.warning(this@ResetPasswordInput,"亲，密码不能低于6位哦~").show()
            }else{
                var p2:String=""
                var p1:String=""
                p1=password1!!.text.toString().trim{it<=' '}
                p2=password2!!.text.toString().trim{it<=' '}
                if(p2!=null&&p1.equals(p2)){
                    return true
                }else{
                    Toasty.warning(this@ResetPasswordInput,"两次输入的密码不匹配哦~").show()
                    return false
                }
            }
        }else{

            Toasty.warning(this@ResetPasswordInput,"请输入您的密码").show()
            password1!!.requestFocus()//设置输入焦点
            return false
        }
        return false
    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@ResetPasswordInput.currentFocus != null) {
                if (this@ResetPasswordInput.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@ResetPasswordInput.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }
    
}
