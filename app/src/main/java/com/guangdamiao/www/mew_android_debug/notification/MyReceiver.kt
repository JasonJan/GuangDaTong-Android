package com.guangdamiao.www.mew_android_debug.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import cn.jpush.android.api.JPushInterface
import cn.jpush.android.service.PushService
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.NavigationMainActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail
import com.guangdamiao.www.mew_android_debug.navigation.school.message.Message
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import org.json.JSONException
import org.json.JSONObject

//JPushInterface.Action_registeration_ID  -----SDK向JPush Server 注册所得到的注册ID
//JpushInterface.Extra_Registeration_ID   -----SDK向JPush Server 注册所得到的全局唯一ID
//JPushInterface.ACTION_MESSAGE_RECEIVED  -----收到了自定义消息 Push
//JPushInterface.EXTRA_TITLE              -----保存服务器推送下来的消息的标题
//JPushInterface.EXTRA_MESSAGE            -----保存服务器推送下来的消息内容
//JPushInterface.EXTRA_EXTRA              -----保存服务器推送下来的附加字段。这是个 JSON 字符串
//JPushInterface.EXTRA_MSG_ID             -----唯一标识消息的 ID, 可用于上报统计等
//JPushInterface.ACTION_NOTIFICATION_RECEIVED--收到了通知 Push
//JPushInterface.EXTRA_NOTIFICATION_TITLE -----保存服务器推送下来的通知的标题
//JPushInterface.EXTRA_ALERT              -----保存服务器推送下来的通知内容
//JPushInterface.EXTRA_EXTRA              -----保存服务器推送下来的附加字段
//JPushInterface.EXTRA_NOTIFICATION_ID    -----通知栏的Notification ID，可以用于清除Notification
//JPushInterface.EXTRA_RICHPUSH_HTML_PATH -----富媒体通知推送下载的HTML的文件路径,用于展现WebView
//JPushInterface.EXTRA_RICHPUSH_HTML_RES  -----富媒体通知推送下载的图片资源的文件名,多个文件名用 “，” 分开
//JPushInterface.EXTRA_MSG_ID             -----唯一标识通知消息的 ID
//JPushInterface.EXTRA_BIG_TEXT           -----大文本通知样式中大文本的内容
//JPushInterface.EXTRA_BIG_PIC_PATH       -----可支持本地图片的路径，或者填网络图片地址
//JPushInterface.EXTRA_INBOX              -----收件箱通知样式中收件箱的内容
//JPushInterface.EXTRA_NOTI_PRIORITY      -----通知的优先级
//JPushInterface.EXTRA_NOTI_CATEGORY      -----通知分类
//JPushInterface.ACTION_NOTIFICATION_OPENED ---用户点击了通知
//JPushInterface.EXTRA_NOTIFICATION_TITLE  ----保存服务器推送下来的通知的标题
//JPushInterface.EXTRA_ALERT              -----保存服务器推送下来的通知内容
//JPushInterface.EXTRA_EXTRA              -----保存服务器推送下来的附加字段
//JPushInterface.EXTRA_NOTIFICATION_ID    -----通知栏的Notification ID
//JPushInterface.EXTRA_MSG_ID             -----唯一标识调整消息的 ID
//JPushInterface.ACTION_NOTIFICATION_CLICK_ACTION  --用户点击了通知栏中自定义的按钮
//JPushInterface.EXTRA_NOTIFICATION_ACTION_EXTRA   --获取通知栏按钮点击事件携带的附加信息
//JPushInterface.ACTION_CONNECTION_CHANGE         --JPush 服务的连接状态发生变化

/**
 * 自定义接收器

 * 如果不定义这个 Receiver，则：
 * 1) 默认用户会打开主界面
 * 2) 接收不到自定义消息
 */
class MyReceiver : BroadcastReceiver() {

    companion object {
        private val TAG = "消息中心--极光推送"
        // 打印所有的 intent extra 数据
        private fun printBundle(bundle: Bundle): String {
            val sb = StringBuilder()
            for (key in bundle.keySet()) {
                if (key == JPushInterface.EXTRA_NOTIFICATION_ID) {
                    sb.append("\nkey:" + key + ", value:" + bundle.getInt(key))
                } else if (key == JPushInterface.EXTRA_CONNECTION_CHANGE) {
                    sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key))
                } else if (key == JPushInterface.EXTRA_EXTRA) {
                    if (TextUtils.isEmpty(bundle.getString(JPushInterface.EXTRA_EXTRA))) {

                        continue
                    }

                    try {
                        val json = JSONObject(bundle.getString(JPushInterface.EXTRA_EXTRA))
                        val it = json.keys()

                        while (it.hasNext()) {
                            val myKey = it.next().toString()
                            sb.append("\nkey:" + key + ", value: [" +
                                    myKey + " - " + json.optString(myKey) + "]")
                        }
                    } catch (e: JSONException) {

                    }

                } else {
                    sb.append("\nkey:" + key + ", value:" + bundle.getString(key))
                }
            }
            return sb.toString()
        }
    }


    override fun onReceive(context: Context, intent: Intent) {

        val pushintent = Intent(context, PushService::class.java)//启动极光推送的服务
        context.startService(pushintent)

        try {

            val bundle = intent.extras
            LogUtils.d_debugprint(TAG, "[MyReceiver] onReceive - " + intent.action + ", extras: " + printBundle(bundle))

            if (JPushInterface.ACTION_REGISTRATION_ID == intent.action) {
                val regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID)
                LogUtils.d_debugprint(TAG, "[MyReceiver] 接收Registration Id : " + regId!!)
                //send the Registration Id to your server...

            } else if (JPushInterface.ACTION_MESSAGE_RECEIVED == intent.action) {
                LogUtils.d_debugprint(TAG, "[MyReceiver] 接收到推送下来的自定义消息: " + bundle.getString(JPushInterface.EXTRA_MESSAGE)!!)
                //processCustomMessage(context, bundle)

            } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED == intent.action) {
                LogUtils.d_debugprint(TAG, "[MyReceiver] 接收到推送下来的通知")
                val notifactionId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID)
                LogUtils.d_debugprint(TAG, "[MyReceiver] 接收到推送下来的通知的ID: " + notifactionId)

            } else if (JPushInterface.ACTION_NOTIFICATION_OPENED == intent.action) {


                /* //打开自定义的Activity
                val i = Intent(context, MainActivity::class.java)
                i.putExtras(bundle)
                //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                context.startActivity(i)*/

                val extra = bundle.getString(JPushInterface.EXTRA_EXTRA)
                val type = JsonUtil.get_key_string("type", extra)
                LogUtils.d_debugprint(TAG, "\n\n\n[MyReceiver] 用户点击打开了通知,extra为 $extra,  type为$type \n")

                if (MainApplication.getInstance().isExistActivityByClass(NavigationMainActivity::class.java)) {
                    /*val lauchIntent=context.packageManager.getLaunchIntentForPackage("com.guangdamiao.www.mew_android_debug")
                    lauchIntent.flags=Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    lauchIntent.putExtra("type",type)
                    context.startActivity(lauchIntent)*/
                    LogUtils.d_debugprint(TAG, "\n\n\n[MyReceiver] 页面还没有跳转了！！！ type=$type \n")
                    if (!type.equals(Constant.JPush_UrgentAsk)) {
                        val detailIntent = Intent(context, Message::class.java)
                        detailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP

                        if (type.equals(Constant.Jpush_LeaveWords)) {

                            detailIntent.putExtra("choose", 0)
                            context.startActivity(detailIntent)
                            LogUtils.d_debugprint(TAG, "\n\n\n[MyReceiver] 页面还没有跳转了！！！ 现在在第一个type中\n")
                        } else if (type.equals(Constant.Jpush_Ask)) {

                            detailIntent.putExtra("choose", 1)
                            context.startActivity(detailIntent)
                        } else if (type.equals(Constant.JPush_Notices)) {

                            detailIntent.putExtra("choose", 2)
                            context.startActivity(detailIntent)
                        }
                        LogUtils.d_debugprint(TAG, "\n\n\n[MyReceiver] 页面已经跳转了！！！ bundle=$bundle \n")
                    } else {
                        val id = JsonUtil.get_key_string("id", extra)
                        LogUtils.d_debugprint(TAG, "\n\n\n[MyReceiver] 页面还没有跳转了！！！ id=$id \n")
                        val intent: Intent = Intent(context, AskDetail::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP

                        val bundle = Bundle()
                        bundle.putString("id", id)
                        if (LogUtils.APP_IS_DEBUG) {
                            bundle.putString("link", Constant.BASEURLFORTEST)
                        } else {
                            bundle.putString("link", Constant.BASEURLFORRELEASE)
                        }
                        bundle.putString("publisherID", "guangdamiao")
                        bundle.putString("isZan", "0")
                        intent.putExtras(bundle)
                        context.startActivity(intent)
                    }
                } else {

                    val lauchIntent = context.packageManager.getLaunchIntentForPackage("com.guangdamiao.www.mew_android_debug")
                    lauchIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                    if (type.equals(Constant.JPush_UrgentAsk)) {
                        val id = JsonUtil.get_key_string("id", extra)
                        val range=JsonUtil.get_key_string("range",extra)
                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                        editor.putString("ask_type_choose", range)
                        editor.commit()

                        val bundle = Bundle()
                        bundle.putString("type", type)
                        bundle.putString("id", id)
                        lauchIntent.putExtra(Constant.JPush_UrgentAsk, bundle)
                    } else if (type.equals(Constant.Jpush_LeaveWords)) {
                        val bundle = Bundle()
                        bundle.putInt("choose", 0)
                        bundle.putString("type", type)
                        lauchIntent.putExtra("message", bundle)
                    } else if (type.equals(Constant.Jpush_Ask)) {
                        val bundle = Bundle()
                        bundle.putInt("choose", 1)
                        bundle.putString("type", type)
                        lauchIntent.putExtra("message", bundle)
                    } else if (type.equals(Constant.JPush_Notices)) {
                        val bundle = Bundle()
                        bundle.putInt("choose", 2)
                        bundle.putString("type", type)
                        lauchIntent.putExtra("message", bundle)
                    }
                    context.startActivity(lauchIntent)
                }


            } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK == intent.action) {
                LogUtils.d_debugprint(TAG, "[MyReceiver] 用户收到到RICH PUSH CALLBACK: " + bundle.getString(JPushInterface.EXTRA_EXTRA)!!)
                //在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity， 打开一个网页等..

            } else if (JPushInterface.ACTION_CONNECTION_CHANGE == intent.action) {
                val connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false)

            } else {
                LogUtils.d_debugprint(TAG, "[MyReceiver] Unhandled intent - " + intent.action)
            }
        } catch (e: Exception) {

        }
    }
}



