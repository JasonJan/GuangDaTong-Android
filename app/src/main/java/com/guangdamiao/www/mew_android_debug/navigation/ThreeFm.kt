package com.guangdamiao.www.mew_android_debug.navigation


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.*
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.bulong.rudeness.RudenessScreenHelper
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.Ask1SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.Ask2SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainAdpter
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.askWrite.AskWrite
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskMainPresenter
import com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk.SearchAskActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskMainView
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.CustomPopWindow
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import es.dmoral.toasty.Toasty
import java.io.File
import java.util.*




class ThreeFm : Fragment(), AskMainView {

    private var caption: TextView? = null
    private var caption_msg: ImageView? = null
    private var caption_msg_point: TextView? = null
    private var main_setting: ImageView? = null
    private var main_right_btn:Button?=null
    private var main_right_msg_btn:Button?=null
    private var caption_ask_Iv:ImageView?=null
    private var main_ask:TextView?=null
    private var ask_some_college_rl:RelativeLayout?=null
    private var ask_all_school_rl:RelativeLayout?=null
    private var ask_same_college_iv:ImageView?=null
    private var ask_all_school_iv:ImageView?=null
    private var popWindow: CustomPopWindow?=null
    private var head_search_rl:RelativeLayout?=null
    private var ask_search_btn:Button?=null
    private val listItems = ArrayList<MyAsk>()
    private var mlistView: ListView? = null
    private var adapter: AskMainAdpter? = null
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    private var mPresenter: AskMainPresenter?=null
    var handler = Handler()
    var pageNow=0
    val orderBy=Constant.OrderBy_Default
    val order=Constant.Order_Default
    val TAG=Constant.Ask_TAG
    var range:String?=null
    var college:String?=null
    var ask_type_choose:String?=null
    private var helper1: Ask1SQLiteOpenHelper?=null
    private var helper2: Ask2SQLiteOpenHelper?=null
    private var flag=0 //第一次加载



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        initView()
        RudenessScreenHelper(activity.application,750.0f)
        initImageLoader()

        val view = inflater!!.inflate(R.layout.fm_three, container, false)
        helper1= Ask1SQLiteOpenHelper(context)
        helper2= Ask2SQLiteOpenHelper(context)
        
        head_search_rl=view!!.findViewById(R.id.ask_search_rl) as RelativeLayout
        ask_search_btn=view!!.findViewById(R.id.ask_search_btn) as Button

        addSomeListener()

        mlistView = view!!.findViewById(R.id.list_ask_lv) as ListView
        ptrClassicFrameLayout = view!!.findViewById(R.id.list_ask_frame) as PtrClassicFrameLayout
        ptrClassicFrameLayout!!.isLoadMoreEnable=true
        ptrClassicFrameLayout!!.loadMoreComplete(true)
        //设置一个Presenter,本身继承了一个接口
        mPresenter= AskMainPresenter(this)


        adapter = AskMainAdpter(context, listItems)
        adapter!!.setAskPresenter(mPresenter!!)//将数据源绑定了Presenter,非常关键
        mlistView!!.setAdapter(adapter)//关键代码

        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val ask_type_choose = read.getString("ask_type_choose", "")
        LogUtils.d_debugprint(Constant.Ask_TAG,"/n/n这里是ThreeFm中的onStart方法"+"此时的ask_choose_type为"+ask_type_choose)
        initData()

        return view!!
    }

    override fun onStart() {
        caption_msg_point!!.visibility = View.GONE

        val read = context.getSharedPreferences("update_ask", Context.MODE_PRIVATE)
        val update_id= read.getString("id", "")
        val update_isZan=read.getString("isZan","")
        val update_commentCount=read.getString("commentCount","")
        val update_zan=read.getString("zan","")
        val update_isDeal=read.getString("isDeal","")
        val update_position=read.getInt("position",0)

        LogUtils.d_debugprint(Constant.Ask_TAG,"\nlistItems.size=${listItems!!.size} \nupdate_id=$update_id,\nupdate_isZan=$update_isZan,\nupdate_commentCount=$update_commentCount,\nupdate_zan=$update_zan,\nupdate_isDeal=$update_isDeal,\nupdate_position=$update_position")
       if(listItems!!.size>0&&listItems!!.size-1>=update_position){
           if(listItems[update_position]!=null&&listItems[update_position].id!=null&&listItems[update_position].id!!.equals(update_id)){
               listItems[update_position].zan=update_zan
               listItems[update_position].zanIs=update_isZan
               listItems[update_position].commentCount=update_commentCount
               if(update_isDeal.equals("1")){
                   listItems[update_position].commentID=update_isDeal
               }
               adapter!!.notifyDataSetChanged()
           }
       }
      super.onStart()
    }


    override fun onResume(){
        super.onResume()
        JEventUtils.onCountEvent(context,Constant.Event_AskSwitchList)//切换帖子列表
        JAnalyticsInterface.onPageStart(context,"帖子主页面")
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(context,"帖子主页面")
        super.onPause()
    }


    override fun update2AddZan(position:Int,listener:IDataRequestListener) {
        adapter!!.listAllAsk.get(position).zanIs="1"
        adapter!!.listAllAsk.get(position).zan=(adapter!!.listAllAsk.get(position).zan!!.toInt()+1).toString()
        adapter!!.notifyDataSetChanged()
        listener.loadSuccess(position)

    }

    override fun update2DeleteAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
            val pDialog= SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
            pDialog.setTitleText("删除成功")
            pDialog.setContentText("亲，您已经成功删除该帖子")
            pDialog.show()
            val handler=Handler()
            handler.postDelayed({
                pDialog.cancel()
            },1236)
        }
    }

    override fun update2ShieldingAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
        }
    }

    private fun addOnTouchListener(){
        mlistView!!.setOnTouchListener(View.OnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    head_search_rl!!.visibility=View.GONE
                }
                MotionEvent.ACTION_MOVE -> {
                    head_search_rl!!.visibility=View.GONE
                }
                MotionEvent.ACTION_UP -> {
                    val handler=Handler()
                    handler.postDelayed({
                        head_search_rl!!.bringToFront()
                        head_search_rl!!.visibility=View.VISIBLE
                    },4000)
                }
            }
            false
        })
    }

    private fun initData() {

        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        college= read.getString("college", "")
        ask_type_choose=read.getString("ask_type_choose","")

        if(!college.equals("")&&ask_type_choose!!.equals(Constant.Ask_Type_College)){
            range=Constant.Ask_Type_College
        }else if(college.equals("")&&ask_type_choose!!.equals(Constant.Ask_Type_College)){
            Toasty.warning(context,"亲，您还未填写学院信息").show()
            range=Constant.Ask_Type_School
            val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
            editor.putString("ask_type_choose",Constant.Ask_All_School)
            editor.commit()
        }else if(ask_type_choose!!.equals(Constant.Ask_All_School)){
            range=Constant.Ask_Type_School
        }else{//第一次帖子类型没选，为空，默认是全校
            range=Constant.Ask_Type_School
        }

        if(flag==0){
            if(range.equals("school")){
                query1NowAll(Ask1SQLiteOpenHelper.A_Name,listItems)//查询缓存数据库
            }else if(range.equals("college")){
                query2NowAll(Ask2SQLiteOpenHelper.A_Name,listItems)//查询缓存数据库
            }
            adapter!!.notifyDataSetChanged()
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 200)
            flag++
        }


        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)

                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }

                }, 100)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                RequestServerList(++pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)

            }, 100)
        })
    }


    private fun RequestServerList(pageNow: Int, orderBy: String, order: String, adapter: AskMainAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
        AskMainRequest.getResultFromServer(context, TAG, Constant.Ask_List, pageNow, orderBy, order, range!!,college!!,listItems,adapter,ptrClassicFrameLayout)
    }

    private fun initView() {
        caption = activity.findViewById(R.id.main_txt1) as TextView
        caption_msg = activity.findViewById(R.id.main_msg) as ImageView
        caption_msg_point = activity.findViewById(R.id.main_unread_msg) as TextView
        main_setting = activity.findViewById(R.id.main_setting) as ImageView
        caption_ask_Iv=activity.findViewById(R.id.ask_down_Iv) as ImageView
        main_ask=activity.findViewById(R.id.main_ask) as TextView
        main_right_btn=activity.findViewById(R.id.main_right_btn) as Button
        main_right_msg_btn=activity.findViewById(R.id.main_right_msg_btn) as Button
        main_right_msg_btn!!.visibility=View.GONE
        main_right_btn!!.visibility=View.VISIBLE
        main_ask!!.visibility=View.VISIBLE
        caption_ask_Iv!!.visibility=View.VISIBLE
        main_setting!!.setImageResource(R.mipmap.ask_write_ask)
        main_setting!!.visibility = View.VISIBLE
        caption_msg!!.visibility = View.GONE
        caption_msg_point!!.visibility = View.GONE
        caption!!.text = "社区"
        range=Constant.Ask_All_School
    }

    private fun addSomeListener(){
       main_ask!!.setOnClickListener{
           caption_ask_Iv!!.setImageResource(R.mipmap.ask_list_type_arrow_up)
           val contentView:View=LayoutInflater.from(context).inflate(R.layout.ask_dialog,null)
           hanleAskDialog(contentView)
           popWindow= CustomPopWindow.PopupWindowBuilder(context)
                   .setView(contentView)
                   .enableBackgroundDark(true)
                   .setBgDarkAlpha(0.7f)
                   .setFocusable(true)
                   .setOutsideTouchable(true)
                   .setAnimationStyle(R.style.ask_type_anim)
                   .setOnDissmissListener(object: PopupWindow.OnDismissListener{
                       override fun onDismiss() {
                           caption_ask_Iv!!.setImageResource(R.mipmap.ask_list_type_arrow_down)
                       }
                   })
                   .create()
           popWindow!!.showAtLocation(main_ask, Gravity.CENTER_HORIZONTAL,0,DensityUtil.dip2px(context,-500.0f))
       }



       main_right_btn!!.setOnClickListener{
           val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
           val token = read.getString("token", "")
           if(token.equals("")){
               val intent:Intent=Intent(context,LoginMain::class.java)
               startActivity(intent)
           }else{
               JEventUtils.onCountEvent(context,Constant.Event_WriteAsk)//发帖子统计
               val intent: Intent =Intent(context,AskWrite::class.java)
               startActivity(intent)
           }
       }

        ask_search_btn!!.setOnClickListener{
            JEventUtils.onCountEvent(context,Constant.Event_AskSearch)//搜索帖子统计

            val intent: Intent =Intent(context,SearchAskActivity::class.java)
            startActivity(intent)
        }

        main_ask!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                main_ask!!.setBackgroundColor(resources.getColor(R.color.headline_black))
                main_ask!!.alpha=0.618f

            }else{
                main_ask!!.setBackgroundColor(Color.TRANSPARENT)
                main_ask!!.alpha=1.0f
            }
            false
        }


        main_right_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                main_setting!!.alpha=0.618f
                main_right_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else{
                main_setting!!.alpha=1.0f
                main_right_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        ask_search_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                ask_search_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                ask_search_btn!!.alpha=1.0f
            }
            false
        }

    }

    private fun hanleAskDialog(contentView:View){
         ask_some_college_rl=contentView.findViewById(R.id.ask_same_college_rl) as RelativeLayout
         ask_all_school_rl=contentView.findViewById(R.id.ask_all_school_rl) as RelativeLayout
         ask_same_college_iv=contentView.findViewById(R.id.ask_same_college_iv) as ImageView
         ask_all_school_iv=contentView.findViewById(R.id.ask_all_school_iv) as ImageView

        ask_some_college_rl!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                ask_some_college_rl!!.setBackgroundColor(context.resources.getColor(R.color.bgcolor))

            }else if(event.action== MotionEvent.ACTION_UP){
                ask_some_college_rl!!.setBackgroundColor(Color.WHITE)
            }
            false
        }

        ask_all_school_rl!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                ask_all_school_rl!!.setBackgroundColor(context.resources.getColor(R.color.bgcolor))

            }else if(event.action== MotionEvent.ACTION_UP){
                ask_all_school_rl!!.setBackgroundColor(Color.WHITE)
            }
            false
        }

         first_get_sharefile()
         val listener:View.OnClickListener=object:View.OnClickListener{
             override fun onClick(v: View?) {
                 if(popWindow!=null){
                     popWindow!!.dissmiss()
                 }
                 when(v!!.id){
                     R.id.ask_same_college_rl -> choose_same_college()
                     R.id.ask_all_school_rl   -> choose_all_school()
                 }
             }
         }
         ask_some_college_rl!!.setOnClickListener(listener)
         ask_all_school_rl!!.setOnClickListener(listener)
    }

    private fun first_get_sharefile(){
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val ask_type_choose= read.getString("ask_type_choose", "")
        val token=read.getString("token","")
        if(ask_type_choose.equals(Constant.Ask_Type_School)||ask_type_choose.equals("")||token.equals("")){
            ask_same_college_iv!!.visibility=View.GONE
            ask_all_school_iv!!.visibility=View.VISIBLE
        }else if(ask_type_choose.equals(Constant.Ask_Type_College)){
            ask_same_college_iv!!.visibility=View.VISIBLE
            ask_all_school_iv!!.visibility=View.GONE
        }else{
            Toasty.info(context,Constant.SERVERBROKEN).show()
        }
    }

    private fun choose_same_college(){
         val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
         val token = read.getString("token", "")
         val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
         if(!token.equals("")){
             editor.putString("ask_type_choose", Constant.Ask_Type_College)
             editor.commit()
             //刷新一下本学院的帖子，并且显示
         }else{
            Toasty.warning(context,"亲，您还未填写学院信息").show()
            editor.putString("ask_type_choose", Constant.Ask_Type_School)
            editor.commit()
         }
         ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
         initData()
    }

    private fun choose_all_school(){
        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
        editor.putString("ask_type_choose", Constant.Ask_Type_School)
        editor.commit()
        //刷新一下全学校的帖子，并且显示
        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
        initData()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            caption_msg_point!!.visibility = View.GONE

        }else{
            caption_msg_point!!.visibility = View.GONE
        }
    }


    //数据库查询
    private fun query1NowAll(table:String,listItems:ArrayList<MyAsk>){


        val sql="select * from "+ Ask1SQLiteOpenHelper.A_Name+" order by createTime desc"
        listItems.clear()
        val cursor= helper1!!.readableDatabase.rawQuery(sql,null)

        while(cursor.moveToNext()){
            val ask= MyAsk()
            ask.id=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Sid))
            ask.createTime=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_CreateTime))
            ask.publisherID=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_PublisherID))
            ask.nickname=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_NickName))
            ask.icon=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Icon))
            ask.gender=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Gender))
            ask.reward=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Reward))
            ask.zan=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Zan))
            ask.commentID=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_CommentID))
            ask.type=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Type))
            ask.label=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Label))
            ask.title=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Title))
            ask.content=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Content))
            ask.link=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Link))
            ask.zanIs=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_IsZan))
            ask.favoriteIs=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_IsFavorite))
            ask.commentCount=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_CommentCount))

            val db_image=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_Image))
            if(db_image!=null&&!db_image.equals("")){
                val db_image_sub=db_image.substring(1,db_image.length-1)
                val image=db_image_sub.split(",") as ArrayList<String>
               // LogUtils.d_debugprint(Constant.Ask_TAG,"\n\ndb_image被分割前是：$db_image,\n分割后是：$db_image_sub,转化为image数组之后是：$image")
                ask.image=image
            }

            val db_timage=cursor.getString(cursor.getColumnIndex(Ask1SQLiteOpenHelper.A_ThumbnailImage))
            if(db_timage!=null&&!db_timage.equals("")){
                val db_timage_sub=db_timage.substring(1,db_timage.length-1)
                val timage=db_timage_sub.split(",") as ArrayList<String>
                LogUtils.d_debugprint(Constant.Ask_TAG,"\n\ndb_timage被分割前是：$db_timage,\n分割后是：$db_timage_sub,转化为image数组之后是：$timage")
                ask.thumbnailImage=timage
            }

            listItems.add(ask)
        }
        LogUtils.d_debugprint(Constant.Ask_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }

    //数据库查询
    private fun query2NowAll(table:String,listItems:ArrayList<MyAsk>){


        val sql="select * from "+ Ask2SQLiteOpenHelper.A_Name+" order by createTime desc"
        listItems.clear()
        val cursor= helper2!!.readableDatabase.rawQuery(sql,null)

        while(cursor.moveToNext()){
            val ask= MyAsk()
            ask.id=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Sid))
            ask.createTime=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_CreateTime))
            ask.publisherID=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_PublisherID))
            ask.nickname=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_NickName))
            ask.icon=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Icon))
            ask.gender=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Gender))
            ask.reward=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Reward))
            ask.zan=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Zan))
            ask.commentID=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_CommentID))
            ask.type=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Type))
            ask.label=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Label))
            ask.title=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Title))
            ask.content=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Content))
            ask.link=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Link))
            ask.zanIs=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_IsZan))
            ask.favoriteIs=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_IsFavorite))
            ask.commentCount=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_CommentCount))

            val db_image=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_Image))
            if(db_image!=null&&!db_image.equals("")){
                val db_image_sub=db_image.substring(1,db_image.length-1)
                val image=db_image_sub.split(",") as ArrayList<String>
                // LogUtils.d_debugprint(Constant.Ask_TAG,"\n\ndb_image被分割前是：$db_image,\n分割后是：$db_image_sub,转化为image数组之后是：$image")
                ask.image=image
            }

            val db_timage=cursor.getString(cursor.getColumnIndex(Ask2SQLiteOpenHelper.A_ThumbnailImage))
            if(db_timage!=null&&!db_timage.equals("")){
                val db_timage_sub=db_timage.substring(1,db_timage.length-1)
                val timage=db_timage_sub.split(",") as ArrayList<String>
                LogUtils.d_debugprint(Constant.Ask_TAG,"\n\ndb_timage被分割前是：$db_timage,\n分割后是：$db_timage_sub,转化为image数组之后是：$timage")
                ask.thumbnailImage=timage
            }

            listItems.add(ask)
        }
        LogUtils.d_debugprint(Constant.Ask_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }

}
