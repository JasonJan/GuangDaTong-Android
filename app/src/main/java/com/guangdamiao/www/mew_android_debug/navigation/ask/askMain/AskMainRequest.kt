package com.guangdamiao.www.mew_android_debug.navigation.ask.askMain

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.os.Handler
import android.widget.BaseAdapter
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class AskMainRequest {

    companion object {

        private var helper1: Ask1SQLiteOpenHelper?=null
        private var db1: SQLiteDatabase?=null
        private var helper2: Ask2SQLiteOpenHelper?=null
        private var db2: SQLiteDatabase?=null


        fun getResultFromServer(context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, orderBy:String, order:String, range:String, college:String, listAskAll: ArrayList<MyAsk>, adapter:BaseAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
            if (NetUtil.checkNetWork(context)) {
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE)
                }

                val jsonObject= JSONObject()
                 jsonObject.put("pageIndex",pageIndex)
                 jsonObject.put("pageSize",pageSize)
                 jsonObject.put("orderBy",orderBy)
                 jsonObject.put("order",order)
                 jsonObject.put("range",range)
                 jsonObject.put("sign",sign)
                 jsonObject.put("token",token)
                 if(range.equals("college")){
                     jsonObject.put("college",college)
                 }
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                    client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                        override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {

                            if(responseBody!=null&&context!=null){
                                val resultDate = String(responseBody!!, charset("utf-8"))
                                val response=JSONObject(resultDate)
                                val getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(getResultFromServer)+"\n\n")
                                var getCode: String=""
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>

                                if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                    getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                }
                                if (Constant.RIGHTCODE.equals(getCode)) {
                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    var askAll:ArrayList<MyAsk>?=null
                                    askAll=JsonUtil.get_ask_Item("data", result)
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + askAll.toString()+"\n\n")
                                    if(pageIndexNow==0){
                                        listAskAll.clear()
                                    }

                                    if(range.equals("school")){
                                        helper1 = Ask1SQLiteOpenHelper(context)
                                        delete1NowAll(Ask1SQLiteOpenHelper.A_Name)
                                    }else if(range.equals("college")){
                                        helper2 = Ask2SQLiteOpenHelper(context)
                                        delete2NowAll(Ask2SQLiteOpenHelper.A_Name)
                                    }


                                    if (askAll != null&&askAll.size>0) {
                                        for(item in askAll){

                                            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                                            val blacklist= read.getString("blacklist", "[]")
                                            val blacklist_array=ArrayList<String>()
                                            val jsonArray= JSONArray(blacklist)
                                            if(jsonArray.length()>0) {
                                                for (i in 0..jsonArray.length()-1) {
                                                    blacklist_array.add(jsonArray.getString(i))
                                                }
                                            }
                                            var isExistBlack=false
                                            for(j in blacklist_array.indices){
                                                if(item.publisherID.equals(blacklist_array[j])){
                                                    isExistBlack=true
                                                }
                                            }

                                            val read2 = context.getSharedPreferences(Constant.ShareFile_BlackAsk, Context.MODE_PRIVATE)
                                            val blackask= read2.getString("blackask", "[]")
                                            val userID=read2.getString("userID","")
                                            val blackask_array=ArrayList<String>()
                                            val jsonArray2= JSONArray(blackask)
                                            if(jsonArray2.length()>0) {
                                                for (i2 in 0..jsonArray2.length()-1) {
                                                    blackask_array.add(jsonArray2.getString(i2))
                                                }
                                            }
                                            var isExistBlackAsk=false
                                            val read3 = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                            val id = read3.getString("id", "")
                                            if(id.equals(userID)){
                                                for(j2 in blackask_array.indices){
                                                    if(item.id.equals(blackask_array[j2])){
                                                        isExistBlackAsk=true
                                                    }
                                                }
                                            }

                                            //LogUtils.d_debugprint(Constant.Ask_TAG,"\n拉黑所有人json为：$blacklist \n拉黑的帖子json为：$blackask")
                                            
                                            if(isExistBlack||isExistBlackAsk){

                                            }else{
                                                val s_id=if(item.id==null) "" else item.id
                                                val createTime=if(item.createTime==null) "" else item.createTime
                                                val publisherID=if(item.publisherID==null) "" else item.publisherID
                                                val nickname=if(item.nickname==null) "" else item.nickname
                                                val icon=if(item.icon==null) "" else item.icon
                                                val gender=if(item.gender==null) "" else item.gender
                                                val reward=if(item.reward==null) "" else item.reward
                                                val zan=if(item.zan==null) "" else item.zan
                                                val commentID=if(item.commentID==null) "" else item.commentID
                                                val type=if(item.type==null) "" else item.type
                                                val label=if(item.label==null) "" else item.label
                                                val title=if(item.title==null) "" else item.title
                                                var content=if(item.content==null) "" else item.content
                                                val link=if(item.link==null) "" else item.link
                                                var image=""
                                                var thumbnailImage=""
                                                if(item.image!=null&&item.image!!.size>0){
                                                    image=item.image.toString()
                                                }
                                                if(item.thumbnailImage!=null&&item.thumbnailImage!!.size>0){
                                                    thumbnailImage=item.thumbnailImage.toString()
                                                }

                                                val commentCount=if(item.commentCount==null) "" else item.commentCount
                                                val isZan=if(item.zanIs==null) "" else item.zanIs
                                                val isFavorite=if(item.favoriteIs==null) "" else item.favoriteIs


                                                if(range.equals("school")){
                                                    helper1 = Ask1SQLiteOpenHelper(context)
                                                    insert1Data(Ask1SQLiteOpenHelper.A_Name,s_id!!,createTime!!,publisherID!!,nickname!!,icon!!,gender!!,reward!!,
                                                            zan!!,commentID!!,type!!, label!!,title!!,content!!, link!!,image,thumbnailImage,commentCount!!,isZan!!,isFavorite!!)
                                                    LogUtils.d_debugprint(Constant. Ask_TAG,"\n在 Ask1中已插入一条数据！！！\n")
                                                }else if(range.equals("college")){
                                                    helper2 = Ask2SQLiteOpenHelper(context)
                                                    insert2Data(Ask2SQLiteOpenHelper.A_Name,s_id!!,createTime!!,publisherID!!,nickname!!,icon!!,gender!!,reward!!,
                                                            zan!!,commentID!!,type!!, label!!,title!!,content!!, link!!,image,thumbnailImage,commentCount!!,isZan!!,isFavorite!!)
                                                    LogUtils.d_debugprint(Constant. Ask_TAG,"\n在 Ask2中已插入一条数据！！！\n")
                                                }

                                                listAskAll.add(item)
                                            }

                                        }
                                         adapter!!.notifyDataSetChanged()
                                         ptrClassicFrameLayout!!.refreshComplete()
                                         ptrClassicFrameLayout!!.loadMoreComplete(true)

                                    } else if(askAll!!.size==0&&pageIndexNow==0){
                                        listAskAll.clear()
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    }else{
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val token = read.getString("token", "")
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    editor.clear()
                                    editor.commit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    Toasty.info(context,Constant.SERVERBROKEN).show()
                                }
                             }
                        }

                        override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                            if(context!=null){
                                ptrClassicFrameLayout!!.refreshComplete()
                                ptrClassicFrameLayout!!.isLoadMoreEnable=false
                                Toasty.error(context,Constant.REQUESTFAIL).show()
                            }
                        }

                  })
            } else {
                if(context!=null){
                    ptrClassicFrameLayout!!.refreshComplete()
                    ptrClassicFrameLayout!!.isLoadMoreEnable=false
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        fun makeFavoriteFromServer(context: Context, TAG: String, urlEnd: String,id:String,token:String,pDialog: SweetAlertDialog) {
            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("id",id)
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            val getResultFromServer=response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String=""
                            var result: String
                            if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                pDialog.cancel()
                                val success=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                        success.setTitleText("收藏成功")
                                        success.setContentText("亲，您已经成功收藏该帖子")
                                        success.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    success.cancel()
                                },1236)

                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                pDialog.cancel()
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val account = read.getString("account", "")
                                val password=read.getString("password","")
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()

                                val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                editor2.putString("account",account)
                                editor2.putString("password",password)
                                editor2.commit()

                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                pDialog.cancel()
                                val fail=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                        fail.setTitleText("收藏失败")
                                        fail.setContentText("亲，您已经收藏过或者帖子已经删除")
                                        fail.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    fail.cancel()
                                },1236)

                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun makeInformFromServer(context: Context, TAG: String, urlEnd: String,id:String,token:String,reason:String,pDialog:SweetAlertDialog) {
            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("id",id)
                jsonObject.put("reason",reason)
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            val getResultFromServer=response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String=""
                            if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {

                                if(urlEnd.equals(Constant.Inform_Ask)){
                                    pDialog.cancel()
                                    val success=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                            success.setTitleText("举报成功")
                                            success.setContentText("亲，您已经成功举报帖子")
                                            success.setConfirmClickListener { sDialog ->
                                                if(AskReport.activity!=null){
                                                    MainApplication.getInstance().finishActivity(AskReport.activity)
                                                }
                                                sDialog.dismissWithAnimation()
                                            }
                                            success.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        if(AskReport.activity!=null){
                                            MainApplication.getInstance().finishActivity(AskReport.activity)
                                        }
                                        success.cancel()
                                    },1236)
                                }else if(urlEnd.equals(Constant.Inform_CommentAsk)){
                                    pDialog.cancel()
                                    val success=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                            success.setTitleText("举报成功")
                                            success.setContentText("亲，您已经成功举报该评论")
                                            success.setConfirmClickListener { sDialog ->
                                                if(AskReport.activity!=null){
                                                    MainApplication.getInstance().finishActivity(AskReport.activity)
                                                }
                                                sDialog.dismissWithAnimation()
                                            }
                                            success.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        if(AskReport.activity!=null){
                                            MainApplication.getInstance().finishActivity(AskReport.activity)
                                        }
                                        success.cancel()
                                    },1236)
                                }


                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                pDialog.cancel()
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()
                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{

                                if(urlEnd.equals(Constant.Inform_Ask)){
                                    pDialog.cancel()
                                    val fail=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                            fail.setTitleText("举报失败")
                                            fail.setContentText("亲，您已经举报过或者该帖子已经删除")
                                            fail.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        if(AskReport.activity!=null){
                                            MainApplication.getInstance().finishActivity(AskReport.activity)
                                        }
                                        fail.cancel()
                                    },1236)

                                }else if(urlEnd.equals(Constant.Inform_CommentAsk)){
                                    pDialog.cancel()
                                    val fail=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                            fail.setTitleText("举报失败")
                                            fail.setContentText("亲，您已经举报过或者该评论已经删除")
                                            fail.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        if(AskReport.activity!=null){
                                            MainApplication.getInstance().finishActivity(AskReport.activity)
                                        }
                                        fail.cancel()
                                    },1236)

                                }

                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                         Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun getSearchResultFromServer(context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, orderBy:String, order:String, range:String, college:String, key:String, type:String, label:String, listAskAll: ArrayList<MyAsk>, adapter:BaseAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
            if (NetUtil.checkNetWork(context)) {
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE)
                }

                val jsonObject= JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("orderBy",orderBy)
                jsonObject.put("order",order)
                jsonObject.put("range",range)
                jsonObject.put("sign",sign)
                jsonObject.put("key",key)
                jsonObject.put("type",type)
                jsonObject.put("label",label)
                jsonObject.put("token",token)
                if(range.equals("college")){
                    jsonObject.put("college",college)
                }
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            val getResultFromServer=response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String=""
                            var msg: String? = null
                            var result: String
                            var getData: List<Map<String, Any?>>

                            if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                var askAll:ArrayList<MyAsk>?=null
                                askAll=JsonUtil.get_ask_Item("data", result)
                                LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + askAll.toString()+"\n")
                                if(pageIndexNow==0){
                                    listAskAll.clear()
                                }
                                if (askAll!= null&&askAll.size>0) {
                                    for(item in askAll){
                                        listAskAll.add(item)
                                    }
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                } else if(askAll!=null&&askAll!!.size==0){
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                }else{
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                }
                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val token = read.getString("token", "")
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()
                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            //跳转到登录，不用传东西过去
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                Toasty.info(context,Constant.SERVERBROKEN).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          ptrClassicFrameLayout!!.refreshComplete()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun makeShareToServer(TAG:String,urlEnd:String,context:Context,objectID: String,shareWay:String,token: String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORTEST+objectID)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+objectID)
                }

                val jsonObject=JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("objectID", objectID)
                jsonObject.put("shareWay",shareWay)
                jsonObject.put("token",token)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                if(JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                    LogUtils.d_debugprint(TAG,"向服务器发送分享的情况  成功！！！\n分享的链接为: $urlEnd\n分享的方式为：$shareWay")
                                }else{
                                    LogUtils.d_debugprint(TAG,"向服务器发送分享的情况  失败！！！\n分享的链接为: $urlEnd\n分享的方式为：$shareWay")
                                }
                            }else{
                                LogUtils.d_debugprint(TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }


                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun getMyAskFromServer(context: Context, TAG: String, urlEnd: String, pageIndexNow: Int,orderBy:String,order:String, listAskAll: ArrayList<MyAsk>, adapter:BaseAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout,listener:IDataRequestListener2String) {
            if (NetUtil.checkNetWork(context)) {
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject= JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("orderBy",orderBy)
                jsonObject.put("order",order)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){


                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            val getResultFromServer=response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String=""
                            var msg: String? = null
                            var result: String
                            var getData: List<Map<String, Any?>>

                            if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                var askAll:ArrayList<MyAsk>?=null
                                askAll=JsonUtil.get_ask_Item("data", result)
                                LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + askAll.toString()+"\n")
                                if(pageIndexNow==0){
                                    listAskAll.clear()
                                }
                                if (askAll != null&&askAll.size>0) {
                                    for(item in askAll){
                                        listAskAll.add(item)
                                    }
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    listener.loadSuccess(listAskAll.size.toString())
                                } else if(pageIndexNow==0){
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    listener.loadSuccess("noMoney")
                                }else{
                                    listener.loadSuccess(listAskAll.size.toString())
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    //Toasty.info(context, "没有更多数据了").show()
                                }
                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val token = read.getString("token", "")
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()
                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            //跳转到登录，不用传东西过去
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                Toasty.info(context,Constant.SERVERBROKEN).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          ptrClassicFrameLayout!!.refreshComplete()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        /**
         * 插入数据
         */
        private fun insert1Data(table:String,s_id: String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,zan:String,commentID:String,type:String,label:String,title:String,content:String,link:String,image:String,thumbnailImage:String,commentCount:String,isZan:String,isFavorite:String) {
            if(table.equals(Ask1SQLiteOpenHelper.A_Name)){
                if( helper1 !=null){
                    db1 = helper1!!.writableDatabase
                    val askItem= ContentValues()
                    askItem.put("s_id",s_id)
                    askItem.put("createTime",createTime)
                    askItem.put("publisherID",publisherID)
                    askItem.put("nickname",nickname)
                    askItem.put("icon",icon)
                    askItem.put("gender",gender)
                    askItem.put("reward",reward)
                    askItem.put("zan",zan)
                    askItem.put("commentID",commentID)
                    askItem.put("type",type)
                    askItem.put("label",label)
                    askItem.put("title",title)
                    askItem.put("content",content)
                    askItem.put("link",link)
                    askItem.put("image",image)
                    askItem.put("thumbnailImage",thumbnailImage)
                    askItem.put("commentCount",commentCount)
                    askItem.put("isZan",isZan)
                    askItem.put("isFavorite",isFavorite)
                    db1!!.insert(table,null,askItem)
                    db1!!.close()
                }
            }
        }

        //数据库删除
        private fun delete1NowAll(table:String){
            if(table.equals(Ask1SQLiteOpenHelper.A_Name)){
                val sql="delete from "+ Ask1SQLiteOpenHelper.A_Name
                val db=helper1!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.Ask_TAG,"本地数据库删除了Ask所有数据！！！")
            }
        }

        /**
         * 插入数据
         */
        private fun insert2Data(table:String,s_id: String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,zan:String,commentID:String,type:String,label:String,title:String,content:String,link:String,image:String,thumbnailImage:String,commentCount:String,isZan:String,isFavorite:String) {
            if(table.equals(Ask2SQLiteOpenHelper.A_Name)){
                if( helper2 !=null){
                    db2 = helper2!!.writableDatabase
                    val askItem= ContentValues()
                    askItem.put("s_id",s_id)
                    askItem.put("createTime",createTime)
                    askItem.put("publisherID",publisherID)
                    askItem.put("nickname",nickname)
                    askItem.put("icon",icon)
                    askItem.put("gender",gender)
                    askItem.put("reward",reward)
                    askItem.put("zan",zan)
                    askItem.put("commentID",commentID)
                    askItem.put("type",type)
                    askItem.put("label",label)
                    askItem.put("title",title)
                    askItem.put("content",content)
                    askItem.put("link",link)
                    askItem.put("image",image)
                    askItem.put("thumbnailImage",thumbnailImage)
                    askItem.put("commentCount",commentCount)
                    askItem.put("isZan",isZan)
                    askItem.put("isFavorite",isFavorite)
                    db2!!.insert(table,null,askItem)
                    db2!!.close()
                }
            }
        }

        //数据库删除
        private fun delete2NowAll(table:String){
            if(table.equals(Ask2SQLiteOpenHelper.A_Name)){
                val sql="delete from "+ Ask2SQLiteOpenHelper.A_Name
                val db=helper2!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.Ask_TAG,"本地数据库删除了Ask2所有数据！！！")
            }
        }
    }
}
