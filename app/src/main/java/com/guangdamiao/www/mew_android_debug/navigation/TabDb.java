package com.guangdamiao.www.mew_android_debug.navigation;


import com.guangdamiao.www.mew_android_debug.R;

/**
 * Created by Jason_Jan on 2017/12/9.
 */

public class TabDb {

    /***
     * 获得底部所有项
     */
    public static String[] getTabsTxt() {
        String[] tabs = {"首页","栏目","社区","我"};
        return tabs;
    }
    /***
     * 获得所有碎片
     */
    public static Class[] getFramgent(){
        Class[] cls = {OneFm.class,TwoFm.class,ThreeFm.class,FourFm.class};
        return cls ;
    }
    /***
     * 获得所有点击前的图片
     */
    public static int[] getTabsImg(){
        int[] img = {R.mipmap.article_black ,R.mipmap.column_black,R.mipmap.club_black,R.mipmap.me_gray};
        return img ;
    }
    /***
     * 获得所有点击后的图片
     */
    public static int[] getTabsImgLight(){
        int[] img = {R.mipmap.article_green,R.mipmap.column_green,R.mipmap.club_green,R.mipmap.me_green};
        return img ;
    }
}
