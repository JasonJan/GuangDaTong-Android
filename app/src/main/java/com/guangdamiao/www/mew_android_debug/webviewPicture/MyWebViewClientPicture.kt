package com.guangdamiao.www.mew_android_debug.webviewPicture

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.webkit.WebView
import android.webkit.WebViewClient
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/18.
 */

class MyWebViewClientPicture(context: Context, imageUrls:ArrayList<String>) : WebViewClient() {

    var imageUrls:ArrayList<String>?=null
    var context: Context?=null

    init {
        this.context=context
        this.imageUrls=imageUrls
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        view.loadUrl(url)
        return true
    }

    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
        LogUtils.d_debugprint("首页WebView", "\tonPageStarted")
        super.onPageStarted(view, url, favicon)
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onPageFinished(view: WebView, url: String) {
        LogUtils.d_debugprint("首页WebView", "\tonPageFinished ")
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.KITKAT){//当Android SDK>=4.4时
            callEvaluateJavascript(view)
        }else {
            callMethod(view)
        }
        super.onPageFinished(view, url)
    }

    private fun callMethod(webView: WebView) {
        var call = "javascript:picturesUrl()"
        webView.loadUrl(call)

    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun callEvaluateJavascript(webView: WebView) {

        webView.evaluateJavascript("picturesUrl()") { value ->
            val jsonObject= JSONObject(value)
            val jsonArray = jsonObject.getJSONArray("image")
            for (i in 0..jsonArray.length() - 1) {
                val item = jsonArray.getString(i)
                imageUrls!!.add(item)
                LogUtils.d_debugprint("webView","\n在InJavaScriptGetPictruesUrl中，调用了js后的返回值为：$value")
            }
           /* webView!!.addJavascriptInterface(MJavascriptInterface(context, imageUrls),"imagelistener")
            webView!!.setWebViewClient(MyWebViewClient())*/
        }
    }
}

