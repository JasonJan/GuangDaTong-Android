package com.guangdamiao.www.mew_android_debug.navigation.ask.askWrite

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.google.gson.Gson
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.AskWriteBean
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.SendImage
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.NavigationMainActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import es.dmoral.toasty.Toasty
import me.nereo.multi_image_selector.MultiImageSelectorActivity
import org.json.JSONObject
import org.xutils.common.Callback
import org.xutils.http.RequestParams
import java.io.File


class AskWrite : BaseActivity() {

    private val REQUEST_IMAGE=618
    private var mSelect_list:ArrayList<String>?=null

    private var title_left_imageview_btn: Button?=null
    private var title_right_image: ImageView?=null
    private var title_right_imageview_btn: Button?=null
    private var title_center_textview: TextView? = null
    private var askWrite_image5_btn:Button?=null
    private val IMG_COUNT=10

    private val IMG_ADD_TAG= Constant.IMG_ADD_TAG
    private var askWrite_gridView:GridView?=null
    private var askWrite_adapter:GVAdapter?=null
    private var askWrite_list:ArrayList<String>?=null
    private var askWrite_content_et:EditText?=null
    private var askWrite_num:TextView?=null
    private var askWrite_content_ll:LinearLayout?=null
    private var askWrite_type_rl:RelativeLayout?=null
    private var askWrite_label_rl:RelativeLayout?=null
    private var askWrite_see_rl:RelativeLayout?=null
    private var askWrite_type_tv:TextView?=null
    private var askWrite_label_tv:TextView?=null
    private var askWrite_see_tv:TextView?=null
    private var askWrite_money_num1:EditText?=null
    private var askWrite_money_num2:TextView?=null
    private var askWrite_title_et:EditText?=null
    private var rule_for_ask_type_num=0
    private var rule_for_ask_see_num=0
    private var rule_for_ask_reward_num=0
    private var rule_for_ask_money_all=0
    private var college_urgent_int=0
    private var school_urgent_int=0
    private var college_normal_int=0
    private var school_normal_int=0
    private var coins_int=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_ask_write)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        AskWriteRequest.requestForRule(this@AskWrite,object:IDataRequestListener2String{
            override fun loadSuccess(response_string: String) {
                val result=JsonUtil.get_key_string("result",response_string)
                coins_int=JsonUtil.get_key_int("coins",result)
                val rules_string=JsonUtil.get_key_string("rules",result)
                val jsonString_urgent=JsonUtil.get_key_string("urgent",rules_string)
                val jsonString_normal=JsonUtil.get_key_string("normal",rules_string)
                college_urgent_int=JsonUtil.get_key_int("college",jsonString_urgent)
                school_urgent_int=JsonUtil.get_key_int("school",jsonString_urgent)
                college_normal_int=JsonUtil.get_key_int("college",jsonString_normal)
                school_normal_int=JsonUtil.get_key_int("school",jsonString_normal)
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"从服务器拿到的规则是：\n"+"急问=====同学院: "+college_urgent_int+
                "急问=====全校: "+school_urgent_int+"普问=====同学院："+college_normal_int+"普问=====全校所有人"+school_normal_int
                )
                rule_for_ask_type_num=college_normal_int

                askWrite_money_num2!!.text=college_normal_int.toString()
            }
        })//点进来就首先请求服务器，获得当前金币数量，以及规则

        initView()
        addSomeListener()
        initData()

    }


    override fun onStart() {
        super.onStart()//规则确定金币需要

        if(askWrite_type_tv!!.text.toString().equals("急问")&&askWrite_see_tv!!.text.toString().equals("本校同学院")){
            rule_for_ask_type_num=college_urgent_int
        }else if(askWrite_type_tv!!.text.toString().equals("普问")&&askWrite_see_tv!!.text.toString().equals("本校同学院")){
            rule_for_ask_type_num=college_normal_int
        }else if(askWrite_type_tv!!.text.toString().equals("急问")&&askWrite_see_tv!!.text.toString().equals("本校所有人")){
            rule_for_ask_type_num=school_urgent_int
        }else if(askWrite_type_tv!!.text.toString().equals("普问")&&askWrite_see_tv!!.text.toString().equals("本校所有人")){
            rule_for_ask_type_num=school_normal_int
        }
        if(askWrite_money_num1!!.text.toString().equals("")){
            rule_for_ask_reward_num=0
        }else{
            LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,askWrite_money_num1!!.getText().toString())
            rule_for_ask_reward_num=askWrite_money_num1!!.getText().toString().toInt()
        }

        rule_for_ask_money_all=rule_for_ask_type_num+rule_for_ask_reward_num

        askWrite_money_num2!!.text=rule_for_ask_money_all.toString()

    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@AskWrite,"发帖子")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@AskWrite,"发帖子")
        super.onPause()
    }


    fun initData(){
        if (askWrite_list == null) {
            askWrite_list = ArrayList<String>()
            mSelect_list==ArrayList<String>()
            askWrite_list!!.add(IMG_ADD_TAG)
        }
        askWrite_adapter = GVAdapter()
        askWrite_gridView!!.setAdapter(askWrite_adapter)

        askWrite_gridView!!.setOnItemClickListener({ parent, view, position, id ->
            if (askWrite_list!![position].equals(IMG_ADD_TAG)) {
                if (askWrite_list!!.size < IMG_COUNT) {
                    //这里实现了跳转
                    val i=Intent(this@AskWrite,MultiImageSelectorActivity::class.java)
                    i.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA,false)
                    i.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT,IMG_COUNT-askWrite_list!!.size)
                    i.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE,MultiImageSelectorActivity.MODE_MULTI)
                    if(mSelect_list!=null&&askWrite_list!!.size<IMG_COUNT){
                        i.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST,mSelect_list)
                    }
                    startActivityForResult(i,REQUEST_IMAGE)


                    /*val i = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(i,0)*/
                } else
                    Toasty.info(this@AskWrite, "亲，最多只能选择9张照片！").show()
            }
        })
        refreshAdapter()
    }

    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_image=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        askWrite_image5_btn=findViewById(R.id.askWrite_image5_btn) as Button
        title_center_textview!!.text="发帖"
        title_center_textview!!.visibility= View.VISIBLE
        title_right_image!!.setImageResource(R.mipmap.ask_write_send)
        title_right_imageview_btn!!.isEnabled=true
        title_right_image!!.visibility=View.VISIBLE
        title_right_imageview_btn!!.visibility=View.VISIBLE
        askWrite_gridView=findViewById(R.id.askWrite_gridview) as GridView
        askWrite_title_et=findViewById(R.id.askWrite_title_et) as EditText
        askWrite_content_et=findViewById(R.id.askWrite_content_et) as EditText
        askWrite_num=findViewById(R.id.askWrite_num) as TextView
        askWrite_content_ll=findViewById(R.id.askWrite_content_ll) as LinearLayout
        askWrite_type_rl=findViewById(R.id.askWrite_type_rl) as RelativeLayout
        askWrite_label_rl=findViewById(R.id.askWrite_label_rl) as RelativeLayout
        askWrite_see_rl=findViewById(R.id.askWrite_see_rl) as RelativeLayout
        askWrite_type_tv=findViewById(R.id.askWrite_type_tv) as TextView
        askWrite_label_tv=findViewById(R.id.askWrite_label_tv) as TextView
        askWrite_see_tv=findViewById(R.id.askWrite_see_tv) as TextView
        askWrite_money_num1=findViewById(R.id.askWrite_money_num1) as EditText
        askWrite_money_num2=findViewById(R.id.askWrite_money_num2) as TextView
        askWrite_money_num2!!.text=college_normal_int.toString()
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            SweetAlertDialog(this@AskWrite, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("提示")
                    .setContentText("您确定要取消发帖？")
                    .setCustomImage(R.mipmap.mew_icon)
                    .setConfirmText("我确定")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                        this.finish()
                        overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
                    }
                    .setCancelText("再想想")
                    .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                    .show()
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        askWrite_image5_btn!!.setOnClickListener{
            val intent:Intent=Intent(this@AskWrite,AskHelp::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }


        title_right_imageview_btn!!.setOnTouchListener { v, event ->
                 if(event.action== MotionEvent.ACTION_DOWN){
                     title_right_image!!.alpha=0.618f
                     title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

                 }else {
                     title_right_image!!.alpha=1.0f
                     title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                 }
                 false
             }

        title_right_imageview_btn!!.setOnClickListener {

            JEventUtils.onCountEvent(this@AskWrite,Constant.Event_WriteAsk)//发帖子

            //点击了右侧的发帖
            if(coins_int<rule_for_ask_money_all){
                    val pDialog1=SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    pDialog1.setTitleText("金币不足")
                    pDialog1.setContentText("亲，您的金币不足以发送此类帖子~")
                    pDialog1.show()
                    val handler= Handler()
                    handler.postDelayed({
                       pDialog1.cancel()
                    },1236)
            }else {
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG, "发送：" + Integer.toString(askWrite_list!!.size - 1) + "\naskWrite_list路径为：" + askWrite_list.toString())
                val askWriteBean = AskWriteBean()
                if (askWrite_title_et!!.text.toString().equals("")) {
                    Toasty.info(this@AskWrite, "帖子标题不能为空喔").show()
                } else {
                    askWriteBean.title = askWrite_title_et!!.text.toString()
                    if (askWrite_content_et!!.text.toString().equals("")) {
                        Toasty.info(this@AskWrite, "帖子内容不能为空喔").show()
                    } else {
                        askWriteBean.content = askWrite_content_et!!.text.toString()
                        askWriteBean.type = askWrite_type_tv!!.text.toString()
                        askWriteBean.label = askWrite_label_tv!!.text.toString()

                        if (askWrite_see_tv!!.text.toString().equals("本校同学院")) {
                            askWriteBean.range = "college"
                        } else if (askWrite_see_tv!!.text.toString().equals("本校所有人")) {
                            askWriteBean.range = "school"
                        }
                        if (askWrite_money_num1!!.text.toString().equals("")) {
                            title_right_imageview_btn!!.isEnabled=false
                            title_right_image!!.setColorFilter(R.color.gray)
                            askWriteBean.reward = "0"

                            upLoad(this@AskWrite, askWriteBean, askWrite_list!!)//发送图片
                        } else {
                            title_right_imageview_btn!!.isEnabled=false
                            title_right_image!!.setColorFilter(R.color.gray)

                            askWriteBean.reward = askWrite_money_num1!!.text.toString()
                            upLoad(this@AskWrite, askWriteBean, askWrite_list!!)//发送图片
                        }

                    }
                }
            }
        }

        askWrite_content_et!!.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if(event!!.action==MotionEvent.ACTION_DOWN){
                    v!!.parent.requestDisallowInterceptTouchEvent(true)
                }else if(event!!.action==MotionEvent.ACTION_UP){
                    v!!.parent.requestDisallowInterceptTouchEvent(false)
                }else if(event!!.action==MotionEvent.ACTION_CANCEL){
                    v!!.parent.requestDisallowInterceptTouchEvent(false)
                }
                return false
            }
        })

        val editChangedListener2reward=EditChangedListener2reward()
        askWrite_money_num1!!.addTextChangedListener(editChangedListener2reward)

        val editChangedListener=EditChangedListener()

        askWrite_content_et!!.addTextChangedListener(editChangedListener)
        askWrite_content_ll!!.setOnTouchListener(OnDoubleClickListener(OnDoubleClickListener.DoubleClickCallback {
            //处理双击事件=====键盘隐藏
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this@AskWrite.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }))

        askWrite_type_rl!!.setOnClickListener{
            val intent=Intent(this, AskWrite_ChooseType::class.java)
            val bundle=Bundle()
            val ask_type=askWrite_type_tv!!.text.toString()
            val ask_label=askWrite_label_tv!!.text.toString()
            val ask_see=askWrite_see_tv!!.text.toString()
            val ask_reward_num=askWrite_money_num1!!.text.toString()
            val ask_money_num=askWrite_money_num2!!.text.toString()

            bundle.putString("ask_type",ask_type)
            bundle.putString("ask_label",ask_label)
            bundle.putString("ask_see",ask_see)
            bundle.putString("ask_reward_num",ask_reward_num)
            bundle.putString("ask_money_num",ask_money_num)

            intent.putExtras(bundle)
            startActivityForResult(intent,Constant.AskWrite_ChooseType)
        }

        askWrite_label_rl!!.setOnClickListener{
            val intent=Intent(this, AskWrite_AddLabel::class.java)
            val bundle=Bundle()
            val ask_type=askWrite_type_tv!!.text.toString()
            val ask_label=askWrite_label_tv!!.text.toString()
            val ask_see=askWrite_see_tv!!.text.toString()
            val ask_reward_num=askWrite_money_num1!!.text.toString()
            val ask_money_num=askWrite_money_num2!!.text.toString()

            bundle.putString("ask_type",ask_type)
            bundle.putString("ask_label",ask_label)
            bundle.putString("ask_see",ask_see)
            bundle.putString("ask_reward_num",ask_reward_num)
            bundle.putString("ask_money_num",ask_money_num)

            intent.putExtras(bundle)
            startActivityForResult(intent,Constant.AskWrite_ChooseLabel)
        }

        askWrite_see_rl!!.setOnClickListener{
            val intent=Intent(this, AskWrite_WhoCanSee::class.java)
            val bundle=Bundle()
            val ask_type=askWrite_type_tv!!.text.toString()
            val ask_label=askWrite_label_tv!!.text.toString()
            val ask_see=askWrite_see_tv!!.text.toString()
            val ask_reward_num=askWrite_money_num1!!.text.toString()
            val ask_money_num=askWrite_money_num2!!.text.toString()

            bundle.putString("ask_type",ask_type)
            bundle.putString("ask_label",ask_label)
            bundle.putString("ask_see",ask_see)
            bundle.putString("ask_reward_num",ask_reward_num)
            bundle.putString("ask_money_num",ask_money_num)

            intent.putExtras(bundle)
            startActivityForResult(intent,Constant.AskWrite_ChooseSee)
        }

    }

    private fun upLoad(context: Context,askWriteBean:AskWriteBean,askWrite_list:ArrayList<String>) {
        val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
        pDialog.titleText = "Loading"
        pDialog.setCancelable(false)
        pDialog.show()

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        if(token.equals("")){
            val intent:Intent=Intent(this@AskWrite,LoginMain::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
        var sign=""
        var params:RequestParams?=null
        if(LogUtils.APP_IS_DEBUG){
            sign=MD5Util.md5(Constant.SALTFORTEST+token)
            params=RequestParams(Constant.BASEURLFORTEST+Constant.SEND_ASK)
        }else{
            sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
            params=RequestParams(Constant.BASEURLFORRELEASE+Constant.SEND_ASK)
        }

        params.isMultipart=true
        params.addHeader("head","send/ask")
        var mapMutable=mutableMapOf<String, SendImage>()
        for(i in askWrite_list.indices){
            val file=File(askWrite_list[i])
            if(file.exists()){
                val length=askWrite_list.size
                val last_prefix=askWrite_list[i].substring(askWrite_list[i].lastIndexOf("."))
                val targetPath= Environment.getExternalStorageDirectory().toString()+"/mew/"+askWrite_list[i].substring(askWrite_list[i].length-20,askWrite_list[i].length).replace("/","")
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"压缩后的图片路径为："+targetPath)

                //ImageTool.storeImage(askWrite_list[i],targetPath)
                ImageFactory.compressAndGenImage(askWrite_list[i],targetPath,512,false)

                val file_new=File(targetPath)
                if(last_prefix.equals(".jpg")){
                    params.addBodyParameter("image"+(i+1),file_new,"image/jpeg")
                }else if(last_prefix.equals(".jpeg")){
                    params.addBodyParameter("image"+(i+1),file_new,"image/jpeg")
                }else if(last_prefix.equals(".png")){
                    params.addBodyParameter("image"+(i+1),file_new,"image/png")
                }else if(last_prefix.equals(".gif")){
                    params.addBodyParameter("image"+(i+1),file_new,"image/gif")
                }else{
                    params.addBodyParameter("image"+(i+1),file_new)
                }
                val newOpts=BitmapFactory.Options()
                newOpts.inJustDecodeBounds=true
                val bitmap=BitmapFactory.decodeFile(targetPath,newOpts)
                newOpts.inJustDecodeBounds=false
                val width=newOpts.outWidth
                val height=newOpts.outHeight
                val image=SendImage()
                image.width=width
                image.height=height
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"得到压缩过后的图片的长度和宽度分别为：\n"+"width="+width+"\nheight="+height)

                mapMutable.put("image"+(i+1),image)

            }
        }
        val gson= Gson()
        val size=gson.toJson(mapMutable)
        LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"将所有图片的长度和宽度获得后，转化为json字符串后的结果为："+size)

        if(mapMutable.size>0){
            params.addBodyParameter("size",size)
        }
        params.addBodyParameter("title",askWriteBean.title)
        params.addBodyParameter("content",askWriteBean.content)
        params.addBodyParameter("reward",askWriteBean.reward)
        params.addBodyParameter("type",askWriteBean.type)
        params.addBodyParameter("range",askWriteBean.range)
        params.addBodyParameter("label",askWriteBean.label)
        params.addBodyParameter("sign",sign)

        LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"上传的参数为："+params.toString())

        org.xutils.x.http().post(params,object: Callback.CommonCallback<JSONObject>{
            override fun onSuccess(result: JSONObject?) {
                val response=result.toString()
                val msg=JsonUtil.get_key_string("msg",response)
                val code=JsonUtil.get_key_string("code",response)
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"服务器返回的code为："+code+"服务器返回的msg为："+msg)
                if(pDialog!=null) pDialog!!.cancel()
                title_right_imageview_btn!!.isEnabled=true
                title_right_image!!.setColorFilter(Color.TRANSPARENT)
                if(code.equals(Constant.RIGHTCODE)){
                    JEventUtils.onCountEvent(context,Constant.Event_SendAsk)
                    /*清除图片*/
                    for(i in askWrite_list.indices){
                        val file=File(askWrite_list[i])
                        if(file.exists()){
                            val last_prefix=askWrite_list[i].substring(askWrite_list[i].lastIndexOf("."))
                            val targetPath=Environment.getExternalStorageDirectory().toString()+"/mew/"+askWrite_list[i].substring(askWrite_list[i].length-12,askWrite_list[i].length).replace("/","")
                            LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"压缩后的图片路径为：正在清除"+targetPath)
                            val file_new=File(targetPath)
                            if(file_new.exists()){
                                file_new.delete()
                                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"压缩后的图片路径为：清除成功 第"+i+"张上传的图片\n"+targetPath)
                            }
                        }
                    }
                    JEventUtils.onCountEvent(this@AskWrite,Constant.Event_SendAsk)//发帖子成功
                    val pSuccess=SweetAlertDialog(this@AskWrite, SweetAlertDialog.SUCCESS_TYPE)
                            pSuccess.setTitleText("提示")
                            pSuccess.setContentText("亲，您已经成功发布帖子！")
                            pSuccess.setConfirmClickListener {
                                var editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                if(askWriteBean.range.equals("college")){
                                    editor.putString("ask_type_choose","college")
                                    editor.commit()
                                }else if(askWriteBean.range.equals("school")){
                                    editor.putString("ask_type_choose","school")
                                    editor.commit()
                                }
                                val intent:Intent=Intent(this@AskWrite, NavigationMainActivity::class.java)
                                intent.putExtra("fragid",2)
                                startActivity(intent)
                                overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
                            }
                            pSuccess.show()
                            val handler= Handler()
                            handler.postDelayed({
                               if(pSuccess!=null){
                                   pSuccess.cancel()
                                   var editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                   if(askWriteBean.range.equals("college")){
                                       editor.putString("ask_type_choose","college")
                                       editor.commit()
                                   }else if(askWriteBean.range.equals("school")){
                                       editor.putString("ask_type_choose","school")
                                       editor.commit()
                                   }
                                   val intent:Intent=Intent(this@AskWrite, NavigationMainActivity::class.java)
                                   intent.putExtra("fragid",2)
                                   startActivity(intent)
                                   overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
                               }
                            },1236)


                }else{
                    /*清除图片*/
                    for(i in askWrite_list.indices){
                        val file=File(askWrite_list[i])
                        if(file.exists()){
                            val last_prefix=askWrite_list[i].substring(askWrite_list[i].lastIndexOf("."))
                            val targetPath=Environment.getExternalStorageDirectory().toString()+"/mew/"+askWrite_list[i].substring(askWrite_list[i].length-12,askWrite_list[i].length).replace("/","")
                            LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"压缩后的图片路径为：正在清除"+targetPath)
                            val file_new=File(targetPath)
                            if(file_new.exists()){
                                file_new.delete()
                                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"压缩后的图片路径为：清除成功 第"+i+"张上传的图片\n"+targetPath)
                            }
                        }
                    }
                    val pFail=SweetAlertDialog(this@AskWrite, SweetAlertDialog.ERROR_TYPE)
                        pFail.setTitleText("发帖失败")
                        pFail.setContentText("亲，"+msg)
                        pFail.show()
                    val handler=Handler()
                    handler.postDelayed({
                        if(pFail!=null) pFail.cancel()
                    },1236)
                }
            }

            override fun onError(ex: Throwable?, isOnCallback: Boolean) {
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,ex.toString())
                if(pDialog!=null) pDialog.cancel()
                title_right_imageview_btn!!.isEnabled=true
                title_right_image!!.setColorFilter(Color.TRANSPARENT)
                val pFail2=SweetAlertDialog(this@AskWrite, SweetAlertDialog.ERROR_TYPE)
                        pFail2.setTitleText("发帖失败")
                        pFail2.setContentText("亲，发布帖子失败！")
                        pFail2.show()
                val handler=Handler()
                handler.postDelayed({
                    if(pFail2!=null) pFail2.cancel()
                },1236)
            }

            override fun onCancelled(cex: Callback.CancelledException?) {
                if(pDialog!=null) pDialog.cancel()
                title_right_imageview_btn!!.isEnabled=true
                title_right_image!!.setColorFilter(Color.TRANSPARENT)
                val pCancel=SweetAlertDialog(this@AskWrite, SweetAlertDialog.SUCCESS_TYPE)
                        pCancel.setTitleText("提示")
                        pCancel.setContentText("亲，您已经取消发布帖子！")
                        pCancel.show()
                val handler=Handler()
                handler.postDelayed({
                    if(pCancel!=null) pCancel.cancel()
                },1236)
            }

            override fun onFinished() {
                title_right_imageview_btn!!.isEnabled=true
                title_right_image!!.setColorFilter(Color.TRANSPARENT)
            }

        })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) {
            LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"选择的图片为空！！！")
            return
        }
        if (requestCode == REQUEST_IMAGE) {
            if(resultCode== Activity.RESULT_OK){

                mSelect_list = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT)
                if (askWrite_list!!.size == IMG_COUNT) {
                    removeItem()
                    refreshAdapter()
                    return
                }

                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"从相册中拿到的数据为：$mSelect_list \n 现在 askWrite_list中的数据为：$askWrite_list")

                if(mSelect_list!!.size>0){

                    for(i in mSelect_list!!.indices){
                        val file=File(mSelect_list!![i])
                        if(file.exists()&&askWrite_list!!.size<IMG_COUNT){

                            removeItem()
                            askWrite_list!!.add(mSelect_list!![i])
                            askWrite_list!!.add(IMG_ADD_TAG)
                            refreshAdapter()

                        }
                    }
                    mSelect_list!!.clear()
                }
            }
        }

       /* if (data == null) {
            LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,"选择的图片为空！！！")
            return
        }
        if (requestCode == 0) {
            val uri = data.data
            val path = ImageTool.getImageAbsolutePath(this, uri)

            if (askWrite_list!!.size === IMG_COUNT) {
                removeItem()
                refreshAdapter()
                return
            }
            removeItem()
            askWrite_list!!.add(path)
            askWrite_list!!.add(IMG_ADD_TAG)
            refreshAdapter()
        }
*/

        if(requestCode==Constant.AskWrite_ChooseType){
            val bundle_from_ask_write=data!!.extras
            if(bundle_from_ask_write!!.getString("ask_type")!=null){
                askWrite_type_tv!!.text=bundle_from_ask_write!!.getString("ask_type")
            }
            if(bundle_from_ask_write!!.getString("ask_label")!=null){
                askWrite_label_tv!!.text=bundle_from_ask_write!!.getString("ask_label")
            }
            if(bundle_from_ask_write!!.getString("ask_see")!=null){
                askWrite_see_tv!!.text=bundle_from_ask_write!!.getString("ask_see")
            }
            if(bundle_from_ask_write!!.getString("ask_reward_num")!=null){
                askWrite_money_num1!!.setText(bundle_from_ask_write!!.getString("ask_reward_num"))
            }
            if(bundle_from_ask_write!!.getString("ask_money_num")!=null){
                askWrite_money_num2!!.text=bundle_from_ask_write!!.getString("ask_money_num")
            }
        }
        if(requestCode==Constant.AskWrite_ChooseLabel){
            val bundle_from_ask_write=data!!.extras
            if(bundle_from_ask_write!!.getString("ask_type")!=null){
                askWrite_type_tv!!.text=bundle_from_ask_write!!.getString("ask_type")
            }
            if(bundle_from_ask_write!!.getString("ask_label")!=null){
                askWrite_label_tv!!.text=bundle_from_ask_write!!.getString("ask_label")
            }
            if(bundle_from_ask_write!!.getString("ask_see")!=null){
                askWrite_see_tv!!.text=bundle_from_ask_write!!.getString("ask_see")
            }
            if(bundle_from_ask_write!!.getString("ask_reward_num")!=null){
                askWrite_money_num1!!.setText(bundle_from_ask_write!!.getString("ask_reward_num"))
            }
            if(bundle_from_ask_write!!.getString("ask_money_num")!=null){
                askWrite_money_num2!!.text=bundle_from_ask_write!!.getString("ask_money_num")
            }
        }
        if(requestCode==Constant.AskWrite_ChooseSee){
            val bundle_from_ask_write=data!!.extras
            if(bundle_from_ask_write!!.getString("ask_type")!=null){
                askWrite_type_tv!!.text=bundle_from_ask_write!!.getString("ask_type")
            }
            if(bundle_from_ask_write!!.getString("ask_label")!=null){
                askWrite_label_tv!!.text=bundle_from_ask_write!!.getString("ask_label")
            }
            if(bundle_from_ask_write!!.getString("ask_see")!=null){
                askWrite_see_tv!!.text=bundle_from_ask_write!!.getString("ask_see")
            }
            if(bundle_from_ask_write!!.getString("ask_reward_num")!=null){
                askWrite_money_num1!!.setText(bundle_from_ask_write!!.getString("ask_reward_num"))
            }
            if(bundle_from_ask_write!!.getString("ask_money_num")!=null){
                askWrite_money_num2!!.text=bundle_from_ask_write!!.getString("ask_money_num")
            }
        }
        
        
    }

    private fun removeItem() {
        if (askWrite_list!!.size!== IMG_COUNT) {
            if (askWrite_list!!.size !== 0) {
                askWrite_list!!.removeAt(askWrite_list!!.size- 1)
            }
        }
    }

    private fun refreshAdapter() {
        if (askWrite_list == null) {
            askWrite_list = ArrayList()
        }
        if (askWrite_list == null) {
            askWrite_adapter = GVAdapter()
        }
        askWrite_adapter!!.notifyDataSetChanged()
    }

    private inner class GVAdapter : BaseAdapter() {

        override fun getCount(): Int {
            return askWrite_list!!.size
        }

        override fun getItem(position: Int): Any? {
            return null
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            val holder: ViewHolder
            if (convertView == null) {
                convertView = LayoutInflater.from(application).inflate(R.layout.activity_add_photo_gv_items, parent, false)
                holder = ViewHolder()
                holder.imageView = convertView!!.findViewById(R.id.main_gridView_item_photo) as ImageView
                holder.checkBox = convertView.findViewById(R.id.main_gridView_item_cb) as CheckBox
                convertView.tag = holder
            } else {
                holder = convertView.tag as ViewHolder
            }
            val s = askWrite_list!!.get(position)
            if (s != IMG_ADD_TAG) {
                holder.checkBox!!.visibility = View.VISIBLE
                holder.imageView!!.setImageBitmap(ImageTool.createImageThumbnail(s))

            } else if(askWrite_list!!.size<=IMG_COUNT){
                holder.checkBox!!.visibility = View.GONE
                holder.imageView!!.setImageResource(R.mipmap.ask_write_add_photo)
            }
            holder.checkBox!!.setOnClickListener {
                askWrite_list!!.removeAt(position)
                refreshAdapter()
            }
            return convertView
        }

        private inner class ViewHolder {
            internal var imageView: ImageView? = null
            internal var checkBox: CheckBox? = null
        }

    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@AskWrite.currentFocus != null) {
                if (this@AskWrite.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@AskWrite.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            SweetAlertDialog(this@AskWrite, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("提示")
                    .setContentText("您确定要取消发帖？")
                    .setCustomImage(R.mipmap.mew_icon)
                    .setConfirmText("我确定")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                        this.finish()
                        overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
                    }
                    .setCancelText("再想想")
                    .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                    .show()
        }
        return super.onKeyDown(keyCode, event)
    }

    internal inner class EditChangedListener : TextWatcher {
        private var temp: CharSequence = ""//监听前的文本
        private val editStart: Int = 0//光标开始位置
        private val editEnd: Int = 0//光标结束位置
        private val charMaxNum = Constant.EDIT_MAX_WORDS

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            temp = s
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if(temp.length<charMaxNum){
                askWrite_num!!.text=temp.length.toString()+"/"+charMaxNum
            }
        }

        override fun afterTextChanged(s: Editable) {
            if(temp.length>charMaxNum){
                Toasty.info(this@AskWrite,"您输入的字数已经超过了限制").show()
            }
        }
    }

    internal inner class EditChangedListener2reward : TextWatcher {
        private var temp: CharSequence = ""//监听前的文本
        private val editStart: Int = 0//光标开始位置
        private val editEnd: Int = 0//光标结束位置
        private val charMaxNum = Constant.EDIT_MAX_WORDS

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            temp = s
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if(!temp.toString().equals("")){
                var reward_num=temp.toString().toInt()
                rule_for_ask_reward_num=reward_num
            }else{
                rule_for_ask_reward_num=0
            }
        }

        override fun afterTextChanged(s: Editable) {
            rule_for_ask_money_all=rule_for_ask_type_num+rule_for_ask_see_num+rule_for_ask_reward_num
            askWrite_money_num2!!.text=rule_for_ask_money_all.toString()
        }
    }

}
