package com.guangdamiao.www.mew_android_debug.navigation.me.me_setting

import android.app.Activity
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jpush.android.api.BasicPushNotificationBuilder
import cn.jpush.android.api.JPushInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.nostra13.universalimageloader.core.ImageLoader


class Me_Setting : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var modify_password:TextView?=null
    private var setting_modify_password_rl:RelativeLayout?=null
    private var setting_push:TextView?=null
    private var setting_voice:TextView?=null
    private var setting_vibrate:TextView?=null
    private var clear_cache:TextView?=null
    private var setting_clear_cache_size:TextView?=null
    private var check_update:TextView?=null
    private var login_out: Button?=null
    private var setting_voice_choose:ImageView?=null
    private var setting_vibrate_choose:ImageView?=null
    private var flag_voice=0
    private var flag_vibrate=0
    private var user_version=""//=====用户当前版本
    private var TAG=Constant.Me_TAG
    private var resultFromServer2=""
    private var token=""

    private var voice_bool=true
    private var vibrate_bool=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_setting)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        user_version= Common.getVersion(this)
        initView()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Me_Setting,"我-设置")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Me_Setting,"我-设置")
        super.onPause()
    }

    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        modify_password=findViewById(R.id.setting_modify_password) as TextView
        setting_modify_password_rl=findViewById(R.id.setting_modify_password_rl) as RelativeLayout
        setting_push=findViewById(R.id.setting_push) as TextView
        setting_voice=findViewById(R.id.setting_voice) as TextView
        setting_vibrate=findViewById(R.id.setting_vibrate) as TextView
        setting_voice_choose=findViewById(R.id.setting_voice_choose) as ImageView
        setting_vibrate_choose=findViewById(R.id.setting_vibrate_choose) as ImageView
        clear_cache=findViewById(R.id.setting_clear_cache) as TextView
        setting_clear_cache_size=findViewById(R.id.setting_clear_cache_size) as TextView
        check_update=findViewById(R.id.setting_check_update) as TextView
        login_out=findViewById(R.id.setting_loginout) as Button

        title_center_textview!!.text="设置"
        title_center_textview!!.visibility= View.VISIBLE

        setting_clear_cache_size!!.text=DataCleanManager2.getTotalCacheSize(this@Me_Setting)
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val loginType=read.getString("loginType","")
        voice_bool= read.getBoolean("voice_bool",true)
        vibrate_bool=read.getBoolean("vibrate_bool",true)

        if(loginType.equals("QQ")){
            setting_modify_password_rl!!.visibility=View.GONE
        }else if(loginType.equals("phone")){
            setting_modify_password_rl!!.visibility=View.VISIBLE
        }else{
            setting_modify_password_rl!!.visibility=View.GONE
        }

        if(voice_bool){
            flag_voice=0
            setting_voice_choose!!.visibility=View.VISIBLE
        }else{
            flag_voice=1
            setting_voice_choose!!.visibility=View.GONE
        }
        if(vibrate_bool){
            flag_vibrate=0
            setting_vibrate_choose!!.visibility=View.VISIBLE

        }else{
            flag_vibrate=1
            setting_vibrate_choose!!.visibility=View.GONE
        }

    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        modify_password!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val id = read.getString("id", "")
            val intent: Intent = Intent(this, Me_Modify_Password::class.java)
            if(!id.equals("")){
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Setting,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }

        }
        setting_push!!.setOnClickListener{
            val intent: Intent = Intent(this, Me_Push_Setting::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
        }
        setting_voice!!.setOnClickListener{
            if(flag_voice++%2==1){
                voice_bool=true
                setting_voice_choose!!.visibility=View.VISIBLE
            }else{
                voice_bool=false
                setting_voice_choose!!.visibility=View.GONE
            }
        }
        setting_vibrate!!.setOnClickListener{
            if(flag_vibrate++%2==1){
                vibrate_bool=true
                setting_vibrate_choose!!.visibility=View.VISIBLE
            }else{
                vibrate_bool=false
                setting_vibrate_choose!!.visibility=View.GONE
            }
        }
        clear_cache!!.setOnClickListener{
            val success=SweetAlertDialog(this@Me_Setting, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    success.setTitleText("提示")
                    success.setContentText("您确定要清理缓存？")
                    success.setCustomImage(R.mipmap.mew_icon)
                    success.setConfirmText("我确定")
                    success.setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                        ImageLoader.getInstance().clearMemoryCache()
                        ImageLoader.getInstance().clearDiscCache()
                        DataCleanManager2.clearAllCache(this@Me_Setting)
                        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("缓存清理")
                                .setContentText("清理成功！")
                                .show()
                        setting_clear_cache_size!!.text=DataCleanManager2.getTotalCacheSize(this@Me_Setting)
                    }
                    success.setCancelText("再想想")
                    success.setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                    success.show()

        }
        check_update!!.setOnClickListener{

            MeSettingRequest.CheckNewVersion2(this@Me_Setting,Constant.Me_TAG)//检查版本
        }
        login_out!!.setOnClickListener{
            //清理登录保存的信息
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            token = read.getString("token", "")
            if(NetUtil.checkNetWork(this)){
                loginout_tell_server()
            }
            val account=read.getString("account","")
            val password=read.getString("password","")
            val editor: SharedPreferences.Editor=this@Me_Setting.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
            editor.clear()
            editor.commit()
            val editor2: SharedPreferences.Editor=this@Me_Setting.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
            editor2.putString("account",account)
            editor2.putString("password",password)
            editor2.commit()
            finish()
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        setStyleBasic(vibrate_bool,voice_bool)
        super.onDestroy()
    }

    /*fun check_new_version_from_server(){
        val sign:String
        val urlPath:String
        if(LogUtils.APP_IS_DEBUG){
            sign= MD5Util.md5(Constant.SALTFORTEST)
            urlPath= Constant.BASEURLFORTEST + Constant.Check_Version
        }else{
            sign= MD5Util.md5(Constant.SALTFORRELEASE)
            urlPath= Constant.BASEURLFORRELEASE + Constant.Check_Version
        }
        val params= mapOf<String,Any?>("sign" to sign)
        val encode= Constant.ENCODE
        LogUtils.d_debugprint(TAG,"向服务器请求的数据："+urlPath+"\n"+params+"\n"+encode)
        Thread(Runnable {
            resultFromServer= HttpUtil.httpPost(urlPath,params,encode)
            LogUtils.d_debugprint(TAG,resultFromServer)
            var getCode:String=""
            var result:String=""
            var curVersion:String=""
            var minVersion:String=""
            getCode= JsonUtil.get_key_string(Constant.Server_Code,resultFromServer!!)
            result= JsonUtil.get_key_string(Constant.Server_Result,resultFromServer!!)
            curVersion= JsonUtil.get_key_string("curVersion",result!!)
            minVersion=JsonUtil.get_key_string("minVersion",result!!)
            LogUtils.d_debugprint(TAG,"系统最新版本:"+curVersion+"\n最小兼容版本："+minVersion)

            if(Constant.RIGHTCODE.equals(getCode)) {
                handler_result1.post {
                 SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                     .setTitleText("当前版本："+Common.getVersion(this))
                     .setContentText("系统最新版本："+curVersion+"\n最新兼容版本："+minVersion)
                     .show()
                }
            }
            else {
                handler_result1.post {
                    Toasty.warning(this@Me_Setting, Constant.SERVERBROKEN).show()
                }
            }
        }).start()    
    }*/

    fun loginout_tell_server(){
        val sign:String
        val urlPath:String
        if(LogUtils.APP_IS_DEBUG){
            sign= MD5Util.md5(Constant.SALTFORTEST+token)
            urlPath= Constant.BASEURLFORTEST + Constant.LogOut
        }else{
            sign= MD5Util.md5(Constant.SALTFORRELEASE+token)
            urlPath= Constant.BASEURLFORRELEASE + Constant.LogOut
        }
        val params= mapOf<String,Any?>("sign" to sign)
        val encode= Constant.ENCODE
        LogUtils.d_debugprint(TAG,"向服务器请求的数据："+urlPath+"\n"+params+"\n"+encode)
        Thread(Runnable {
            resultFromServer2= HttpUtil.httpPost(urlPath,params,encode)
            LogUtils.d_debugprint(TAG,resultFromServer2)
            var getCode:String=""
            getCode= JsonUtil.get_key_string(Constant.Server_Code,resultFromServer2!!)
            if(Constant.RIGHTCODE.equals(getCode)) {
                LogUtils.d_debugprint(TAG,"成功退出！")
            }
            else {
            }
        }).start()
    }

    fun setStyleBasic(isOpenVibrate:Boolean,isOpenSound:Boolean) {
        val builder = BasicPushNotificationBuilder(this@Me_Setting)
        builder.statusBarDrawable = R.mipmap.mew_icon
        builder.notificationFlags = Notification.FLAG_AUTO_CANCEL  //设置为点击后自动消失
        if (isOpenVibrate && !isOpenSound) {
            builder.notificationDefaults = Notification.DEFAULT_VIBRATE  //设置为铃声（ Notification.DEFAULT_SOUND）或者震动（ Notification.DEFAULT_VIBRATE）
        } else if (isOpenSound && !isOpenVibrate) {
            builder.notificationDefaults = Notification.DEFAULT_SOUND
        } else if (isOpenSound && isOpenVibrate) {
            builder.notificationDefaults = Notification.DEFAULT_ALL
        } else {
            builder.notificationDefaults = Notification.DEFAULT_LIGHTS
        }
        JPushInterface.setDefaultPushNotificationBuilder(builder)

        val editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
        editor.putBoolean("voice_bool",voice_bool)
        editor.putBoolean("vibrate_bool",vibrate_bool)
        editor.commit()

        LogUtils.d_debugprint(Constant.Me_TAG, "极光声音和振动设置成功！！！\n声音为：" + isOpenSound + "\n振动为：" + isOpenVibrate)
    }


}
