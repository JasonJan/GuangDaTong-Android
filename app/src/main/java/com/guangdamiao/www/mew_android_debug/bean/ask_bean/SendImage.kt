package com.guangdamiao.www.mew_android_debug.bean.ask_bean

/**
 * Created by Jason_Jan on 2017/12/15.
 */

class SendImage {

    var width=0
    var height=0

    constructor():super()
    constructor(width:Int,height:Int){
        this.width=width
        this.height=height
    }

    override fun toString(): String {
        return "[width=,$width,height=,$height]"
    }
}

