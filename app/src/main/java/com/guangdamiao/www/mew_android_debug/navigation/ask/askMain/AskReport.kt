package com.guangdamiao.www.mew_android_debug.navigation.ask.askMain

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import es.dmoral.toasty.Toasty

class AskReport : BaseActivity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_imageview: ImageView?=null

    private var ask_report_edit:EditText?=null
    private var ask_report_num:TextView?=null
    private var ask_report_remin_num= Constant.EDIT_MAX_WORDS
    private var getBundle:Bundle?=null

    companion object {
        var activity: Activity?=null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ask_report)
        MainApplication.getInstance().addActivity(this)
        getBundle=intent.extras
        activity =this
        initView()
        addSomeListener()
    }

    override fun onStart() {
        super.onStart()
        JAnalyticsInterface.onPageStart(this@AskReport,"举报帖子")
    }

    override fun onDestroy() {
        super.onDestroy()
        JAnalyticsInterface.onPageEnd(this@AskReport,"举报帖子")
    }

    fun initView() {
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview!!.visibility=View.GONE
        title_center_textview!!.text = "举报TA"
        title_right_textview!!.text = "完成"
        title_center_textview!!.visibility = View.VISIBLE
        title_right_textview!!.visibility = View.VISIBLE
        ask_report_edit=findViewById(R.id.ask_report_word_et) as EditText
        ask_report_num=findViewById(R.id.ask_report_word_num_tv) as TextView

    }

    fun addSomeListener() {
        title_left_imageview_btn!!.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        val editChangedListener=EditChangedListener()
        ask_report_edit!!.addTextChangedListener(editChangedListener)

        //点击右侧的完成，请求服务器
        title_right_textview!!.setOnClickListener{
            var report_edit=ask_report_edit!!.text.toString()
            if(report_edit.equals("")){
                Toasty.info(this@AskReport,"亲，您还没有输入举报的内容喔").show()
            }else if(report_edit.length>ask_report_remin_num){
               // Toasty.info(this@AskReport,"您输入的字数已经超过了限制").show()
            }else{
                if(null!=getBundle){
                   val id=getBundle!!.getString("id")
                   val reason=report_edit
                   val token=getBundle!!.getString("token")

                    val pDialog = SweetAlertDialog(this@AskReport, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Loading");
                    pDialog.setCancelable(true)
                    pDialog.show()
                    AskMainRequest.makeInformFromServer(this@AskReport,Constant.Ask_TAG,Constant.Inform_Ask,id,token,reason,pDialog)
                }else{
                    Toasty.info(this@AskReport,Constant.SERVERBROKEN).show()
                }
            }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    internal inner class EditChangedListener : TextWatcher {
        private var temp: CharSequence = ""//监听前的文本
        private val editStart: Int = 0//光标开始位置
        private val editEnd: Int = 0//光标结束位置
        private val charMaxNum = ask_report_remin_num

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            temp = s
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if(temp.length<charMaxNum){
                ask_report_num!!.text=temp.length.toString()+"/"+ask_report_remin_num
            }
        }

        override fun afterTextChanged(s: Editable) {
            if(temp.length>charMaxNum){
                Toasty.info(this@AskReport,"您输入的字数已经超过了限制").show()
            }
        }
    }
}
