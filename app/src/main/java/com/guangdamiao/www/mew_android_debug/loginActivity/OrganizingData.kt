package com.guangdamiao.www.mew_android_debug.loginActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject


class  OrganizingData : Activity() {

    private var TAG:String=Constant.OrganizingData_Tag
    private var back: ImageView? = null
    private var colleges_txt: TextView? = null
    private var but1: Button? = null
    private var password1: EditText? = null
    private var password2: EditText? = null
    private var academyPosition: Int = 0
    private var resultFromServerRegister:String?=null
    private var bundle: Bundle?=null
    private var college_:String?=null
    private var phone_:String?=null
    private var result_:String?=null
    private var bundle_from_choose:Bundle?=null//选择学院中传过来的数据



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        LoginCacheActivity.addActivity(this)
        fullscreen()
        bundle = intent.extras
        if(bundle!=null){
            result_=bundle!!.getString("result","")
        }
        setContentView(R.layout.activity_organizing_data)
        initView()
        addSomeListener()
    }

    fun addSomeListener(){
        colleges_txt!!.setOnClickListener{
            if(!password1!!.text.toString().equals("")&&password1!!.text.toString()!=null){
                var str:String=password1!!.text.toString()
                bundle!!.putString("password1",str)
            }

            if(!password2!!.text.toString().equals("")&&password2!!.text.toString()!=null){
                var str:String=password2!!.text.toString()
                bundle!!.putString("password2",str)
            }
            val bundle2:Bundle= Bundle()
            bundle2.putString("password1", bundle!!.getString("password1"))
            bundle2.putString("password2",bundle!!.getString("password2"))
            bundle2.putString("phone",bundle!!.getString("phone"))
            bundle2.putString("result",bundle!!.getString("result"))
            if(colleges_txt!!.text!=null){
                if(!colleges_txt!!.text.toString().equals("请选择")&&!colleges_txt!!.text.toString().equals(""))
                   bundle2.putString("college_old",colleges_txt!!.text.toString())
            }
            val intent1:Intent=Intent(this,ChooseAcademy::class.java)
            intent1.putExtras(bundle2!!)
            startActivityForResult(intent1,0x717)
            overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
        }
        back!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        but1!!.setOnClickListener{
            var same: Boolean? = null
            same = samePassword()
            if (same) {
                if (academyPosition == 0) {
                    Toasty.warning(this,Constant.OrganizingData_NoAcademy).show()
                } else {
                    var str:String=password1!!.text.toString()
                    var str1:String=colleges_txt!!.text.toString()
                    bundle_from_choose!!.putString("password",str)
                    bundle_from_choose!!.putString("college", str1)

                    val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                    pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                    pDialog.titleText = "Loading"
                    pDialog.setCancelable(true)
                    pDialog.show()
                    RegisterOverFromServer(pDialog)
                }
            }
        }
    }

    fun initView(){
        colleges_txt = findViewById(R.id.organizing_data_colleges) as TextView
        back = findViewById(R.id.organizing_data_back) as ImageView
        but1 = findViewById(R.id.organizing_data_but1) as Button
        password1 = findViewById(R.id.organizing_data_edt1) as EditText
        password2 = findViewById(R.id.organizing_data_edt2) as EditText
    }


    fun samePassword():Boolean{
        if(!TextUtils.isEmpty(password1!!.text.toString().trim{ it<=' '})){
            if(password1!!.text.toString().trim{it<=' '}.length<6){
                Toasty.warning(this,"亲，密码不能低于6位哦~").show()
            }else{
                var p2:String=""
                var p1:String=""
                p1=password1!!.text.toString().trim{it<=' '}
                p2=password2!!.text.toString().trim{it<=' '}
                if(p2!=null&&p1.equals(p2)){
                    return true
                }else{
                    Toasty.warning(this,"两次输入的密码不匹配哦~").show()
                    return false
                }
            }
        }else{
            Toasty.warning(this,"请输入您的密码").show()
            password1!!.requestFocus()//设置输入焦点
            return false
        }
        return false
    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@OrganizingData.currentFocus != null) {
                if (this@OrganizingData.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@OrganizingData.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //返回标识
        if(resultCode==0x717){
            bundle_from_choose=data!!.extras
            password1!!.setText(bundle_from_choose!!.getString("password1"))
            password2!!.setText(bundle_from_choose!!.getString("password2"))
            college_=bundle_from_choose!!.getString("college")
            phone_=bundle_from_choose!!.getString("phone")
            result_=bundle_from_choose!!.getString("result")
            colleges_txt!!.setText(college_)
            academyPosition=bundle_from_choose!!.getInt("academyPosition")+1

        }
    }

    fun RegisterOverFromServer(pDialog: SweetAlertDialog){
        if(NetUtil.checkNetWork(this)){
            var result: String? = ""
            var phone: String? = ""
            var password: String? = ""
            var college: String? = ""
            var sign: String? = ""
            var verifyToken: String? =""
            var school: String = Constant.SCHOOLDEFULT
            val salt:String
            val urlPath:String
            if(LogUtils.APP_IS_DEBUG){
                salt = Constant.SALTFORTEST
                urlPath = Constant.BASEURLFORTEST + Constant.OrganizingData_Register
            }else{
                salt = Constant.SALTFORRELEASE
                urlPath = Constant.BASEURLFORRELEASE + Constant.OrganizingData_Register
            }
            result = result_
            phone = phone_
            password = password1!!.text.toString()
            college = college_
            verifyToken = JsonUtil.get_key_string("verifyToken", result!!)
            sign = MD5Util.md5(salt + verifyToken)
            bundle_from_choose!!.putString("sign",sign)

            val jsonObject=JSONObject()
            jsonObject.put("account",phone)
            jsonObject.put("password",password)
            jsonObject.put("college",college)
            jsonObject.put("school",school)
            jsonObject.put("sign",sign)
            val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
            LogUtils.d_debugprint(Constant.LoginMain_Tag,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())
            val client=AsyncHttpClient(true,80,443)
            client.post(this@OrganizingData,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if(responseBody!=null){
                        if(pDialog!=null) pDialog.cancel()
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response=JSONObject(resultDate)
                        resultFromServerRegister=response.toString()

                        LogUtils.d_debugprint(TAG,resultFromServerRegister!!)
                        var getCode: String = ""
                        getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServerRegister!!)
                        if (Constant.RIGHTCODE.equals(getCode)) {
                            var msg: String = ""
                            var result: String = ""
                            var account: String = ""
                            var password: String = ""
                            var college: String = ""
                            var school: String = ""
                            var sign: String = ""
                            msg = JsonUtil.get_key_string(Constant.Server_Msg, resultFromServerRegister!!)
                            result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServerRegister!!)
                            account = bundle_from_choose!!.getString("phone")
                            password = bundle_from_choose!!.getString("password")
                            college = bundle_from_choose!!.getString("college")
                            school = Constant.SCHOOLDEFULT
                            sign = bundle_from_choose!!.getString("sign")

                            //用bundle_from_choose记录服务器返回数据
                            var dataFromServer: Bundle = Bundle()
                            dataFromServer.putString("msg", msg)
                            dataFromServer.putString("result", result)
                            dataFromServer.putString("account", account)
                            dataFromServer.putString("password", password)
                            dataFromServer.putString("college", college)
                            dataFromServer.putString("school", school)
                            dataFromServer.putString("sign", sign)
                            //添加到全局文件中，方便所有页面的调用
                            val editor:SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit();
                            editor.putString("account",account)
                            editor.putString("password",password)
                            editor.putString("college",college)
                            editor.putString("school",school)
                            editor.putString("sign",sign)
                            editor.commit()

                            SweetAlertDialog(this@OrganizingData, SweetAlertDialog.SUCCESS_TYPE).setTitleText("亲~").setContentText("注册成功")
                                    .setConfirmClickListener(object : SweetAlertDialog.OnSweetClickListener {
                                        override fun onClick(sDialog: SweetAlertDialog) {
                                            sDialog.dismissWithAnimation()
                                            overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
                                            //LoginCacheActivity.finishActivity()
                                            /*直接跳登录页面*/
                                            val intent = Intent(this@OrganizingData, LoginMain::class.java)
                                            startActivity(intent)
                                            overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out)
                                        }
                                    }).show()

                        } else {
                            var msg: String = ""
                            msg = JsonUtil.get_key_string(Constant.Server_Msg, resultFromServerRegister!!)
                            SweetAlertDialog(this@OrganizingData, SweetAlertDialog.ERROR_TYPE).setTitleText("提示").setContentText("注册失败！\n$msg").show()
                        }

                     }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                         if(pDialog!=null) pDialog.cancel()
                        Toasty.error(this@OrganizingData,"网络超时，请稍候重试！").show()
                }
            })


        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }
    }
}
