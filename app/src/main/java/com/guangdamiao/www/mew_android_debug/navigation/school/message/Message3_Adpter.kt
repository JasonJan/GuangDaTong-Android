package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message3
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfoRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.BadgeView
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.util.*

class Message3_Adpter(protected var context: Context, message3: ArrayList<Message3>,protected var badge3:BadgeView?) : BaseAdapter() {
    private var message3 : ArrayList<Message3>?=null


    init {
        this.message3 = message3

    }

    override fun getCount(): Int {
        if(message3!!.size==0){
            update3MessageNum(context)
        }
        return message3!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder: Message3_Adpter.ViewHolder

        if (convertView == null) {
            holder = Message3_Adpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.message3_item, parent, false)
            holder.list_message3_icon = convertView.findViewById(R.id.list_message3_icon) as ImageView
            holder.list_message3_ll = convertView.findViewById(R.id.list_message3_ll) as LinearLayout
            holder.list_message3_nick_tv = convertView.findViewById(R.id.list_message3_nick_tv) as TextView
            holder.list_message3_sex_tv = convertView.findViewById(R.id.list_message3_sex_tv) as TextView
            holder.list_message3_time = convertView.findViewById(R.id.list_message3_time) as TextView
            holder.list_message3_words = convertView.findViewById(R.id.list_message3_words_tv) as TextView
            holder.list_message3_leaveWords_tv=convertView.findViewById(R.id.list_message3_leaveWords_tv) as TextView
            holder.list_message3_report1_tv=convertView.findViewById(R.id.list_message3_report1_tv) as TextView
            holder.list_message3_report2_tv=convertView.findViewById(R.id.list_message3_report2_tv) as TextView
            holder.list_message3_report3_tv=convertView.findViewById(R.id.list_message3_report3_tv) as TextView
            holder.message3_unread_msg=convertView.findViewById(R.id.message3_unread_msg) as TextView

            convertView.tag = holder
        } else {
            holder = convertView.tag as Message3_Adpter.ViewHolder
        }

        update3MessageNum(context)
        val listmessage3 = message3!!.get(position)//获取一个
        //蓝色字体+红色字体+下划线
        val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
        val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))

        //头像
        val icon = listmessage3.icon
        ImageLoader.getInstance().displayImage(icon, holder.list_message3_icon)

        //昵称
        holder!!.list_message3_nick_tv!!.text=listmessage3!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.list_message3_nick_tv!!.measure(spec,spec)
        var nickwidth= holder!!.list_message3_nick_tv!!.measuredWidth
        //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nickname为：${listmessage3!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,167.0f)} ")
        var nicklength=listmessage3!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(context,167.0f)){
            nicklength-=2
            val name_display=listmessage3!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.list_message3_nick_tv!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.list_message3_nick_tv!!.measure(spec,spec)
            nickwidth= holder!!.list_message3_nick_tv!!.measuredWidth
           // LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }

        //性别
        if (!listmessage3.gender.equals("") && listmessage3.gender != null) {
            if (listmessage3.gender.equals("女")) {
                holder!!.list_message3_sex_tv!!.text = "♀"
                holder!!.list_message3_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.ask_red))
            } else {
                holder!!.list_message3_sex_tv!!.text = "♂"
                holder!!.list_message3_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.headline))
            }
        } else {
            holder!!.list_message3_sex_tv!!.visibility = View.GONE
        }

        //时间
        val time = listmessage3.createTime!!.substring(2, 16).replace("T", " ")
        holder!!.list_message3_time!!.text = time

        //是否读过，显示红点
        holder!!.message3_unread_msg!!.visibility=View.GONE
        if(listmessage3.isRead.equals("0")){
            holder!!.message3_unread_msg!!.visibility=View.VISIBLE
        }

        //内容
        if(listmessage3.type!!.equals(Constant.Message_ContactAdded)){
            holder.list_message3_words!!.text="我添加你为联系人啦！"
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_ZanUser)){
            holder.list_message3_words!!.text="我赞了你的主页"
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_GiveCoins)){
            val content1="我赠送了你"
            val content2= JsonUtil.get_key_string("coins",listmessage3.detail!!)
            val content3="金币。"
            val builder1= SpannableStringBuilder(content1+content2+content3)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder1.setSpan(redSpan,5,5+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE

            val message=JsonUtil.get_key_string("words",listmessage3.detail!!)
            if(message.equals("")){
                holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            }else{
                val content="留言："
                val words=content+message
                holder.list_message3_leaveWords_tv!!.text=words
                holder.list_message3_leaveWords_tv!!.visibility=View.VISIBLE
            }
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_WantCoins)){
            val content1="我向你索要"
            val content2= JsonUtil.get_key_string("coins",listmessage3.detail!!)
            val content3="金币。"
            val builder1= SpannableStringBuilder(content1+content2+content3)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder1.setSpan(redSpan,5,5+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE

            val message=JsonUtil.get_key_string("words",listmessage3.detail!!)
            if(message.equals("")){
                holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            }else{
                val content="留言："
                val words=content+message
                holder.list_message3_leaveWords_tv!!.text=words
                holder.list_message3_leaveWords_tv!!.visibility=View.VISIBLE
            }
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_AskInformed)){
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.VISIBLE

            val content1="亲，有人举报您的帖子："
            val content2=JsonUtil.get_key_string("title",listmessage3.detail!!)
            val builder1= SpannableStringBuilder(content1+content2)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder1.setSpan(buleSpan,11,11+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder1.setSpan(UnderlineSpan(),11,11+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_report1_tv!!.text=builder1

            val content3="举报理由："
            val content4=JsonUtil.get_key_string("reason",listmessage3.detail!!)
            val builder2= SpannableStringBuilder(content3+content4)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder2.setSpan(redSpan,5,5+content4.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_report2_tv!!.text=builder2
            holder.list_message3_report2_tv!!.visibility=View.VISIBLE

            val content5=Constant.Message_AskInformed_ToUser
            if(!content5.equals("")){
                holder!!.list_message3_report3_tv!!.text=content5
                holder!!.list_message3_report3_tv!!.setTextColor(context.resources.getColor(R.color.ask_red))
                holder!!.list_message3_report3_tv!!.visibility=View.VISIBLE
            }else{
                holder!!.list_message3_report3_tv!!.visibility=View.GONE
            }

        }else if(listmessage3.type!!.equals(Constant.Message_UserInformed)){
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE

            holder!!.list_message3_report1_tv!!.visibility=View.VISIBLE
            holder!!.list_message3_report2_tv!!.visibility=View.VISIBLE


            val content1="亲，有人举报您！"
            holder!!.list_message3_report1_tv!!.text=content1
            val content2="举报理由："
            val content3=JsonUtil.get_key_string("reason",listmessage3.detail!!)
            val builder1= SpannableStringBuilder(content2+content3)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder1.setSpan(redSpan,5,5+content3.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_report2_tv!!.text=builder1
            val content5=Constant.Message_UserInformed_ToUser
            if(!content5.equals("")){
                holder!!.list_message3_report3_tv!!.text=content5
                holder!!.list_message3_report3_tv!!.setTextColor(context.resources.getColor(R.color.ask_red))
                holder!!.list_message3_report3_tv!!.visibility=View.VISIBLE
            }else{
                holder!!.list_message3_report3_tv!!.visibility=View.GONE
            }
        }else if(listmessage3.type!!.equals(Constant.Message_CommentAskInformed)){
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            val content1="亲，您在帖子"
            val content2=JsonUtil.get_key_string("title",listmessage3.detail!!)
            val content3="里发表的评论"
            val content4=JsonUtil.get_key_string("comment",listmessage3.detail!!)
            val content5="被人举报了！"

            val builder1= SpannableStringBuilder(content1+content2+content3+content4+content5)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder1.setSpan(buleSpan,6,6+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder1.setSpan(UnderlineSpan(),6,6+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            val buleSpan2= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder1.setSpan(buleSpan2,6+content2.length+6,6+content2.length+6+content4.length,Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder1.setSpan(UnderlineSpan(),6+content2.length+6,6+content2.length+6+content4.length,Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_report1_tv!!.text=builder1
            holder.list_message3_report1_tv!!.visibility=View.VISIBLE

            val content6="举报理由："
            val content7=JsonUtil.get_key_string("reason",listmessage3.detail!!)
            val builder2= SpannableStringBuilder(content6+content7)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder2.setSpan(redSpan,5,5+content7.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.list_message3_report2_tv!!.text=builder2
            holder!!.list_message3_report2_tv!!.visibility=View.VISIBLE

            val content8=Constant.Message_CommentAskInformed_ToUser
            if(!content8.equals("")){
                holder!!.list_message3_report3_tv!!.text=content8
                holder!!.list_message3_report3_tv!!.setTextColor(context.resources.getColor(R.color.ask_red))
                holder!!.list_message3_report3_tv!!.visibility=View.VISIBLE
            }else{
                holder!!.list_message3_report3_tv!!.visibility=View.GONE
            }
        }else if(listmessage3.type!!.equals(Constant.Message_ReportAdopted)){
            val content1="亲，您的反馈已被系统采纳，感谢您！\n系统赠送您"
            val coins=JsonUtil.get_key_string("coins",listmessage3.detail!!)
            val content2="金币！"
            val builder1= SpannableStringBuilder(content1+coins+content2)
            builder1.setSpan(redSpan,23,23+coins.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_AdviseAdopted)){
            val content1="亲，您的建议已被系统采纳，感谢您！\n系统赠送您"
            val coins=JsonUtil.get_key_string("coins",listmessage3.detail!!)
            val content2="金币！"
            val builder1= SpannableStringBuilder(content1+coins+content2)
            builder1.setSpan(redSpan,23,23+coins.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_AskDeleted)){
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE

            val content1="亲，您发送的帖子："
            val content2=JsonUtil.get_key_string("title",listmessage3.detail!!)
            val content3="\n已被系统删除！"
            holder.list_message3_report1_tv!!.text=content1
            val builder1= SpannableStringBuilder(content2+content3)
            builder1.setSpan(buleSpan,0,content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message3_report2_tv!!.text=builder1

            val content4=Constant.Message_ForbiddenSendAsk_ToUser
            val builder2=SpannableStringBuilder(content4)
            builder2.setSpan(redSpan,0,content4.length,Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message3_report3_tv!!.text=builder2

            holder.list_message3_report1_tv!!.visibility=View.VISIBLE
            holder.list_message3_report2_tv!!.visibility=View.VISIBLE
            holder.list_message3_report3_tv!!.visibility=View.VISIBLE
        }else if(listmessage3.type!!.equals(Constant.Message_CommentAskDeleted)){
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE

            val content1="亲，您的帖子评论："
            val content2=JsonUtil.get_key_string("title",listmessage3.detail!!)
            val content3="\n已被系统删除！"
            holder.list_message3_report1_tv!!.text=content1
            val builder1= SpannableStringBuilder(content2+content3)
            builder1.setSpan(buleSpan,0,content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message3_report2_tv!!.text=builder1

            val content4=Constant.Message_ForbiddenSendAskComment_ToUser
            val builder2=SpannableStringBuilder(content4)
            builder2.setSpan(redSpan,0,content4.length,Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message3_report3_tv!!.text=builder2

            holder.list_message3_report1_tv!!.visibility=View.VISIBLE
            holder.list_message3_report2_tv!!.visibility=View.VISIBLE
            holder.list_message3_report3_tv!!.visibility=View.VISIBLE
        }else if(listmessage3.type!!.equals(Constant.Message_ChatForbidden)){
            val content1="亲，您被人举报多次，并且系统已查明该况情属实！\n"
            val content2=Constant.Message_ForbiddenChat_ToUser
            val builder1= SpannableStringBuilder(content1+content2)
            builder1.setSpan(redSpan,24,24+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_RewardForInform)){
            val content1="亲，您的举报已被系统查实有效，感谢您!\n系统赠送您"
            val coins=JsonUtil.get_key_string("coins",listmessage3.detail!!)
            val content2="金币！"
            val builder1= SpannableStringBuilder(content1+coins+content2)
            builder1.setSpan(redSpan,25,25+coins.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_ReportRejected)){
            holder.list_message3_words!!.text="亲，您的反馈未被系统采纳！\n喵喵衷心地感谢您的热心反馈！"
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_AdviseRejected)){
            holder.list_message3_words!!.text="亲，您的建议未被系统采纳！\n喵喵衷心地感谢您的热心建议！"
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_InformRejected)){
            val content1="亲，您的举报已被系统查实无效！\n"
            val content2=Constant.Message_MuchInformed_ToUser
            val builder1= SpannableStringBuilder(content1+content2)
            builder1.setSpan(redSpan,16,16+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else if(listmessage3.type!!.equals(Constant.Message_InformForbidden)){
            val content1="亲，您恶意举报他人，并且系统已查明该情况属实！\n"
            val content2=Constant.Message_ForbiddenInformed_ToUser
            val builder1= SpannableStringBuilder(content1+content2)
            builder1.setSpan(redSpan,24,24+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder.list_message3_words!!.text=builder1
            holder.list_message3_words!!.visibility=View.VISIBLE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }else{
            holder.list_message3_words!!.visibility=View.GONE
            holder.list_message3_leaveWords_tv!!.visibility=View.GONE
            holder.list_message3_report1_tv!!.visibility=View.GONE
            holder.list_message3_report2_tv!!.visibility=View.GONE
            holder.list_message3_report3_tv!!.visibility=View.GONE
        }

        holder.list_message3_icon!!.setOnClickListener{

            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val userID=read.getString("id","")
            if(!userID.equals(listmessage3.senderID)){
                val intent: Intent = Intent(context, IconInfo::class.java)
                val bundle= Bundle()
                bundle.putString("id",listmessage3.senderID)
                bundle.putString("icon",listmessage3.icon)
                bundle.putString("sex",listmessage3.gender)
                bundle.putString("nickname",listmessage3.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }else{
                val intent:Intent=Intent(context, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listmessage3.senderID)
                bundle.putString("icon",listmessage3.icon)
                bundle.putString("sex",listmessage3.gender)
                bundle.putString("nickname",listmessage3.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        holder.list_message3_ll!!.setOnClickListener{
            update3Message(context,listmessage3.id!!)
            holder.message3_unread_msg!!.visibility=View.GONE
            listmessage3.isRead="1"
            update3MessageNum(context)


            if(listmessage3.type!!.equals(Constant.Message_ZanUser)||listmessage3.type!!.equals(Constant.Message_GiveCoins)
                    ||listmessage3.type!!.equals(Constant.Message_ContactAdded)){
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val userID=read.getString("id","")
                if(!userID.equals(listmessage3.senderID)){
                    val intent: Intent = Intent(context, IconInfo::class.java)
                    val bundle= Bundle()
                    bundle.putString("id",listmessage3.senderID)
                    bundle.putString("icon",listmessage3.icon)
                    bundle.putString("sex",listmessage3.gender)
                    bundle.putString("nickname",listmessage3.nickname)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }else{
                    val intent:Intent=Intent(context, IconSelfInfo::class.java)
                    val bundle=Bundle()
                    bundle.putString("id",listmessage3.senderID)
                    bundle.putString("icon",listmessage3.icon)
                    bundle.putString("sex",listmessage3.gender)
                    bundle.putString("nickname",listmessage3.nickname)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }

            }else if(listmessage3.type!!.equals(Constant.Message_WantCoins)){
                giveTAmoney(listmessage3)
            }else if(listmessage3.type!!.equals(Constant.Message_AskInformed)||listmessage3.type!!.equals(Constant.Message_CommentAskInformed)){
                val intent:Intent=Intent(context, AskDetail::class.java)
                if(NetUtil.checkNetWork(context)){
                    val bundle=Bundle()
                    val askID= JsonUtil.get_key_string("askID",listmessage3.detail!!)
                    bundle.putString("id",askID)
                    if(LogUtils.APP_IS_DEBUG){
                        bundle.putString("link",Constant.BASEURLFORTEST)
                    }else{
                        bundle.putString("link",Constant.BASEURLFORRELEASE)
                    }
                    bundle.putString("publisherID",listmessage3.senderID)
                    bundle.putString("isZan","1")
                    bundle.putInt("position",position)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }else{
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }


        return convertView!!
    }

    class ViewHolder{
        var list_message3_icon: ImageView?=null
        var list_message3_ll: LinearLayout?=null
        var list_message3_nick_tv: TextView?=null
        var list_message3_sex_tv: TextView?=null
        var list_message3_time: TextView?=null
        var list_message3_words: TextView?=null
        var list_message3_leaveWords_tv:TextView?=null
        var list_message3_report1_tv:TextView?=null
        var list_message3_report2_tv:TextView?=null
        var list_message3_report3_tv:TextView?=null
        var message3_unread_msg:TextView?=null

    }

    private fun giveTAmoney(listmessage3:Message3){
        val money= JsonUtil.get_key_string("coins",listmessage3.detail!!)
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        //确认赠送
        val content1="亲，您确定要赠送TA "
        val content2=" 金币吗？"


        SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("确认赠送")
                .setContentText(content1+money+content2)
                .setCustomImage(R.mipmap.mew_icon)
                .setConfirmText("赠送")
                .setConfirmClickListener { sDialog ->
                    sDialog.dismissWithAnimation()
                    val pDialog1 = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog1.setTitleText("Loading")
                    pDialog1.setCancelable(true)
                    pDialog1.show()
                    val amount=money
                    val words=""
                    IconInfoRequest.requestGiveCoins(context,listmessage3.senderID!!,amount,words,token,object: IDataRequestListener2String {
                        override fun loadSuccess(response_string: String?) {
                            if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                pDialog1.cancel()
                                LogUtils.d_debugprint(Constant.Message_TAG,"消息中心中：赠送金币成功！！！\n")
                                val pDialog= SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                pDialog.setTitleText("提示")
                                pDialog.setContentText("亲，您已经成功赠送TA金币")
                                pDialog.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pDialog.cancel()
                                },1236)
                            }else{
                                if(JsonUtil.get_key_string("msg",response_string)!=null){
                                    pDialog1.cancel()
                                    val pDialog= SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                    },1236)
                                }
                            }
                        }
                    })
                }
                .setCancelText("取消")
                .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                .show()
    }

    private fun update3Message(context:Context,s_id:String){
        val sql="update message3 set isRead='1' where s_id=$s_id;"
        val helper3=Message3SQLiteOpenHelper(context)
        val db=helper3!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message3 s_id=$s_id 数据=====设为已读！！！\n\n")
    }

    private fun update3MessageNum(context:Context){
        val unread_old=query3NowAll()

        if (Activity::class.java.isInstance(context)) {

            val activity = context as Activity
            val bt3=activity.findViewById(R.id.bt3) as Button
            if(badge3==null){
                badge3= BadgeView(context,bt3)
            }
            if(unread_old!=0){

                badge3!!.badgePosition= BadgeView.POSITION_TOP_RIGHT
                if(unread_old<99){
                    badge3!!.text=unread_old.toString()
                    badge3!!.visibility=View.VISIBLE
                }else{
                    badge3!!.text="99+"
                    badge3!!.visibility=View.VISIBLE
                }
                badge3!!.textSize=10.0f
                badge3!!.setTextColor(Color.WHITE)
                badge3!!.show()

            }else{
                badge3!!.visibility=View.GONE
            }
        }

    }

    private fun query3NowAll():Int{
        val sql="select * from "+ Message3SQLiteOpenHelper.M3_Name+" where isRead='0'"
        val helper3=Message3SQLiteOpenHelper(context)
        val cursor=helper3!!.readableDatabase.rawQuery(sql,null)
        var message3_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message3_unread++
            }
        }
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message3有多少条未读的：$message3_unread\n\n")
        return message3_unread

    }


}
