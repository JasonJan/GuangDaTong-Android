package com.guangdamiao.www.mew_android_debug.bean.ask_bean


class CommentConfig {
    enum class Type private constructor(private val value: String) {
        PUBLIC("public"), REPLY("reply")
    }


    var askID: String = ""
    var commentPosition=0
    var token=""
    var content: String = ""
    var commentType: Type? = null
    var commentReceiverID: String? = ""
    var hint:String?=""

    override fun toString(): String {
        return "askID = " + askID+"commentPositon="+commentPosition+"; content = " + content+"; commentType ＝ " + commentType+"; commentReceiverID = " + commentReceiverID
    }
}
