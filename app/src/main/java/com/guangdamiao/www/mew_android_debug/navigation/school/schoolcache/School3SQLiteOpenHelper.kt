package com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2037/8/30.
 */

class School3SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "school3.db"
        private val version = 1
        val S3_Name="school3"
        val S3_Id="id"
        val S3_Sid="s_id"
        val S3_Title="title"
        val S3_CreateTime="createTime"
        val S3_Type="type"
        val S3_Link="link"
        val S3_ReadCount="readCount"
        val S3_Zan="zan"
        val S3_IsZan="isZan"
        val S3_IsFavorite="isFavorite"
        val S3_UrlEnd="urlEnd"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table school3(id integer primary key autoincrement," +
                "${School3SQLiteOpenHelper.S3_Sid} varchar(20)," +
                "${School3SQLiteOpenHelper.S3_Title} varchar(20),"+
                "${School3SQLiteOpenHelper.S3_CreateTime} varchar(100)," +
                "${School3SQLiteOpenHelper.S3_Type} varchar(5)," +
                "${School3SQLiteOpenHelper.S3_Link} varchar(100)," +
                "${School3SQLiteOpenHelper.S3_ReadCount} varchar(4)," +
                "${School3SQLiteOpenHelper.S3_Zan} varchar(2)," +
                "${School3SQLiteOpenHelper.S3_IsZan} varchar(2)," +
                "${School3SQLiteOpenHelper.S3_IsFavorite} varchar(2)," +
                "${School3SQLiteOpenHelper.S3_UrlEnd} varchar(50))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
