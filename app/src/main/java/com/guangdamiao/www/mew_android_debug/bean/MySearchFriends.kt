package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MySearchFriends {

    var icon: String? =""
    var id:String?=""
    var nickname:String?=""
    var gender:String?=""
    var college:String?=""
    var school:String?=""

    constructor():super()
    constructor(icon:String,id:String,nickname:String,gender:String,college:String,school:String){
        this.icon=icon
        this.id=id
        this.nickname=nickname
        this.gender=gender
        this.college=college
        this.school=school
    }

    override fun toString(): String {
        return "MySearchFriends[icon=$icon,id=$id，nickname=$nickname,gender=$gender,college=$college,school=$school]"
    }
}