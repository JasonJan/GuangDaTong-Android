package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.BaseAdapter
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyMoney

class MoneyAdapter(context: Context, myMoneyList:ArrayList<MyMoney>) : BaseAdapter() {
    private var mInflater: LayoutInflater?=null//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    var myMoneyList:ArrayList<MyMoney>?=null
    init {
        this.mContext = context
        this.myMoneyList=myMoneyList
        this.mInflater = LayoutInflater.from(mContext)
    }
    override fun getItem(arg0: Int): Any {

        return myMoneyList!![arg0]
    }
    override fun getItemId(position: Int): Long {

        return position.toLong()
    }
    override fun getCount(): Int {

        return myMoneyList!!.size
    }
    override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
        var convertView = convertView
        var holder: MoneyAdapter.ViewHolder

        if (convertView == null) {
            holder= MoneyAdapter.ViewHolder()
            convertView = mInflater!!.inflate(R.layout.mine_item_money, parent,false)
            holder.amount=convertView!!.findViewById(R.id.mine_money_changNum) as TextView
            holder.description=convertView!!.findViewById(R.id.mine_money_reason) as TextView
            holder.createTime=convertView!!.findViewById(R.id.mine_money_time) as TextView

            convertView.tag=holder
        }else{
            holder=convertView.tag as MoneyAdapter.ViewHolder
        }

        val bean=myMoneyList!!.get(position)

        holder.amount = convertView!!.findViewById(R.id.mine_money_changNum) as TextView
        holder.description=convertView!!.findViewById(R.id.mine_money_reason) as TextView
        holder.createTime=convertView!!.findViewById(R.id.mine_money_time) as TextView

        holder.amount!!.text = bean.amount
        bean.amount=bean.amount!!.trim()
        //LogUtils.d_debugprint(Constant.Me_TAG,"此刻bean第一个字符是：${bean.amount!![0]}")
        if( bean.amount!![0].equals('+')){
            holder.amount!!.setTextColor(mContext!!.resources.getColor(R.color.ask_green))
        }else if(bean.amount!![0].equals('-')){
            holder.amount!!.setTextColor(mContext!!.resources.getColor(R.color.ask_red))
        }
        holder.description!!.text=bean.description
        val time = bean.createTime!!.substring(2, 16).replace("T", " ")
        holder.createTime!!.text=time

        return convertView
    }

    class ViewHolder{
        var amount:TextView?=null
        var description:TextView?=null
        var createTime:TextView?=null
    }
}
