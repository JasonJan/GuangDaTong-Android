package com.guangdamiao.www.mew_android_debug.navigation

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTabHost
import android.support.v4.content.ContextCompat
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jpush.android.api.JPushInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyLocation
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.me.me_setting.MeSettingRequest
import com.guangdamiao.www.mew_android_debug.utils.LocationUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.guangdamiao.www.mew_android_debug.utils.SystemUtils
import es.dmoral.toasty.Toasty
import java.io.IOException

class NavigationMainActivity : FragmentActivity(), TabHost.OnTabChangeListener {
    private var mTabHost: FragmentTabHost? = null
    private var msg_point:TextView?=null
    private val handler_location = Handler()//=====位置信息要用到的消息处理
    private var str_location = ""//=====存放位置信息的字符串
    private var clickTime: Long = 0
    private var msgText: EditText? = null
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 7
    private var myLocation:MyLocation?= MyLocation()



    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)

        fullscreen()//=====全屏显示主题色
        setContentView(R.layout.activity_navigation_main)
        JAnalyticsInterface.onPageStart(this,this.javaClass.canonicalName)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CALL_PHONE2)
        }


        initHost() //=====初始化FragmentTabHost
        initTab() //=====初始化底部导航栏
        mTabHost!!.onTabChanged(TabDb.getTabsTxt()[0]) //=====默认选中

        initView()//推送的消息
        getLocation()//获得地址信息,并且向服务器传递推送ID和位置信息

        val isOpenAPP=intent.getBooleanExtra("openAPP",false)
        if(isOpenAPP){
            MeSettingRequest.CheckNewVersion1(this@NavigationMainActivity,Constant.School_TAG)//检查版本
        }

        //registerMessageReceiver();  // used for receive msg


        val id = intent.getIntExtra("fragid", 0)
        if (id == 2) {
            mTabHost!!.currentTab = 2
        }

        JpushJumpActivity()
    }

    fun JpushJumpActivity(){
        val message=intent.getBundleExtra("message")
        if(message!=null){
            LogUtils.d_debugprint(Constant.School_TAG,"\n\n这里传过来的type是什么呢？？？"+message.toString())

            LogUtils.d_debugprint(Constant.School_TAG,"这里进入了Navigation，而且type为：${message.getString("type")},但是就是执行不了跳转哦！")
            val read = this@NavigationMainActivity.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                SystemUtils.jumpMessageActivity(this@NavigationMainActivity,message.getInt("choose"))
            }else{
                SystemUtils.jumpLoginMainActivity(this@NavigationMainActivity)
            }
        }

        val urgentAsk=intent.getBundleExtra(Constant.JPush_UrgentAsk)
        if(urgentAsk!=null){
            LogUtils.d_debugprint(Constant.School_TAG,"\n\n这里传过来的急问id是什么呢？？？"+urgentAsk.getString("id"))
            SystemUtils.jumpUrgentAskActivity(this@NavigationMainActivity,urgentAsk.getString("id"))
        }
    }


    private fun initView() {
        msg_point=findViewById(R.id.main_unread_msg) as TextView
        msgText = findViewById(R.id.msg_rec) as EditText
    }

    private fun fullscreen() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
    }

    private fun initHost() {
        mTabHost = findViewById(R.id.main_tab) as FragmentTabHost
        //调用setup方法 设置view
        mTabHost!!.setup(this, supportFragmentManager, R.id.main_view)
        //去除分割线
        mTabHost!!.tabWidget.dividerDrawable = null//亲测，没什么用，4.0版本就不显示了
        //监听事件
        mTabHost!!.setOnTabChangedListener(this)
    }

    private fun initTab() {
        val tabs = TabDb.getTabsTxt()
        for (i in tabs.indices) {
            //新建TabSpec


            val tabSpec = mTabHost!!.newTabSpec(TabDb.getTabsTxt()[i])
            //设置view
            val view = LayoutInflater.from(this).inflate(R.layout.tab_foot, null)

            val foot_ll=view.findViewById(R.id.foot_ll) as LinearLayout
            (view.findViewById(R.id.foot_tv) as TextView).text = TabDb.getTabsTxt()[i]
            (view.findViewById(R.id.foot_iv) as ImageView).setImageResource(TabDb.getTabsImg()[i])
            tabSpec.setIndicator(view)

            foot_ll.setOnTouchListener { v, event ->
                if(event.action== MotionEvent.ACTION_DOWN){
                   foot_ll!!.alpha=0.618f
                    (view.findViewById(R.id.foot_tv) as TextView).text = TabDb.getTabsTxt()[i]
                    (view.findViewById(R.id.foot_tv) as TextView).setTextColor(resources.getColor(R.color.headline))
                    (view.findViewById(R.id.foot_iv) as ImageView).setImageResource(TabDb.getTabsImgLight()[i])

                }else if(event.action== MotionEvent.ACTION_UP){
                    if(i!=mTabHost!!.currentTab){
                        foot_ll!!.alpha=1.0f
                        (view.findViewById(R.id.foot_tv) as TextView).text = TabDb.getTabsTxt()[i]
                        (view.findViewById(R.id.foot_tv) as TextView).setTextColor(resources.getColor(R.color.huise))
                        (view.findViewById(R.id.foot_iv) as ImageView).setImageResource(TabDb.getTabsImg()[i])
                    }else{
                        foot_ll!!.alpha=1.0f
                        (view.findViewById(R.id.foot_tv) as TextView).text = TabDb.getTabsTxt()[i]
                        (view.findViewById(R.id.foot_tv) as TextView).setTextColor(resources.getColor(R.color.headline))
                        (view.findViewById(R.id.foot_iv) as ImageView).setImageResource(TabDb.getTabsImgLight()[i])
                    }
                }
                false
            }

            //加入TabSpec
            mTabHost!!.addTab(tabSpec, TabDb.getFramgent()[i], null)
        }
    }

    override fun onResume() {
        JAnalyticsInterface.onPageStart(this,"主页面")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this,"主页面")
        super.onPause()
    }

    override fun onTabChanged(arg0: String) {
        //从分割线中获得多少个切换界面
        val tabw = mTabHost!!.tabWidget
        for (i in 0..tabw.childCount - 1) {
            val v = tabw.getChildAt(i)
            val tv = v.findViewById(R.id.foot_tv) as TextView
            val iv = v.findViewById(R.id.foot_iv) as ImageView

            //修改当前的界面按钮颜色图片
            if (i == mTabHost!!.currentTab) {
                tv.setTextColor(resources.getColor(R.color.headline))
                iv.setImageResource(TabDb.getTabsImgLight()[i])//点击的图标
            } else {
                tv.setTextColor(resources.getColor(R.color.huise))
                iv.setImageResource(TabDb.getTabsImg()[i])//默认的图标颜色
            }
        }
    }

    override fun onDestroy() {
        LocationUtil.remove()
        super.onDestroy()
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun getLocation() {
        //=====首先申请权限
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_LOCATION)

        } else {
            LocationUtil.initLocation(this)
            LogUtils.d_debugprint(Constant.School_TAG, "经度："+java.lang.Double.toString(LocationUtil.longitude))
            LogUtils.d_debugprint(Constant.School_TAG, "纬度："+java.lang.Double.toString(LocationUtil.latitude))
        }
        val handler_result=object: Handler(){
            override fun handleMessage(msg: Message) {
                if (msg.what == 0) {
                    val rid = JPushInterface.getRegistrationID(applicationContext)
                    val read = this@NavigationMainActivity.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                    val userId = read.getString("id", "")
                    FirstRequest.toServerLocationAndID(this@NavigationMainActivity,rid,userId,myLocation!!)
                }
                super.handleMessage(msg)
            }
        }

        //=====开启线程获得地址
        Thread(Runnable {
            try {
                myLocation = LocationUtil.getAddress(LocationUtil.location, applicationContext)
                LocationUtil.remove()

                handler_result.sendEmptyMessage(0)
                //FirstRequest.toServerLocationAndID(this@NavigationMainActivity,rid,id,myLocation!!)

            } catch (e: IOException) {
                e.printStackTrace()
            }


            //FirstRequest.toServerLocationAndID(this@NavigationMainActivity,rid,id,myLocation!!)
           //Toasty.custom(getApplicationContext(),str_location,getResources().getDrawable(R.mipmap.mew_icon),getColor(R.color.headline),getColor(R.color.white), Toast.LENGTH_LONG,true,true).show();

        }).start()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (SystemClock.uptimeMillis() - clickTime <= 1500) {
                //如果两次的时间差＜1s，就不执行操作
                MainApplication.getInstance().finishActivity()
                this@NavigationMainActivity.overridePendingTransition(0,R.anim.zoom_exit)
            } else {
                //当前系统时间的毫秒值
                clickTime = SystemClock.uptimeMillis()
                Toasty.info(this@NavigationMainActivity, "再次点击退出").show()
                return false
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {

        private val REQUEST_PERMISSION_LOCATION = 255 //=====int should be between 0 and 255
    }


}
