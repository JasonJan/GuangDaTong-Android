package com.guangdamiao.www.mew_android_debug.bean.message_bean

import java.io.Serializable


class Message1 : Serializable {
    var id: String? = null
    var words: String? = null
    var senderID:String?=null
    var createTime:String?=null
    var icon:String?=null
    var gender:String?=null
    var nickname:String?=null
    var isRead:String?=""

    constructor():super()
    constructor(isRead:String,id:String,nickname:String,words:String,senderID:String,createTime:String, icon:String,gender:String){
        this.id=id
        this.senderID=senderID
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.words=words
        this.gender=gender
        this.isRead=isRead
    }
    override fun toString(): String {
        return "Message1[id=$id,\ncreateTime=$createTime,\nicon=$icon,\nnickname=$nickname,\nwords=$words,\ngender=$gender]"
    }
}
