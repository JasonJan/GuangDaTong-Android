package com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class School4SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "school4.db"
        private val version = 1
        val S4_Name="school4"
        val S4_Id="id"
        val S4_Sid="s_id"
        val S4_Title="title"
        val S4_CreateTime="createTime"
        val S4_Type="type"
        val S4_Link="link"
        val S4_ReadCount="readCount"
        val S4_Zan="zan"
        val S4_IsZan="isZan"
        val S4_IsFavorite="isFavorite"
        val S4_UrlEnd="urlEnd"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table school4(id integer primary key autoincrement," +
                "${School4SQLiteOpenHelper.S4_Sid} varchar(20)," +
                "${School4SQLiteOpenHelper.S4_Title} varchar(20),"+
                "${School4SQLiteOpenHelper.S4_CreateTime} varchar(100)," +
                "${School4SQLiteOpenHelper.S4_Type} varchar(5)," +
                "${School4SQLiteOpenHelper.S4_Link} varchar(100)," +
                "${School4SQLiteOpenHelper.S4_ReadCount} varchar(4)," +
                "${School4SQLiteOpenHelper.S4_Zan} varchar(2)," +
                "${School4SQLiteOpenHelper.S4_IsZan} varchar(2)," +
                "${School4SQLiteOpenHelper.S4_IsFavorite} varchar(2)," +
                "${School4SQLiteOpenHelper.S4_UrlEnd} varchar(50))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
