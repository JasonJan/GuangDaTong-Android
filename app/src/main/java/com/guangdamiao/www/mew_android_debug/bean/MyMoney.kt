package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyMoney {

    var amount: String? = null
    var description:String?=null
    var createTime:String?=null

    constructor():super()
    constructor(amount:String,description:String,createTime:String){
        this.amount=amount
        this.description=description
        this.createTime=createTime
    }

    override fun toString(): String {
        return "MyMoney[amount=$amount,description=$description，createTime=$createTime]"
    }
}
