package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.view.View
import android.widget.RelativeLayout
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message1
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message2
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message3
import com.guangdamiao.www.mew_android_debug.bean.message_bean.MessageUserData
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.school.message.*
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class Message_Request {



    companion object {

        private var helper1: Message1SQLiteOpenHelper?=null
        private var db1: SQLiteDatabase?=null
        private var helper2: Message2SQLiteOpenHelper?=null
        private var db2: SQLiteDatabase?=null
        private var helper3: Message3SQLiteOpenHelper?=null
        private var db3: SQLiteDatabase?=null


        fun getResultFromServerNotices(activity: Activity,context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<Message3>, adapter: Message3_Adpter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,no_data_rl: RelativeLayout) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject=JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()
                            if(!activity.isFinishing){
                                var getResultFromServer=""
                                getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常

                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString())
                                    if (getData != null&&getData.size>0) {
                                        no_data_rl!!.visibility= View.GONE
                                        for (i in getData.indices) {
                                            val senderID_=getData[i].getValue("senderID").toString()
                                            val icon_=getData[i].getValue("icon").toString()
                                            val id_=getData[i].getValue("id").toString()
                                            val createTime_=getData[i].getValue("createTime").toString()
                                            val gender_=getData[i].getValue("gender").toString()
                                            val nickname_=getData[i].getValue("nickname").toString()
                                            val detail_=getData[i].getValue("detail").toString()
                                            val type_=getData[i].getValue("type").toString()

                                            helper3= Message3SQLiteOpenHelper(context)
                                            if(!hasData3(id_)){
                                                insertData3(id_,icon_,gender_,nickname_,senderID_,createTime_,detail_,type_,"0")
                                                LogUtils.d_debugprint(Constant.Message_TAG,"在Message3中已插入一条数据！！！")
                                            }

                                        }//=====所有的对象已经存放到一个List了

                                        //请求消息中心更新用户资料
                                        helper3=Message3SQLiteOpenHelper(context)
                                        val IDs=query3IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(response_string: String?) {
                                                    getData = JsonUtil.getListMap("list", response_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    if (getData != null && getData.size > 0) {
                                                        helper3=Message3SQLiteOpenHelper(context)
                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            //更新数据库
                                                            update3IDs(id,icon,nickname,gender)
                                                        }
                                                        LogUtils.d_debugprint(Constant.Message_TAG,"现在，Message.message_userData的长度为：${Message.message_userData.size} \n\n\n")

                                                        //查找数据库
                                                        query3NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)

                                                    }
                                                }
                                            })
                                        }else{
                                             query3NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)

                                        }

                                    } else if(pageIndexNow==0&&listAll.size==0&&getData.size==0){
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        no_data_rl!!.visibility= View.VISIBLE
                                    }else{
                                        no_data_rl!!.visibility= View.GONE
                                        //请求消息中心更新用户资料
                                        helper3=Message3SQLiteOpenHelper(context)
                                        val IDs=query3IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(reponse_string: String?) {
                                                    getData = JsonUtil.getListMap("list", reponse_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    helper3=Message3SQLiteOpenHelper(context)
                                                    if (getData != null && getData.size > 0) {

                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            update3IDs(id,icon,nickname,gender)
                                                        }
                                                        //更新数据库


                                                        //查找数据库
                                                        query3NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)

                                                    }else{
                                                        //查找数据库
                                                        query3NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }
                                                }
                                            })
                                        }else{
                                            query3NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }

                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.error(context, Constant.SERVERBROKEN).show()
                                }
                            }else{
                                return
                            }
                        }
                    }
                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }
                })
            }else{
                if(!activity.isFinishing){
                    ptrClassicFrameLayout!!.refreshComplete()
                    Toasty.info(context,Constant.NONETWORK).show()
                }else{
                    return
                }
            }

        }

        fun getResultFromServerAskMsg(activity:Activity,context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<Message2>, adapter: Message2_Adpter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,no_data_rl:RelativeLayout) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject=JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()
                            if(!activity.isFinishing){
                                var getResultFromServer=""
                                getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常

                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString())
                                    if (getData != null&&getData.size>0) {
                                        no_data_rl!!.visibility= View.GONE
                                        for (i in getData.indices) {
                                            val senderID=getData[i].getValue("senderID").toString()
                                            val askID=getData[i].getValue("askID").toString()
                                            val icon_=getData[i].getValue("icon").toString()
                                            val id=getData[i].getValue("id").toString()
                                            val createTime_=getData[i].getValue("createTime").toString()
                                            val gender_=getData[i].getValue("gender").toString()
                                            val nickname_=getData[i].getValue("nickname").toString()
                                            val detail_=getData[i].getValue("detail").toString()
                                            val type=getData[i].getValue("type").toString()

                                            helper2= Message2SQLiteOpenHelper(context)
                                            if(!hasData2(id)){
                                                insertData2(id,askID,icon_,gender_,nickname_,senderID,createTime_,detail_,type,"0")
                                                LogUtils.d_debugprint(Constant.Message_TAG,"在Message2中已插入一条数据！！！")
                                            }
                                        }
                                        //请求消息中心更新用户资料
                                        helper2=Message2SQLiteOpenHelper(context)
                                        val IDs=query2IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(response_string: String?) {
                                                    getData = JsonUtil.getListMap("list", response_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    if (getData != null && getData.size > 0) {
                                                        helper2=Message2SQLiteOpenHelper(context)
                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            //更新数据库
                                                            update2IDs(id,icon,nickname,gender)
                                                        }

                                                        LogUtils.d_debugprint(Constant.Message_TAG,"现在，Message.message_userData的长度为：${Message.message_userData.size} \n\n\n")

                                                        //查找数据库
                                                        query2NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }else{
                                                        query2NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }
                                                }
                                            })
                                        }else{
                                            query2NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }

                                    }else if(pageIndexNow==0&&listAll.size==0&&getData.size==0){
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        no_data_rl!!.visibility= View.VISIBLE
                                    } else {
                                        //请求消息中心更新用户资料
                                        no_data_rl!!.visibility= View.GONE
                                        helper2=Message2SQLiteOpenHelper(context)
                                        val IDs=query2IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(reponse_string: String?) {
                                                    getData = JsonUtil.getListMap("list", reponse_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    helper2=Message2SQLiteOpenHelper(context)
                                                    if (getData != null && getData.size > 0) {

                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            update2IDs(id,icon,nickname,gender)
                                                        }

                                                        //查找数据库
                                                        query2NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)

                                                    }else{
                                                        //查找数据库
                                                        query2NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }
                                                }
                                            })
                                        }else{
                                            query2NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }

                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.error(context, Constant.SERVERBROKEN).show()
                                }
                            }
                        }
                    }
                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                if(!activity.isFinishing){
                    ptrClassicFrameLayout!!.refreshComplete()
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        fun getResultFromServer(activity: Activity, context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<Message1>, adapter: Message1_Adpter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,no_data_rl:RelativeLayout) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject=JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if (responseBody!=null) {
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()

                            if (!activity.isFinishing) {
                                var getResultFromServer = ""
                                getResultFromServer = response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常

                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString())
                                    if (getData != null&&getData.size>0) {
                                        no_data_rl!!.visibility= View.GONE
                                        for (i in getData.indices) {
                                            val senderID = getData[i].getValue("senderID").toString()
                                            val icon_ = getData[i].getValue("icon").toString()
                                            val id_ = getData[i].getValue("id").toString()
                                            val createTime_ = getData[i].getValue("createTime").toString()
                                            val gender_ = getData[i].getValue("gender").toString()
                                            val nickname_ = getData[i].getValue("nickname").toString()
                                            val words_ = getData[i].getValue("words").toString()

                                           helper1= Message1SQLiteOpenHelper(context)
                                           if(!hasData1(id_)){
                                               insertData1(id_,icon_,gender_,nickname_,senderID,createTime_,words_,"0")
                                               LogUtils.d_debugprint(Constant.Message_TAG,"在Message1中已插入一条数据！！！")
                                           }
                                        }

                                        //请求消息中心更新用户资料
                                        helper1=Message1SQLiteOpenHelper(context)
                                        val IDs=query1IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(response_string: String?) {
                                                    getData = JsonUtil.getListMap("list", response_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    if (getData != null && getData.size > 0) {
                                                        helper1=Message1SQLiteOpenHelper(context)
                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            //更新数据库
                                                            update1IDs(id,icon,nickname,gender)
                                                        }

                                                        LogUtils.d_debugprint(Constant.Message_TAG,"现在，Message.message_userData的长度为：${Message.message_userData.size} \n\n\n")
                                                        //查找数据库
                                                        query1NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)

                                                    }else{
                                                        //查找数据库
                                                        query1NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }
                                                }
                                            })
                                        }else{
                                            query1NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }

                                    } else if(pageIndexNow==0&&listAll.size==0&&getData.size==0) {
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        no_data_rl!!.visibility= View.VISIBLE
                                    }else{
                                        no_data_rl!!.visibility= View.GONE
                                        //请求消息中心更新用户资料
                                        helper1=Message1SQLiteOpenHelper(context)
                                        val IDs=query1IDs(0)
                                        if(IDs.size>0){
                                            Message_Request.listUserData(context,IDs,ptrClassicFrameLayout,object:IDataRequestListener2String{
                                                override fun loadSuccess(reponse_string: String?) {
                                                    getData = JsonUtil.getListMap("list", reponse_string!!)//=====这里可能发生异常
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                                                    helper1=Message1SQLiteOpenHelper(context)
                                                    if (getData != null && getData.size > 0) {

                                                        for (i in getData.indices) {

                                                            val id=getData[i].getValue("id").toString()
                                                            val icon=getData[i].getValue("icon").toString()
                                                            val nickname=getData[i].getValue("nickname").toString()
                                                            val gender=getData[i].getValue("gender").toString()

                                                            val messageUserData=MessageUserData()
                                                            messageUserData.icon=icon
                                                            messageUserData.nickname=nickname
                                                            messageUserData.gender=gender

                                                            if(Message.message_userData[id]==null&&messageUserData!=null){
                                                                Message.message_userData.put(id,messageUserData)
                                                            }
                                                            update1IDs(id,icon,nickname,gender)
                                                        }
                                                        //更新数据库


                                                        //查找数据库
                                                        query1NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)

                                                    }else{
                                                        //查找数据库
                                                        query1NowAll(listAll,0,context)
                                                        adapter!!.notifyDataSetChanged()
                                                        ptrClassicFrameLayout!!.refreshComplete()
                                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                                    }
                                                }
                                            })
                                        }else{
                                            query1NowAll(listAll,0,context)
                                            adapter!!.notifyDataSetChanged()
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }
                                    }
                                } else if (Constant.LOGINFAILURECODE.equals(getCode)) {

                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account = read.getString("account", "")
                                    val password = read.getString("password", "")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account", account)
                                    editor2.putString("password", password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                } else {

                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.error(context, Constant.SERVERBROKEN).show()
                                }
                            } else {
                                return
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                if(!activity.isFinishing){
                    ptrClassicFrameLayout!!.refreshComplete()
                    Toasty.info(context,Constant.NONETWORK).show()
                } else{
                    return
                }
            }
        }

        fun listUserData(context: Context, IDs: ArrayList<String>,ptrClassicFrameLayout: PtrClassicFrameLayout,listener:IDataRequestListener2String) {
            if(NetUtil.checkNetWork(context)){

                var sign = ""
                var urlPath = ""

                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + Constant.Message_List_UserData
                    sign = MD5Util.md5(Constant.SALTFORTEST)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + Constant.Message_List_UserData
                    sign = MD5Util.md5(Constant.SALTFORRELEASE)
                }
                val jsonObject=JSONObject()
                val jsonArray= JSONArray()
                for(i in IDs.indices){
                    jsonArray.put(i,IDs[i])
                }
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(Constant.Message_TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if (responseBody != null) {
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response = JSONObject(resultDate)
                            var getResultFromServer = ""
                            getResultFromServer = response.toString()
                            LogUtils.d_debugprint(Constant.Message_TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            val getCode: String
                            var msg: String? = null
                            var result: String

                            msg=JsonUtil.get_key_string(Constant.Server_Msg,getResultFromServer!!)
                            getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常

                                result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                if(!result.equals("")){
                                    listener.loadSuccess(result)
                                }

                            }else{
                                ptrClassicFrameLayout.refreshComplete()

                                Toasty.error(context,msg).show()
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          ptrClassicFrameLayout.refreshComplete()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }
        }


        /**
         * 1插入数据
         */
        private fun insertData1(s_id: String,icon:String,gender:String,nickname:String,senderID:String,createTime:String,words:String,isRead:String) {
            if(helper1!=null){
                db1 = helper1!!.writableDatabase
                val message1Item= ContentValues()
                message1Item.put("s_id",s_id)
                message1Item.put("icon",icon)
                message1Item.put("gender",gender)
                message1Item.put("nickname",nickname)
                message1Item.put("senderID",senderID)
                message1Item.put("createTime",createTime)
                message1Item.put("words",words)
                message1Item.put("isRead",isRead)
                db1!!.insert(Message1SQLiteOpenHelper.M1_Name,null,message1Item)
                db1!!.close()
            }
        }

        /**
         * 1检查数据库中是否已经有该条记录
         */
        private fun hasData1(s_id: String): Boolean {
            val cursor = helper1!!.readableDatabase.rawQuery("select id from message1 where s_id =?", arrayOf(s_id))

            //判断是否有下一个
            return cursor.moveToNext()
        }


        /**
         * 2插入数据
         */
        private fun insertData2(s_id: String,askID:String,icon:String,gender:String,nickname:String,senderID:String,createTime:String,detail:String,type:String,isRead:String) {
            if(helper2!=null){
                db2 = helper2!!.writableDatabase
                val message2Item= ContentValues()
                message2Item.put("s_id",s_id)
                message2Item.put("askID",askID)
                message2Item.put("icon",icon)
                message2Item.put("gender",gender)
                message2Item.put("nickname",nickname)
                message2Item.put("senderID",senderID)
                message2Item.put("createTime",createTime)
                message2Item.put("detail",detail)
                message2Item.put("type",type)
                message2Item.put("isRead",isRead)
                db2!!.insert(Message2SQLiteOpenHelper.M2_Name,null,message2Item)
                db2!!.close()
            }
        }

        /**
         * 2检查数据库中是否已经有该条记录
         */
        private fun hasData2(s_id: String): Boolean {
            val cursor = helper2!!.readableDatabase.rawQuery("select id from message2 where s_id =?", arrayOf(s_id))

            //判断是否有下一个
            return cursor.moveToNext()
        }

        /**
         * 3插入数据
         */
        private fun insertData3(s_id: String,icon:String,gender:String,nickname:String,senderID:String,createTime:String,detail:String,type:String,isRead:String) {
            if(helper3!=null){
                db3 = helper3!!.writableDatabase
                val message3Item= ContentValues()
                message3Item.put("s_id",s_id)
                message3Item.put("icon",icon)
                message3Item.put("gender",gender)
                message3Item.put("nickname",nickname)
                message3Item.put("senderID",senderID)
                message3Item.put("createTime",createTime)
                message3Item.put("detail",detail)
                message3Item.put("type",type)
                message3Item.put("isRead",isRead)
                db3!!.insert(Message3SQLiteOpenHelper.M3_Name,null,message3Item)         
                db3!!.close()
            }
        }

        /**
         * 3检查数据库中是否已经有该条记录
         */
        private fun hasData3(s_id: String): Boolean {
            val cursor = helper3!!.readableDatabase.rawQuery("select id from message3 where s_id =?", arrayOf(s_id))

            //判断是否有下一个
            return cursor.moveToNext()
        }

        private fun query1NowAll(listItems: java.util.ArrayList<Message1>, pageNow:Int,context:Context){
            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select * from "+Message1SQLiteOpenHelper.M1_Name+" order by createTime desc limit $limit_page offset $offset_page"
            if(pageNow==0) listItems.clear()

            val cursor= helper1!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val message1=Message1()
                message1.id=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_Sid))
                message1.icon=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_Icon))
                message1.gender=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_Gender))
                message1.nickname=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_NickName))
                message1.senderID=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_SenderID))
                message1.createTime=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_CreateTime))
                message1.words=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_Words))
                message1.isRead=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_IsRead))

                val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                val blacklist= read.getString("blacklist", "[]")
                val blacklist_array=ArrayList<String>()
                val jsonArray= JSONArray(blacklist)
                if(jsonArray.length()>0) {
                    for (i in 0..jsonArray.length()-1) {
                        blacklist_array.add(jsonArray.getString(i))
                    }
                }
                var isExistBlack=false
                for(j in blacklist_array.indices){
                    if(message1.senderID.equals(blacklist_array[j])){
                        isExistBlack=true
                    }
                }
                if(isExistBlack){

                }else{
                    listItems.add(message1)
                }

            }
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message1为：\n\n"+listItems.toString())
        }

        private fun query1IDs(pageNow:Int):ArrayList<String>{

            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select senderID from "+Message1SQLiteOpenHelper.M1_Name+" order by createTime desc limit $limit_page offset $offset_page"
            val IDs=ArrayList<String>()

            val cursor= helper1!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val item=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_SenderID))
                if(IDs.size==0) IDs.add(item)
                for(i in IDs.indices){
                    if(!IDs[i].equals(item)){
                        if(i==IDs.size-1) IDs.add(item)
                    }else{
                        break
                    }
                }
            }

            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message1的IDs为：\n\n"+IDs.toString())

            return IDs
        }

        private fun update1IDs(senderID:String,icon:String,nickname: String,gender:String){
            val sql="update message1 set icon='$icon',nickname='$nickname',gender='$gender' where senderID='$senderID';"
            val db=helper1!!.writableDatabase
            db!!.execSQL(sql)
            db!!.close()
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message1所有数据=====更新了刚请求过来的20个新的资料！！！\n\n")
        }

        private fun query2NowAll(listItems: java.util.ArrayList<Message2>, pageNow:Int,context:Context){
            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select * from "+Message2SQLiteOpenHelper.M2_Name+" order by createTime desc limit $limit_page offset $offset_page;"

            if(pageNow==0) listItems.clear()
            val cursor= helper2!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val message2=Message2()
                message2.id=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Sid))
                message2.askID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_AskID))
                message2.icon=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Icon))
                message2.gender=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Gender))
                message2.nickname=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_NickName))
                message2.senderID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
                message2.createTime=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_CreateTime))
                message2.detail=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Detail))
                message2.type=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Type))
                message2.isRead=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_IsRead))

                val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                val blacklist= read.getString("blacklist", "[]")
                val blacklist_array=ArrayList<String>()
                val jsonArray= JSONArray(blacklist)
                if(jsonArray.length()>0) {
                    for (i in 0..jsonArray.length()-1) {
                        blacklist_array.add(jsonArray.getString(i))
                    }
                }
                var isExistBlack=false
                for(j in blacklist_array.indices){
                    if(message2.senderID.equals(blacklist_array[j])){
                        isExistBlack=true
                    }
                }
                if(isExistBlack){

                }else{
                    listItems.add(message2)
                }
            }
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2为：\n\n"+listItems.toString())
        }

        private fun query2IDs(pageNow:Int):ArrayList<String>{

            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select senderID from "+Message2SQLiteOpenHelper.M2_Name+" order by createTime desc limit $limit_page offset $offset_page"
            val IDs=ArrayList<String>()

            val cursor= helper2!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val item=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
                if(IDs.size==0) IDs.add(item)
                for(i in IDs.indices){
                    if(!IDs[i].equals(item)){
                        if(i==IDs.size-1) IDs.add(item)
                    }else{
                        break
                    }
                }
            }

            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2的IDs为：\n\n"+IDs.toString())

            return IDs
        }

        private fun update2IDs(senderID:String,icon:String,nickname: String,gender:String){
            val sql="update message2 set icon='$icon',nickname='$nickname',gender='$gender' where senderID='$senderID';"
            val db=helper2!!.writableDatabase
            db!!.execSQL(sql)
            db!!.close()
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message2所有数据=====更新了刚请求过来的20个新的资料！！！\n\n")
        }

        private fun query3NowAll(listItems: java.util.ArrayList<Message3>, pageNow: Int,context:Context){
            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select * from "+Message3SQLiteOpenHelper.M3_Name+" order by createTime desc limit $limit_page offset $offset_page"

            if(pageNow==0) listItems.clear()

            val cursor= helper3!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val message3=Message3()
                message3.id=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_Sid))
                message3.icon=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_Icon))
                message3.gender=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_Gender))
                message3.nickname=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_NickName))
                message3.senderID=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_SenderID))
                message3.createTime=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_CreateTime))
                message3.detail=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_Detail))
                message3.type=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_Type))
                message3.isRead=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_IsRead))

                val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                val blacklist= read.getString("blacklist", "[]")
                val blacklist_array=ArrayList<String>()
                val jsonArray= JSONArray(blacklist)
                if(jsonArray.length()>0) {
                    for (i in 0..jsonArray.length()-1) {
                        blacklist_array.add(jsonArray.getString(i))
                    }
                }
                var isExistBlack=false
                for(j in blacklist_array.indices){
                    if(message3.senderID.equals(blacklist_array[j])){
                        isExistBlack=true
                    }
                }
                if(isExistBlack){

                }else{
                   listItems.add(message3)
                }

            }
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message3为：\n\n"+listItems.toString())
        }

        private fun query3IDs(pageNow:Int):ArrayList<String>{

            val offset_page=pageNow*Constant.PAGESIZE
            val limit_page=Constant.PAGESIZE
            val sql="select senderID from "+Message3SQLiteOpenHelper.M3_Name+" order by createTime desc limit $limit_page offset $offset_page"
            val IDs=ArrayList<String>()

            val cursor= helper3!!.readableDatabase.rawQuery(sql,null)
            while(cursor.moveToNext()){
                val item=cursor.getString(cursor.getColumnIndex(Message3SQLiteOpenHelper.M3_SenderID))
                if(IDs.size==0) IDs.add(item)

                for(i in IDs.indices){
                    if(!IDs[i].equals(item)){
                        if(i==IDs.size-1) IDs.add(item)
                    }else{
                        break
                    }
                }
            }

            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message3的IDs为：\n\n"+IDs.toString())

            return IDs
        }

        private fun update3IDs(senderID:String,icon:String,nickname: String,gender:String){
            val sql="update message3 set icon='$icon',nickname='$nickname',gender='$gender' where senderID='$senderID';"
            val db=helper3!!.writableDatabase
            db!!.execSQL(sql)
            db!!.close()
            LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message3所有数据=====更新了刚请求过来的20个新的资料！！！\n\n")
        }

        fun getMsgNum(activity: Activity, context: Context, urlEnd:String,TAG: String,listener:IDataRequestListener2String) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = 0
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if(token.equals("")) return
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject=JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if (responseBody!=null) {
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (!activity.isFinishing) {
                                var getResultFromServer = ""
                                getResultFromServer = response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "$urlEnd 中 json解析出来的对象是=" + getData.toString())
                                    if (getData != null&&getData.size>0) {
                                        //保存到数据库吧=====注意分情况
                                        if(urlEnd.equals(Constant.Message_List_LeaveWords)){

                                            for (i in getData.indices) {
                                                val senderID = getData[i].getValue("senderID").toString()
                                                val icon_ = getData[i].getValue("icon").toString()
                                                val id_ = getData[i].getValue("id").toString()
                                                val createTime_ = getData[i].getValue("createTime").toString()
                                                val gender_ = getData[i].getValue("gender").toString()
                                                val nickname_ = getData[i].getValue("nickname").toString()
                                                val words_ = getData[i].getValue("words").toString()

                                                helper1 = Message1SQLiteOpenHelper(context)
                                                if (!hasData1(id_)) {
                                                    insertData1(id_, icon_, gender_, nickname_, senderID, createTime_, words_, "0")
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "在Message1中已插入一条数据！！！")
                                                }
                                            }

                                        }else if(urlEnd.equals(Constant.Message_List_AskMsg)){

                                            for (i in getData.indices) {
                                                val senderID = getData[i].getValue("senderID").toString()
                                                val askID = getData[i].getValue("askID").toString()
                                                val icon_ = getData[i].getValue("icon").toString()
                                                val id = getData[i].getValue("id").toString()
                                                val createTime_ = getData[i].getValue("createTime").toString()
                                                val gender_ = getData[i].getValue("gender").toString()
                                                val nickname_ = getData[i].getValue("nickname").toString()
                                                val detail_ = getData[i].getValue("detail").toString()
                                                val type = getData[i].getValue("type").toString()

                                                helper2 = Message2SQLiteOpenHelper(context)
                                                if (!hasData2(id)) {
                                                    insertData2(id, askID, icon_, gender_, nickname_, senderID, createTime_, detail_, type, "0")
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "在Message2中已插入一条数据！！！")
                                                }
                                            }

                                        }else if(urlEnd.equals(Constant.Message_List_Notices)){

                                            for (i in getData.indices) {
                                                val senderID_ = getData[i].getValue("senderID").toString()
                                                val icon_ = getData[i].getValue("icon").toString()
                                                val id_ = getData[i].getValue("id").toString()
                                                val createTime_ = getData[i].getValue("createTime").toString()
                                                val gender_ = getData[i].getValue("gender").toString()
                                                val nickname_ = getData[i].getValue("nickname").toString()
                                                val detail_ = getData[i].getValue("detail").toString()
                                                val type_ = getData[i].getValue("type").toString()

                                                helper3 = Message3SQLiteOpenHelper(context)
                                                if (!hasData3(id_)) {
                                                    insertData3(id_, icon_, gender_, nickname_, senderID_, createTime_, detail_, type_, "0")
                                                    LogUtils.d_debugprint(Constant.Message_TAG, "在Message3中已插入一条数据！！！")
                                                }
                                            }
                                        }

                                        listener.loadSuccess(getData.size.toString())
                                    } else {

                                        listener.loadSuccess("0")
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        //Toasty.info(context,Constant.NONETWORK).show()
                    }

                })
            }
        }

    }


}
