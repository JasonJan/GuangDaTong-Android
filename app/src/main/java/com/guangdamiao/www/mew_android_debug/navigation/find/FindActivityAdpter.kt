package com.guangdamiao.www.mew_android_debug.navigation.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

/**

 */
class FindActivityAdpter(protected var context: Context, list_activity: ArrayList<ListFindAll>) : BaseAdapter() {
    private var list_activity :ArrayList<ListFindAll>?=null


    init {
        this.list_activity = list_activity

    }

    override fun getCount(): Int {
        return list_activity!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list3, parent, false)
        }
        val listactivity = list_activity!!.get(position)//获取一个
        val list_icon = ViewHolder.get<ImageView>(convertView, R.id.activity_list_icon)
        val list_title = ViewHolder.get<TextView>(convertView, R.id.activity_list_title)
        val list_time = ViewHolder.get<TextView>(convertView, R.id.activity_list_time1)
        val list_price = ViewHolder.get<TextView>(convertView, R.id.activity_list_price1)
        val list_description = ViewHolder.get<TextView>(convertView, R.id.activity_list_description)
        val time = listactivity.createTime!!.substring(0, 19).replace("T", " ")

        list_title.setText(listactivity.title)
        if(listactivity.title!!.length>20){
            list_title.text=listactivity.title!!.substring(0,18)+"..."
        }

        list_time.setText(time)
        list_price.setText(listactivity.fee)
        list_description.text = "摘要：" + listactivity.description!!
        ImageLoader.getInstance().displayImage( listactivity.icon!!, list_icon)

        val link = listactivity.link
        val webView_id = listactivity.id
        val zan = listactivity.zan_
        val isZan = listactivity.isZan
        val readCount = listactivity.readCount

        list_icon!!.setOnClickListener{
            if(listactivity.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listactivity.icon!!!!)
        }

        convertView!!.setOnClickListener {
            val intent = Intent(context, FindWebView::class.java)
            val bundle = Bundle()
            bundle.putInt("position",position)
            bundle.putString("id", webView_id)
            bundle.putString("link", link)
            bundle.putString("zan", zan)
            bundle.putString("isZan", isZan)
            bundle.putString("readCount", readCount)
            bundle.putString("isFavorite", "1")
            bundle.putString("urlEnd", Constant.Find_Activity)
            bundle.putString("title_first", list_title.text.toString())
            intent.putExtras(bundle)
            context.startActivity(intent)
            //这里处理点击事件
        }



        return convertView
    }


}
