package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message1
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.WordsActivity
import com.guangdamiao.www.mew_android_debug.utils.BadgeView
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.nostra13.universalimageloader.core.ImageLoader
import org.json.JSONArray
import java.util.*

class Message1_Adpter(protected var context: Context, message1: ArrayList<Message1>,protected var badge1:BadgeView?) : BaseAdapter() {
    private var message1 :ArrayList<Message1>?=null

    init {
        this.message1 = message1
    }

    override fun getCount(): Int {
        if(message1!!.size==0){
            update1MessageNum(context)
        }
        return message1!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder: Message1_Adpter.ViewHolder

        if (convertView == null) {
            holder= Message1_Adpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.message1_item, parent, false)
            holder.list_message1_icon=convertView.findViewById(R.id.list_message1_icon) as ImageView
            holder.list_message1_ll=convertView.findViewById(R.id.list_message1_ll) as LinearLayout
            holder.list_message1_nick_tv=convertView.findViewById(R.id.list_message1_nick_tv) as TextView
            holder.list_message1_sex_tv=convertView.findViewById(R.id.list_message1_sex_tv) as TextView
            holder.list_message1_time=convertView.findViewById(R.id.list_message1_time) as TextView
            holder.list_message1_words=convertView.findViewById(R.id.list_message1_words_tv) as TextView
            holder.message1_unread_msg=convertView.findViewById(R.id.message1_unread_msg) as TextView

            convertView.tag=holder
        }else{
            holder=convertView.tag as Message1_Adpter.ViewHolder
        }

        update1MessageNum(context)
        val listMessage1 = message1!!.get(position)//获取一个

        //头像
        val icon=listMessage1.icon
        ImageLoader.getInstance().displayImage(icon,holder.list_message1_icon)

        //昵称
        holder!!.list_message1_nick_tv!!.text=listMessage1!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.list_message1_nick_tv!!.measure(spec,spec)
        var nickwidth= holder!!.list_message1_nick_tv!!.measuredWidth
        var nicklength=listMessage1!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(context,167.0f)){
            nicklength-=2
            val name_display=listMessage1!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.list_message1_nick_tv!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.list_message1_nick_tv!!.measure(spec,spec)
            nickwidth= holder!!.list_message1_nick_tv!!.measuredWidth
        }

        //性别
        if (!listMessage1.gender.equals("")&&listMessage1.gender!=null){
            if(listMessage1.gender.equals("女")){
                holder!!.list_message1_sex_tv!!.text="♀"
                holder!!.list_message1_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.ask_red))
            }else{
                holder!!.list_message1_sex_tv!!.text="♂"
                holder!!.list_message1_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.headline))
            }
        }else{
            holder!!.list_message1_sex_tv!!.visibility=View.GONE
        }

        //时间
        val time = listMessage1.createTime!!.substring(2, 16).replace("T", " ")
        holder!!.list_message1_time!!.text=time

        //是否读过，显示红点
        holder!!.message1_unread_msg!!.visibility=View.GONE
        if(listMessage1.isRead.equals("0")){
            holder!!.message1_unread_msg!!.visibility=View.VISIBLE
        }


        //内容
        holder!!.list_message1_words!!.text=listMessage1.words

        holder!!.list_message1_ll!!.setOnClickListener {
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val id=read.getString("id","")
            val token = read.getString("token", "")
            if(!id.equals(listMessage1.senderID)){
                if(!listMessage1.senderID.equals("")&&!token.equals("")){
                    val intent: Intent = Intent(context, WordsActivity::class.java)
                    val bundle= Bundle()
                    bundle.putString("ID",listMessage1.senderID)
                    bundle.putString("nickname",listMessage1.nickname)
                    bundle.putString("token",token)
                    bundle.putString("centerWords", Constant.Leave_Words)
                    bundle.putString("rightWords","发送")
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }
                listMessage1.isRead="1"
                update1Message(context,listMessage1.id!!)
                holder.message1_unread_msg!!.visibility=View.GONE
                update1MessageNum(context)
            }
        }

        holder.list_message1_icon!!.setOnClickListener{
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val id=read.getString("id","")
            if(!id.equals(listMessage1.senderID)){
                val intent:Intent=Intent(context, IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listMessage1.senderID)
                bundle.putString("icon",listMessage1.icon)
                bundle.putString("sex",listMessage1.gender)
                bundle.putString("nickname",listMessage1.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }

        }

        return convertView!!
    }

    class ViewHolder{
        var list_message1_icon: ImageView?=null
        var list_message1_ll:LinearLayout?=null
        var list_message1_nick_tv: TextView?=null
        var list_message1_sex_tv:TextView?=null
        var list_message1_time:TextView?=null
        var list_message1_words:TextView?=null
        var message1_unread_msg:TextView?=null
    }


    private fun update1Message(context:Context,s_id:String){
        val sql="update message1 set isRead='1' where s_id=$s_id;"
        val helper1=Message1SQLiteOpenHelper(context)
        val db=helper1!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message1 s_id=$s_id 数据=====设为已读！！！\n\n")

    }

    private fun update1MessageNum(context:Context){
        val unread_old=query1NowAll()

        if (Activity::class.java.isInstance(context)) {

            val activity = context as Activity
            val bt1=activity.findViewById(R.id.bt1) as Button
            if(badge1==null){
                badge1= BadgeView(context,bt1)
            }
            if(unread_old!=0){

                badge1!!.badgePosition= BadgeView.POSITION_TOP_RIGHT
                if(unread_old<99){
                    badge1!!.text=unread_old.toString()
                    badge1!!.visibility=View.VISIBLE
                }else{
                    badge1!!.text="99+"
                    badge1!!.visibility=View.VISIBLE
                }
                badge1!!.textSize=10.0f
                badge1!!.setTextColor(Color.WHITE)
                badge1!!.show()

            }else{
                badge1!!.visibility=View.GONE
            }
        }

    }

    private fun query1NowAll():Int{
        val sql="select * from "+ Message1SQLiteOpenHelper.M1_Name+" where isRead='0'"
        val helper1=Message1SQLiteOpenHelper(context)
        val cursor=helper1!!.readableDatabase.rawQuery(sql,null)
        var message1_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message1SQLiteOpenHelper.M1_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message1_unread++
            }

        }
        //LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message1有多少条未读的：$message1_unread\n\n")
        return message1_unread

    }
}
