package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Black

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyBlackFriends
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil

import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

class Main_BlackBookAdapter(context:Context, MyBlackFriends: ArrayList<MyBlackFriends>) : BaseAdapter(){
    private val mInflater: LayoutInflater//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    private var MyBlackFriends:ArrayList<MyBlackFriends>?=null
    private var onShowItemClickListener:OnShowItemClickListener?=null

    init {
        this.mContext = context
        this.MyBlackFriends=MyBlackFriends
        this.mInflater = LayoutInflater.from(mContext)
    }
    override fun getItem(arg0: Int): Any {

        return MyBlackFriends!![arg0]
    }
    override fun getItemId(position: Int): Long {

        return position.toLong()
    }
    override fun getCount(): Int {

        return MyBlackFriends!!.size
    }
    override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
        var convertView = convertView
        var holder:ViewHolder


        if (convertView == null) {
            holder= ViewHolder()
            convertView = mInflater.inflate(R.layout.mine_item_black_friends, null)
            holder.indexText = convertView!!.findViewById(R.id.mine_item_blackfriend) as TextView
            holder.mine_blackfriend_icon=convertView!!.findViewById(R.id.mine_blackfriend_icon) as ImageView
            holder.mine_blackfriend_next=convertView!!.findViewById(R.id.mine_blackfriend_next) as ImageView
            holder.checkBox=convertView!!.findViewById(R.id.mine_item_choose_cb) as CheckBox
            holder.mine_black_rl=convertView!!.findViewById(R.id.mine_black_rl) as RelativeLayout
            holder.mine_item_blackTime_tv=convertView!!.findViewById(R.id.mine_item_blackTime_tv) as TextView
            convertView.tag=holder
        }else{
            holder=convertView.tag as ViewHolder
        }

        val bean=MyBlackFriends!!.get(position)
        if(bean.isShow!!){
            holder.checkBox!!.visibility=View.VISIBLE
            holder.mine_blackfriend_next!!.setImageResource(R.mipmap.next)
            holder.mine_blackfriend_next!!.visibility=View.GONE
        }else{
            holder.checkBox!!.visibility=View.GONE
            holder.mine_blackfriend_next!!.setImageResource(R.mipmap.next)
            holder.mine_blackfriend_next!!.visibility=View.GONE
        }

        holder.indexText!!.text = MyBlackFriends!![position].nickname
        holder.indexText!!.setTextColor(mContext!!.resources.getColor(R.color.black))
        getMyIcon(MyBlackFriends!![position].icon!!,holder.mine_blackfriend_icon!!)

        //拉黑时间
        holder.mine_item_blackTime_tv!!.text="拉黑时间："+MyBlackFriends!![position]!!.createTime!!.substring(2, 16).replace("T", " ")
        
        //昵称
        holder!!.indexText!!.text=MyBlackFriends!![position]!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.indexText!!.measure(spec,spec)
        var nickwidth= holder!!.indexText!!.measuredWidth
        //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nickname为：${MyBlackFriends!![position]!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,187.0f)} ")
        var nicklength=MyBlackFriends!![position]!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(mContext,250.0f)){
            nicklength-=2
            val name_display=MyBlackFriends!![position]!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.indexText!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.indexText!!.measure(spec,spec)
            nickwidth= holder!!.indexText!!.measuredWidth
            // LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }
        
        holder.mine_black_rl!!.setOnClickListener{
            if(!bean.isChecked!!){
                val intent: Intent = Intent(mContext, IconInfo::class.java)
                val bundle= Bundle()
                bundle.putString("id",bean.id)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex","")
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }
        }

        if(bean.isShow!!){
            holder.checkBox!!.visibility=View.VISIBLE
            holder.mine_black_rl!!.setOnClickListener{
                if(!bean.isChecked!!){
                    bean.isChecked=true
                    holder.checkBox!!.isChecked=true
                }else{
                    holder.checkBox!!.isChecked=false
                    bean.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(bean)
            }
        }else{
            holder.checkBox!!.visibility=View.GONE
        }


        holder.checkBox!!.isChecked=bean.isChecked!!

        return convertView
    }

    private fun getMyIcon(icon_url:String,icon_object:ImageView) {
        if (icon_url !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            } else {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            }
        }
    }

   class ViewHolder{
       var indexText:TextView?=null
       var mine_blackfriend_icon:ImageView?=null
       var mine_blackfriend_next:ImageView?=null
       var checkBox: CheckBox?=null
       var mine_black_rl:RelativeLayout?=null
       var mine_item_blackTime_tv:TextView?=null
   }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: MyBlackFriends)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }
}
