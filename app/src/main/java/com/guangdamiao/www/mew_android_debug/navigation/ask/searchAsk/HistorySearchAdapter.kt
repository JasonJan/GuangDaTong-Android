package com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.support.v4.widget.SimpleCursorAdapter
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils

/**
 * Created by Jason_Jan on 2017/12/11.
 */

class HistorySearchAdapter(private val m_context: Context, layout: Int, private val m_cursor: Cursor,
                           from: Array<String>, to: IntArray, flags: Int) : SimpleCursorAdapter(m_context, layout, m_cursor, from, to, flags) {
    private val miInflater: LayoutInflater? = null
    private var db: SQLiteDatabase?=null
    private var helper=RecordSQLiteOpenHelper(m_context)

    override fun bindView(arg0: View?, arg1: Context?, arg2: Cursor) {
        var convertView: View? = null
        if (arg0 == null) {
            convertView = miInflater!!.inflate(R.layout.ask_search_item, null)
        } else {
            convertView = arg0
        }
        val askSearch_result_rl = convertView!!.findViewById(R.id.askSearch_result_rl) as RelativeLayout
        val ask_history_item_tv = convertView.findViewById(R.id.ask_history_item_tv) as TextView
        ask_history_item_tv.text = arg2.getString(arg2.getColumnIndex("name"))


        //如果点击了这一行
        askSearch_result_rl!!.setOnClickListener{
             val type=selectSomething(arg2.getString(arg2.getColumnIndex("name")),2)
             val label=selectSomething(arg2.getString(arg2.getColumnIndex("name")),3)
             LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG,"HistorySearch中："+"\nkey="+arg2.getColumnIndex("name")+"\ntype="+type+"\nlabel="+label)
             //跳转吧
             val intent: Intent = Intent(m_context,AskSearchResult::class.java)

             val bundle= Bundle()
             bundle.putString("key",arg2.getString(arg2.getColumnIndex("name")))
             bundle.putString("type",type)
             bundle.putString("label",label)
             intent.putExtras(bundle)
             m_context.startActivity(intent)
        }

    }


    /**
     * 查询数据库
     */
    private fun selectSomething(tempName: String,column:Int): String {
        var something=""
        val cursor = helper.readableDatabase.rawQuery("select * from records where name =?", arrayOf(tempName))
        while(cursor.moveToNext()){
             something=cursor.getString(column)
        }
        return something
    }
}
