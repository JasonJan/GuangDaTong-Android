package com.guangdamiao.www.mew_android_debug.webviewPicture

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bm.library.PhotoView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import es.dmoral.toasty.Toasty


class PhotoBrowserActivity : Activity(), View.OnClickListener {

    private var crossIv: ImageView? = null
    private var mPager: ViewPager? = null
    private var centerIv: ImageView? = null
    private var photoOrderTv: TextView? = null
    private var saveTv: Button? = null
    private var curImageUrl: String? = ""
    private var imageUrls: Array<String>? = arrayOf()

    private var curPosition = -1
    private var initialedPositions: IntArray? = null
    private var objectAnimator: ObjectAnimator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        MainApplication.getInstance().addActivity(this)
        setContentView(R.layout.activity_photo_browser)
        imageUrls = intent.getStringArrayExtra("imageUrls")
        curImageUrl = intent.getStringExtra("curImageUrl")
        initialedPositions = IntArray(imageUrls!!.size)
        initInitialedPositions()

        photoOrderTv = findViewById(R.id.photoOrderTv) as TextView
        saveTv = findViewById(R.id.saveTv) as Button
        saveTv!!.visibility=View.GONE
        saveTv!!.setOnClickListener(this)
        centerIv = findViewById(R.id.centerIv) as ImageView
        crossIv = findViewById(R.id.crossIv) as ImageView
        crossIv!!.setOnClickListener(this)

        mPager = findViewById(R.id.pager) as ViewPager
        mPager!!.pageMargin = (resources.displayMetrics.density * 15).toInt()
        mPager!!.adapter = object : PagerAdapter() {
            override fun getCount(): Int {
                return imageUrls!!.size
            }


            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return view === `object`
            }

            override fun instantiateItem(container: ViewGroup, position: Int): Any? {
                if (imageUrls!![position] != null && "" != imageUrls!![position]) {
                    val view = PhotoView(this@PhotoBrowserActivity)
                    view.enable()
                    view.scaleType = ImageView.ScaleType.FIT_CENTER
                    Glide.with(this@PhotoBrowserActivity).load(imageUrls!![position]).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).fitCenter().crossFade().listener(object : RequestListener<String, GlideDrawable> {
                        override fun onException(e: Exception, model: String, target: Target<GlideDrawable>, isFirstResource: Boolean): Boolean {
                            if (position == curPosition) {
                                saveTv!!.visibility=View.VISIBLE
                                hideLoadingAnimation()
                            }
                            showErrorLoading()
                            return false
                        }

                        override fun onResourceReady(resource: GlideDrawable, model: String, target: Target<GlideDrawable>, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                            occupyOnePosition(position)
                            if (position == curPosition) {
                                saveTv!!.visibility=View.VISIBLE
                                hideLoadingAnimation()
                            }
                            return false
                        }
                    }).into(view)

                    view!!.setOnClickListener {
                           finish()
                           overridePendingTransition(0,R.anim.zoom_exit)
                    }

                    container.addView(view)
                    return view
                }
                return null
            }


            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                releaseOnePosition(position)
                container.removeView(`object` as View)
            }

        }

        curPosition = if (returnClickedPosition() == -1) 0 else returnClickedPosition()
        mPager!!.currentItem = curPosition
        mPager!!.tag = curPosition
        if (initialedPositions!![curPosition] != curPosition) {//如果当前页面未加载完毕，则显示加载动画，反之相反；
            showLoadingAnimation()
        }
        photoOrderTv!!.text = (curPosition + 1).toString() + "/" + imageUrls!!.size//设置页面的编号


        mPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (initialedPositions!![position] != position) {//如果当前页面未加载完毕，则显示加载动画，反之相反；
                    showLoadingAnimation()
                } else {
                    hideLoadingAnimation()
                }
                curPosition = position
                photoOrderTv!!.text = (position + 1).toString() + "/" + imageUrls!!.size//设置页面的编号
                mPager!!.tag = position//为当前view设置tag
                //设置可以缩放


            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun returnClickedPosition(): Int {
        if (imageUrls == null || curImageUrl == null) {
            return -1
        }
        for (i in imageUrls!!.indices) {
            if (curImageUrl == imageUrls!![i]) {
                return i
            }
        }
        return -1
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.crossIv -> {
                                finish()
                                this@PhotoBrowserActivity.overridePendingTransition(0,R.anim.zoom_exit)
                             }
            R.id.saveTv -> savePhotoToLocal()
            else -> {
            }
        }
    }

    private fun showLoadingAnimation() {
        centerIv!!.visibility = View.VISIBLE
        centerIv!!.setImageResource(R.drawable.loading)
        if (objectAnimator == null) {
            objectAnimator = ObjectAnimator.ofFloat(centerIv, "rotation", 0f, 360f)
            objectAnimator!!.duration = 2000
            objectAnimator!!.repeatCount = ValueAnimator.INFINITE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                objectAnimator!!.setAutoCancel(true)
            }
        }
        objectAnimator!!.start()
    }

    private fun hideLoadingAnimation() {
        releaseResource()
        centerIv!!.visibility = View.GONE
    }

    private fun showErrorLoading() {
        centerIv!!.visibility = View.VISIBLE
        releaseResource()
        centerIv!!.setImageResource(R.drawable.load_error)
    }

    private fun releaseResource() {
        if (objectAnimator != null) {
            objectAnimator!!.cancel()
        }
        if (centerIv!!.animation != null) {
            centerIv!!.animation.cancel()
        }
    }

    private fun occupyOnePosition(position: Int) {
        initialedPositions!![position] = position
    }

    private fun releaseOnePosition(position: Int) {
        initialedPositions!![position] = -1
    }

    private fun initInitialedPositions() {
        for (i in initialedPositions!!.indices) {
            initialedPositions!![i] = -1
        }
    }

    private fun savePhotoToLocal() {
        val containerTemp = mPager!!.findViewWithTag(mPager!!.currentItem) as ViewGroup ?: return
        val photoViewTemp = containerTemp.getChildAt(0) as PhotoView
       /* photoViewTemp.enable()*/
        if (photoViewTemp != null) {

            try{
                val glideBitmapDrawable = photoViewTemp.drawable as GlideBitmapDrawable ?: return
                val bitmap = glideBitmapDrawable.bitmap ?: return
                FileUtils.savePhoto(this, bitmap, object : FileUtils.SaveResultCallback {
                    override fun onSavedSuccess() {
                        runOnUiThread { Toasty.info(this@PhotoBrowserActivity,"保存成功！").show() }
                    }

                    override fun onSavedFailed() {
                        runOnUiThread { Toast.makeText(this@PhotoBrowserActivity, "保存失败！", Toast.LENGTH_SHORT).show() }
                    }
                })
            }catch (e:Exception){
                e.printStackTrace()
                Toasty.info(this@PhotoBrowserActivity,"保存失败！").show()
            }

        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            finish()
            this@PhotoBrowserActivity.overridePendingTransition(0,R.anim.zoom_exit)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        releaseResource()
        if (mPager != null) {
            mPager!!.removeAllViews()
            mPager = null
        }
        super.onDestroy()
    }

}
