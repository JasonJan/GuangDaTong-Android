package com.guangdamiao.www.mew_android_debug.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.guangdamiao.www.mew_android_debug.R;
import com.guangdamiao.www.mew_android_debug.global.Constant;
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils;

import java.util.List;

import cn.jiguang.analytics.android.api.JAnalyticsInterface;

public class StartLoadingActivity extends Activity {

    private static final int LOAD_DISPLAY_TIME=1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.RGBA_8888);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DITHER);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black);
        setContentView(R.layout.activity_start_loading);

        SharedPreferences read = StartLoadingActivity.this.getSharedPreferences(Constant.Companion.getShareFile_isFirstInstall(), Context.MODE_PRIVATE);
        Boolean isFirst=read.getBoolean("isFirst",true);
        if(isFirst){

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    SharedPreferences.Editor editor = StartLoadingActivity.this.getSharedPreferences(Constant.Companion.getShareFile_isFirstInstall(), Context.MODE_PRIVATE).edit();
                    editor.putBoolean("isFirst",false);
                    editor.commit();
                    //Go to main activity, and finish load activity
                    Intent mainIntent = new Intent(StartLoadingActivity.this, GuideActivity.class);
                    StartLoadingActivity.this.startActivity(mainIntent);
                    StartLoadingActivity.this.finish();
                }
            }, LOAD_DISPLAY_TIME);


        }else{

            Uri uri=getIntent().getData();
            if(uri!=null){
                List<String> pathSegments=uri.getPathSegments();
                String uriQuery=uri.getQuery();
                Intent intent;

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        //Go to main activity, and finish load activity
                        Intent mainIntent = new Intent(StartLoadingActivity.this, NavigationMainActivity.class);
                        if(getIntent().getBundleExtra("message")!=null){
                            mainIntent.putExtra("message",getIntent().getBundleExtra("message"));
                        }else{
                            mainIntent.putExtra("openAPP",true);
                        }
                        if(getIntent().getBundleExtra(Constant.Companion.getJPush_UrgentAsk())!=null){
                            mainIntent.putExtra(Constant.Companion.getJPush_UrgentAsk(),getIntent().getBundleExtra(Constant.Companion.getJPush_UrgentAsk()));
                        }
                        StartLoadingActivity.this.startActivity(mainIntent);
                        StartLoadingActivity.this.finish();
                    }
                }, LOAD_DISPLAY_TIME);

            }else{
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        //Go to main activity, and finish load activity
                        Intent mainIntent = new Intent(StartLoadingActivity.this, NavigationMainActivity.class);
                        if(getIntent().getBundleExtra("message")!=null){
                            mainIntent.putExtra("message",getIntent().getBundleExtra("message"));
                        }else{
                            mainIntent.putExtra("openAPP",true);
                        }
                        if(getIntent().getBundleExtra(Constant.Companion.getJPush_UrgentAsk())!=null){
                            mainIntent.putExtra(Constant.Companion.getJPush_UrgentAsk(),getIntent().getBundleExtra(Constant.Companion.getJPush_UrgentAsk()));
                        }
                        StartLoadingActivity.this.startActivity(mainIntent);
                        StartLoadingActivity.this.finish();
                    }
                }, LOAD_DISPLAY_TIME);
            }
        }

    }

    @Override
    protected  void onResume(){
        JAnalyticsInterface.onPageStart(this,"启动页");
        super.onResume();
    }

    @Override
    protected  void onPause(){
        JAnalyticsInterface.onPageEnd(this,"启动页");
        super.onPause();
    }
}
