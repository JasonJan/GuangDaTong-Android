package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Ask

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainAdpter
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskMainPresenter
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskMainView
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import java.util.*

class Mine_Ask : BaseActivity() , AskMainView {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_imageview: ImageView?=null

    private val listItems = ArrayList<MyAsk>()
    private var mlistView: ListView? = null
    private var adapter: AskMainAdpter? = null
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    private var mPresenter: AskMainPresenter?=null
    var handler = Handler()
    var pageNow=0
    val orderBy=Constant.OrderBy_Default
    val order=Constant.Order_Default
    val TAG=Constant.Me_TAG
    private var no_data_rl: RelativeLayout?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mine_ask)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
         MainApplication.getInstance().addActivity(this)
      
        initView()
        addSomeListener()

        mlistView = findViewById(R.id.list_ask_lv) as ListView
        ptrClassicFrameLayout = findViewById(R.id.list_ask_frame) as PtrClassicFrameLayout
        //设置一个Presenter,本身继承了一个接口
        mPresenter= AskMainPresenter(this)

        adapter = AskMainAdpter(this@Mine_Ask, listItems)
        adapter!!.setAskPresenter(mPresenter!!)//将数据源绑定了Presenter,非常关键
        mlistView!!.setAdapter(adapter)//关键代码
        initData()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Mine_Ask,"我的帖子")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Mine_Ask,"我的帖子")
        super.onPause()
    }

    override fun update2AddZan(position:Int,listener: IDataRequestListener) {
        adapter!!.listAllAsk.get(position).zanIs="1"
        adapter!!.listAllAsk.get(position).zan=(adapter!!.listAllAsk.get(position).zan!!.toInt()+1).toString()
        adapter!!.notifyDataSetChanged()
        listener.loadSuccess(position)
    }

    override fun update2DeleteAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
            val pDialog= SweetAlertDialog(this@Mine_Ask, SweetAlertDialog.SUCCESS_TYPE)
            pDialog.setTitleText("删除成功")
            pDialog.setContentText("亲，您已经成功删除该帖子")
            pDialog.show()
            val handler= Handler()
            handler.postDelayed({
                pDialog.cancel()
            },1236)
        }
    }

    override fun update2ShieldingAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
        }
    }

    fun initView(){
        no_data_rl=findViewById(R.id.no_data_rl) as RelativeLayout
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text="我的帖子"
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView 
        title_center_textview!!.visibility= View.VISIBLE
        title_right_imageview!!.visibility=View.GONE
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initData() {

        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 250)

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                RequestServerList(++pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)

            }, 200)
        })
    }

    private fun RequestServerList(pageNow: Int, orderBy: String, order: String, adapter: AskMainAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
        AskMainRequest.getMyAskFromServer(this@Mine_Ask, TAG, Constant.URL_List_MyAsk, pageNow, orderBy, order,listItems,adapter,ptrClassicFrameLayout,object:IDataRequestListener2String{
            override fun loadSuccess(response_string: String?) {
                if("noMoney".equals(response_string)){
                    no_data_rl!!.visibility=View.VISIBLE
                }else{
                    no_data_rl!!.visibility=View.GONE
                }
            }
        })
    }

}
