package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindFavoriteAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.find.FindWebView
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import java.util.*

/**

 */
class Find_Favorite1Adpter(protected var context: Context, list_partTimeJob: ArrayList<ListFindFavoriteAll>) : BaseAdapter() {

    private var list_partTimeJob:ArrayList<ListFindFavoriteAll>?=null
    private var icon: String? = null
    private var onShowItemClickListener: Find_Favorite1Adpter.OnShowItemClickListener?=null


    init {
        this.list_partTimeJob = list_partTimeJob

    }

    override fun getCount(): Int {
        return list_partTimeJob!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder: Find_Favorite1Adpter.ViewHolder

        if (convertView == null) {
            holder= Find_Favorite1Adpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list, parent, false)
            holder.find_list_icon=convertView.findViewById(R.id.find_list_icon) as ImageView
            holder.find_list_title=convertView.findViewById(R.id.find_list_title) as TextView
            holder.find_list_time1=convertView.findViewById(R.id.find_list_time1) as TextView
            holder.find_list_position1=convertView.findViewById(R.id.find_list_position1) as TextView
            holder.find_list_fee1=convertView.findViewById(R.id.find_list_fee1) as TextView
            holder.partTimeJob_favorite_cb=convertView.findViewById(R.id.partTimeJob_favorite_cb) as CheckBox
            holder.find_list_to=convertView.findViewById(R.id.find_list_to) as ImageView
            holder.partTimeJob_favorite_ll=convertView.findViewById(R.id.partTimeJob_favorite_ll) as LinearLayout
            convertView.tag=holder
        }else{
            holder=convertView.tag as Find_Favorite1Adpter.ViewHolder
        }

        val listpartTimeJob = list_partTimeJob!!.get(position)//获取一个


        val time = listpartTimeJob.createTime!!.substring(0, 19).replace("T", " ")
        LogUtils.d_debugprint(Constant.Me_Favorite_TAG, "兼职发表时间："+time)
        icon = listpartTimeJob.icon
        holder.find_list_title!!.setText(listpartTimeJob.title)
        if(listpartTimeJob.title!!.length>18){
            holder.find_list_title!!.setText(listpartTimeJob!!.title!!.substring(0,18)+"...")
        }
        holder.find_list_time1!!.setText(time)
        holder.find_list_position1!!.setText(listpartTimeJob.position)
        holder.find_list_fee1!!.setText(listpartTimeJob.fee)
        ImageLoader.getInstance().displayImage( icon!!, holder.find_list_icon)

        holder.find_list_icon!!.setOnClickListener{
            if(listpartTimeJob.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listpartTimeJob.icon!!)
        }


        val link = listpartTimeJob.link
        val webView_id = listpartTimeJob.id
        val zan = listpartTimeJob.zan_
        val isZan = listpartTimeJob.isZan
        val readCount = listpartTimeJob.readCount

        holder.partTimeJob_favorite_ll!!.setOnClickListener {

                val intent = Intent(context, FindWebView::class.java)
                val bundle = Bundle()

                bundle.putString("id", webView_id)
                if(webView_id.equals("")){
                    Toasty.info(context,"亲，该内容已被删除了哦~").show()
                }else{
                    bundle.putString("link", link)
                    bundle.putString("zan", zan)
                    bundle.putString("isZan", isZan)
                    bundle.putString("readCount", readCount)
                    bundle.putString("isFavorite", "1")
                    bundle.putString("urlEnd", Constant.Find_PartTimeJob)
                    bundle.putString("title_first", listpartTimeJob.title)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }

        }

        if(listpartTimeJob.isShow!!){
            holder.partTimeJob_favorite_cb!!.visibility=View.VISIBLE
            holder.find_list_to!!.visibility=View.GONE
            holder.partTimeJob_favorite_ll!!.setOnClickListener{
                if(!listpartTimeJob.isChecked!!){
                    listpartTimeJob.isChecked=true
                    holder.partTimeJob_favorite_cb!!.isChecked=true
                }else{
                    holder.partTimeJob_favorite_cb!!.isChecked=false
                    listpartTimeJob.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(listpartTimeJob)
            }
        }else{
            holder.partTimeJob_favorite_cb!!.visibility=View.GONE
            holder.find_list_to!!.visibility=View.VISIBLE
        }


        holder.partTimeJob_favorite_cb!!.isChecked=listpartTimeJob.isChecked!!

        return convertView!!
    }

    class ViewHolder{
        var find_list_icon:ImageView?=null
        var find_list_title:TextView?=null
        var partTimeJob_favorite_cb:CheckBox?=null
        var find_list_time1:TextView?=null
        var find_list_position1:TextView?=null
        var find_list_fee1:TextView?=null
        var find_list_to:ImageView?=null
        var partTimeJob_favorite_ll:LinearLayout?=null
    }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: ListFindFavoriteAll)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }



}
