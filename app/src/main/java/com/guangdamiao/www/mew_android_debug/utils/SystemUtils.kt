package com.guangdamiao.www.mew_android_debug.utils

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail

import com.guangdamiao.www.mew_android_debug.navigation.school.message.Message

/**
 * Created by Jason_Jan on 2017/12/23.
 */

object SystemUtils {
    /**
     * 判断应用是否已经启动
     * @param context 一个context
     * *
     * @param packageName 要判断应用的包名
     * *
     * @return boolean
     */
    fun isAppAlive(context: Context, packageName: String): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val processInfos = activityManager.runningAppProcesses
        for (i in processInfos.indices) {
            if (processInfos[i].processName == packageName) {
                LogUtils.d_debugprint("极光推送NotificationLaunch",
                        String.format("the %s is running, isAppAlive return true", packageName))
                return true
            }
        }
        LogUtils.d_debugprint("极光推送NotificationLaunch",
                String.format("the %s is not running, isAppAlive return false", packageName))
        return false
    }

    fun jumpMessageActivity(context: Context,choose:Int) {
        val intent = Intent(context, Message::class.java)
        intent.putExtra("choose",choose)
        context.startActivity(intent)
    }

    fun jumpLoginMainActivity(context:Context){
        val intent:Intent=Intent(context, LoginMain::class.java)
        context.startActivity(intent)
    }

    fun jumpUrgentAskActivity(context:Context,id:String){
        val intent:Intent=Intent(context, AskDetail::class.java)
        val bundle= Bundle()
        bundle.putString("id",id)
        if(LogUtils.APP_IS_DEBUG){
            bundle.putString("link", Constant.BASEURLFORTEST)
        }else{
            bundle.putString("link", Constant.BASEURLFORRELEASE)
        }
        bundle.putString("publisherID","guangdamiao")
        bundle.putString("isZan","0")
        intent.putExtras(bundle)
        context.startActivity(intent)
    }

}
