package com.guangdamiao.www.mew_android_debug.navigation.ask.presenter;

import android.content.Context;
import android.view.View;

import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentConfig;
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentItem;
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.AskModel;
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener;
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskDetailView;

import java.util.ArrayList;


/**
 * 
* @ClassName: AskMainPresenter
* @Description: 通知model请求服务器和通知view更新
* @author JasonJan
* @date 2017-8-1
*
 */

public class AskDetailPresenter {
	private AskModel mAskModel;
	private AskDetailView mAskDetailView;

	public AskDetailPresenter(AskDetailView view){
		this.mAskDetailView = view;
		mAskModel = new AskModel();
	}
	
	/**
	* @Title: deleteAsk 
	* @Description: 删除动态 
	* @param
	* @return void    返回类型 
	* @throws
	 */
	public void deleteAsk(final Context context,final String token,final ArrayList<String> askIDs){
		mAskModel.deleteAsk(context,token,askIDs,new IDataRequestListener() {
			@Override
			public void loadSuccess(Object object) {
				mAskDetailView.update2DeleteAsk();
			}
		});
	}


	/**
	 * 
	* @Title: addZan 
	* @Description: 点赞
	* @param
	* @return void    返回类型 
	* @throws
	 */
	public void addZan(final String id, final String token, final Context context){
		mAskDetailView.update2AddZan(new IDataRequestListener() {

			@Override
			public void loadSuccess(Object object) {
				mAskModel.addZan(id,token,context);
			}

		});
	}

/*
	*//**
	 *
	 * @Title: addComment
	 * @Description: 增加评论
	 * @param  content
	 * @param  comment  CommentConfig
	 * @return void    返回类型
	 * @throws
	 */
	public void addComment(final Context context,final String content, final CommentConfig comment){
		if(comment == null){
			return;
		}
		mAskModel.addComment(context,comment,new IDataRequestListener() {

			@Override
			public void loadSuccess(Object object) {

				mAskDetailView.update2AddComment();
			}
		});
	}
	
	/**
	 * 
	* @Title: deleteComment 
	* @Description: 删除评论 
	* @param @param AskPosition
	* @param @param commentId     
	* @return void    返回类型 
	* @throws
	 */
	public void deleteComment(final Context context,final CommentItem bean, final int position){
		mAskModel.deleteComment(context,bean,new IDataRequestListener(){

			@Override
			public void loadSuccess(Object object) {
				mAskDetailView.update2DeleteComment(position);
			}
			
		});
	}

	/**
	 * 弹出隐藏编辑框
	 * @param commentConfig
	 */
	public void showEditTextBody(CommentConfig commentConfig){
		mAskDetailView.updateEditTextBodyVisible(View.VISIBLE, commentConfig);
	}


	public void adoptComment(final Context context,final CommentItem bean, final int position){
		mAskModel.adoptComment(context,bean,new IDataRequestListener(){

			@Override
			public void loadSuccess(Object object) {
				mAskDetailView.update2AdoptComment(position);
			}

		});
	}


}
