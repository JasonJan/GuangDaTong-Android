package com.guangdamiao.www.mew_android_debug.navigation;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.guangdamiao.www.mew_android_debug.R;

import java.util.List;

/**
 * Created by Jason_Jan on 2017/12/27.
 */

public class GuidePageAdapter extends PagerAdapter {
    private List<View> viewList;
    private int[] mResIds=new int[]{
            R.mipmap.intro_school,
            R.mipmap.intro_discovery_1,
            R.mipmap.intro_discovery_2,
            R.mipmap.intro_discovery_3,
            R.mipmap.intro_ask_1,
            R.mipmap.intro_ask_2,
            R.mipmap.intro_ask_3,
            R.mipmap.intro_mew_coin,
    };

    public GuidePageAdapter(List<View> viewList) {
        this.viewList = viewList;
    }

    /**
     * @return 返回页面的个数
     */
    @Override
    public int getCount() {
        if (viewList != null){
            return viewList.size();
        }
        return 0;
    }

    /**
     * 判断对象是否生成界面
     * @param view
     * @param object
     * @return
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * 初始化position位置的界面
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //在此设置背景图片，提高加载速度，解决OOM问题
        View view=viewList.get(position);
        int count=getCount();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        view.setBackgroundResource(mResIds[position%count]);
        view.setLayoutParams(params);
        container.addView(viewList.get(position));
        return viewList.get(position);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(viewList.get(position));
    }
}
