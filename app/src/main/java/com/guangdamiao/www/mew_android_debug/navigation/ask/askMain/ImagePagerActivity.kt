package com.guangdamiao.www.mew_android_debug.navigation.ask.askMain

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.*
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import com.nostra13.universalimageloader.core.assist.ImageSize
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import com.nostra13.universalimageloader.utils.MemoryCacheUtils
import es.dmoral.toasty.Toasty
import uk.co.senab.photoview.PhotoView
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class ImagePagerActivity : Activity() {
    private val guideViewList = ArrayList<View>()
    private var guideGroup: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imagepager)
        val viewPager = findViewById(R.id.pager) as ViewPager
        guideGroup = findViewById(R.id.guideGroup) as LinearLayout

        val startPos = intent.getIntExtra(INTENT_POSITION, 0)
        val imgUrls = intent.getStringArrayListExtra(INTENT_IMGURLS)

        val mAdapter = ImageAdapter(this)
        mAdapter.setDatas(imgUrls)
        viewPager.adapter = mAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                for (i in guideViewList.indices) {
                    guideViewList[i].isSelected = if (i == position) true else false
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        viewPager.currentItem = startPos

        addGuideView(guideGroup!!, startPos, imgUrls)
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@ImagePagerActivity,"浏览帖子图片")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@ImagePagerActivity,"浏览帖子图片")
        super.onPause()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            finish()
            overridePendingTransition(0,R.anim.zoom_exit)
        }
        return super.onKeyDown(keyCode, event)
    }



    private fun addGuideView(guideGroup: LinearLayout, startPos: Int, imgUrls: ArrayList<String>?) {
        if (imgUrls != null && imgUrls.size > 0) {
            guideViewList.clear()
            for (i in imgUrls!!.indices) {
                val view = View(this)
                view.setBackgroundResource(R.drawable.selector_guide_bg)
                view.isSelected = if (i == startPos) true else false
                val layoutParams = LinearLayout.LayoutParams(resources.getDimensionPixelSize(R.dimen.gudieview_width),
                        resources.getDimensionPixelSize(R.dimen.gudieview_heigh))
                layoutParams.setMargins(10, 0, 0, 0)
                guideGroup.addView(view, layoutParams)
                guideViewList.add(view)
            }
        }
    }

    private class ImageAdapter(private val context: Context) : PagerAdapter() {

        private var datas: List<String>? = ArrayList()
        private val inflater: LayoutInflater
        private val options: DisplayImageOptions

        fun setDatas(datas: List<String>?) {
            if (datas != null)
                this.datas = datas
        }

        init {
            this.inflater = LayoutInflater.from(context)
            options = DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.mipmap.icon_empty)
                    .showImageOnFail(R.mipmap.icon_empty)
                    .resetViewBeforeLoading(true)
                    .cacheOnDisc(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(FadeInBitmapDisplayer(300))
                    .build()
        }

        override fun getCount(): Int {
            if (datas == null) return 0
            return datas!!.size
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = inflater.inflate(R.layout.item_pager_image, container, false)
            if (view != null) {
                val imageView = view.findViewById(R.id.image) as PhotoView
                val imagePager_btn=view.findViewById(R.id.imagePager_btn) as Button
                //预览imageView
                val smallImageView = ImageView(context)
                val layoutParams = FrameLayout.LayoutParams(imageSize!!.width, imageSize!!.height)
                layoutParams.gravity = Gravity.CENTER
                smallImageView.layoutParams = layoutParams
                smallImageView.scaleType = ImageView.ScaleType.CENTER_CROP
                (view as FrameLayout).addView(smallImageView)

                //loading
                val loading = ProgressBar(context)
                val loadingLayoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT)
                loadingLayoutParams.gravity = Gravity.CENTER
                loading.layoutParams = loadingLayoutParams
                view.addView(loading)

                val imgurl = datas!![position]
                ImageLoader.getInstance().displayImage(imgurl.trim(), imageView, options, object : SimpleImageLoadingListener() {
                    override fun onLoadingStarted(imageUri: String?, view: View?) {
                        //获取内存中的缩略图
                        val memoryCacheKey = MemoryCacheUtils.generateKey(imageUri, imageSize!!)
                        val bmp = ImageLoader.getInstance().memoryCache.get(memoryCacheKey)
                        if (bmp != null && !bmp.isRecycled) {
                            smallImageView.visibility = View.VISIBLE
                            smallImageView.setImageBitmap(bmp)
                        }
                        loading.visibility = View.VISIBLE
                    }

                    override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                        loading.visibility = View.GONE
                        smallImageView.visibility = View.GONE
                    }

                    override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
                        super.onLoadingFailed(imageUri, view, failReason)
                    }
                })

                imageView!!.setOnPhotoTapListener { view, x, y ->
                    if (Activity::class.java.isInstance(context)) {
                        // 转化为activity，然后finish就行了
                        val activity = context as Activity
                        activity.finish()
                        context.overridePendingTransition(0,R.anim.zoom_exit)
                    }
                }

                imagePager_btn!!.setOnClickListener{
                    val bitmapDrawable=imageView!!.drawable as BitmapDrawable
                    val bm=bitmapDrawable.bitmap
                    saveImageToGallery(context,bm)
                }

                container.addView(view, 0)
            }
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

        override fun saveState(): Parcelable? {
            return null
        }

        fun saveImageToGallery(context: Context, bmp: Bitmap) {
            // 首先保存图片
            val appDir = File(Environment.getExternalStorageDirectory(), "mew")
            if (!appDir.exists()) {
                appDir.mkdir()
            }
            val fileName = System.currentTimeMillis().toString() + ".jpg"
            val file = File(appDir, fileName)
            try {
                val fos = FileOutputStream(file)
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                fos.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            // 其次把文件插入到系统图库
            try {
                MediaStore.Images.Media.insertImage(context.contentResolver,
                        file.absolutePath, fileName, null)

            } catch (e: FileNotFoundException) {
                //Toasty.info(context,"亲，保存失败3！保存路径：$fileName").show()
                e.printStackTrace()
            }

            // 最后通知图库更新
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + android.R.attr.path)))
            Toasty.info(context,"亲，保存成功！保存路径：$appDir").show()
        }

    }

    companion object {
        val INTENT_IMGURLS = "imgurls"
        val INTENT_POSITION = "position"
        var imageSize: ImageSize? = null


        fun startImagePagerActivity(context: Context, imgUrls: ArrayList<String>,position: Int) {
            val intent = Intent(context, ImagePagerActivity::class.java)
            intent.putStringArrayListExtra(INTENT_IMGURLS, ArrayList(imgUrls))
            intent.putExtra(INTENT_POSITION, position)
            context.startActivity(intent)
        }
    }
}
