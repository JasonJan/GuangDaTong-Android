package com.guangdamiao.www.mew_android_debug.navigation.school

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Handler
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School1SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School2SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School3SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School4SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

/**
 * 第一个方法是用自己写的网络请求和上拉下拉，觉得不好用，暂时放在这里。
 * 第二个方法是用的第三方库的网络请求asyn-http,以及上拉下拉，觉得还不错。
 * @return
 */
class SameListRequest {

    companion object {

        private var helper1: School1SQLiteOpenHelper?=null
        private var db1: SQLiteDatabase?=null
        private var helper2: School2SQLiteOpenHelper?=null
        private var db2: SQLiteDatabase?=null
        private var helper3: School3SQLiteOpenHelper?=null
        private var db3: SQLiteDatabase?=null
        private var helper4: School4SQLiteOpenHelper?=null
        private var db4: SQLiteDatabase?=null
        

        fun getResultFromServer2(context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<ListSchoolAll>, adapter:MyListViewAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                val school = Constant.SCHOOLDEFULT
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE)
                }

                val jsonObject= JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("school",school)
                jsonObject.put("sign",sign)
                jsonObject.put("token",token)
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG,Constant.TOSERVER+urlPath+"\n提交的参数："+jsonObject.toString())


                val client= AsyncHttpClient(true,80,443)


                    client.post(context,urlPath,stringEntity,Constant.CONTENTTYPE,object: AsyncHttpResponseHandler() {

                        override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                           if(responseBody!=null&&context!=null){
                               val resultDate = String(responseBody!!, charset("utf-8"))
                               val response=JSONObject(resultDate)
                               var getResultFromServer = ""
                               if (response != null) {
                                   getResultFromServer = response.toString()
                               }
                               LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(getResultFromServer) + "\n\n")
                               var getCode: String? = ""
                               var msg: String? = null
                               var result: String
                               var getData: List<Map<String, Any?>>
                               if (JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!) != null) {
                                   getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                               }
                               if (Constant.RIGHTCODE.equals(getCode)) {

                                   msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                   result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                   getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                   LogUtils.d_debugprint(TAG, "json解析出来的对象是\n\n" + getData.toString() + "\n\n")
                                   if (getData != null&&getData.size>0) {
                                       if (pageIndexNow == 0) {
                                           listAll.clear()
                                       }
                                       //清空数据库缓存
                                        if(urlEnd.equals(Constant.School_ListGuidance)){
                                            helper1= School1SQLiteOpenHelper(context)
                                          deleteNowAll(School1SQLiteOpenHelper.S1_Name)
                                      }else if(urlEnd.equals(Constant.School_ListSummary)){
                                            helper2= School2SQLiteOpenHelper(context)
                                            deleteNowAll(School2SQLiteOpenHelper.S2_Name)
                                      }else if(urlEnd.equals(Constant.School_ListClub)){
                                            helper3= School3SQLiteOpenHelper(context)
                                            deleteNowAll(School3SQLiteOpenHelper.S3_Name)
                                      }else if(urlEnd.equals(Constant.School_ListLook)){
                                            helper4= School4SQLiteOpenHelper(context)
                                            deleteNowAll(School4SQLiteOpenHelper.S4_Name)
                                      }


                                       for (i in getData.indices) {
                                           val title_ = getData[i].getValue("title").toString()
                                           val id_ = getData[i].getValue("id").toString()
                                           val createTime_ = getData[i].getValue("createTime").toString()
                                           val type_ = getData[i].getValue("type").toString()
                                           val link_ = getData[i].getValue("link").toString()
                                           val readCount_ = getData[i].getValue("readCount").toString()
                                           val zan_ = getData[i].getValue("zan").toString()
                                           val isZan_ = getData[i].getValue("isZan").toString()
                                           val isFavorite_ = getData[i].getValue("isFavorite").toString()

                                           if (title_ != null && id_ != null && createTime_ != null && type_ != null && link_ != null && readCount_ != null && zan_ != null && isZan_ != null && isFavorite_ != null) {
                                               val listItem = ListSchoolAll()
                                               listItem.title = title_
                                               listItem.id = id_
                                               listItem.createTime = createTime_
                                               listItem.type = type_
                                               listItem.link = link_
                                               listItem.readCount = readCount_
                                               listItem.zan_ = zan_
                                               listItem.isZan = isZan_
                                               listItem.isFavorite = isFavorite_
                                               listItem.urlEnd = urlEnd
                                               listAll.add(listItem)
                                           }

                                           if(urlEnd.equals(Constant.School_ListGuidance)){
                                               helper1= School1SQLiteOpenHelper(context)
                                               if(!hasData(School1SQLiteOpenHelper.S1_Name,id_)){
                                                   insertData(School1SQLiteOpenHelper.S1_Name,id_,title_,createTime_,type_,link_,readCount_,zan_,isZan_,isFavorite_,urlEnd)
                                                   LogUtils.d_debugprint(Constant.School_TAG,"\n在school1中已插入一条数据！！！\n")
                                               }
                                           }else if(urlEnd.equals(Constant.School_ListSummary)){
                                               helper2= School2SQLiteOpenHelper(context)
                                               if(!hasData(School2SQLiteOpenHelper.S2_Name,id_)){
                                                   insertData(School2SQLiteOpenHelper.S2_Name,id_,title_,createTime_,type_,link_,readCount_,zan_,isZan_,isFavorite_,urlEnd)
                                                   LogUtils.d_debugprint(Constant.School_TAG,"\n在school2中已插入一条数据！！！\n")
                                               }
                                           }else if(urlEnd.equals(Constant.School_ListClub)){
                                               helper3= School3SQLiteOpenHelper(context)
                                               if(!hasData(School3SQLiteOpenHelper.S3_Name,id_)){
                                                   insertData(School3SQLiteOpenHelper.S3_Name,id_,title_,createTime_,type_,link_,readCount_,zan_,isZan_,isFavorite_,urlEnd)
                                                   LogUtils.d_debugprint(Constant.School_TAG,"\n在school3中已插入一条数据！！！\n")
                                               }
                                           }else if(urlEnd.equals(Constant.School_ListLook)){
                                               helper4= School4SQLiteOpenHelper(context)
                                               if(!hasData(School4SQLiteOpenHelper.S4_Name,id_)){
                                                   insertData(School4SQLiteOpenHelper.S4_Name,id_,title_,createTime_,type_,link_,readCount_,zan_,isZan_,isFavorite_,urlEnd)
                                                   LogUtils.d_debugprint(Constant.School_TAG,"\n在school4中已插入一条数据！！！\n")
                                               }
                                           }

                                       }
                                      /* if(urlEnd.equals(Constant.School_ListGuidance)){
                                           queryNowAll(School1SQLiteOpenHelper.S1_Name,listAll)
                                       }else if(urlEnd.equals(Constant.School_ListSummary)){
                                           queryNowAll(School2SQLiteOpenHelper.S2_Name,listAll)
                                       }else if(urlEnd.equals(Constant.School_ListClub)){
                                           queryNowAll(School3SQLiteOpenHelper.S3_Name,listAll)
                                       }else if(urlEnd.equals(Constant.School_ListLook)){
                                           queryNowAll(School4SQLiteOpenHelper.S4_Name,listAll)
                                       }*/
                                       adapter!!.notifyDataSetChanged()
                                       if(ptrClassicFrameLayout!=null){
                                           ptrClassicFrameLayout!!.refreshComplete()
                                           ptrClassicFrameLayout!!.loadMoreComplete(true)
                                       }
                                   } else {
                                       ptrClassicFrameLayout!!.refreshComplete()
                                       ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                       ptrClassicFrameLayout!!.loadMoreComplete(false)
                                   }
                               }
                            }
                        }

                        override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                            if(null!=context){
                                ptrClassicFrameLayout!!.refreshComplete()
                                ptrClassicFrameLayout!!.isLoadMoreEnable=false
                                Toasty.error(context,Constant.REQUESTFAIL).show()
                            }
                        }

                    })
            }else{
                if(null!=context){
                    ptrClassicFrameLayout!!.refreshComplete()
                    ptrClassicFrameLayout!!.isLoadMoreEnable=false
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        fun makeFavoriteFromServer(context: Context, TAG: String, urlEnd: String,id:String,token:String,pDialog: SweetAlertDialog) {
            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("id",id)
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            val getResultFromServer=response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String=""
                            var result: String
                            if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                pDialog.cancel()
                                val pSuccess=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                pSuccess.setTitleText("收藏成功")
                                pSuccess.setContentText("亲，您已经成功收藏~")
                                pSuccess.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pSuccess.cancel()
                                },1236)

                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                pDialog.cancel()
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val account = read.getString("account", "")
                                val password=read.getString("password","")
                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()

                                val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                editor2.putString("account",account)
                                editor2.putString("password",password)
                                editor2.commit()

                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                pDialog.cancel()
                                val pFail=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                pFail.setTitleText("收藏失败")
                                pFail.setContentText("亲，您已经收藏过或者信息已经删除")
                                pFail.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pFail.cancel()
                                },1236)
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun makeReport(context:Context,objectID: String,description:String,token: String,urlEnd:String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORTEST+objectID)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+objectID)
                }

                val jsonObject=JSONObject()
                jsonObject.put("objectID", objectID)
                jsonObject.put("description",description)
                jsonObject.put("token",token)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.School_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.School_TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun addZan_requestServer( id: String, token: String,urlEnd:String,context:Context) {

            if(NetUtil.checkNetWork(context)){
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST + token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + token)
                }

                val jsonObject = JSONObject()
                jsonObject.put("id", id)
                jsonObject.put("sign", sign)
                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(Constant.School_TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

                val client = AsyncHttpClient(true, 80, 443)
                client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode==200){
                                val response_string=response.toString()
                                if(JsonUtil.get_key_string("code",response_string).equals(Constant.RIGHTCODE)){
                                    LogUtils.d_debugprint(Constant.School_TAG,"首页内容中 "+urlEnd+"的type中 "+"点赞成功！！！")
                                }else if(JsonUtil.get_key_string("code",response_string).equals(Constant.LOGINFAILURECODE)){
                                    LogUtils.d_debugprint(Constant.School_TAG,"首页内容中 "+urlEnd+"的type中 "+"点赞失败！！！  原因："+response_string)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()

                                }
                            }else{
                                LogUtils.d_debugprint(Constant.School_TAG,"首页内容中 "+urlEnd+"的type中 "+"点赞失败！！！")
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        /**
         * 插入数据
         */
        private fun insertData(table:String,s_id: String,title:String,createTime:String,type:String,link:String,readCount:String,zan:String,isZan:String,isFavorite:String,urlEnd:String) {
            if(table.equals(School1SQLiteOpenHelper.S1_Name)){
                if(helper1 !=null){
                    db1 = helper1!!.writableDatabase
                    val school1Item= ContentValues()
                    school1Item.put("s_id",s_id)
                    school1Item.put("title",title)
                    school1Item.put("createTime",createTime)
                    school1Item.put("type",type)
                    school1Item.put("link",link)
                    school1Item.put("readCount",readCount)
                    school1Item.put("zan",zan)
                    school1Item.put("isZan",isZan)
                    school1Item.put("isFavorite",isFavorite)
                    school1Item.put("urlEnd",urlEnd)
                    db1!!.insert(table,null,school1Item)
                    db1!!.close()
                }
            }else if(table.equals(School2SQLiteOpenHelper.S2_Name)){
                if(helper2 !=null){
                    db2 = helper2!!.writableDatabase
                    val school2Item= ContentValues()
                    school2Item.put("s_id",s_id)
                    school2Item.put("title",title)
                    school2Item.put("createTime",createTime)
                    school2Item.put("type",type)
                    school2Item.put("link",link)
                    school2Item.put("readCount",readCount)
                    school2Item.put("zan",zan)
                    school2Item.put("isZan",isZan)
                    school2Item.put("isFavorite",isFavorite)
                    school2Item.put("urlEnd",urlEnd)
                    db2!!.insert(table,null,school2Item)
                    db2!!.close()
                }
            }else if(table.equals(School3SQLiteOpenHelper.S3_Name)){
                if(helper3 !=null){
                    db3 = helper3!!.writableDatabase
                    val school3Item= ContentValues()
                    school3Item.put("s_id",s_id)
                    school3Item.put("title",title)
                    school3Item.put("createTime",createTime)
                    school3Item.put("type",type)
                    school3Item.put("link",link)
                    school3Item.put("readCount",readCount)
                    school3Item.put("zan",zan)
                    school3Item.put("isZan",isZan)
                    school3Item.put("isFavorite",isFavorite)
                    school3Item.put("urlEnd",urlEnd)
                    db3!!.insert(table,null,school3Item)
                    db3!!.close()
                }
            }else if(table.equals(School4SQLiteOpenHelper.S4_Name)){
                if(helper4 !=null){
                    db4 = helper4!!.writableDatabase
                    val school4Item= ContentValues()
                    school4Item.put("s_id",s_id)
                    school4Item.put("title",title)
                    school4Item.put("createTime",createTime)
                    school4Item.put("type",type)
                    school4Item.put("link",link)
                    school4Item.put("readCount",readCount)
                    school4Item.put("zan",zan)
                    school4Item.put("isZan",isZan)
                    school4Item.put("isFavorite",isFavorite)
                    school4Item.put("urlEnd",urlEnd)
                    db4!!.insert(table,null,school4Item)
                    db4!!.close()
                }
            }
           
        }

        /**
         * 检查数据库中是否已经有该条记录
         */
        private fun hasData(table:String,s_id: String): Boolean {
            if(table.equals(School1SQLiteOpenHelper.S1_Name)){
                val cursor = helper1!!.readableDatabase.rawQuery("select id from $table where s_id =?", arrayOf(s_id))
                return cursor.moveToNext() 
            }else if(table.equals(School2SQLiteOpenHelper.S2_Name)){
                val cursor = helper2!!.readableDatabase.rawQuery("select id from $table where s_id =?", arrayOf(s_id))
                return cursor.moveToNext()
            }else if(table.equals(School3SQLiteOpenHelper.S3_Name)){
                val cursor = helper3!!.readableDatabase.rawQuery("select id from $table where s_id =?", arrayOf(s_id))
                return cursor.moveToNext()
            }else if(table.equals(School4SQLiteOpenHelper.S4_Name)){
                val cursor = helper4!!.readableDatabase.rawQuery("select id from $table where s_id =?", arrayOf(s_id))
                return cursor.moveToNext()
            }
           return false
        }

        //数据库查询
        private fun queryNowAll(table:String,listItems:ArrayList<ListSchoolAll>){
            var cursor: Cursor?=null
            if(table.equals(School1SQLiteOpenHelper.S1_Name)){
                val sql="select * from "+ School1SQLiteOpenHelper.S1_Name+" order by id asc"
                listItems.clear()
                cursor= helper1!!.readableDatabase.rawQuery(sql,null) 
            }else if(table.equals(School2SQLiteOpenHelper.S2_Name)){
                val sql="select * from "+ School2SQLiteOpenHelper.S2_Name+" order by id asc"
                listItems.clear()
                cursor= helper2!!.readableDatabase.rawQuery(sql,null)
            }else if(table.equals(School3SQLiteOpenHelper.S3_Name)){
                val sql="select * from "+ School3SQLiteOpenHelper.S3_Name+" order by id asc"
                listItems.clear()
                cursor= helper3!!.readableDatabase.rawQuery(sql,null)
            }else if(table.equals(School4SQLiteOpenHelper.S4_Name)){
                val sql="select * from "+ School4SQLiteOpenHelper.S4_Name+" order by id asc"
                listItems.clear()
                cursor= helper4!!.readableDatabase.rawQuery(sql,null)
            }
            
            while(cursor!!.moveToNext()){
                val school= ListSchoolAll()
                school.id=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Sid))
                school.title=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Title))
                school.createTime=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_CreateTime))
                school.type=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Type))
                school.link=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Link))
                school.readCount=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_ReadCount))
                school.zan_=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Zan))
                school.isZan=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_IsZan))
                school.urlEnd=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_UrlEnd))
                school.isFavorite=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_IsFavorite))
                
                listItems.add(school)
            }
            LogUtils.d_debugprint(Constant.School_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
        }

        //数据库删除
        private fun deleteNowAll(table:String){
            if(table.equals(School1SQLiteOpenHelper.S1_Name)){
                val sql="delete from "+ School1SQLiteOpenHelper.S1_Name
                val db=helper1!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.School_TAG,"本地数据库删除了school1所有数据！！！")
            }else if(table.equals(School2SQLiteOpenHelper.S2_Name)){
                val sql="delete from "+ School2SQLiteOpenHelper.S2_Name
                val db=helper2!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.School_TAG,"本地数据库删除了school2所有数据！！！")

            }else if(table.equals(School3SQLiteOpenHelper.S3_Name)){
                val sql="delete from "+ School3SQLiteOpenHelper.S3_Name
                val db=helper3!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.School_TAG,"本地数据库删除了school3所有数据！！！")

            }else if(table.equals(School4SQLiteOpenHelper.S4_Name)){
                val sql="delete from "+ School4SQLiteOpenHelper.S4_Name
                val db=helper4!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.School_TAG,"本地数据库删除了school4所有数据！！！")
            }
        }

    }
}
