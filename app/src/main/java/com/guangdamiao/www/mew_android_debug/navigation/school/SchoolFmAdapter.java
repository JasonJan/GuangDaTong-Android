package com.guangdamiao.www.mew_android_debug.navigation.school;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Jason_Jan on 2017/12/9.
 */

public class SchoolFmAdapter extends FragmentPagerAdapter {
    private List<Fragment> fmList;
    private FragmentManager fm;


    public SchoolFmAdapter(FragmentManager fm, List<Fragment> fmList) {
        super(fm);
        this.fm = fm;
        this.fmList = fmList;
    }

    @Override
    public Fragment getItem(int arg0) {
        return fmList.get(arg0%fmList.size());
    }

    @Override
    public int getCount() {
        return fmList.size();
    }
    @Override
    public int getItemPosition(Object object) {
        //没有找到child  要求重新加载
        return POSITION_NONE;
    }
}
