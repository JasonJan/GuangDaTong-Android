package com.guangdamiao.www.mew_android_debug.bean;

/**
 * Created by Jason_Jan on 2017/12/9.
 */

public class HTab {

    private String name;

    public HTab(String name) {
        super();
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
