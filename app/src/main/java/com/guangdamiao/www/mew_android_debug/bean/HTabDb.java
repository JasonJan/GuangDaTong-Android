package com.guangdamiao.www.mew_android_debug.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jason_Jan on 2017/12/9.
 */

public class HTabDb {
    private static final List<HTab> Selected = new ArrayList<HTab>();
    private static final List<HTab> MessageSelected = new ArrayList<HTab>();
    private static final List<HTab> FindSelected=new ArrayList<HTab>();
    private static final List<HTab> FavoriteSelected=new ArrayList<HTab>();

    static{
        Selected.add(new HTab("特别推荐"));
        Selected.add(new HTab("图文咨询"));
        Selected.add(new HTab("最近更新"));
        Selected.add(new HTab("本月热点"));

    }

    static{
        MessageSelected.add(new HTab("留言"));
        MessageSelected.add(new HTab("帖子"));
        MessageSelected.add(new HTab("通知"));

    }

    static{
        FindSelected.add(new HTab("面试"));
        FindSelected.add(new HTab("学习"));
        FindSelected.add(new HTab("职场"));

    }
    
    static{
        FavoriteSelected.add(new HTab("帖子"));
        FavoriteSelected.add(new HTab("特别推荐"));
        FavoriteSelected.add(new HTab("图文咨询"));
        FavoriteSelected.add(new HTab("最近更新"));
        FavoriteSelected.add(new HTab("本月热点"));
        FavoriteSelected.add(new HTab("面试"));
        FavoriteSelected.add(new HTab("学习"));
        FavoriteSelected.add(new HTab("职场"));
    }


    public static List<HTab> getSelected() {
        return Selected;
    }
    public static List<HTab> getMessageSelected() {return MessageSelected;}
    public static List<HTab> getFindSelected() {return FindSelected;}
    public static List<HTab> getFavoriteSelected(){return FavoriteSelected;}

}
