package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.ask


import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.favorite_bean.MyFavoriteAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import es.dmoral.toasty.Toasty
import java.util.*


/**
 * Created by Jason_Jan on 2017/12/26.
 */

class AskFavoriteFm : Fragment() ,AskFavoriteAdpter.OnShowItemClickListener{

    private var name: String? = null
    private var mlistView: ListView? = null

    var pageNow=0
    val TAG=Constant.Me_Favorite_TAG
    var loading_first:SweetAlertDialog?=null
    var flag=0

    var adapter: AskFavoriteAdpter? = null
    var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()

    private var isShow=false
    private var opreateView: LinearLayout?=null
    private var selectList=ArrayList<MyFavoriteAsk>()
    private var dataList=ArrayList<MyFavoriteAsk>()
    private var lay:LinearLayout?=null
    //监听我的收藏页面的收藏按钮是否点击
    private var title_right_imageview_btn: Button?=null
    private var title_right_textview: TextView?=null
    private var no_data_rl: RelativeLayout?=null

    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view_ = inflater!!.inflate(R.layout.fragment_favorite_new, container, false)


        lay=activity.findViewById(R.id.lay) as LinearLayout
        no_data_rl=view_.findViewById(R.id.no_data_rl) as RelativeLayout
        title_right_imageview_btn=activity.findViewById(R.id.title_right_imageview_btn) as Button
        title_right_textview=activity.findViewById(R.id.title_right_textview) as TextView

        mlistView = view_!!.findViewById(R.id.list_favorite_new) as ListView
        ptrClassicFrameLayout = view_!!.findViewById(R.id.list_view_frame) as PtrClassicFrameLayout
        adapter= AskFavoriteAdpter(context, dataList)
        mlistView!!.setAdapter(adapter!!)
        adapter!!.setOnShowItemClickListener(this)

        initData()
        addSomeListener()

        return view_
    }

    override fun onResume() {

        super.onResume()
    }


    private fun addSomeListener(){

      title_right_imageview_btn!!.setOnClickListener{
          title_right_textview!!.text=""
          //刷新列表，处理删除的情况
          if(isShow){
              isShow=false
          }else{
              isShow=true
              for(bean in dataList){
                  bean.isShow=true
              }
              adapter!!.notifyDataSetChanged()
              showOpervate()
              title_right_imageview_btn!!.isClickable=false
          }
      }

    }

    private fun initData() {

        if(flag==0){
           /* loading_first= SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
            loading_first!!.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
            loading_first!!.setTitleText("Loading")
            loading_first!!.setCancelable(false)
            loading_first!!.show()*/
            flag++
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
        }

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 100)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                RequestServerList(++pageNow,adapter!!,ptrClassicFrameLayout!!)

            }, 100)
        })
    }

    private fun RequestServerList(pageNow: Int, adapter: AskFavoriteAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
        Ask_ListRequest.getMyFavoriteFromServer(activity,context, TAG, Constant.URL_Favorite_Ask, pageNow,dataList,adapter,ptrClassicFrameLayout,loading_first,object:IDataRequestListener2String{
            override fun loadSuccess(response_string: String?) {
                if("noAskFavorite".equals(response_string)){
                    no_data_rl!!.visibility=View.VISIBLE
                }else{
                    no_data_rl!!.visibility=View.GONE
                }
            }
        })
    }

    override fun onShowItemClick(bean: MyFavoriteAsk) {
        if (bean.isChecked!! && !selectList.contains(bean)) {
            selectList.add(bean)
        } else if (!bean.isChecked!! && selectList.contains(bean)) {
            selectList.remove(bean)
        }
    }


    /**
     * 显示操作界面
     */
    private fun showOpervate() {
        lay!!.visibility=View.VISIBLE
        val anim = AnimationUtils.loadAnimation(context, R.anim.operate_in)
        lay!!.animation=anim
        // 返回、删除、全选和反选按钮初始化及点击监听
        val tvBack = activity.findViewById(R.id.operate_back) as TextView
        val tvDelete = activity.findViewById(R.id.operate_delete) as TextView
        val tvSelect = activity.findViewById(R.id.operate_select) as TextView
        val tvInvertSelect = activity.findViewById(R.id.invert_select) as TextView

        tvBack.setOnClickListener {
            if (isShow) {
                selectList.clear()
                for (bean in dataList) {
                    bean.isChecked=false
                    bean.isShow=false
                }
                isShow = false
                title_right_textview!!.text="删除"
                title_right_textview!!.visibility=View.VISIBLE
                title_right_imageview_btn!!.isClickable=true
                adapter!!.notifyDataSetChanged()
                dismissOperate()
            }
        }
        tvSelect.setOnClickListener {
            for (bean in dataList) {
                if (!bean.isChecked!!) {
                    bean.isChecked=true
                    if (!selectList.contains(bean)) {
                        selectList.add(bean)
                    }
                }
            }
            adapter!!.notifyDataSetChanged()
        }
        tvInvertSelect.setOnClickListener {
            for (bean in dataList) {
                if (!bean.isChecked!!) {
                    bean.isChecked=true
                    if (!selectList.contains(bean)) {
                        selectList.add(bean)
                    }
                } else {
                    bean.isChecked=false
                    if (selectList.contains(bean)) {
                        selectList.remove(bean)
                    }
                }
            }
            adapter!!.notifyDataSetChanged()
        }
        tvDelete.setOnClickListener {
            /*if(lay!=null) dismissOperate()*/
            if (selectList != null && selectList.size > 0) {
                val IDs=ArrayList<String>()
                for(i in selectList.indices){
                    IDs.add(selectList[i].favorID!!)
                }
                val token=get_token()
                if(!token.equals("")){
                    //这里进行数据库的删除
                    val pDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
                    pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                    pDialog.titleText = "Loading"
                    pDialog.setCancelable(false)
                    pDialog.show()
                   /* val handler=Handler()

                    handler.postDelayed({
                        if(pDialog!=null) pDialog.cancel()
                    },6180)*/

                    Ask_ListRequest.deleteFavoriteAsk(context,IDs,token,object: IDataRequestListener2String {
                        override fun loadSuccess(response_string: String?) {
                            if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                if(pDialog!=null) pDialog.cancel()
                                val pDialog2= SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                pDialog2.setTitleText("删除成功")
                                pDialog2.setContentText("亲，您已经成功删除收藏的帖子")
                                pDialog2.show()
                                val handler=Handler()
                                handler.postDelayed({
                                    pDialog2.cancel()
                                },1236)
                                if (isShow) {
                                    selectList.clear()
                                    for (bean in dataList) {
                                        bean.isChecked=false
                                        bean.isShow=false
                                    }
                                    isShow = false
                                    title_right_textview!!.text="删除"
                                    title_right_textview!!.visibility=View.VISIBLE
                                    title_right_imageview_btn!!.isClickable=true
                                    adapter!!.notifyDataSetChanged()
                                    dismissOperate()
                                }
                            }else if(!JsonUtil.get_key_string("msg",response_string).equals("")){
                                if(pDialog!=null) pDialog.cancel()
                                val pDialog2= SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                pDialog2.setTitleText("删除失败")
                                pDialog2.setContentText("亲，${JsonUtil.get_key_string("msg",response_string)}")
                                pDialog2.show()
                                val handler=Handler()
                                handler.postDelayed({
                                    pDialog2.cancel()
                                },1236)
                                if (isShow) {
                                    selectList.clear()
                                    for (bean in dataList) {
                                        bean.isChecked=false
                                        bean.isShow=false
                                    }
                                    isShow = false
                                    title_right_textview!!.text="删除"
                                    title_right_textview!!.visibility=View.VISIBLE
                                    title_right_imageview_btn!!.isClickable=true
                                    adapter!!.notifyDataSetChanged()
                                    dismissOperate()
                                }
                            }
                        }
                    })

                    dataList.removeAll(selectList)
                    adapter!!.notifyDataSetChanged()
                    selectList.clear()
                }
            } else {
                Toasty.info(context,"亲，您还没有选择要删除的帖子喔").show()
            }
        }
    }

    private fun get_token():String{
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(context, LoginMain::class.java)
            context.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    /**
     * 隐藏操作界面
     */
    private fun dismissOperate() {
        val anim = AnimationUtils.loadAnimation(context, R.anim.operate_out)
        lay!!.visibility=View.GONE
        lay!!.animation=anim
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if(!isVisibleToUser){
            JAnalyticsInterface.onPageEnd(context,"收藏帖子")
            if (isShow) {
                selectList.clear()
                for (bean in dataList) {
                    bean.isChecked=false
                    bean.isShow=false
                }
                isShow = false
                title_right_textview!!.text="删除"
                title_right_textview!!.visibility=View.VISIBLE
                title_right_imageview_btn!!.isClickable=true
                adapter!!.notifyDataSetChanged()
                dismissOperate()
            }
        }else{
            JAnalyticsInterface.onPageStart(context,"收藏帖子")
        }
        super.setUserVisibleHint(isVisibleToUser)
    }

}
