package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class ListSchoolAll {

    var title: String?=""
    var id: String?=""
    var createTime: String?= ""
    var type:String?=""
    var link:String?=""
    var readCount:String?=""
    var zan_:String?=""
    var isZan:String?=""
    var isFavorite:String?=""
    var urlEnd:String?=""

    constructor():super()
    constructor(createTime: String,title:String,id:String,type:String,link:String,readCount:String,zan_:String,isZan:String,isFavorite:String,urlEnd:String) : super() {
        this.createTime = createTime
        this.title=title
        this.id=id
        this.type=type
        this.link=link
        this.readCount=readCount
        this.zan_=zan_
        this.isZan=isZan
        this.isFavorite=isFavorite
        this.urlEnd=urlEnd
    }

    override fun toString(): String {
        return "ListSchoolAll[title=$title,id=$id,createTime=$createTime,type=$type,link:String,readCount=$readCount,zan_=$zan_,isZan=$isZan,isFavorite=$isFavorite,urlEnd=$urlEnd]"
    }
}
