package com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class RecordSQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "askSearch.db"
        private val version = 1
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table records(id integer primary key autoincrement,name varchar(200),type varchar(50),label varchar(50))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

}
