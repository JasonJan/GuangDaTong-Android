package com.guangdamiao.www.mew_android_debug.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.Comment
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyDetailAsk
import com.guangdamiao.www.mew_android_debug.bean.favorite_bean.MyFavoriteAsk
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class JsonUtil {
    companion object {

        //将Json数据解析成相应的映射对象
        fun <T> parseJsonWithGson(jsonData: String, type: Class<T>): T {
            val gson = Gson()
            val result = gson.fromJson(jsonData, type)
            return result
        }

        // 将Json数组解析成相应的映射对象列表
        fun <T> parseJsonArrayWithGson(jsonData: String, type: Class<T>): List<T> {
            val gson = Gson()
            val result = gson.fromJson<List<T>>(jsonData, object : TypeToken<List<T>>() {}.type)
            return result
        }

        /**
         * 获取String数组数据
         * @param key
         * *
         * @param jsonString
         * *
         * @return
         */
        fun getList(key: String, jsonString: String): List<String> {
            val list = ArrayList<String>()
            try {
                val jsonObject = JSONObject(jsonString)
                val jsonArray = jsonObject.getJSONArray(key)
                for (i in 0..jsonArray.length() - 1) {
                    val msg = jsonArray.getString(i)
                    list.add(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return list
        }

        /**
         * 获取对象的Map集合数据
         * @param key
         * *
         * @param jsonString
         * *
         * @return
         */
        fun getListMap(key: String, jsonString: String): List<Map<String, Any>> {
            val list = ArrayList<Map<String, Any>>()
            try {
                val jsonObject = JSONObject(jsonString)
                val jsonArray = jsonObject.getJSONArray(key)
                for (i in 0..jsonArray.length() - 1) {
                    val jsonObject2 = jsonArray.getJSONObject(i)
                    val map = HashMap<String, Any>()
                    val iterator = jsonObject2.keys()
                    while (iterator.hasNext()) {
                        val json_key = iterator.next()
                        var json_value: Any? = jsonObject2.get(json_key)
                        if (json_value == null) {
                            json_value = ""
                        }
                        map.put(json_key, json_value)
                    }
                    list.add(map)
                }
            } catch (e: JSONException) {

                e.printStackTrace()
            }
            return list
        }


        fun get_ask_favorite(key:String,jsonString:String): ArrayList<MyFavoriteAsk>? {
            var myAsk=ArrayList<MyFavoriteAsk>()
            try{
                val jsonObject=JSONObject(jsonString)
                val jsonArray=jsonObject.getJSONArray(key)
                for(i in 0..jsonArray.length()-1){
                    val json_ask=jsonArray.getJSONObject(i)
                    var my_ask= MyFavoriteAsk()
                    val content=json_ask.getString("content")
                    val createTime=json_ask.getString("createTime")
                    val gender=json_ask.getString("gender")
                    val icon=json_ask.getString("icon")
                    val id=json_ask.getString("id")
                    val isZan=json_ask.getString("isZan")
                    val label=json_ask.getString("label")
                    val link=json_ask.getString("link")
                    val nickname=json_ask.getString("nickname")
                    val publisherID=json_ask.getString("publisherID")
                    val reward=json_ask.getString("reward")
                    val type=json_ask.getString("type")
                    val title=json_ask.getString("title")
                    val favorID=json_ask.getString("favorID")

                    my_ask.favorID=favorID
                    my_ask.content=content
                    my_ask.createTime=createTime
                    my_ask.gender=gender
                    my_ask.icon=icon.trim()
                    my_ask.id=id
                    my_ask.zanIs=isZan
                    my_ask.label=label
                    my_ask.link=link
                    my_ask.nickname=nickname
                    my_ask.publisherID=publisherID
                    my_ask.reward=reward
                    my_ask.type=type
                    my_ask.title=title

                    myAsk?.add(my_ask)
                }
                return myAsk
            }catch (e:JSONException){
                e.printStackTrace()
            }
            return myAsk
        }


        fun get_ask_Item(key:String,jsonString:String): ArrayList<MyAsk>? {
            var myAsk=ArrayList<MyAsk>()
            try{
                val jsonObject=JSONObject(jsonString)
                val jsonArray=jsonObject.getJSONArray(key)
                for(i in 0..jsonArray.length()-1){
                    val json_ask=jsonArray.getJSONObject(i)
                    var my_ask= MyAsk()
                    val commentCount=json_ask.getString("commentCount")
                    val commentID=json_ask.getString("commentID")
                    val content=json_ask.getString("content")
                    val createTime=json_ask.getString("createTime")
                    val gender=json_ask.getString("gender")
                    val icon=json_ask.getString("icon")
                    val id=json_ask.getString("id")
                    val isFavorite=json_ask.getString("isFavorite")
                    val isZan=json_ask.getString("isZan")
                    val label=json_ask.getString("label")
                    val link=json_ask.getString("link")
                    val nickname=json_ask.getString("nickname")
                    val publisherID=json_ask.getString("publisherID")
                    val reward=json_ask.getString("reward")
                    val type=json_ask.getString("type")
                    val zan=json_ask.getString("zan")
                    val title=json_ask.getString("title")


                    val image_array=json_ask.getJSONArray("image")
                    var image_list:ArrayList<String>?= ArrayList()
                    for(j1 in 0..image_array.length()-1){
                        val image_item=image_array.getString(j1)
                        if(LogUtils.APP_IS_DEBUG){
                            image_list?.add(image_item)
                        }else{
                            image_list?.add(image_item)
                        }
                    }
                    val thumbnailImage_array=json_ask.getJSONArray("thumbnailImage")
                    var image_list2:ArrayList<String>?= ArrayList()
                    for(j2 in 0..thumbnailImage_array.length()-1){
                        val image_item=thumbnailImage_array.getString(j2)
                        if(LogUtils.APP_IS_DEBUG){
                            image_list2?.add(image_item)
                        }else{
                            image_list2?.add(image_item)
                        }
                    }
                    my_ask.commentID=commentID
                    my_ask.commentCount=commentCount
                    my_ask.content=content
                    my_ask.createTime=createTime
                    my_ask.gender=gender
                    my_ask.icon=icon
                    my_ask.id=id
                    my_ask.favoriteIs=isFavorite
                    my_ask.zan=zan
                    my_ask.zanIs=isZan
                    my_ask.label=label
                    my_ask.link=link
                    my_ask.nickname=nickname
                    my_ask.publisherID=publisherID
                    my_ask.reward=reward
                    my_ask.type=type
                    my_ask.title=title
                    my_ask.image=image_list
                    my_ask.thumbnailImage=image_list2

                    myAsk?.add(my_ask)
                }
                return myAsk
            }catch (e:JSONException){
                e.printStackTrace()
            }
            return myAsk
        }

        fun get_ask_detail_Item(key:String,jsonString:String): MyDetailAsk {
            var myDetailAsk=MyDetailAsk()
            try{

                    val jsonObject_string=get_key_string(key,jsonString)
                    val jsonObject=JSONObject(jsonObject_string)

                    val commentCount=jsonObject.getString("commentCount")
                    val content=jsonObject.getString("content")
                    val createTime=jsonObject.getString("createTime")
                    var gender=""
                    if(jsonObject.getString("gender")!=null){
                        gender=jsonObject.getString("gender")
                    }
                    val icon=jsonObject.getString("icon")
                    val link=jsonObject.getString("link")
                    val id=jsonObject.getString("id")
                    val isFavorite=jsonObject.getString("isFavorite")
                    val isZan=jsonObject.getString("isZan")
                    val label=jsonObject.getString("label")
                    val nickname=jsonObject.getString("nickname")
                    val publisherID=jsonObject.getString("publisherID")
                    val reward=jsonObject.getString("reward")
                    val type=jsonObject.getString("type")
                    val zan=jsonObject.getString("zan")
                    val title=jsonObject.getString("title")


                    val image_array=jsonObject.getJSONArray("image")
                    var image_list:ArrayList<String>?= ArrayList()
                    for(j1 in 0..image_array.length()-1){
                        val image_item=image_array.getString(j1)
                        if(LogUtils.APP_IS_DEBUG){
                            image_list?.add(image_item)
                        }else{
                            image_list?.add(image_item)
                        }
                    }
                    val thumbnailImage_array=jsonObject.getJSONArray("thumbnailImage")
                    var image_list2:ArrayList<String>?= ArrayList()
                    for(j2 in 0..thumbnailImage_array.length()-1){
                        val image_item=thumbnailImage_array.getString(j2)
                        if(LogUtils.APP_IS_DEBUG){
                            image_list2?.add(image_item)
                        }else{
                            image_list2?.add(image_item)
                        }
                    }

                    //这里明天继续写，主要是写采纳的评论
                    var adviseComment= Comment()
                    if(jsonObject.has("adviseComment")){
                        val comment_object=jsonObject.getJSONObject("adviseComment")
                        val id2=comment_object.getString("id")
                        val publisherID2=comment_object.getString("publisherID")
                        val createTime2=comment_object.getString("createTime")
                        val icon2=comment_object.getString("icon")
                        val nickname2=comment_object.getString("nickname")
                        val content2=comment_object.getString("content")
                        var gender2=""
                        if(comment_object.has("gender")){
                            gender2=comment_object.getString("gender")
                        }
                        adviseComment.id=id2
                        adviseComment.publisherID=publisherID2
                        adviseComment.createTime=createTime2
                        adviseComment.icon=icon2
                        adviseComment.nickname=nickname2
                        adviseComment.content=content2
                        adviseComment.gender=gender2
                        //添加到myDetailAsk中
                        myDetailAsk.adviseComment=adviseComment
                    }else{
                        myDetailAsk.adviseComment=null
                    }
                    
                    myDetailAsk.commentCount=commentCount
                    myDetailAsk.content=content
                    myDetailAsk.createTime=createTime
                    myDetailAsk.gender=gender
                    myDetailAsk.icon=icon
                    myDetailAsk.id=id
                    myDetailAsk.link=link
                    myDetailAsk.favoriteIs=isFavorite
                    myDetailAsk.zan=zan
                    myDetailAsk.zanIs=isZan
                    myDetailAsk.label=label
                    myDetailAsk.nickname=nickname
                    myDetailAsk.publisherID=publisherID
                    myDetailAsk.reward=reward
                    myDetailAsk.type=type
                    myDetailAsk.title=title
                    myDetailAsk.image=image_list
                    myDetailAsk.thumbnailImage=image_list2


                return myDetailAsk
            }catch (e:JSONException){
                e.printStackTrace()
            }
            return myDetailAsk
        }


        fun get_key_string(key:String,jsonString:String):String {
            var str: String = ""

            try {

               val jsonObj:JSONObject= JSONObject(jsonString)
                str=jsonObj.getString(key)


            } catch (e: JSONException) {
                e.printStackTrace()
            }
           return str
        }

        fun get_key_boolean(key:String,jsonString:String):Boolean {
            var str: Boolean =true
            try {
                val jsonObj:JSONObject= JSONObject(jsonString)
                str=jsonObj.getBoolean(key)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return str
        }

        fun get_key_int(key:String,jsonString:String):Int {
            var str: Int=0
            try {
                val jsonObj:JSONObject= JSONObject(jsonString)
                str=jsonObj.getInt(key)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return str
        }

        fun toUtf8(str:String):String{
            var result=""
            try{

                result=String(str.toByteArray(), charset("UTF-8"))

            }catch (e:UnsupportedEncodingException){
                e.printStackTrace()
            }
            return result
        }


    }

}
