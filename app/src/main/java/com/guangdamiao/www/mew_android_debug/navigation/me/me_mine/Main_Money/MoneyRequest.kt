package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyMoney
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */


class MoneyRequest {

    companion object {

        fun requestFromServer(context: Context, pageIndexNow: Int, listAll: ArrayList<MyMoney>, adapter: MoneyAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout, listener:IDataRequestListener2String) {

            if (NetUtil.checkNetWork(context)) {
                val pageSize= Constant.PAGESIZE
                val sign:String
                val urlPath:String
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if(LogUtils.APP_IS_DEBUG){
                    sign= MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath= Constant.BASEURLFORTEST + Constant.URL_Mine_Account_Book
                }else{
                    sign= MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath= Constant.BASEURLFORRELEASE + Constant.URL_Mine_Account_Book
                }

                val jsonObject=JSONObject()
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("pageIndex",pageIndexNow)
                jsonObject.put("orderBy",Constant.OrderBy_Default)
                jsonObject.put("order",Constant.Order_Default)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")

                LogUtils.d_debugprint(Constant.Me_TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：\n" + jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            LogUtils.d_debugprint(Constant.Me_TAG, response!!.toString())
                            val resultFromServer=response!!.toString()
                            LogUtils.d_debugprint(Constant.Me_TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(resultFromServer!!)+"\n\n")
                            var getCode: String = ""
                            getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!)
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                var result: String = ""
                                var getData: List<Map<String, Any?>>
                                result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServer!!)
                                getData = JsonUtil.getListMap("data", result)
                                LogUtils.d_debugprint(Constant.Me_TAG, "json解析出来的对象是=" + JSONFormatUtil.formatJson(getData.toString()))

                                if (getData != null && getData.size>0) {
                                    if(pageIndexNow==0){
                                        listAll.clear()
                                    }
                                    for (i in getData.indices) {
                                        //LogUtils.d_debugprint(Constant.Me_TAG,"\n现在i是$i")
                                        val myMoney = MyMoney()
                                        if (getData[i].getValue("createTime")!= null && getData[i].getValue("description")!= null && getData[i].getValue("amount")!= null ) {
                                            myMoney.createTime = getData[i].getValue("createTime").toString()
                                            myMoney.description = getData[i].getValue("description").toString()
                                            myMoney.amount = getData[i].getValue("amount").toString()
                                            listAll.add(myMoney)
                                        }
                                    }
                                    listener.loadSuccess(result)
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                } else if(pageIndexNow==0&&getData.size==0){
                                    listener.loadSuccess("noMoney")
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    //Toasty.info(context,"亲，您目前还没有金币记录哦~快去分享赢金币吧~").show()
                                }else{
                                    listener.loadSuccess(result)
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                }
                            }else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val account=read.getString("account","")
                                val password=read.getString("password","")
                                val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                editor2.putString("account",account)
                                editor2.putString("password",password)
                                editor2.commit()
                                editor.clear()
                                editor.commit()
                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            //跳转到登录，不用传东西过去
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                Toasty.info(context, Constant.SERVERBROKEN).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                         ptrClassicFrameLayout!!.refreshComplete()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }

    }
}
