package com.guangdamiao.www.mew_android_debug.loginActivity

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.Loading_view
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class TermServiceActivity : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var urlFirst:String?=null
    private var urlPath:String?=null
    private var webView_content: WebView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        setContentView(R.layout.me_webview)
        initView()
        addSomeListener()
        getUrl()
    }


    private fun getUrl(){
        if(LogUtils.APP_IS_DEBUG){
            urlFirst= Constant.BASEURLFORTEST
        }else{
            urlFirst= Constant.BASEURLFORRELEASE
        }
        urlPath=urlFirst!!
        val loading_view= Loading_view(this@TermServiceActivity,R.style.CustomDialog)
        loading_view.show()
        if(urlPath!=null&&!urlPath.equals("")){
            val wvcc: WebChromeClient =object: WebChromeClient(){
                override fun onReceivedTitle(view: WebView?, title: String?) {
                    if(loading_view!=null) loading_view.cancel()
                    super.onReceivedTitle(view, title)
                }
            }
            webView_content!!.setWebChromeClient(wvcc)
            webView_content!!.loadUrl(urlPath)
            webView_content!!.setWebViewClient(object: WebViewClient(){
                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    if(loading_view!=null) loading_view.cancel()
                    super.onReceivedError(view, request, error)

                }
            })
        }
    }

    fun initView() {
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text = "服务条款"
        title_center_textview!!.visibility = View.VISIBLE
        webView_content = findViewById(R.id.webView_content) as WebView
        val webSettings: WebSettings =webView_content!!.settings
        webSettings.javaScriptEnabled=true
    }


    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
