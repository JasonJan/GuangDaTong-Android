package com.guangdamiao.www.mew_android_debug.navigation.school

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import java.util.*

class MyListViewAdpter(protected var context: Context, list_guidance: ArrayList<ListSchoolAll>) : BaseAdapter() {
    private var list_guidance: ArrayList<ListSchoolAll>? =null

    init {
        this.list_guidance = list_guidance
    }

    override fun getCount(): Int {
        return list_guidance!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.school_list, parent, false)
        }
        val listGuidance = list_guidance!!.get(position)//获取一个
        val status_view = ViewHolder.get<ImageView>(convertView, R.id.school_list_status)
        val title_view = ViewHolder.get<TextView>(convertView, R.id.school_list_content)
        val school_favorite_rl = ViewHolder.get<RelativeLayout>(convertView, R.id.school_favorite_rl)


        if (listGuidance.type == "NEW") {
            status_view.setImageResource(R.mipmap.school_new)
            status_view.visibility = View.VISIBLE
        } else if (listGuidance.type == "HOT") {
            status_view.setImageResource(R.mipmap.school_hot)
            status_view.visibility = View.VISIBLE
        } else {
            //没有就不设置new或者hot的图标
            status_view.visibility = View.GONE
        }


        title_view.text=listGuidance.title
        if(listGuidance.title!!.length>20){
            title_view.text=listGuidance.title!!.substring(0,18)+"..."
        }

        val link = listGuidance.link
        val webView_id = listGuidance.id

        val zan = listGuidance.zan_
        val isZan = listGuidance.isZan
        val readCount = listGuidance.readCount
        val isFavorite = listGuidance.isFavorite

        LogUtils.d_debugprint(Constant.School_TAG,"当前选中的listSchool为：${listGuidance.toString()}")

        convertView!!.setOnClickListener {
            val intent = Intent(context, SchoolWebView::class.java)//=====跳转到WebView
            val bundle = Bundle()
            bundle.putInt("position",position)
            bundle.putString("id", webView_id)
            bundle.putString("link", link)
            bundle.putString("zan", zan)
            bundle.putString("isZan", isZan)
            bundle.putString("readCount", readCount)
            bundle.putString("isFavorite", isFavorite)
            bundle.putString("urlEnd", listGuidance.urlEnd)
            bundle.putString("title_first", title_view!!.text!!.toString())


            intent.putExtras(bundle)
            context.startActivity(intent)
            //这里处理点击事件
        }



        return convertView
    }
}
