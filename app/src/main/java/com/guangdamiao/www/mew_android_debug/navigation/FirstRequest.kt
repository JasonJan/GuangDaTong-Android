package com.guangdamiao.www.mew_android_debug.navigation

import android.content.Context
import android.content.SharedPreferences
import com.guangdamiao.www.mew_android_debug.bean.MyLocation
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.utils.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class FirstRequest {

    companion object {
        fun toServerLocationAndID1(context: Context,regID:String,userID:String,location: MyLocation) {

            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + Constant.URL_PushInfo
                    sign = MD5Util.md5(Constant.SALTFORTEST + regID)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + Constant.URL_PushInfo
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + regID)
                }

                val jsonObject = JSONObject()
                jsonObject.put("regID", regID)
                jsonObject.put("userID",userID)
                jsonObject.put("OSType","android")
                if(!location.country.equals("")){
                    jsonObject.put("country",location.country)
                    jsonObject.put("province",location.province)
                    jsonObject.put("city",location.city)
                    jsonObject.put("district",location.district)
                    jsonObject.put("county",location.county)
                    jsonObject.put("street",location.street)
                    jsonObject.put("siteName",location.siteName)
                }
                jsonObject.put("sign", sign)
                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(Constant.School_TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

                val client = AsyncHttpClient(true, 80, 443)
                client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                       if(responseBody!=null){
                           val resultDate = String(responseBody!!, charset("utf-8"))
                           val response=JSONObject(resultDate)
                           if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                               val response_string = response.toString()
                               if (JsonUtil.get_key_string("code", response_string).equals(Constant.RIGHTCODE)) {
                                   LogUtils.d_debugprint(Constant.School_TAG, "NavigationMainActivity中：上传推送ID和位置信息成功！！！\n")
                               }
                           }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        LogUtils.d_debugprint(Constant.School_TAG, "NavigationMainActivity中：上传推送ID和位置信息失败！！！\n")
                    }

                })
            } else {
                //Toasty.info(context, Constant.NONETWORK).show()
            }
        }

        fun toServerLocationAndID( context: Context, regID:String, userID:String, location: MyLocation) {

            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + Constant.URL_PushInfo
                    sign = MD5Util.md5(Constant.SALTFORTEST + regID)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + Constant.URL_PushInfo
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + regID)
                }

                val version=Common.getVersion(context)
                val params=mapOf<String,String>("regID" to regID,"userID" to userID,"OSType" to "android","version" to version,"sign" to sign,"country" to location.country!!,"province" to location.province!!,
                        "city" to location.city!!,"district" to location.district!!,"county" to location.county!!,"street" to location.street!!,"siteName" to location.siteName!!)

                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_Location, Context.MODE_PRIVATE).edit();
                editor.putString("country",location.county)
                editor.putString("province",location.province)
                editor.putString("city",location.city)
                editor.putString("district",location.district)
                editor.putString("county",location.county)
                editor.putString("street",location.street)
                editor.putString("siteName",location.siteName)
                editor.commit()

                LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的params=" + params.toString())
                Thread(Runnable {
                    var getResultFromServer=""
                    HttpUtil2.httpPost(urlPath, params, "UTF-8",object: HttpCallBackListener {
                        override fun onSuccess(response: String) {
                            getResultFromServer = response
                            LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.GETDATAFROMSERVER + getResultFromServer)
                            if(JsonUtil.get_key_string(Constant.Server_Code,getResultFromServer).equals(Constant.RIGHTCODE)){
                                LogUtils.d_debugprint(Constant.LoginMain_Tag, "推送ID和位置信息上传成功！！！\n\n")
                            }else{
                                LogUtils.d_debugprint(Constant.LoginMain_Tag, "推送ID和位置信息上传失败！！！")
                            }
                        }

                        override fun onError() {
                            LogUtils.d_debugprint(Constant.LoginMain_Tag, "推送ID和位置信息上传失败！！！\n\n")
                        }
                    })
                }).start()

            } else {
                //Toasty.info(context, Constant.NONETWORK).show()
            }
        }

    }
}
