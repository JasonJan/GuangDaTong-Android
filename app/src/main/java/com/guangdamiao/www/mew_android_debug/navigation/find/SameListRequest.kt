package com.guangdamiao.www.mew_android_debug.navigation.find

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.widget.BaseAdapter
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find1SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find2SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find3SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class SameListRequest {

    companion object {

        private var helper1: Find1SQLiteOpenHelper?=null
        private var db1: SQLiteDatabase?=null
        private var helper2: Find2SQLiteOpenHelper?=null
        private var db2: SQLiteDatabase?=null
        private var helper3: Find3SQLiteOpenHelper?=null
        private var db3: SQLiteDatabase?=null
      

        fun getResultFromServer(context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, orderBy:String,order:String,listAll: ArrayList<ListFindAll>,adapter:BaseAdapter,ptrClassicFrameLayout: PtrClassicFrameLayout) {
            if (NetUtil.checkNetWork(context)) {
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                val school = Constant.SCHOOLDEFULT
                val encode = Constant.ENCODE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE)
                }
                val jsonObject= JSONObject()
                 jsonObject.put("pageIndex",pageIndex)
                 jsonObject.put("pageSize",pageSize)
                 jsonObject.put("orderBy",orderBy)
                 jsonObject.put("order",order)
                 jsonObject.put("school",school)
                 jsonObject.put("sign",sign)
                 jsonObject.put("token",token)
                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                    client.post(context,urlPath,stringEntity,Constant.ContentType+Constant.Chartset,object: AsyncHttpResponseHandler(){

                        override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                            if(responseBody!=null&&context!=null){
                                val resultDate = String(responseBody!!, charset("utf-8"))
                                val response=JSONObject(resultDate)
                                val getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(getResultFromServer)+"\n\n")
                                var getCode: String=""
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                    getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                }
                                if (Constant.RIGHTCODE.equals(getCode)) {
                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString()+"\n\n")
                                    if(pageIndexNow==0){
                                        listAll.clear()
                                    }
                                    //清空数据库缓存
                                    if(urlEnd.equals(Constant.Find_PartTimeJob)){
                                         helper1 = Find1SQLiteOpenHelper(context)
                                         deleteNowAll(Find1SQLiteOpenHelper.F1_Name)
                                    }else if(urlEnd.equals(Constant.Find_Training)){
                                         helper2 = Find2SQLiteOpenHelper(context)
                                         deleteNowAll(Find2SQLiteOpenHelper.F2_Name)
                                    }else if(urlEnd.equals(Constant.Find_Activity)){
                                         helper3 = Find3SQLiteOpenHelper(context)
                                         deleteNowAll(Find3SQLiteOpenHelper.F3_Name)
                                    }


                                    if (getData != null&&getData.size>0) {
                                        for (i in getData.indices) {
                                            val title_=getData[i].getValue("title").toString()
                                            val id_=getData[i].getValue("id").toString()
                                            val createTime_=getData[i].getValue("createTime").toString()
                                            val link_=getData[i].getValue("link").toString()
                                            val readCount_=getData[i].getValue("readCount").toString()
                                            val zan_=getData[i].getValue("zan").toString()
                                            val isZan_=getData[i].getValue("isZan").toString()
                                            val isFavorite_=getData[i].getValue("isFavorite").toString()
                                            val icon_=getData[i].getValue("icon").toString()
                                            val fee_=getData[i].getValue("fee").toString()
                                            val position_=getData[i].getValue("position").toString()
                                            val description_=getData[i].getValue("description").toString()

                                            val listItem = ListFindAll()
                                            listItem.title = if (title_==null) "" else title_
                                            listItem.id = if(id_==null) "" else id_
                                            listItem.createTime = if(createTime_==null) "" else createTime_
                                            listItem.fee =if(fee_==null) "" else fee_
                                            listItem.link=if(link_==null) "" else link_
                                            listItem.position=if(position_==null) "" else position_
                                            listItem.description=if(description_==null) "" else description_
                                            listItem.icon=if(icon_==null) "" else icon_
                                            listItem.readCount=if(readCount_==null) "" else readCount_
                                            listItem.zan_=if(zan_==null) "" else zan_
                                            listItem.isZan=if(isZan_==null) "" else isZan_
                                            listItem.isFavorite=if(isFavorite_==null) "" else isFavorite_

                                            listAll.add(listItem)

                                            if(urlEnd.equals(Constant.Find_PartTimeJob)){
                                                 helper1 =  Find1SQLiteOpenHelper(context)
                                                 insertData( Find1SQLiteOpenHelper.F1_Name, id_, icon_, title_, createTime_, position_, fee_, description_, link_, readCount_, zan_,isZan_,isFavorite_)
                                                LogUtils.d_debugprint(Constant. Find_TAG,"\n在 Find1中已插入一条数据！！！\n")

                                            }else if(urlEnd.equals(Constant.Find_Training)){
                                                 helper2 =  Find2SQLiteOpenHelper(context)
                                                insertData( Find2SQLiteOpenHelper.F2_Name, id_, icon_, title_, createTime_, position_, fee_, description_, link_, readCount_, zan_,isZan_,isFavorite_)
                                                LogUtils.d_debugprint(Constant. Find_TAG,"\n在 Find2中已插入一条数据！！！\n")

                                            }else if(urlEnd.equals(Constant.Find_Activity)){
                                                 helper3 =  Find3SQLiteOpenHelper(context)
                                                insertData( Find3SQLiteOpenHelper.F3_Name, id_, icon_, title_, createTime_, position_, fee_, description_, link_, readCount_, zan_,isZan_,isFavorite_)
                                                LogUtils.d_debugprint(Constant. Find_TAG,"\n在 Find3中已插入一条数据！！！\n")

                                            }
                                        }

                                        adapter!!.notifyDataSetChanged()
                                        if(ptrClassicFrameLayout!=null){
                                            ptrClassicFrameLayout!!.refreshComplete()
                                            ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        }
                                    } else {
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                    }
                                }
                             }
                        }

                        override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                            if(null!=context){
                                ptrClassicFrameLayout!!.refreshComplete()
                                ptrClassicFrameLayout!!.isLoadMoreEnable=false
                                Toasty.error(context,Constant.REQUESTFAIL).show()
                            }
                        }

                  })
            } else {
                if(null!=context){
                    ptrClassicFrameLayout!!.refreshComplete()
                    ptrClassicFrameLayout!!.isLoadMoreEnable=false
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        fun addZan_requestServer( id: String, token: String,urlEnd:String,context:Context) {

            if(NetUtil.checkNetWork(context)){
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST + token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + token)
                }

                val jsonObject = JSONObject()
                jsonObject.put("id", id)
                jsonObject.put("sign", sign)
                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(Constant.Find_TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

                val client = AsyncHttpClient(true, 80, 443)
                client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode==200){
                                val response_string=response.toString()
                                if(JsonUtil.get_key_string("code",response_string).equals(Constant.RIGHTCODE)){
                                    LogUtils.d_debugprint(Constant.Find_TAG,"栏目内容中 "+urlEnd+"的type中 "+"点赞成功！！！")
                                }else if(JsonUtil.get_key_string("code",response_string).equals(Constant.LOGINFAILURECODE)){
                                    LogUtils.d_debugprint(Constant.Find_TAG,"栏目内容中 "+urlEnd+"的type中 "+"点赞失败！！！  原因："+response_string)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()

                                }
                            }else{
                                LogUtils.d_debugprint(Constant.Find_TAG,"栏目内容中 "+urlEnd+"的type中 "+"点赞失败！！！")
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun makeContact(context:Context,objectID: String,words:String,token: String,urlEnd:String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("objectID", objectID)
                jsonObject.put("words",words)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.School_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.School_TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          listener.loadSuccess("failed")
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun makeApply(context:Context,id: String,name:String,phone: String,participationCount:String,qq:String,weChat:String,token:String,urlEnd:String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id", id)
                jsonObject.put("name",name)
                jsonObject.put("phone",phone)
                jsonObject.put("participationCount",participationCount)
                jsonObject.put("qq",qq)
                jsonObject.put("weChat",weChat)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Find_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.Find_TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          listener.loadSuccess("failed")


                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        /**
         * 插入数据
         */
        private fun insertData(table:String,s_id: String,icon:String,title:String,createTime:String,position:String,fee:String,description:String,link:String,readCount:String,zan:String,isZan:String,isFavorite:String) {
            if(table.equals(Find1SQLiteOpenHelper.F1_Name)){
                if( helper1 !=null){
                     db1 =  helper1!!.writableDatabase
                     val find1Item= ContentValues()
                     find1Item.put("s_id",s_id)
                     find1Item.put("icon",icon)
                     find1Item.put("title",title)
                     find1Item.put("createTime",createTime)
                     find1Item.put("position",position)
                     find1Item.put("fee",fee)
                     find1Item.put("description",description)
                     find1Item.put("link",link)
                     find1Item.put("readCount",readCount)
                     find1Item.put("zan",zan)
                     find1Item.put("isZan",isZan)
                     find1Item.put("isFavorite",isFavorite)
                     db1!!.insert(table,null,find1Item)
                     db1!!.close()
                }
            }else if(table.equals(Find2SQLiteOpenHelper.F2_Name)){
                if( helper2 !=null){
                    db2 =  helper2!!.writableDatabase
                    val find2Item= ContentValues()
                    find2Item.put("s_id",s_id)
                    find2Item.put("icon",icon)
                    find2Item.put("title",title)
                    find2Item.put("createTime",createTime)
                    find2Item.put("position",position)
                    find2Item.put("fee",fee)
                    find2Item.put("description",description)
                    find2Item.put("link",link)
                    find2Item.put("readCount",readCount)
                    find2Item.put("zan",zan)
                    find2Item.put("isZan",isZan)
                    find2Item.put("isFavorite",isFavorite)
                    db2!!.insert(table,null,find2Item)
                    db2!!.close()
                }
            }else if(table.equals(Find3SQLiteOpenHelper.F3_Name)){
                if( helper3 !=null){
                    db3 =  helper3!!.writableDatabase
                    val find3Item= ContentValues()
                    find3Item.put("s_id",s_id)
                    find3Item.put("icon",icon)
                    find3Item.put("title",title)
                    find3Item.put("createTime",createTime)
                    find3Item.put("position",position)
                    find3Item.put("fee",fee)
                    find3Item.put("description",description)
                    find3Item.put("link",link)
                    find3Item.put("readCount",readCount)
                    find3Item.put("zan",zan)
                    find3Item.put("isZan",isZan)
                    find3Item.put("isFavorite",isFavorite)
                    db3!!.insert(table,null,find3Item)
                    db3!!.close()
                }
            }
        }
       
        //数据库删除
        private fun deleteNowAll(table:String){
            if(table.equals(Find1SQLiteOpenHelper.F1_Name)){
                val sql="delete from "+ Find1SQLiteOpenHelper.F1_Name
                val db=  helper1!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.Find_TAG,"本地数据库删除了Find1所有数据！！！")
            }else if(table.equals(Find2SQLiteOpenHelper.F2_Name)){
                val sql="delete from "+ Find2SQLiteOpenHelper.F2_Name
                val db=  helper2!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.Find_TAG,"本地数据库删除了Find2所有数据！！！")

            }else if(table.equals(Find3SQLiteOpenHelper.F3_Name)){
                val sql="delete from "+ Find3SQLiteOpenHelper.F3_Name
                val db=  helper3!!.writableDatabase
                db!!.execSQL(sql)
                db!!.close()
                LogUtils.d_debugprint(Constant.Find_TAG,"本地数据库删除了Find3所有数据！！！")

            }
        }
    }
}
