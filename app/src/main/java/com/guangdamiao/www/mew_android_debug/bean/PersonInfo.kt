package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class PersonInfo {

    var id: String? = ""
    var icon:String?=""
    var level:String?=""
    var zan:String?=""
    var nickname:String?=""
    var gender:String?=""
    var coins:String?=""
    var school:String?=""
    var college:String?=""
    var grade:String?=""
    var zanIs:String?=""

    constructor():super()
    constructor(id: String,icon:String,level:String,zan:String,nickname:String,gender:String,coins:String,school:String,college:String,grade:String,zanIs:String) : super() {
        this.id = id
        this.icon=icon
        this.level=level
        this.zan=zan
        this.nickname=nickname
        this.gender=gender
        this.coins=coins
        this.school=school
        this.college=college
        this.grade=grade
        this.zanIs=zanIs
    }

    override fun toString(): String {
        return "PersonInfo[id=$id,icon=$icon,level=$level,zan=$zan,nickname=$nickname,gender=$gender,coins=$coins,school=$school,college=$college,grade=$grade,zanIs=$zanIs]"
    }
}
