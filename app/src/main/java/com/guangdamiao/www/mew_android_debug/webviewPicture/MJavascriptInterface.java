package com.guangdamiao.www.mew_android_debug.webviewPicture;

import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;

import com.guangdamiao.www.mew_android_debug.global.LogUtils;
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jason_Jan on 2017/12/17.
 */

public class MJavascriptInterface {
    private Context context;
    private ArrayList<String> imageUrls_temp;
    private String[] imageUrls;
    private int curImageUrl_Index;

    public MJavascriptInterface(Context context,ArrayList<String> imageUrls) {
        this.context = context;
        this.imageUrls_temp = imageUrls;
        /*this.imageUrls=new String[3];
        for(int i=0;i<imageUrls_temp.size();i++){
            this.imageUrls[i]=imageUrls_temp.get(i);
        }*/
    }

    @android.webkit.JavascriptInterface
    @SuppressWarnings("unused")
    public void openImage(int i) {
        if(this.imageUrls!=null&&this.imageUrls[curImageUrl_Index]!=null){
            Intent intent = new Intent();
            intent.putExtra("imageUrls", this.imageUrls);
            intent.putExtra("curImageUrl", this.imageUrls[curImageUrl_Index]);
            intent.setClass(context, PhotoBrowserActivity.class);
            context.startActivity(intent);
        }
    }

   /* @android.webkit.JavascriptInterface
    @SuppressWarnings("unused")
    public void showSource(String html){
        LogUtils.INSTANCE.d_debugprint("WebView源码","\n当前WebView源码为："+html+"\n");
        StringUtils.sourceHtml=html;
        this.imageUrls_temp=StringUtils.returnImageUrlsFromHtml();
        this.imageUrls=new String[imageUrls_temp.size()];
        for(int i=0;i<imageUrls_temp.size();i++){
            this.imageUrls[i]=imageUrls_temp.get(i);
        }
    }*/

    @JavascriptInterface
    @SuppressWarnings("unused")
    public void getPicturesUrl(String json){
        try{
            JSONObject jsonObject=new JSONObject(json);
            JSONArray jsonArray = new JSONObject(json).getJSONArray("imageURLArray");
            curImageUrl_Index= JsonUtil.Companion.get_key_int("imageIndex",json);
            this.imageUrls_temp.clear();
            this.imageUrls=null;
            LogUtils.INSTANCE.d_debugprint("webView","\n在InJavaScriptGetPictruesUrl中，调用了js后的返回值为："+json);
            for (int i=0;i<jsonArray.length();i++) {
                String item = jsonArray.getString(i);
                this.imageUrls_temp.add(item);
            }
            this.imageUrls=new String[imageUrls_temp.size()];
            for(int i=0;i<imageUrls_temp.size();i++){
                this.imageUrls[i]=imageUrls_temp.get(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

}
