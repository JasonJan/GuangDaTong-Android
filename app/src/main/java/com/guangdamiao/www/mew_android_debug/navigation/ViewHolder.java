package com.guangdamiao.www.mew_android_debug.navigation;

import android.util.SparseArray;
import android.view.View;
/**
 * Created by Jason_Jan on 2017/12/16.
 */

/**
 * 函数作用：通过一个id直接返回相关视图
 */
public class ViewHolder {
    public static <T extends View> T get(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            view.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }
        return (T) childView;
    }
}
