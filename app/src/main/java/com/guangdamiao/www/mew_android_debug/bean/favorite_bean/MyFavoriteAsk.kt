package com.guangdamiao.www.mew_android_debug.bean.favorite_bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyFavoriteAsk {

    var favorID:String?=null
    var id: String? = null
    var createTime:String?=null
    var publisherID:String?=null
    var nickname:String?=null
    var icon:String?=null
    var gender:String?=null
    var reward:String?=null
    var type:String?=null
    var label:String?=null
    var content:String?=null
    var link:String?=null
    var title:String?=null
    var zanIs:String?=null
    var isShow:Boolean?=false
    var isChecked:Boolean?=false

    constructor():super()
    constructor(isShow:Boolean,isChecked:Boolean,favorID:String,title:String,id:String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,type:String,label:String,content:String,link:String,isZan:String){
        this.favorID=favorID
        this.id=id
        this.title=title
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.type=type
        this.label=label
        this.content=content
        this.link=link
        this.zanIs=isZan
        this.gender=gender
        this.isShow=isShow
        this.isChecked=isChecked
    }

    override fun toString(): String {
        return "\n\n\nMyFavoriteAsk[\nfavorID=$favorID,\nid=$id,\ntitle=$title,\ncreateTime=$createTime,\npublisherID=$publisherID,\nnickname=$nickname,\nicon=$icon,\nreward=$reward,\ntype=$type,\nlabel=$label,\ncontent=$content,\nlink=$link,\nisZan=$zanIs,\nisShow=$isShow,\nisChecked=$isChecked]"
    }
}
