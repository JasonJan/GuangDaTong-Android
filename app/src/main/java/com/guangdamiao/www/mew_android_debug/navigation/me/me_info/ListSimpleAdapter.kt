package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R


/**
 * Created by Jason_Jan on 2017/12/24.
 */
 class ListSimpleAdapter(private val context: Context, private val items: ArrayList<String>,private val old_value:String) : BaseAdapter() {
    private val mInflater: LayoutInflater//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    private var choose_status:ImageView?=null
    init {
        mContext = context
        mInflater = LayoutInflater.from(mContext)
    }
    override fun getItem(arg0: Int): Any {

        return items!![arg0]
    }
    override fun getItemId(position: Int): Long {

        return position.toLong()
    }
    override fun getCount(): Int {

        return items!!.size
    }
    override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
        var convertView = convertView
        val indexText: TextView
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.choose_item, null)
        }
        indexText = convertView!!.findViewById(R.id.choose_listitem) as TextView
        indexText.text = items!![position]
        choose_status=convertView.findViewById(R.id.choose_status) as ImageView
        if(indexText.text.equals(old_value)){
            choose_status!!.visibility= View.VISIBLE
        }else{
            choose_status!!.visibility= View.GONE
        }
        indexText.setTextColor(Color.BLACK)
        return convertView
    }
}

