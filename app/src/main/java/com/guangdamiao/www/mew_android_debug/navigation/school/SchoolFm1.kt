package com.guangdamiao.www.mew_android_debug.navigation.school


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School1SQLiteOpenHelper


/**
 * A simple [Fragment] subclass.
 */
class SchoolFm1 : Fragment() {

    private var name: String? = null
    private val listItems = ArrayList<ListSchoolAll>()
    private var mlistView: ListView? = null
    private var adapter: MyListViewAdpter? = null
    private val TAG = Constant.School_TAG
    private var pageNow = 0
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()

    var flag=0

    private var helper1: School1SQLiteOpenHelper?=null

    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view_ = inflater!!.inflate(R.layout.fragment_school_all, container, false)

        helper1= School1SQLiteOpenHelper(context)
        mlistView = view_!!.findViewById(R.id.list_find_new) as ListView
        ptrClassicFrameLayout = view_!!.findViewById(R.id.list_find_frame) as PtrClassicFrameLayout


        adapter = MyListViewAdpter(context, listItems)
        mlistView!!.setAdapter(adapter!!)//关键代码

        initData()

        return view_
    }

    override fun onStart() {
        val read = context.getSharedPreferences("update_school1", Context.MODE_PRIVATE)
        val update_position = read.getInt("position", 0)
        val update_id=read.getString("id","")
        val update_isZan=read.getString("isZan","")
        val update_readCount=read.getString("readCount","")
        val update_zan=read.getString("zan","")
        if(listItems.size>0&&listItems.size-1>=update_position){
            if(listItems[update_position]!=null&&listItems[update_position].id!!.equals(update_id)){
                listItems[update_position].zan_=update_zan
                listItems[update_position].isZan=update_isZan
                listItems[update_position].readCount=update_readCount

                adapter!!.notifyDataSetChanged()
            }
        }
        super.onStart()
    }

    private fun initData() {

        if(flag==0){

            flag++
            queryNowAll(School1SQLiteOpenHelper.S1_Name,listItems)
            adapter!!.notifyDataSetChanged()
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(true) }, 150)

        }


        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                     if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                         ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                     }
                }, 382)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener{
                handler.postDelayed(
                        {
                            //请求更多页
                            RequestServerList(++pageNow, adapter!!, ptrClassicFrameLayout!!)
                        }, 382)
        }
    }


    private fun RequestServerList(pageNow: Int,adapter:MyListViewAdpter,ptrClassicFrameLayout:PtrClassicFrameLayout) {
        SameListRequest.getResultFromServer2(context, TAG, Constant.School_ListGuidance, pageNow, listItems,adapter,ptrClassicFrameLayout)
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            JAnalyticsInterface.onPageStart(context,"首页导航")
        }else{
            JAnalyticsInterface.onPageEnd(context,"首页导航")
        }
    }

    //数据库查询
    private fun queryNowAll(table:String,listItems:ArrayList<ListSchoolAll>){


        val sql="select * from "+School1SQLiteOpenHelper.S1_Name+" order by id asc"
        listItems.clear()
        val cursor= helper1!!.readableDatabase.rawQuery(sql,null)

        while(cursor.moveToNext()){
            val school= ListSchoolAll()
            school.id=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Sid))
            school.title=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Title))
            school.createTime=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_CreateTime))
            school.type=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Type))
            school.link=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Link))
            school.readCount=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_ReadCount))
            school.zan_=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_Zan))
            school.isZan=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_IsZan))
            school.urlEnd=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_UrlEnd))
            school.isFavorite=cursor.getString(cursor.getColumnIndex(School1SQLiteOpenHelper.S1_IsFavorite))

            listItems.add(school)
        }
        LogUtils.d_debugprint(Constant.School_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }



}
