package com.guangdamiao.www.mew_android_debug.navigation.ask.modle;


import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyDetailAsk;

/**
 * 
* @ClassName: IDataRequestListener 
* @Description: 请求后台数据服务器响应后的回调 
* @author yiw
* @date 2017-8-1
*
 */
public interface DetailRequestListener {

	public void loadSuccess(MyDetailAsk myDetailAsk, int pageNow);

}
