package com.guangdamiao.www.mew_android_debug.overrideView

import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil

/**
 * Created by Jason_Jan on 2017/6/30.
 */

class EditTextWithDel2 : EditText {
    private var imgInable: Drawable? = null
    /* private val imgAble: Drawable? = null*/
    private var mContext: Context? = null


    constructor(context: Context) : super(context) {
        mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context
        init()
    }




    private fun init() {
        imgInable = mContext!!.resources.getDrawable(R.mipmap.input_clear)
        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (length() < 1) {
                    setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                } else {
                    setCompoundDrawablesWithIntrinsicBounds(null, null, imgInable, null)
                }
            }
        })

    }
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (imgInable != null && event.action == MotionEvent.ACTION_UP) {
            val rect = Rect()
            getGlobalVisibleRect(rect)
            rect.left = rect.right - DensityUtil.dip2px(context,160.0f)
            rect.right=rect.left+DensityUtil.dip2px(context,130.0f)
            if (rect.contains(event.rawX.toInt(), event.rawY.toInt())) setText("")
        }
        return super.onTouchEvent(event)
    }
}
