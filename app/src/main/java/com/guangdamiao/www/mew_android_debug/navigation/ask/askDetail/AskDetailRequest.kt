package com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentItem
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyDetailAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.DetailRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskDetailPresenter
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.liaoinstan.springview.widget.SpringView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/3.
 */

class AskDetailRequest {

    companion object {
         fun getCommentFromServer(askPublishID:String,isAdopt:Boolean,isAdoptID:String,context: Context, TAG: String, urlEnd: String, askID:String,pageNow:Int,pageSize:Int,orderBy:String,order:String,listAll:ArrayList<CommentItem>,adapter:CommentAdapter,springView:SpringView,mPresenter:AskDetailPresenter,listener:IDataRequestListener2String){
             if (NetUtil.checkNetWork(context)) {
                 var sign = ""
                 var urlPath = ""
                 if (LogUtils.APP_IS_DEBUG) {
                     urlPath = Constant.BASEURLFORTEST + urlEnd
                     sign = MD5Util.md5(Constant.SALTFORTEST+askID)
                 } else {
                     urlPath = Constant.BASEURLFORRELEASE + urlEnd
                     sign = MD5Util.md5(Constant.SALTFORRELEASE+askID)
                 }

                 val jsonObject = JSONObject()
                 jsonObject.put("id",askID)
                 jsonObject.put("pageIndex",pageNow)
                 jsonObject.put("pageSize",pageSize)
                 jsonObject.put("orderBy",orderBy)
                 jsonObject.put("order",order)
                 jsonObject.put("sign", sign)

                 val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                 LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                 val client = AsyncHttpClient(true, 80, 443)

                 client.post(context, urlPath, stringEntity, Constant.ContentType + Constant.Chartset, object : AsyncHttpResponseHandler() {

                     override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                         if(responseBody!=null){
                             val resultDate = String(responseBody!!, charset("utf-8"))
                             val response=JSONObject(resultDate)
                             val getResultFromServer = response.toString()
                             LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(getResultFromServer)+"\n\n")
                             var getCode: String = ""
                             var msg: String? = null
                             var result: String
                             var getData: List<Map<String, Any?>>
                             if (JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!) != null) {
                                 getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                             }
                             if (Constant.RIGHTCODE.equals(getCode)) {
                                 result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                 getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                 LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString()+"\n\n")
                                 if(listAll.size>0){
                                     for(i in listAll.indices){
                                         LogUtils.d_debugprint(TAG,"之前的评论 "+i+" 为："+listAll[i].toString()+"\n\n")
                                     }
                                 }
                                 if(pageNow==0){
                                     listAll.clear()
                                 }
                                 if (getData != null) {
                                     for (i in getData.indices) {
                                         val id=getData[i].getValue("id").toString()
                                         val commentReceiverID=getData[i].getValue("commentReceiverID").toString()
                                         val commmentReceiverNickname=getData[i].getValue("commentReceiverNickname").toString()
                                         val publisherID=getData[i].getValue("publisherID").toString()
                                         val createTime=getData[i].getValue("createTime").toString()
                                         val icon=getData[i].getValue("icon").toString()
                                         val nickname=getData[i].getValue("nickname").toString()
                                         val content=getData[i].getValue("content").toString()
                                         var gender=""
                                         if(getData[i].getValue("gender")!=null&&!getData[i].getValue("gender").equals("")){
                                             gender=getData[i].getValue("gender").toString()
                                         }

                                         val listItem = CommentItem()
                                         listItem.id=id
                                         listItem.askID=askID
                                         listItem.commentReceiverID=commentReceiverID
                                         listItem.commentReceiverNickname=commmentReceiverNickname
                                         listItem.publisherID=publisherID
                                         listItem.createTime=createTime
                                         listItem.icon=icon
                                         listItem.nickname=nickname
                                         listItem.content=content
                                         listItem.gender=gender

                                         listAll.add(listItem)
                                     }
                                     if(listAll.size>0){
                                         adapter!!.set_Datas(listAll,askPublishID,isAdopt,isAdoptID,mPresenter)
                                     }
                                     springView!!.onFinishFreshAndLoad()//停止刷新
                                     listener.loadSuccess(result)//方便AskDeatail中修改holder.isLoading的值
                                     adapter!!.notifyDataSetChanged()

                                 }else{
                                     Toasty.info(context, "亲，没有更多的评论了").show()
                                 }

                             } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                 val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                 val account=read.getString("account","")
                                 val password=read.getString("password","")
                                 val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                 val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                 editor2.putString("account",account)
                                 editor2.putString("password",password)
                                 editor2.commit()
                                 editor.clear()
                                 editor.commit()
                                 SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                         .setTitleText(Constant.LOGINFAILURETITLE)
                                         .setContentText(Constant.LOGINFAILURE)
                                         .setCustomImage(R.mipmap.mew_icon)
                                         .setConfirmText("确定")
                                         .setConfirmClickListener { sDialog ->
                                             sDialog.dismissWithAnimation()
                                             //跳转到登录，不用传东西过去
                                             val intent = Intent(context, LoginMain::class.java)
                                             context.startActivity(intent)
                                         }
                                         .setCancelText("取消")
                                         .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                         .show()
                             }else{
                                 Toasty.info(context, "亲，帖子已经被删除啦！").show()
                             }
                          }
                     }

                     override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                           Toasty.error(context,Constant.REQUESTFAIL).show()
                     }

                 })
             } else {
                 Toasty.info(context, Constant.NONETWORK).show()
             }
         }

         fun getAskDetailServer(activity: Activity, context: Context, TAG: String, urlEnd: String, id:String, pageNow:Int, springView: SpringView, successListener: DetailRequestListener) {
            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+id)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+id)
                }

                val jsonObject = JSONObject()

                jsonObject.put("sign", sign)
                jsonObject.put("id",id)

                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client = AsyncHttpClient(true, 80, 443)

                client.post(context, urlPath, stringEntity, Constant.ContentType + Constant.Chartset, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            springView.onFinishFreshAndLoad()//停止刷新
                            val getResultFromServer = response.toString()
                            LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                            var getCode: String = ""

                            if (JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!) != null) {
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                            }
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                var askDetail: MyDetailAsk? = null
                                askDetail = JsonUtil.get_ask_detail_Item(Constant.Server_Result, getResultFromServer)
                                LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + askDetail.toString() + "\n")
                                if (null!=askDetail) {

                                    successListener.loadSuccess(askDetail,pageNow)
                                    //updateView(askDetail,pageNow)

                                } else {
                                    Toasty.info(context, "没有更多数据了").show()
                                }
                            } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                val token = read.getString("token", "")
                                val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                editor.clear()
                                editor.commit()
                                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setTitleText(Constant.LOGINFAILURETITLE)
                                        .setContentText(Constant.LOGINFAILURE)
                                        .setCustomImage(R.mipmap.mew_icon)
                                        .setConfirmText("确定")
                                        .setConfirmClickListener { sDialog ->
                                            sDialog.dismissWithAnimation()
                                            //跳转到登录，不用传东西过去
                                            val intent = Intent(context, LoginMain::class.java)
                                            context.startActivity(intent)
                                        }
                                        .setCancelText("取消")
                                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                        .show()
                            }else{
                                Toasty.info(context,"亲，帖子已经被删除啦！").show()
                                if(activity!=null) activity.finish()
                                activity.overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }
    }
}
