package com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo

import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.school.SameListRequest
import com.guangdamiao.www.mew_android_debug.navigation.school.message.Message1SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class WordsActivity : BaseActivity() {
    
    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_imageview: ImageView?=null
    private var title_right_imageview_btn:Button?=null

    private var words_et: EditText?=null
    private var word_num_tv: TextView?=null
    private var remin_num= Constant.EDIT_MAX_WORDS
    private var getBundle:Bundle?=null

    private var ID=""
    private var token=""
    private var centerWords=""
    private var rightWords=""
    private var helper1: Message1SQLiteOpenHelper?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_words)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        getBundle=intent.extras
        helper1= Message1SQLiteOpenHelper(this@WordsActivity)
        initView()
        addSomeListener()
    }

    override fun onResume(){

        if(centerWords.equals(Constant.Leave_Words)){
            JAnalyticsInterface.onPageStart(this@WordsActivity,"留言")
        }else if(centerWords.equals(Constant.Report_Words)){
            JAnalyticsInterface.onPageStart(this@WordsActivity,"举报")
        }else if(centerWords.equals(Constant.School_Report)){
            JAnalyticsInterface.onPageStart(this@WordsActivity,"首页反馈")
        }else if(centerWords.equals(Constant.ContactService_Words)){
            JAnalyticsInterface.onPageStart(this@WordsActivity,"联系客服")
        }
        super.onResume()
    }

    override fun onPause(){
        if(centerWords.equals(Constant.Leave_Words)){
            JAnalyticsInterface.onPageEnd(this@WordsActivity,"留言")
        }else if(centerWords.equals(Constant.Report_Words)){
            JAnalyticsInterface.onPageEnd(this@WordsActivity,"举报")
        }else if(centerWords.equals(Constant.School_Report)){
            JAnalyticsInterface.onPageEnd(this@WordsActivity,"首页反馈")
        }else if(centerWords.equals(Constant.ContactService_Words)){
            JAnalyticsInterface.onPageStart(this@WordsActivity,"联系客服")
        }
        super.onPause()
    }

    fun initView() {
        if(getBundle!=null){
            ID=getBundle!!.getString("ID")
            token=getBundle!!.getString("token")
            centerWords=getBundle!!.getString("centerWords")
            rightWords=getBundle!!.getString("rightWords")
        }
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview!!.visibility= View.GONE
        title_center_textview!!.text = centerWords
        title_right_textview!!.text =""
        title_center_textview!!.visibility = View.VISIBLE
        title_right_textview!!.visibility = View.VISIBLE
        title_right_imageview_btn!!.visibility= View.VISIBLE
        words_et=findViewById(R.id.words_et) as EditText
        word_num_tv=findViewById(R.id.words_num_tv) as TextView

        if(centerWords.equals(Constant.School_Report)){
            words_et!!.hint="亲，请简单描述一下您栏目的问题吧~"
        }else if(centerWords.equals(Constant.ContactService_Words)){
            words_et!!.hint="亲，请简单描述一下您想问的问题吧~"
        }

    }

    fun addSomeListener() {
        title_left_imageview_btn!!.setOnClickListener {
            finish()
            //overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)

            }
            false
        }

        val editChangedListener=EditChangedListener()
        words_et!!.addTextChangedListener(editChangedListener)

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        //点击右侧的完成，请求服务器
        title_right_imageview_btn!!.setOnClickListener{
           if(!ID.equals("")&&!token.equals("")){
               if(!words_et!!.text.toString().trim().equals("")){
                   if(centerWords.equals(Constant.Leave_Words)){
                       JEventUtils.onCountEvent(this@WordsActivity,Constant.Event_LeaveMessage)//留言次数统计
                       val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                       pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                       pDialog1.setTitleText("Loading")
                       pDialog1.setCancelable(true)
                       pDialog1.show()
                       IconInfoRequest.requestLeaveWords(this@WordsActivity,ID,words_et!!.text.toString(),token,object:IDataRequestListener2String{
                           override fun loadSuccess(response_string: String?) {
                               LogUtils.d_debugprint(Constant.Icon_Info_TAG,"给TA留言服务器返回的数据："+response_string)
                               if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                   LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中："+centerWords+" 成功！！！\n")
                                   //插入自己发送的留言
                                   val read = this@WordsActivity.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                   val icon= read.getString("icon", "")
                                   val gender=read.getString("gender","")
                                   val nickname=read.getString("nickname","")
                                   val senderID=read.getString("id","")

                                   val result=JsonUtil.get_key_string("result",response_string!!)
                                   val s_id=JsonUtil.get_key_string("id",result)
                                   val createTimeServer=JsonUtil.get_key_string("createTime",result).substring(0, 20).replace("T", " ")
                                   //val s_id=createTimeServer
                                   var Tonickname=""
                                   if(getBundle!=null){
                                       Tonickname=getBundle!!.getString("nickname")
                                   }
                                   val words="回复$Tonickname:"+words_et!!.text.toString()
                                   val isRead="1"
                                   insertData1(s_id,icon,gender,nickname,senderID,createTimeServer,words,isRead)
                                   LogUtils.d_debugprint(Constant.Icon_Info_TAG,"\n自己发送留言后，成功保存在数据库！！！\nicon=$icon\ngender=$gender\nnickname=$nickname\nsenderID=$senderID\ncreateTimeServer=$createTimeServer\nwords=$words")

                                   pDialog1.cancel()
                                   val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                   pDialog.setTitleText("提示")
                                   pDialog.setContentText("亲，您已经成功"+centerWords)
                                   pDialog.show()
                                   val handler= Handler()
                                   handler.postDelayed({
                                       pDialog.cancel()
                                       finish()
                                       overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                   },1236)
                               }else{
                                   if(JsonUtil.get_key_string("msg",response_string)!=null){
                                       pDialog1.cancel()
                                       val pDialog=SweetAlertDialog(this@WordsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                       pDialog.setCustomImage(R.mipmap.mew_icon)
                                       pDialog.setTitleText("提示")
                                       pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                       pDialog.show()
                                       val handler= Handler()
                                       handler.postDelayed({
                                           pDialog.cancel()
                                       },1236)
                                   }
                               }
                           }
                       })
                   }else if(centerWords.equals(Constant.Report_Words)){
                       JEventUtils.onCountEvent(this@WordsActivity,Constant.Event_ReportNum)//举报次数统计
                       val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                       pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
                       pDialog1.setTitleText("Loading")
                       pDialog1.setCancelable(true)
                       pDialog1.show()
                       IconInfoRequest.requestInformUsers(this@WordsActivity,ID,words_et!!.text.toString(),token,object:IDataRequestListener2String{
                           override fun loadSuccess(response_string: String?) {

                               LogUtils.d_debugprint(Constant.Icon_Info_TAG,"举报TA=====服务器返回的数据："+response_string)
                               if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                   LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：举报TA 成功！！！\n")
                                   pDialog1.cancel()
                                   val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                   pDialog.setTitleText("提示")
                                   pDialog.setContentText("亲，您已经成功举报TA~")
                                   pDialog.show()
                                   val handler= Handler()
                                   handler.postDelayed({
                                       pDialog.cancel()
                                       finish()
                                       overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                   },1236)
                               }else{
                                   if(JsonUtil.get_key_string("msg",response_string)!=null){
                                       pDialog1.cancel()
                                       val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                       pDialog.setCustomImage(R.mipmap.mew_icon)
                                       pDialog.setTitleText("提示")
                                       pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                       pDialog.show()
                                       val handler= Handler()
                                       handler.postDelayed({
                                           pDialog.cancel()
                                       },1236)
                                   }
                               }
                           }
                       })
                   }else if(centerWords.equals(Constant.School_Report)){
                       JEventUtils.onCountEvent(this@WordsActivity,Constant.Event_SchoolReport)//首页反馈次数统计
                       var url_source=""
                       var urlEnd=""
                       if(getBundle!=null){
                           url_source=getBundle!!.getString("urlEnd")
                           if(url_source.equals(Constant.School_ListGuidance)){
                               urlEnd=Constant.URL_Report_Guidance
                           }
                           if(url_source.equals(Constant.School_ListSummary)){
                               urlEnd=Constant.URL_Report_Summary
                           }
                           if(url_source.equals(Constant.School_ListClub)){
                               urlEnd=Constant.URL_Report_Club
                           }
                           if(url_source.equals(Constant.School_ListLook)){
                               urlEnd=Constant.URL_Report_Look
                           }
                       }
                       val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                       pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                       pDialog1.setTitleText("Loading")
                       pDialog1.setCancelable(true)
                       pDialog1.show()
                       if(!urlEnd.equals("")){
                           SameListRequest.makeReport(this@WordsActivity,ID,words_et!!.text.toString(),token,urlEnd,object:IDataRequestListener2String{
                               override fun loadSuccess(response_string: String?) {

                                   LogUtils.d_debugprint(Constant.School_TAG,"反馈首页中=====服务器返回的数据："+response_string)
                                   if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                       LogUtils.d_debugprint(Constant.School_TAG,"反馈首页内容中：反馈 成功！！！\n")
                                       pDialog1.cancel()
                                       val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                       pDialog.setTitleText("提示")
                                       pDialog.setContentText("亲，您已经成功反馈问题~")
                                       pDialog.show()
                                       val handler= Handler()
                                       handler.postDelayed({
                                           pDialog.cancel()
                                           finish()
                                           overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                       },1236)
                                   }else{
                                       if(JsonUtil.get_key_string("msg",response_string)!=null){
                                           pDialog1.cancel()
                                           val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                           pDialog.setCustomImage(R.mipmap.mew_icon)
                                           pDialog.setTitleText("提示")
                                           pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                           pDialog.show()
                                           val handler= Handler()
                                           handler.postDelayed({
                                               pDialog.cancel()
                                           },1236)
                                       }
                                   }
                               }
                           })
                       }
                   }else if(centerWords.equals(Constant.ContactService_Words)){
                       JEventUtils.onCountEvent(this@WordsActivity,Constant.Event_ContractService)//联系客服次数统计
                       var url_source=""
                       var urlEnd=""
                       if(getBundle!=null){
                           url_source=getBundle!!.getString("urlEnd")
                           if(url_source.equals(Constant.Find_PartTimeJob)){
                               urlEnd=Constant.URL_ContactService_PartTimeJob
                           }
                           if(url_source.equals(Constant.Find_Training)){
                               urlEnd=Constant.URL_ContactService_Training
                           }
                           if(url_source.equals(Constant.Find_Activity)){
                               urlEnd=Constant.URL_ContactService_Activity
                           }

                       }
                       val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                       pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                       pDialog1.setTitleText("Loading")
                       pDialog1.setCancelable(true)
                       pDialog1.show()
                       if(!urlEnd.equals("")){
                           com.guangdamiao.www.mew_android_debug.navigation.find.SameListRequest.makeContact(this@WordsActivity,ID,words_et!!.text.toString(),token,urlEnd,object:IDataRequestListener2String{
                               override fun loadSuccess(response_string: String?) {

                                   LogUtils.d_debugprint(Constant.Find_TAG,"栏目模块=====联系客服中"+"urlEnd="+urlEnd+"=====服务器返回的数据："+response_string)
                                   if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                       LogUtils.d_debugprint(Constant.Find_TAG,"现模块=====联系客服中=====发送 成功！！！\n")
                                       pDialog1.cancel()
                                       val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                       pDialog.setTitleText("提示")
                                       pDialog.setContentText("亲，信息已成功发送到客服~")
                                       pDialog.show()
                                       val handler= Handler()
                                       handler.postDelayed({
                                           pDialog.cancel()
                                           finish()
                                           overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                       },1236)
                                   }else{
                                       if(JsonUtil.get_key_string("msg",response_string)!=null){
                                           pDialog1.cancel()
                                           val pDialog= SweetAlertDialog(this@WordsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                           pDialog.setCustomImage(R.mipmap.mew_icon)
                                           pDialog.setTitleText("提示")
                                           pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                           pDialog.show()
                                           val handler= Handler()
                                           handler.postDelayed({
                                               pDialog.cancel()
                                           },1236)
                                       }
                                   }
                               }
                           })
                       }
                   }
               }else{
                   if(centerWords.equals(Constant.Leave_Words)){
                       Toasty.info(this@WordsActivity,"亲，是不是还没想好对TA说什么呢...").show()
                   }else if(centerWords.equals(Constant.Report_Words)){
                       Toasty.info(this@WordsActivity,"亲，请简述一下举报理由喔~")
                   }else if(centerWords.equals(Constant.School_Report)){
                       Toasty.info(this@WordsActivity,"亲，请简述一下反馈原因喔~")
                   }else if(centerWords.equals(Constant.ContactService_Words)){
                       Toasty.info(this@WordsActivity,"亲，是不是还没想好对客服君说什么呢...").show()
                   }
               }
           }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish()
            overridePendingTransition(0, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@WordsActivity.currentFocus != null) {
                if (this@WordsActivity.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@WordsActivity.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    internal inner class EditChangedListener : TextWatcher {
        private var temp: CharSequence = ""//监听前的文本
        private val editStart: Int = 0//光标开始位置
        private val editEnd: Int = 0//光标结束位置
        private val charMaxNum = remin_num

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            temp = s
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if(temp.length<charMaxNum){
                if(temp.length==0){
                    title_right_textview!!.text =""
                }else{
                    title_right_textview!!.text =rightWords
                }
                word_num_tv!!.text=temp.length.toString()+"/"+remin_num
            }
        }

        override fun afterTextChanged(s: Editable) {
            if(temp.length>charMaxNum){
                Toasty.info(this@WordsActivity,"您输入的字数已经超过了限制").show()
            }
        }
    }


    private fun insertData1(s_id: String,icon:String,gender:String,nickname:String,senderID:String,createTime:String,words:String,isRead:String) {
        if(helper1!=null){
            val db1 = helper1!!.writableDatabase
            val message1Item= ContentValues()
            message1Item.put("s_id",s_id)
            message1Item.put("icon",icon)
            message1Item.put("gender",gender)
            message1Item.put("nickname",nickname)
            message1Item.put("senderID",senderID)
            message1Item.put("createTime",createTime)
            message1Item.put("words",words)
            message1Item.put("isRead",isRead)
            db1!!.insert(Message1SQLiteOpenHelper.M1_Name,null,message1Item)
            db1!!.close()
        }
    }

}
