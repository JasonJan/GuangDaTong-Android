package com.guangdamiao.www.mew_android_debug.navigation.ask.view;


import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentConfig;
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener;

/**
 * 
* @ClassName: AskMainView
* @Description: view,服务器响应后更新界面 
* @author JasonJan
*
 */
public interface AskDetailView {

	 void update2AddZan(final IDataRequestListener listener);
	 void updateEditTextBodyVisible(int visibility, CommentConfig commentConfig);
	 void update2AddComment();
	 void update2DeleteComment(int postion);
	 void update2DeleteAsk();
	 void update2AdoptComment(int position);
	/*
	 void update2AddComment(int circlePosition, CommentItem addItem);
	 void update2DeleteComment(int circlePosition, String commentId);
	 void updateEditTextBodyVisible(int visibility, CommentConfig commentConfig);*/

}
