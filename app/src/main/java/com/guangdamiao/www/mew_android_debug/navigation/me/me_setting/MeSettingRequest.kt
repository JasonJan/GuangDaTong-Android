package com.guangdamiao.www.mew_android_debug.navigation.me.me_setting

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*

import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/18.
 */

class MeSettingRequest {
    companion object {
        fun RequestSetPush(context: Context,token:String,params:HashMap<String,Boolean>) {
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign= MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath= Constant.BASEURLFORTEST+ Constant.URL_Me_SetPush
                }else{
                    sign= MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath= Constant.BASEURLFORRELEASE+ Constant.URL_Me_SetPush
                }

                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("urgentSchool",params.get("urgentSchool"))
                jsonObject.put("urgentCollege",params.get("urgentCollege"))
                jsonObject.put("comment",params.get("comment"))
                jsonObject.put("leaveWords",params.get("leaveWords"))
                jsonObject.put("activity",params.get("activity"))
                jsonObject.put("system",params.get("system"))

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Me_TAG, Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity, Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                LogUtils.d_debugprint(Constant.Me_TAG,"在设置推送开关中，服务器返回的数据："+response_string)
                                LogUtils.d_debugprint(Constant.Me_TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(response_string)+"\n\n")
                                if(!response_string.equals("")&& JsonUtil.get_key_string("code",response_string).equals(Constant.RIGHTCODE)){
                                    LogUtils.d_debugprint(Constant.Me_TAG,"在设置推送开关中，设置成功！！！"+"最新的开关情况为：")
                                    //将修改好的推送设置写到全局文件中

                                    LogUtils.d_debugprint(Constant.Me_TAG,"\n在我的推送设置中\n 设置的急问通知选择的urgentSchool是"+params.get("urgentSchool")+"\n urgentCollege是 "+params.get("urgentCollege")+"\ncomment是 "+params.get("comment")+"\nleaveWords是 "+params.get("leaveWords")+"\nactivity_bool是 "+params.get("activity")+"\nsystem_bool是 "+params.get("system"))
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }

        fun CheckNewVersion1(context:Context,TAG:String){
            if(NetUtil.checkNetWork(context)){
                val sign:String
                val urlPath:String
                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST)
                    urlPath=Constant.BASEURLFORTEST+Constant.Check_Version
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Check_Version
                }
                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("OSType","android")

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity, Constant.ContentType,object: AsyncHttpResponseHandler(){
                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if (responseBody != null) {
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response = JSONObject(resultDate)
                            val response_string = response.toString()
                            var getCode: String = ""
                            var result: String = ""
                            var curVersion: String = ""
                            var minVersion: String = ""
                            getCode = JsonUtil.get_key_string(Constant.Server_Code, response_string!!)
                            result = JsonUtil.get_key_string(Constant.Server_Result, response_string!!)
                            curVersion = JsonUtil.get_key_string("curVersion", result!!)
                            minVersion = JsonUtil.get_key_string("minVersion", result!!)
                            val updateLog=JsonUtil.get_key_string("updateLog",result!!)
                            LogUtils.d_debugprint(TAG, "系统最新版本:" + curVersion + "\n最小兼容版本：" + minVersion)
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                /*SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("当前版本：" + Common.getVersion(context))
                                        .setContentText("系统最新版本：" + curVersion + "\n最新兼容版本：" + minVersion)
                                        .show()*/
                                LogUtils.d_debugprint(Constant.School_TAG,"版本更新：${Common.compareVersion(Common.getVersion(context),curVersion)} \n\n\n")
                                if(Common.compareVersion(Common.getVersion(context),minVersion)==-1){
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setTitleText("栏目新版本$curVersion")
                                            .setContentText("软件版本过低，可能无法正常使用！\n$updateLog")
                                            .setConfirmText("立即更新")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()

                                                val intent= Intent()
                                                intent.action="android.intent.action.VIEW"
                                                var new_version_url:Uri?=null
                                                if(LogUtils.APP_IS_DEBUG){
                                                    new_version_url= Uri.parse(Constant.BASEURLFORTEST)
                                                }else{
                                                    new_version_url= Uri.parse(Constant.BASEURLFORRELEASE)
                                                }
                                                intent.data=new_version_url
                                                context.startActivity(intent)

                                            }
                                            .setCancelText("下次再说")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()

                                }else if(Common.compareVersion(Common.getVersion(context),curVersion)==-1){
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setTitleText("栏目新版本$curVersion")
                                            .setContentText("新版本发布啦，快去更新吧！\n$updateLog")
                                            .setConfirmText("立即更新")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()

                                                val intent= Intent()
                                                intent.action="android.intent.action.VIEW"
                                                var new_version_url:Uri?=null
                                                if(LogUtils.APP_IS_DEBUG){
                                                    new_version_url= Uri.parse(Constant.BASEURLFORTEST)
                                                }else{
                                                    new_version_url= Uri.parse(Constant.BASEURLFORRELEASE)
                                                }
                                                intent.data=new_version_url
                                                context.startActivity(intent)

                                            }
                                            .setCancelText("下次再说")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }

                            }
                        }

                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                        //Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                //Toasty.info(context,Constant.NONETWORK).show()
            }



        }

        fun CheckNewVersion2(context:Context,TAG:String){
            if(NetUtil.checkNetWork(context)){
                val sign:String
                val urlPath:String
                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST)
                    urlPath=Constant.BASEURLFORTEST+Constant.Check_Version
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Check_Version
                }
                val jsonObject= JSONObject()
                jsonObject.put("sign",sign)
                jsonObject.put("OSType","android")

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val pDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
                pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                pDialog.titleText = "Loading"
                pDialog.setCancelable(false)
                pDialog.show()

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity, Constant.ContentType,object: AsyncHttpResponseHandler(){
                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if (responseBody != null) {
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response = JSONObject(resultDate)
                            val response_string = response.toString()
                            var getCode: String = ""
                            var result: String = ""
                            var curVersion: String = ""
                            var minVersion: String = ""
                            getCode = JsonUtil.get_key_string(Constant.Server_Code, response_string!!)
                            result = JsonUtil.get_key_string(Constant.Server_Result, response_string!!)
                            curVersion = JsonUtil.get_key_string("curVersion", result!!)
                            minVersion = JsonUtil.get_key_string("minVersion", result!!)
                            val updateLog=JsonUtil.get_key_string("updateLog",result!!)
                            if(pDialog!=null) pDialog.cancel()
                            LogUtils.d_debugprint(TAG, "系统最新版本:" + curVersion + "\n最小兼容版本：" + minVersion)
                            if (Constant.RIGHTCODE.equals(getCode)) {
                                /*SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("当前版本：" + Common.getVersion(context))
                                        .setContentText("系统最新版本：" + curVersion + "\n最新兼容版本：" + minVersion)
                                        .show()*/
                                LogUtils.d_debugprint(Constant.School_TAG,"版本更新：${Common.compareVersion(Common.getVersion(context),curVersion)} \n\n\n")
                                if(Common.compareVersion(Common.getVersion(context),minVersion)==-1){
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText("栏目新版本$curVersion")
                                            .setContentText("软件版本过低，可能无法正常使用！\n$updateLog")
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("立即更新")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()

                                                val intent= Intent()
                                                intent.action="android.intent.action.VIEW"
                                                var new_version_url:Uri?=null
                                                if(LogUtils.APP_IS_DEBUG){
                                                    new_version_url= Uri.parse(Constant.BASEURLFORTEST)
                                                }else{
                                                    new_version_url= Uri.parse(Constant.BASEURLFORRELEASE)
                                                }
                                                intent.data=new_version_url
                                                context.startActivity(intent)

                                            }
                                            .setCancelText("下次再说")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()

                                }else if(Common.compareVersion(Common.getVersion(context),curVersion)==-1){
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText("栏目新版本$curVersion")
                                            .setContentText("新版本发布啦，快去更新吧！\n$updateLog")
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("立即更新")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()

                                                val intent= Intent()
                                                intent.action="android.intent.action.VIEW"
                                                var new_version_url:Uri?=null
                                                if(LogUtils.APP_IS_DEBUG){
                                                    new_version_url= Uri.parse(Constant.BASEURLFORTEST)
                                                }else{
                                                    new_version_url= Uri.parse(Constant.BASEURLFORRELEASE)
                                                }
                                                intent.data=new_version_url
                                                context.startActivity(intent)

                                            }
                                            .setCancelText("下次再说")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else if(Common.compareVersion(Common.getVersion(context),curVersion)==0){
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText("提示")
                                            .setContentText("亲，当前已经是最新版啦！\n")
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("OK")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText("提示")
                                            .setContentText("亲，当前已经是最新版啦！")
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("OK")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }

                            }
                        }

                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        if(pDialog!=null) pDialog.cancel()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }

        }
    }
}
