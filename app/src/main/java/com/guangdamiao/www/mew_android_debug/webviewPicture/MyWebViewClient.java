package com.guangdamiao.www.mew_android_debug.webviewPicture;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Jason_Jan on 2017/12/17.
 */

public class MyWebViewClient extends WebViewClient {


   /* @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }*/

    @Override
    public void onPageFinished(WebView view, String url) {
        view.getSettings().setJavaScriptEnabled(true);
       /* view.loadUrl("javascript:window.imagelistener.showSource('<head>'+" +
                "document.getElementsByTagName('html')[0].innerHTML+'</head>');");*/
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//sdk>19才有用

            String script = "picturesUrl()";
            view.evaluateJavascript(script, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String responseJson) {

                }
            });
        } else {//sdk<19后,通过prompt来获取*/

        view.loadUrl("javascript:picturesUrl()");
        addImageClickListener(view);//待网页加载完全后设置图片点击的监听方法

        super.onPageFinished(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        view.getSettings().setJavaScriptEnabled(true);
        super.onPageStarted(view, url, favicon);
    }
    private void addImageClickListener(WebView webView) {
        webView.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName(\"img\"); " +
                "for(var i=0;i<objs.length;i++) " +
                "{"
                + " objs[i].onclick=function() " +
                " { "
                + "  window.imagelistener.openImage(i); " +//通过js代码找到标签为img的代码块，设置点击的监听方法与本地的openImage方法进行连接
                " } " +
                "}" +
                "})()");
    }
}
