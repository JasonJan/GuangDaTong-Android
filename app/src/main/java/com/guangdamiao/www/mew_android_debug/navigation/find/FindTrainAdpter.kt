package com.guangdamiao.www.mew_android_debug.navigation.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

class FindTrainAdpter(protected var context: Context, list_train: ArrayList<ListFindAll>) : BaseAdapter() {
    private var list_train:ArrayList<ListFindAll>?=null

    init {
        this.list_train = list_train

    }

    override fun getCount(): Int {
        return list_train!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list2, parent, false)
        }

        val listTrain = list_train!!.get(position)//获取一个

        val list_icon = ViewHolder.get<ImageView>(convertView, R.id.train_list_icon)
        val list_title = ViewHolder.get<TextView>(convertView, R.id.train_list_title)
        val list_time = ViewHolder.get<TextView>(convertView, R.id.train_list_time1)
        val list_position = ViewHolder.get<TextView>(convertView, R.id.train_list_position1)
        val list_fee = ViewHolder.get<TextView>(convertView, R.id.train_list_fee1)
        val list_description = ViewHolder.get<TextView>(convertView, R.id.train_list_description)
        val time = listTrain.createTime!!.substring(0, 19).replace("T", " ")

        list_title.setText(listTrain.title)
        if(listTrain.title!!.length>20){
            list_title.text=listTrain.title!!.substring(0,18)+"..."
        }

        list_time.setText(time)
        list_position.setText(listTrain.position)
        list_fee.setText(listTrain.fee)
        list_description.text = "摘要：" + listTrain.description!!
        ImageLoader.getInstance().displayImage( listTrain.icon!!, list_icon)
        val link = listTrain.link
        val webView_id = listTrain.id
        val zan = listTrain.zan_
        val isZan = listTrain.isZan
        val readCount = listTrain.readCount
        val isFavorite = listTrain.isFavorite

        list_icon!!.setOnClickListener{
            if(listTrain.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listTrain.icon!!)
        }

        convertView!!.setOnClickListener {
            val intent = Intent(context, FindWebView::class.java)
            val bundle = Bundle()
            bundle.putInt("position",position)
            bundle.putString("id", webView_id)
            bundle.putString("link", link)
            bundle.putString("zan", zan)
            bundle.putString("isZan", isZan)
            bundle.putString("readCount", readCount)
            bundle.putString("isFavorite", isFavorite)
            bundle.putString("urlEnd", Constant.Find_Training)
            bundle.putString("title_first", list_title.text.toString())
            intent.putExtras(bundle)
            context.startActivity(intent)
            //这里处理点击事件
        }


        return convertView
    }

}
