package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import java.util.*

class Me_ChooseGrade : Activity() {
    private var mListView: ListView? = null//从别人得到暂用
    private var items: ArrayList<String> = ArrayList<String>()//从别人得到暂用
    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var bundle:Bundle?=null
    private var old_grade:String=""
    private val TAG= Constant.Me_TAG



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_choose)
        MainApplication.getInstance().addActivity(this)
        fullscreen()
        bundle=intent.extras//接收别人传过来的bundle
        if(bundle!!.getString("me_info_sex")!=null){
            old_grade=bundle!!.getString("me_info_grade")
        }
        initView()
        initValue()
        addSomeListener()
    }

    fun addSomeListener(){
        val itemClickListener= AdapterView.OnItemClickListener { parent, view, position, id ->

            LogUtils.d_debugprint(TAG,"年级选择："+items[position])
            bundle!!.putString("me_info_grade",items[position])
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseGrade,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        mListView!!.choiceMode=ListView.CHOICE_MODE_SINGLE
        mListView!!.setOnItemClickListener(itemClickListener)
        title_left_imageview_btn!!.setOnClickListener{
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseGrade,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }
    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        mListView=findViewById(R.id.choose_listview) as ListView
    }

    fun initValue(){
        title_center_textview!!.text="请选择年级"
        title_center_textview!!.visibility= View.VISIBLE
        /*val resources:Resources=resources
        var item=resources.getStringArray(R.array.me_grade)
        for(i in item){
            items.add(i)
        }*/
        val calendar=Calendar.getInstance()
        val mYear=calendar.get(Calendar.YEAR)
        items.add("其他")
        for(i in mYear downTo 2000){
            items.add(i.toString()+"级")
        }
        mListView!!.adapter= ListSimpleAdapter(applicationContext, items, old_grade)//=====适配器植入点
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseGrade,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

}
