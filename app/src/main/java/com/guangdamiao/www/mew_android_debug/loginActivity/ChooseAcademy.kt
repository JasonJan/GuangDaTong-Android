package com.guangdamiao.www.mew_android_debug.loginActivity

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

class ChooseAcademy : Activity() {

    private var mListView: ListView? = null//从别人得到暂用
    private var items: ArrayList<String> = ArrayList<String>()//从别人得到暂用
    private var back: ImageView?=null
    private var resultFromServer: String? = null
    private var bundle:Bundle?=null
    private val TAG=Constant.Choose_Academy_TAG
    private var choose_academy_status:ImageView?=null
    private var college_old:String=""


    public override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        fullscreen()
        bundle=intent.extras//接收别人传过来的bundle
        if(bundle!=null&&bundle!!.getString("college_old")!=null){
            college_old=bundle!!.getString("college_old")
        }

        MyServerRequest()

        setContentView(R.layout.activity_choose_academy)//加载主布局
        mListView = findViewById(R.id.choose_academy_listview) as ListView
        back=findViewById(R.id.choose_academy_back) as ImageView
        LogUtils.d_debugprint(TAG,items.toString())
        LogUtils.d_debugprint("bundle此刻为：",bundle.toString())
        addSomeListener()

    }

    fun addSomeListener(){
        val itemClickListener=AdapterView.OnItemClickListener { parent, view, position, id ->
            LogUtils.d_debugprint(TAG,"第"+position+"个学院")
            LogUtils.d_debugprint(TAG,"学院名称："+items[position])
            if(bundle==null){
                bundle= Bundle()
            }
            bundle!!.putString("college",items[position])
            bundle!!.putInt("academyPosition",position)
            intent.putExtras(bundle)//添加到自己的intent
            setResult(0x717,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        mListView!!.setOnItemClickListener(itemClickListener)
        back!!.setOnClickListener{
            if(bundle==null){
                bundle=Bundle()
            }
            intent.putExtras(bundle)//添加到自己的intent
            setResult(0x717,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
    }

    fun MyServerRequest(){
        if(NetUtil.checkNetWork(this)){
           val pageSize=100
           val pageIndex=0
           val school=Constant.SCHOOLDEFULT
           val sign:String
           val urlPath:String
           if(LogUtils.APP_IS_DEBUG){
                sign= MD5Util.md5(Constant.SALTFORTEST)
                urlPath= Constant.BASEURLFORTEST + Constant.OrganizingData_Academy
           }else{
                sign= MD5Util.md5(Constant.SALTFORRELEASE)
                urlPath= Constant.BASEURLFORRELEASE + Constant.OrganizingData_Academy
           }

           val jsonObject=JSONObject()
           //jsonObject.put("pageSize",pageSize)
           jsonObject.put("pageIndex",pageIndex)
           jsonObject.put("school",school)
           jsonObject.put("sign",sign)

           val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
           LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())
           val client = AsyncHttpClient(true, 80, 443)

           client.post(this,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
               override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                   if(responseBody!=null){
                       val resultDate = String(responseBody!!, charset("utf-8"))
                       val response=JSONObject(resultDate)
                       resultFromServer=response.toString()
                       var getCode: String = ""
                       getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!)
                       if (Constant.RIGHTCODE.equals(getCode)) {
                           var result: String = ""
                           var list: List<String>?
                           result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServer!!)
                           list = JsonUtil.getList("colleges", result)
                           for (i in list.indices) {
                               items!!.add(list.get(i))
                           }
                           mListView!!.adapter = CustomListAdapter(applicationContext)//加载getView
                           LogUtils.d_debugprint(TAG,items.toString())
                       } else {
                           val msg=JsonUtil.get_key_string("msg",resultFromServer!!)
                           Toasty.error(this@ChooseAcademy,msg).show()
                       }
                    }
               }

               override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                     Toasty.error(this@ChooseAcademy,"网络超时，请稍候重试！").show()
               }
           })

        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }
    }


    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    internal inner class CustomListAdapter(context: Context) : BaseAdapter() {
        private val mInflater: LayoutInflater//从别人得到 自己用
        private var mContext: Context? = null//从别人得到 自己用
        init {
            mContext = context
            mInflater = LayoutInflater.from(mContext)
        }
        override fun getItem(arg0: Int): Any {

            return items!![arg0]
        }
        override fun getItemId(position: Int): Long {

            return position.toLong()
        }
        override fun getCount(): Int {

            return items!!.size
        }
        override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
            var convertView = convertView
            val indexText: TextView
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_coustom, null)
            }
            indexText = convertView!!.findViewById(R.id.choose_academy_listitem) as TextView
            choose_academy_status=convertView.findViewById(R.id.choose_academy_status) as ImageView
            if(items[position].equals(college_old)){
                choose_academy_status!!.visibility=View.VISIBLE
            }else{
                choose_academy_status!!.visibility=View.GONE
            }
            indexText.text = items[position]
            indexText.setTextColor(Color.BLACK)
            return convertView
        }
    }
}
