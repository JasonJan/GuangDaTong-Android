package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class Me_ChooseName : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview:TextView?=null
    private var title_right_textview_btn:Button?=null
    private var me_chooseName_et:EditText?=null

    private var bundle:Bundle?=null
    private var old_name:String=""
    private val TAG= Constant.Me_TAG


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_choose_name)
        MainApplication.getInstance().addActivity(this)
        fullscreen()
        bundle=intent.extras//接收别人传过来的bundle
        if(bundle!!.getString("me_info_nick")!=null){
            old_name=bundle!!.getString("me_info_nick")
        }
        initView()
        initValue()
        addSomeListener()
    }


    

    fun addSomeListener(){

        title_left_imageview_btn!!.setOnClickListener{
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseName,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        title_right_textview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_textview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_right_textview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_textview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_right_textview_btn!!.alpha=1.0f
            }
            false
        }

        title_right_textview_btn!!.setOnClickListener{
           if(!me_chooseName_et!!.text.toString().trim().equals("")){
               bundle!!.putString("me_info_nick",me_chooseName_et!!.text.toString().trim())
               intent.putExtras(bundle)
               setResult(Constant.Me_ChooseName,intent)
               finish()
               overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
           }else{
               Toasty.info(this@Me_ChooseName,"亲，您还没有有输入昵称~").show()
           }
        }

    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_right_textview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        title_right_textview=findViewById(R.id.title_right_textview) as TextView
        me_chooseName_et=findViewById(R.id.me_chooseName_et) as EditText
    }

    fun initValue(){
        title_center_textview!!.text="更改昵称"
        title_right_textview!!.text="确定"
        title_center_textview!!.visibility=View.VISIBLE
        title_right_textview!!.visibility= View.VISIBLE
        title_right_textview_btn!!.visibility=View.VISIBLE
        me_chooseName_et!!.setText(old_name)
        if(!old_name.equals("")){
            me_chooseName_et!!.setSelection(old_name.length)
        }


    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.Me_ChooseName,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
