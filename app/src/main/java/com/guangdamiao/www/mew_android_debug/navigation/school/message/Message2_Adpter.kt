package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message2
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.utils.BadgeView
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.util.*

class Message2_Adpter(protected var context: Context, message2: ArrayList<Message2>,protected var badge2:BadgeView?) : BaseAdapter() {
   
    private var message2 : ArrayList<Message2>?=null


    init {
        this.message2 = message2
    }

    override fun getCount(): Int {
        if(message2!!.size==0){
            update2MessageNum(context)
        }
        return message2!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder: Message2_Adpter.ViewHolder

        //蓝色字体+红色字体+下划线
        val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
        val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))


        if (convertView == null) {
            holder = Message2_Adpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.message2_item, parent, false)
            holder.list_message2_icon = convertView.findViewById(R.id.list_message2_icon) as ImageView
            holder.list_message2_ll = convertView.findViewById(R.id.list_message2_ll) as LinearLayout
            holder.list_message2_nick_tv = convertView.findViewById(R.id.list_message2_nick_tv) as TextView
            holder.list_message2_sex_tv = convertView.findViewById(R.id.list_message2_sex_tv) as TextView
            holder.list_message2_time = convertView.findViewById(R.id.list_message2_time) as TextView
            holder.list_message2_words = convertView.findViewById(R.id.list_message2_words_tv) as TextView
            holder.message2_unread_msg=convertView.findViewById(R.id.message2_unread_msg) as TextView

            convertView.tag = holder
        } else {
            holder = convertView.tag as Message2_Adpter.ViewHolder
        }

        update2MessageNum(context)
        val listMessage2 = message2!!.get(position)//获取一个

        //头像
        val icon = listMessage2.icon
        ImageLoader.getInstance().displayImage(icon, holder.list_message2_icon)

        //昵称
        holder!!.list_message2_nick_tv!!.text=listMessage2!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.list_message2_nick_tv!!.measure(spec,spec)
        var nickwidth= holder!!.list_message2_nick_tv!!.measuredWidth
        //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nickname为：${listMessage2!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,167.0f)} ")
        var nicklength=listMessage2!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(context,167.0f)){
            nicklength-=2
            val name_display=listMessage2!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.list_message2_nick_tv!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.list_message2_nick_tv!!.measure(spec,spec)
            nickwidth= holder!!.list_message2_nick_tv!!.measuredWidth
            //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }

        //性别
        if (!listMessage2.gender.equals("") && listMessage2.gender != null) {
            if (listMessage2.gender.equals("女")) {
                holder!!.list_message2_sex_tv!!.text = "♀"
                holder!!.list_message2_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.ask_red))
            } else {
                holder!!.list_message2_sex_tv!!.text = "♂"
                holder!!.list_message2_sex_tv!!.setTextColor(context!!.resources.getColor(R.color.headline))
            }
        } else {
            holder!!.list_message2_sex_tv!!.visibility = View.GONE
        }

        //时间
        val time = listMessage2.createTime!!.substring(2, 16).replace("T", " ")
        holder!!.list_message2_time!!.text = time

        //是否读过，显示红点
        holder!!.message2_unread_msg!!.visibility=View.GONE
        if(listMessage2.isRead.equals("0")){
            holder!!.message2_unread_msg!!.visibility=View.VISIBLE
        }

        //内容
        if(listMessage2.type.equals("zanAsk")&&listMessage2.detail!=null){
            val content1="我赞了你的帖子"
            val content2=JsonUtil.get_key_string("title",listMessage2.detail!!)
            val length2=content2.length
            val words=content1+content2
            val builder= SpannableStringBuilder(words)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,7,7+length2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder.setSpan(UnderlineSpan(),7,7+length2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message2_words!!.text=builder
        }else if(listMessage2.type.equals(Constant.Message_AdoptComment)&&listMessage2.detail!=null){
            val content1="我在帖子"
            val content2=JsonUtil.get_key_string("title",listMessage2.detail!!)
            val content3="采纳了你的评论，"
            val content4=JsonUtil.get_key_string("coins",listMessage2.detail!!)
            val content5="个金币归你啦！"
            val words=content1+content2+content3+content4+content5
            val builder= SpannableStringBuilder(words)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,4,4+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder.setSpan(UnderlineSpan(),4,4+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            val redSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.ask_red))
            builder.setSpan(redSpan,4+content2.length+8,4+content2.length+8+content4.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message2_words!!.text=builder
        }else if(listMessage2.type.equals(Constant.Message_FavoriteAsk)&&listMessage2.detail!=null){
            val content1="我收藏了你的帖子"
            val content2=JsonUtil.get_key_string("title",listMessage2.detail!!)
            val words=content1+content2
            val builder= SpannableStringBuilder(words)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,8,8+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder.setSpan(UnderlineSpan(),8,8+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message2_words!!.text=builder
        }else if(listMessage2.type.equals(Constant.Message_CommentAsk)&&listMessage2.detail!=null){
            val content1="我回复了你的帖子"
            val content2=JsonUtil.get_key_string("title",listMessage2.detail!!)
            val content3=":"
            val content4=JsonUtil.get_key_string("comment",listMessage2.detail!!)
            val words=content1+content2+content3+content4
            val builder= SpannableStringBuilder(words)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,8,8+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder.setSpan(UnderlineSpan(),8,8+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message2_words!!.text=builder

        }else if(listMessage2.type.equals(Constant.Message_CommentUser)&&listMessage2.detail!=null){
            val content1="我在帖子"
            val content2=JsonUtil.get_key_string("title",listMessage2.detail!!)
            val content3="@你:"
            val content4=JsonUtil.get_key_string("comment",listMessage2.detail!!)
            val words=content1+content2+content3+content4
            val builder= SpannableStringBuilder(words)
            val buleSpan= ForegroundColorSpan(context!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,4,4+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            builder.setSpan(UnderlineSpan(),4,4+content2.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder.list_message2_words!!.text=builder
        }else{
            holder.list_message2_words!!.text="亲，消息不见了"
        }


        holder.list_message2_icon!!.setOnClickListener{
            val intent: Intent = Intent(context, IconInfo::class.java)
            val bundle= Bundle()
            bundle.putString("id",listMessage2.senderID)
            bundle.putString("icon",listMessage2.icon)
            bundle.putString("sex",listMessage2.gender)
            bundle.putString("nickname",listMessage2.nickname)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }

        holder.list_message2_ll!!.setOnClickListener{
            if(NetUtil.checkNetWork(context)){

                update2Message(context,listMessage2.id!!)
                holder.message2_unread_msg!!.visibility=View.GONE
                listMessage2.isRead="1"
                update2MessageNum(context)

                if(listMessage2.askID!=null){
                    val intent:Intent=Intent(context, AskDetail::class.java)
                    val bundle=Bundle()
                    bundle.putString("id",listMessage2.askID)
                    if(LogUtils.APP_IS_DEBUG){
                        bundle.putString("link",Constant.BASEURLFORTEST)
                    }else{
                        bundle.putString("link",Constant.BASEURLFORRELEASE)
                    }
                    bundle.putString("publisherID",listMessage2.senderID)
                    bundle.putString("isZan","1")
                    bundle.putInt("position",position)

                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }

        }

        return convertView!!

    }

    class ViewHolder{
        var list_message2_icon: ImageView?=null
        var list_message2_ll:LinearLayout?=null
        var list_message2_nick_tv: TextView?=null
        var list_message2_sex_tv:TextView?=null
        var list_message2_time:TextView?=null
        var list_message2_words:TextView?=null
        var message2_unread_msg:TextView?=null
    }

    private fun update2Message(context:Context,s_id:String){
        val sql="update message2 set isRead='1' where s_id=$s_id;"
        val helper2=Message2SQLiteOpenHelper(context)
        val db=helper2!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message2 s_id=$s_id 数据=====设为已读！！！\n\n")
    }

    private fun update2MessageNum(context:Context){
        val unread_old=query2NowAll()

        if (Activity::class.java.isInstance(context)) {

            val activity = context as Activity
            val bt2=activity.findViewById(R.id.bt2) as Button
            if(badge2==null){
                badge2= BadgeView(context,bt2)
            }
            if(unread_old!=0){

                badge2!!.badgePosition= BadgeView.POSITION_TOP_RIGHT
                if(unread_old<99){
                    badge2!!.text=unread_old.toString()
                    badge2!!.visibility=View.VISIBLE
                }else{
                    badge2!!.text="99+"
                    badge2!!.visibility=View.VISIBLE
                }
                badge2!!.textSize=10.0f
                badge2!!.setTextColor(Color.WHITE)
                badge2!!.show()

            }else{
                badge2!!.visibility=View.GONE
            }
        }

    }

    private fun query2NowAll():Int{
        val sql="select * from "+ Message2SQLiteOpenHelper.M2_Name+" where isRead='0'"
        val helper2=Message2SQLiteOpenHelper(context)
        val cursor=helper2!!.readableDatabase.rawQuery(sql,null)
        var message2_unread=0
        while(cursor.moveToNext()){
            val senderID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                message2_unread++
            }
        }
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2有多少条未读的：$message2_unread\n\n")
        return message2_unread

    }

        
}
