package com.guangdamiao.www.mew_android_debug.navigation.find

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.view.*
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.PlatActionListener
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.api.ShareParams
import cn.jiguang.share.qqmodel.QQ
import cn.jiguang.share.qqmodel.QZone
import cn.jiguang.share.wechat.Wechat
import cn.jiguang.share.wechat.WechatMoments
import com.bulong.rudeness.RudenessScreenHelper
import com.bumptech.glide.Glide
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.WordsActivity
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find1SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find2SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find3SQLiteOpenHelper
import com.guangdamiao.www.mew_android_debug.navigation.school.SameListRequest
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.guangdamiao.www.mew_android_debug.webviewPicture.MJavascriptInterface
import com.guangdamiao.www.mew_android_debug.webviewPicture.MyWebViewClient
import es.dmoral.toasty.Toasty
import java.util.*


class FindWebView : FragmentActivity(){

    private var webView_back: ImageView?=null
    private var webView_backBut: Button?=null
    private var webView_menu:ImageView?=null
    private var webView_menuBut:Button?=null
    private var webView_content: WebView?=null
    private var webView_title:TextView?=null
    private var bundle:Bundle?=null
    private var urlPath:String?=null
    private var urlFirst:String?=null
    private var id:String?=null
    private var link:String?=null
    private val TAG=Constant.Find_TAG
    private var webView_read:TextView?=null
    private var webView_readNum:TextView?=null
    private var webView_zan:ImageView?=null
    private var webView_zanNum:TextView?=null
    private var webView_zan_btn:Button?=null
    private var webView_applly:Button?=null
    private var webView_contact:Button?=null
    private var readCount_old=""
    private var isZan=""//服务器传过来的数据都是String
    private var zanNum_old=""
    private var isFavorite=""
    private var urlEnd=""//区分是哪个碎片
    private var title_first="标题"
    private var zanNum_new=0
    private var position=0
    var flag_isPop=0


    private var popWindow:CustomPopWindow?=null
    private var popWindow_share:CustomPopWindow?=null
    private var loading_first:Loading_view?=null

    var share_id=""
    var share_shareWay=""
    var share_token=""
    private var imageUrls=ArrayList<String>()


    override fun onResume() {
        JAnalyticsInterface.onPageStart(this,"栏目webView")
        super.onResume()
        if(webView_content!=null){
            webView_content!!.resumeTimers()
            webView_content!!.onResume()
        }
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this,"栏目webView")
        super.onPause()
        if (webView_content != null) {
            webView_content!!.onPause()
            webView_content!!.pauseTimers()
        }
    }

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RudenessScreenHelper(application,750.0f)
        setContentView(R.layout.activity_school_web_view)
        MainApplication.getInstance().addActivity(this)//便于统一管理
        fullscreen()
        //加载
        loading_first= Loading_view(this@FindWebView,R.style.CustomDialog)
        loading_first!!.show()


        initView()
        addSomeListener()
        getUrl()


         if(urlEnd.equals(Constant.Find_PartTimeJob)){//更新阅读数量
             updateNowAll(Find1SQLiteOpenHelper.F1_Name)
         }else if(urlEnd.equals(Constant.Find_Training)){
             updateNowAll(Find2SQLiteOpenHelper.F2_Name)
         }else if(urlEnd.equals(Constant.Find_Activity)){
             updateNowAll(Find3SQLiteOpenHelper.F3_Name)
         }

         if (Build.VERSION.SDK_INT >= 23) {
             if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                 ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 0)
             }
         }

    }

    private fun getWebViewTitle(){
         val wvcc:WebChromeClient=object:WebChromeClient(){
            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                /*webView_title!!.text=title
                if(title!=null&&title!!.length>18){
                    webView_title!!.text=title!!.substring(0,16)+"..."
                }*/

                if(!webView_title!!.text.toString().equals("页面不存在或已删除")){
                    showBottom()
                }else{

                }
                if(loading_first!=null){
                    loading_first!!.dismiss()
                }
            }
        }

        webView_content!!.loadUrl(urlPath)
        webView_content!!.addJavascriptInterface(MJavascriptInterface(this,imageUrls),"imagelistener")
        webView_content!!.setWebViewClient(MyWebViewClient())
        webView_content!!.setWebChromeClient(wvcc)
       /* webView_content!!.setWebViewClient(object:WebViewClient(){
            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                if(loading_first!=null){
                    loading_first!!.dismiss()
                }
                webView_title!!.setText("亲，找不到标题了")
                webView_read!!.visibility= View.GONE
                webView_readNum!!.visibility= View.GONE
                webView_zan!!.visibility= View.GONE
                webView_zanNum!!.visibility= View.GONE
                webView_contact!!.visibility=View.GONE
                webView_applly!!.visibility=View.GONE
            }
        })*/
    }

    private fun showBottom(){
        webView_readNum!!.text=readCount_old
        webView_zanNum!!.text=zanNum_old
        webView_read!!.visibility= View.VISIBLE
        webView_readNum!!.visibility=View.VISIBLE
        webView_zan!!.visibility=View.VISIBLE
        webView_zanNum!!.visibility=View.VISIBLE
        webView_applly!!.visibility=View.GONE
        webView_contact!!.visibility=View.GONE
        webView_zan_btn!!.visibility=View.VISIBLE
    }

    private fun getUrl(){
        if(LogUtils.APP_IS_DEBUG){
            urlFirst= Constant.BASEURLFORTEST
        }else{
            urlFirst=Constant.BASEURLFORRELEASE
        }
        if(bundle!=null){
            id=bundle!!.getString("id")
            link=bundle!!.getString("link")
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            urlPath="$link?id=$id&token=$token&OSType=android"
            urlEnd=bundle!!.getString("urlEnd") //确定来自于哪个分类，以此确定收藏的请求网址
        }
        if(urlPath!=null){
            getWebViewTitle()
        }
    }

    private fun initView() {
        webView_back=findViewById(R.id.webView_back) as ImageView
        webView_menu=findViewById(R.id.webView_menu) as ImageView
        webView_content=findViewById(R.id.webView_content) as WebView
        webView_title=findViewById(R.id.webView_title) as TextView
        webView_backBut=findViewById(R.id.webView_backBut) as Button
        webView_menuBut=findViewById(R.id.webView_menuBut) as Button
        webView_read=findViewById(R.id.webView_read) as TextView
        webView_readNum=findViewById(R.id.webView_readNum) as TextView
        webView_zan=findViewById(R.id.webView_zan) as ImageView
        webView_zan_btn=findViewById(R.id.webView_zan_btn) as Button
        webView_zanNum=findViewById(R.id.webView_zanNum) as TextView
        webView_applly=findViewById(R.id.webView_apply) as Button
        webView_contact=findViewById(R.id.webView_contact) as Button
        bundle=intent.extras
        readCount_old= if(bundle!!.getString("readCount")==null) "" else bundle!!.getString("readCount")
        zanNum_old=if(bundle!!.getString("zan")==null) "" else bundle!!.getString("zan")
        isZan=if(bundle!!.getString("isZan")==null) "" else bundle!!.getString("isZan")
        isFavorite=if(bundle!!.getString("isFavorite")==null) "" else bundle!!.getString("isFavorite")
        title_first=if(bundle!!.getString("title_first")==null) "标题" else bundle!!.getString("title_first")
        position= if(bundle!!.getInt("position")==null) 0 else bundle!!.getInt("position")
        webView_title!!.text=title_first
        if(title_first!=null&&title_first!!.length>18){
            webView_title!!.text=title_first!!.substring(0,16)+"..."
        }

        if(isZan.equals("1")){
            webView_zan!!.setImageResource(R.mipmap.zan_blue)
        }
        zanNum_new=zanNum_old.toInt()

        val webSettings:WebSettings=webView_content!!.settings
        webSettings.javaScriptEnabled=true
        webSettings.blockNetworkImage=false
        webSettings.useWideViewPort=true
        webSettings.loadWithOverviewMode=true
        webSettings.builtInZoomControls=true
        webSettings.setSupportZoom(true)
        webSettings.displayZoomControls=false

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE)

        webView_read!!.visibility= View.GONE
        webView_readNum!!.visibility= View.GONE
        webView_zan!!.visibility= View.GONE
        webView_zanNum!!.visibility= View.GONE
        webView_contact!!.visibility=View.GONE
        webView_applly!!.visibility=View.GONE

    }

    private fun addSomeListener(){
        webView_backBut!!.setOnClickListener{
            if(webView_content!!.canGoBack()){
                webView_content!!.goBack()
            }else{
                if(flag_isPop==0){
                    finish()
                    overridePendingTransition(0,R.anim.slide_right_out)
                }else{
                    finish()
                }
            }
        }
        //这里弹出一个ActionSheet
        webView_menuBut!!.setOnClickListener{
            showDialog(id!!)
        }
        webView_zan_btn!!.setOnClickListener{//如果要赞提示登录
            make_zan()
        }

        webView_contact!!.setOnClickListener{
            make_contact()
        }

        webView_applly!!.setOnClickListener{
            make_applly()
        }

        webView_backBut!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                webView_backBut!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                webView_backBut!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                webView_backBut!!.setBackgroundColor(Color.TRANSPARENT)
                webView_backBut!!.alpha=1.0f
            }
            false
        }

        webView_menuBut!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                webView_menuBut!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                webView_menuBut!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                webView_menuBut!!.setBackgroundColor(Color.TRANSPARENT)
                webView_menuBut!!.alpha=1.0f
            }
            false
        }

        webView_zan_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                webView_zan!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                webView_zan!!.alpha=1.0f
            }
            false
        }

        webView_contact!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                webView_contact!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                webView_contact!!.alpha=1.0f
            }
            false
        }

        webView_applly!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                webView_applly!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                webView_applly!!.alpha=1.0f
            }
            false
        }

    }

    fun make_applly(){
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        val account=read.getString("account","")
        if(!id.equals("")&&!token.equals("")&&!account.equals("")){
            val intent:Intent=Intent(this@FindWebView, SignUp::class.java)
            val bundle=Bundle()
            bundle.putString("id",id)
            bundle.putString("account",account)
            bundle.putString("token",token)
            bundle.putString("urlEnd",urlEnd)
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }else{
            val intent = Intent(this@FindWebView, LoginMain::class.java)
            this@FindWebView.startActivity(intent)
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
        }
    }

    fun make_contact(){
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        if(!id.equals("")&&!token.equals("")){
            val intent:Intent=Intent(this@FindWebView, WordsActivity::class.java)
            val bundle=Bundle()
            bundle.putString("ID",id)
            bundle.putString("token",token)
            bundle.putString("urlEnd",urlEnd)
            bundle.putString("centerWords",Constant.ContactService_Words)
            bundle.putString("rightWords","发送")
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }else{
            val intent = Intent(this@FindWebView, LoginMain::class.java)
            this@FindWebView.startActivity(intent)
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
        }
    }

    fun make_zan(){

        val token=get_token()
        //开始点赞=====请求服务器
        if(!token.equals("")&&!id.equals("")&&!isZan.equals("1")){

            if(isZan.equals("0")){
                zanNum_new=zanNum_old.toInt()+1
                webView_zanNum!!.text=zanNum_new.toString()
                webView_zan!!.setImageResource(R.mipmap.zan_blue)
                isZan="1"
            }

            if(urlEnd.equals(Constant.Find_PartTimeJob)){//这里的点赞用的是首页的点赞的接口！！！
                updateNowAll(Find1SQLiteOpenHelper.F1_Name)
                com.guangdamiao.www.mew_android_debug.navigation.find.SameListRequest.addZan_requestServer(id!!,token,Constant.Zan_PartTimeJob,this@FindWebView)
            }else if(urlEnd.equals(Constant.Find_Training)){
                updateNowAll(Find2SQLiteOpenHelper.F2_Name)
                com.guangdamiao.www.mew_android_debug.navigation.find.SameListRequest.addZan_requestServer(id!!,token,Constant.Zan_Training,this@FindWebView)
            }else if(urlEnd.equals(Constant.Find_Activity)){
                updateNowAll(Find3SQLiteOpenHelper.F3_Name)
                com.guangdamiao.www.mew_android_debug.navigation.find.SameListRequest.addZan_requestServer(id!!,token,Constant.Zan_Activity,this@FindWebView)
            }else{
                Toasty.info(this@FindWebView,"亲，点赞失败~").show()
            }
        }
    }


    fun showDialog(id:String){
        flag_isPop=1
        val contentView:View= LayoutInflater.from(this).inflate(R.layout.pop_three_dialog,null)
        hanleDialog(contentView,id)
        popWindow= CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow!!.showAtLocation(webView_menuBut, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(this,260.0f))

    }

    private fun hanleDialog(contentView:View,id: String){
        val pop_three_share=contentView.findViewById(R.id.pop_three_share) as TextView
        val pop_three_favorite=contentView.findViewById(R.id.pop_three_favorite) as TextView
        val pop_three_cancle=contentView.findViewById(R.id.pop_three_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){
                    R.id.pop_three_share    -> make_share(id)
                    R.id.pop_three_favorite -> make_favorite(id)
                    R.id.pop_three_cancle   -> popWindow!!.dissmiss()
                }
            }
        }
        pop_three_share!!.setOnClickListener(listener)
        pop_three_favorite!!.setOnClickListener(listener)
        pop_three_cancle!!.setOnClickListener(listener)
    }

    fun make_share(id:String){
        val contentView:View= LayoutInflater.from(this).inflate(R.layout.ask_share,null)
        hanleShareDialog(contentView,webView_title!!.text.toString(),webView_title!!.text.toString(),link!!,id)//这个函数还没有写
        popWindow_share= CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_share_anim)
                .create()
        popWindow_share!!.showAtLocation(webView_menuBut, Gravity.CENTER,0,0)
    }

    private fun hanleShareDialog(contentView: View,title: String,content: String,link:String,id:String){
        val ask_share_qq=contentView.findViewById(R.id.ask_share_qq) as RelativeLayout
        val ask_share_wechat=contentView.findViewById(R.id.ask_share_wechat) as RelativeLayout
        val ask_share_qq_zone=contentView.findViewById(R.id.ask_share_qq_zone) as RelativeLayout
        val ask_share_wechat_friends=contentView.findViewById(R.id.ask_share_wechat_friends) as RelativeLayout
        val ask_share_copy=contentView.findViewById(R.id.ask_share_copy) as RelativeLayout


        ask_share_qq!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,id, QQ.Name,"QQFriend")
        }

        ask_share_qq_zone!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,id, QZone.Name,"QQZone")
        }

        ask_share_wechat!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,id, Wechat.Name,"weChatFriend")
        }

        ask_share_wechat_friends!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,id, WechatMoments.Name,"weChatCircle")
        }

        ask_share_copy!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            val cm=this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val mClipData= ClipData.newPlainText("Label",link+"?id="+id)
            cm.primaryClip=mClipData
            Toasty.info(this,"亲，复制成功，赶快分享一下吧~").show()
        }

    }

    private fun shareAsk(title:String,id:String,shareType:String,shareWay:String){
        JEventUtils.onCountEvent(this,Constant.Event_DiscoveryShare)//分享栏目内容统计

        val id=bundle!!.getString("id")
        val link=bundle!!.getString("link")
        val urlPath=link+"?id="+id

        val shareParams= ShareParams()
        shareParams.shareType= Platform.SHARE_WEBPAGE
        shareParams.text=title
        shareParams.title="广大通"
        if(shareType.equals(WechatMoments.Name)){
            shareParams.title=title
        }
        shareParams.url=urlPath
        shareParams.imagePath=MainApplication.ImagePath

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        share_token = read.getString("token", "")
        share_id=id
        share_shareWay=shareWay

        JShareInterface.share(shareType, shareParams, mPlatActionListener)
    }


    private val mPlatActionListener = object : PlatActionListener {
        override fun onComplete(platform: Platform, action: Int, data: HashMap<String, Any>) {
            LogUtils.d_debugprint(Constant.Find_TAG,"栏目内容分享成功！！！即将向服务器发送数据~~")
            Toasty.info(this@FindWebView,"亲，分享成功！").show()
            if(urlEnd.equals(Constant.Find_PartTimeJob)){
                AskMainRequest.makeShareToServer(Constant.Find_TAG,Constant.URL_Share_PartTimeJob,this@FindWebView,share_id,share_shareWay,share_token)
            }else if(urlEnd.equals(Constant.Find_Training)){
                AskMainRequest.makeShareToServer(Constant.Find_TAG,Constant.URL_Share_Training,this@FindWebView,share_id,share_shareWay,share_token)
            }else if(urlEnd.equals(Constant.Find_Activity)){
                AskMainRequest.makeShareToServer(Constant.Find_TAG,Constant.URL_Share_Activity,this@FindWebView,share_id,share_shareWay,share_token)
            }
        }

        override fun onError(platform: Platform, action: Int, errorCode: Int, error: Throwable) {
            LogUtils.d_debugprint(Constant.Find_TAG,"栏目分享失败！！！")
            Toasty.info(this@FindWebView,"亲，帖子分享失败！").show()
        }

        override fun onCancel(platform: Platform, action: Int) {
            LogUtils.d_debugprint(Constant.Find_TAG,"栏目分享取消！！！")
            Toasty.info(this@FindWebView,"亲，您已取消分享！").show()
        }
    }

    fun make_favorite(id:String){
        JEventUtils.onCountEvent(this@FindWebView,Constant.Event_DiscoveryCollect)//收藏栏目内容

        val token=get_token()
        //开始收藏=====请求服务器
        if(token!=""){

            val pDialog = SweetAlertDialog(this@FindWebView, SweetAlertDialog.PROGRESS_TYPE)
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true)
            pDialog.show()

            if(urlEnd.equals(Constant.Find_PartTimeJob)){
                SameListRequest.makeFavoriteFromServer(this@FindWebView,Constant.Find_TAG,Constant.Find_Favorite_PartTimeJob,id,token,pDialog)
            }else if(urlEnd.equals(Constant.Find_Training)){
                SameListRequest.makeFavoriteFromServer(this@FindWebView,Constant.Find_TAG,Constant.Find_Favorite_Training,id,token,pDialog)

            }else if(urlEnd.equals(Constant.Find_Activity)){
                SameListRequest.makeFavoriteFromServer(this@FindWebView,Constant.Find_TAG,Constant.Find_Favorite_Activity,id,token,pDialog)

            }else{
                Toasty.info(this@FindWebView,"亲，收藏失败~").show()
            }
        }
    }

    private fun get_token():String{
        val read = this@FindWebView.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(this@FindWebView, LoginMain::class.java)
            this@FindWebView.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    private fun fullscreen() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            if(webView_content!!.canGoBack()){
                webView_content!!.goBack()
                return true
            }else{
                if(flag_isPop==0){
                    finish()
                    overridePendingTransition(0,R.anim.slide_right_out)
                }else{
                    finish()
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    //数据库更新
    private fun updateNowAll(table:String){

        if(table.equals(Find1SQLiteOpenHelper.F1_Name)){
            val editor: SharedPreferences.Editor=getSharedPreferences("update_find1", Context.MODE_APPEND).edit()
            editor.putInt("position",position)
            editor.putString("id",id)
            editor.putString("isZan",isZan)
            editor.putString("readCount",(readCount_old.toInt()+1).toString())
            editor.putString("zan",zanNum_new.toString())
            editor.commit()

        }else if(table.equals(Find2SQLiteOpenHelper.F2_Name)){
            val editor: SharedPreferences.Editor=getSharedPreferences("update_find2", Context.MODE_APPEND).edit()
            editor.putInt("position",position)
            editor.putString("id",id)
            editor.putString("isZan",isZan)
            editor.putString("readCount",(readCount_old.toInt()+1).toString())
            editor.putString("zan",zanNum_new.toString())
            editor.commit()

        }else if(table.equals(Find3SQLiteOpenHelper.F3_Name)){
            val editor: SharedPreferences.Editor=getSharedPreferences("update_find3", Context.MODE_APPEND).edit()
            editor.putInt("position",position)
            editor.putString("id",id)
            editor.putString("isZan",isZan)
            editor.putString("readCount",(readCount_old.toInt()+1).toString())
            editor.putString("zan",zanNum_new.toString())
            editor.commit()

        }
    }

    override fun onDestroy() {
        Thread(Runnable {
            Glide.get(this@FindWebView).clearDiskCache()//清理磁盘缓存需要在子线程中执行
        }).start()
        Glide.get(this).clearMemory();//清理内存缓存可以在UI主线程中进行
        if (webView_content != null) {
            webView_content!!.destroy()
            webView_content=null
        }
        super.onDestroy()
    }
}
