package com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.PlatActionListener
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.api.ShareParams
import cn.jiguang.share.qqmodel.QQ
import cn.jiguang.share.qqmodel.QZone
import cn.jiguang.share.wechat.Wechat
import cn.jiguang.share.wechat.WechatMoments
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentConfig
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentItem
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyDetailAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.NavigationMainActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskReport
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.ImagePagerActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.MultiImageView
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.DetailRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskDetailPresenter
import com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk.AskSearchResult
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskDetailView
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.liaoinstan.springview.container.DefaultFooter
import com.liaoinstan.springview.container.DefaultHeader
import com.liaoinstan.springview.widget.SpringView
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.ImageSize
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.io.File
import java.util.*

class AskDetail : BaseActivity(), AskDetailView {

    private var title_left_imageview_btn: Button?=null
    private var title_right_image: ImageView?=null
    private var title_right_imageview_btn:Button?=null
    private var title_center_textview: TextView? = null
    private var title_right_textview:TextView?=null
    private var bundle:Bundle?=null
    private var publisherID=""
    private var popWindow:CustomPopWindow?=null
    var ask_three_favorite:TextView?=null
    var ask_three_report:TextView?=null
    var ask_three_cancle:TextView?=null
    var ask_report_five1:TextView?=null
    var ask_report_five2:TextView?=null
    var ask_report_five3:TextView?=null
    var ask_report_five4:TextView?=null
    var ask_report_cancle:TextView?=null
    private var popWindow_report: CustomPopWindow?=null
    private var popWindow_share: CustomPopWindow?=null
    private var popWindow_comment:CustomPopWindow?=null
    private var holder=ViewHolder()
    private var icon=""//帖子详情楼主头像
    private var icon_adopt=""//采纳评论人的头像
    private var mPresenter: AskDetailPresenter?=null //处理服务器交互和view更新的类

    private var token=""//判断是哪一个用户
    private var pageNow=0//第一次默认的评论页数
    private val pageSize=Constant.PAGESIZE
    private val orderBy=Constant.OrderBy_Default
    private val order=Constant.Order_Default
    private val listItems = ArrayList<CommentItem>()
    private var mCommentConfig:CommentConfig?=null
    private var loading_first:Loading_view?=null
    private var loading2master:Loading_view?=null
    private var loading2user:  Loading_view?=null

    private var mScreenHeight: Int = 0
    private var mEditTextBodyHeight: Int = 0
    private var mCurrentKeyBoardH: Int = 0
    private var mSelectAskItemH: Int = 0
    private var mSelectCommentItemOffset: Int = 0

    var ask_share_qq:RelativeLayout?=null
    var ask_share_wechat:RelativeLayout?=null
    var ask_share_qq_zone:RelativeLayout?=null
    var ask_share_wechat_friends:RelativeLayout?=null
    var ask_share_copy:RelativeLayout?=null

    var update_isZan=""
    var update_commentCount=""
    var update_zan=""
    var update_isDeal="" //用于finish之后，更新某一个帖子，写到一个全局变量

    var share_id=""
    var share_shareWay=""
    var share_token=""
    var flag_isPop=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainApplication.getInstance().addActivity(this)
        setContentView(R.layout.activity_ask_detail)
        initImageLoader()
        if(intent.extras!=null) bundle=intent.extras
        fullScreen()
        mPresenter= AskDetailPresenter(this)
        initView()
        addSomeListener()
        //请求服务器=====填充帖子详情列表

        loading_first=Loading_view(this@AskDetail,R.style.CustomDialog)
        loading_first!!.show()

        holder.ask_detail_springView!!.callFresh()

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 0)
            }
        }
    }


    override fun onResume() {
        JAnalyticsInterface.onPageStart(this,"帖子详情")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this,"帖子详情")
        super.onPause()
    }

    private fun fullScreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    private fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_image=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_right_textview=findViewById(R.id.title_right_textview) as TextView
        title_center_textview!!.text="帖子详情"
        title_center_textview!!.visibility= View.VISIBLE
        if(null!=bundle){
           publisherID=bundle!!.getString("publisherID")//为了查看是否是自己的帖子
        }
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val userID_from_shareFile = read.getString("id", "")
        token = read.getString("token", "")
        if(!publisherID.equals("")&&publisherID.equals(userID_from_shareFile)){//是自己的帖子，显示删除
            title_right_image!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.text="删除"
            title_right_textview!!.visibility=View.VISIBLE
        }else{//别人的帖子显示三个点
            title_right_image!!.visibility=View.VISIBLE
            title_right_imageview_btn!!.visibility=View.VISIBLE
        }
        title_right_textview!!.setOnClickListener{
            if(mPresenter!=null&&bundle!=null&&bundle!!.getString("id")!=null){
                val IDs=ArrayList<String>()
                IDs.add(bundle!!.getString("id"))
                SweetAlertDialog(this@AskDetail, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.mipmap.mew_icon)
                        .setTitleText("提示")
                        .setContentText("删除这条帖子？")
                        .setCancelText("再想想")
                        .setConfirmText("我确定")
                        .showCancelButton(true)
                        .setCancelClickListener(object:SweetAlertDialog.OnSweetClickListener {
                            override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                                sweetAlertDialog!!.cancel()
                            }
                        })
                        .setConfirmClickListener(object:SweetAlertDialog.OnSweetClickListener{
                            override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                                sweetAlertDialog!!.cancel()
                                mPresenter!!.deleteAsk(this@AskDetail,token,IDs)
                            }
                        })
                        .show()

            }
        }

        val read2 = this.getSharedPreferences("update_ask", Context.MODE_PRIVATE)
        update_isZan = read2.getString("isZan", "0")
        update_commentCount=read2.getString("commentCount","")
        update_zan=read2.getString("zan","0")
        update_isDeal=read2.getString("isDeal","")

        initHolder()
    }

    override fun update2DeleteAsk() {
        //跳转到ThreeFm即可
            val pDialog= SweetAlertDialog(this@AskDetail, SweetAlertDialog.SUCCESS_TYPE)
            pDialog.setTitleText("删除成功")
            pDialog.setContentText("亲，您已经成功删除该帖子")
            pDialog.show()
            val handler=Handler()
            handler.postDelayed({
                pDialog.cancel()
            },1236)
            val intent:Intent=Intent(this@AskDetail, NavigationMainActivity::class.java)
            intent.putExtra("fragid",2)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)

    }

    //点赞的界面处理
    override fun update2AddZan(listener: IDataRequestListener) {
       holder.ask_detail_zan!!.setImageResource(R.mipmap.zan_blue)
       update_isZan="1"
       holder!!.ask_detail_zan_num!!.text=(holder!!.ask_detail_zan_num!!.text.toString().toInt()+1).toString()
       update_zan= holder!!.ask_detail_zan_num!!.text.toString()
       listener.loadSuccess(token)
    }

    override fun update2AddComment() {

        holder.ask_detail_springView!!.callFresh()

    }

    override fun update2AdoptComment(position: Int) {
        holder.ask_detail_springView!!.callFresh()
    }

    //弹出编辑框
    override fun updateEditTextBodyVisible(visibility: Int, commentConfig: CommentConfig?) {
        //setViewTreeObserver()
        mCommentConfig=commentConfig
        holder.ask_detail_editTextVodyLl!!.visibility=View.VISIBLE

        measureAskItemHighAndCommentItemOffset(commentConfig)

        //偏移的scrollview
        if(holder.ask_detail_sv!=null&&mCommentConfig!=null){
            holder.ask_detail_sv!!.postDelayed(object:Runnable{
                override fun run() {
                    holder.ask_detail_sv!!.smoothScrollTo(0,getScrollviewOffset(mCommentConfig))
                }
            },300)
        }

        if(View.VISIBLE==visibility){
            holder.ask_detail_commentEt!!.requestFocus()
            holder.ask_detail_commentEt!!.hint=commentConfig!!.hint
            CommonUtils.showSoftInput(this@AskDetail,holder.ask_detail_commentEt)
        }else if(View.VISIBLE==visibility){
            //隐藏键盘
            CommonUtils.hideSoftInput(this@AskDetail,holder.ask_detail_commentEt)
        }
    }

    override fun update2DeleteComment(position: Int) {
        if(listItems[position]!=null){
            listItems.removeAt(position)
            holder!!.ask_detail_comment_num!!.text=(holder!!.ask_detail_comment_num!!.text.toString().toInt()-1).toString()
            holder.commentAdapter!!.notifyDataSetChanged()
            val pDialog=SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                pDialog.setTitleText("提示")
                pDialog.setContentText("亲，您已经成功删除该评论")
                pDialog.show()
            val handler=Handler()
            handler.postDelayed({
                pDialog.cancel()
            },1236)
        }
    }

    //隐藏输入框
    private fun hideEditTextBody(){
        if(holder.ask_detail_editTextVodyLl!=null){
            holder.ask_detail_commentEt!!.setText("")
            holder.ask_detail_editTextVodyLl!!.visibility=View.GONE
            CommonUtils.hideSoftInput(this@AskDetail,holder.ask_detail_commentEt)
        }
    }


    private fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
           /* val intent:Intent=Intent(this@AskDetail, NavigationMainActivity::class.java)
            intent.putExtra("fragid",2)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)*/

            val editor: SharedPreferences.Editor=getSharedPreferences("update_ask", Context.MODE_APPEND).edit()
            editor.putString("isZan",update_isZan)
            editor.putString("zan",update_zan)
            editor.putString("commentCount",update_commentCount)
            editor.putString("isDeal",update_isDeal)
            editor.commit()

            if(flag_isPop==0){
                finish()
                overridePendingTransition(0,R.anim.slide_right_out)
            }else{
                finish()
            }
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else {
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }


        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else {
                title_right_imageview_btn!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        holder.ask_comment_all_ll!!.setOnTouchListener(View.OnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    holder.ask_detail_editTextVodyLl!!.visibility=View.GONE
                }
                MotionEvent.ACTION_MOVE -> {
                    holder.ask_detail_editTextVodyLl!!.visibility=View.GONE
                }
                MotionEvent.ACTION_UP -> {
                    holder.ask_detail_editTextVodyLl!!.visibility=View.GONE
                }
            }
            false
        })

    }

    fun updateView(myDetailAsk: MyDetailAsk,pageNow: Int){
        //保存发帖人的id publisherID
        //判断是否是自己的帖子
        val read = this@AskDetail.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token=read.getString("token","")
        val current_userID = read.getString("id", "")


        title_right_imageview_btn!!.setOnClickListener{//点击了右侧的三个点
            if(myDetailAsk.id!=null){
                flag_isPop=1
                showDialog(myDetailAsk.id!!)//show举报或者收藏的对话框
            }

        }

        if(current_userID.equals(myDetailAsk.publisherID)){
            title_right_image!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.text="删除"
            title_right_textview!!.visibility=View.VISIBLE
        }else{
            title_right_image!!.visibility=View.VISIBLE
            title_right_imageview_btn!!.visibility=View.VISIBLE
        }

        //左侧头像
        icon = myDetailAsk.icon!!
        getIcon(icon,holder.detail_icon_iv!!)

        //点击了头像
        holder.detail_icon_iv!!.setOnClickListener{
            //自己的头像
            if(current_userID!!.equals(myDetailAsk.publisherID)){
                val intent:Intent=Intent(this@AskDetail, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",myDetailAsk.publisherID)
                bundle.putString("icon",myDetailAsk.icon)
                bundle.putString("sex",myDetailAsk.gender)
                bundle.putString("nickname",myDetailAsk.nickname)
                intent.putExtras(bundle)
                this@AskDetail.startActivity(intent)
            }else{
                val intent:Intent=Intent(this@AskDetail, IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",myDetailAsk.publisherID)
                bundle.putString("icon",myDetailAsk.icon)
                bundle.putString("sex",myDetailAsk.gender)
                bundle.putString("nickname",myDetailAsk.nickname)
                intent.putExtras(bundle)
                this@AskDetail.startActivity(intent)
            }
        }

        holder.ask_detail_title_rl!!.setOnClickListener{
            //自己的头像
            if(current_userID!!.equals(myDetailAsk.publisherID)){
                val intent:Intent=Intent(this@AskDetail, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",myDetailAsk.publisherID)
                bundle.putString("icon",myDetailAsk.icon)
                bundle.putString("sex",myDetailAsk.gender)
                bundle.putString("nickname",myDetailAsk.nickname)
                intent.putExtras(bundle)
                this@AskDetail.startActivity(intent)
            }else{
                val intent:Intent=Intent(this@AskDetail, IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",myDetailAsk.publisherID)
                bundle.putString("icon",myDetailAsk.icon)
                bundle.putString("sex",myDetailAsk.gender)
                bundle.putString("nickname",myDetailAsk.nickname)
                intent.putExtras(bundle)
                this@AskDetail.startActivity(intent)
            }
        }

        //标题
        if(!myDetailAsk.title.equals("")&&myDetailAsk.title!=null){
            holder.ask_detail_title_tv!!.text="\""+myDetailAsk.title+"\""
        }else{
            holder.ask_detail_title_tv!!.text="无题"
        }

        holder.ask_detail_title_tv!!.setOnLongClickListener{
            val cmb=this@AskDetail!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cmb.text= holder!!.ask_detail_title_tv!!.text
            Toasty.info(this@AskDetail!!,"成功复制该帖子标题！").show()
            true
        }

        //昵称
        holder.ask_detail_nick_tv!!.text=myDetailAsk.nickname
        //昵称
        holder!!.ask_detail_nick_tv!!.text=myDetailAsk!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.ask_detail_nick_tv!!.measure(spec,spec)
        var nickwidth= holder!!.ask_detail_nick_tv!!.measuredWidth
        LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nickname为：${myDetailAsk!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的137.0f为：${DensityUtil.dip2px(this@AskDetail,137.0f)} ")
        var nicklength=myDetailAsk!!.nickname!!.length
        while(nickwidth>DensityUtil.dip2px(this@AskDetail,137.0f)){
            nicklength-=2
            val name_display=myDetailAsk!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.ask_detail_nick_tv!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.ask_detail_nick_tv!!.measure(spec,spec)
            nickwidth= holder!!.ask_detail_nick_tv!!.measuredWidth
            LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }

        //性别
        if (!myDetailAsk.gender.equals("")&&myDetailAsk.gender!=null){
            if(myDetailAsk.gender.equals("女")){
                holder.ask_detail_sex_tv!!.text="♀"
                holder.ask_detail_sex_tv!!.setTextColor(this@AskDetail.resources.getColor(R.color.ask_red))
                holder.ask_detail_sex_tv!!.visibility=View.VISIBLE
            }else{
                holder.ask_detail_sex_tv!!.text="♂"
                holder.ask_detail_sex_tv!!.setTextColor(this@AskDetail.resources.getColor(R.color.headline))
                holder.ask_detail_sex_tv!!.visibility=View.VISIBLE
            }
        }else{
            holder.ask_detail_sex_tv!!.visibility=View.GONE
        }

        //是否被采纳
        if(myDetailAsk.adviseComment!=null){
            update_isDeal="1"
            holder.ask_detail_deal_tv!!.visibility=View.VISIBLE
        }else{
            update_isDeal="0"
            holder.ask_detail_deal_tv!!.visibility=View.GONE
        }


        //左侧标签
        if(!myDetailAsk.label.equals("")){
            holder.ask_detail_left_type!!.setText("#"+myDetailAsk.label)
            if(myDetailAsk.label.equals("寻物启事")){
                holder.ask_detail_left_type!!.setTextColor(this@AskDetail.resources.getColor(R.color.ask_red))
            }else if(myDetailAsk.label.equals("失物招领")){
                holder.ask_detail_left_type!!.setTextColor(this@AskDetail.resources.getColor(R.color.ask_green))
            }else{
                holder.ask_detail_left_type!!.setTextColor(this@AskDetail.resources.getColor(R.color.ask_yellow))
            }

            holder.ask_detail_left_type!!.setOnClickListener{
                val intent: Intent =Intent(this@AskDetail, AskSearchResult::class.java)
                val bundle=Bundle()
                bundle.putString("key","")
                bundle.putString("type","")
                bundle.putString("label",myDetailAsk.label)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }

        holder.ask_detail_left_type!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                holder.ask_detail_left_type!!.alpha=0.618f
            }else{
                holder.ask_detail_left_type!!.alpha=1.0f
            }
            false
        }

        //时间
        val time = myDetailAsk.createTime!!.substring(2, 16).replace("T", " ")
        holder.ask_detail_time!!.text=time

        //内容
        holder.ask_detail_content!!.text=myDetailAsk.content
        holder.ask_detail_content!!.setOnLongClickListener{
            val cmb=this@AskDetail!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cmb.text= holder!!.ask_detail_content!!.text
            Toasty.info(this@AskDetail!!,"成功复制该帖子内容！").show()
            true
        }


        //是否有悬赏
        if(!myDetailAsk.reward.equals("0")){
            holder.ask_detail_reward_tv!!.visibility=View.VISIBLE
            holder.ask_detail_reward_num_tv!!.text=myDetailAsk.reward
            holder.ask_detail_reward_num_tv!!.visibility=View.VISIBLE
            holder.ask_detail_reward_tv2!!.visibility=View.VISIBLE
        }else{
            holder.ask_detail_reward_tv!!.visibility=View.GONE
            holder.ask_detail_reward_num_tv!!.visibility=View.GONE
            holder.ask_detail_reward_tv2!!.visibility=View.GONE
        }

        //是否是急问
        if(myDetailAsk.type.equals("急问")){
            holder.ask_detail_urgent_iv!!.visibility=View.VISIBLE
            holder.ask_detail_urgent_tv!!.visibility=View.VISIBLE
        }else{
            holder.ask_detail_urgent_iv!!.visibility=View.GONE
            holder.ask_detail_urgent_tv!!.visibility=View.GONE
        }

        //是否赞过
        if(bundle!=null&&bundle!!.getString("isZan")!=null&&!bundle!!.getString("isZan").equals("")){
            myDetailAsk.zanIs=bundle!!.getString("isZan")
        }
        if(myDetailAsk.zanIs.equals("1")){
            /*||bundle!!.getString("isZan").equals("1")*/
            holder.ask_detail_zan!!.setImageResource(R.mipmap.zan_blue)
        }else{
            holder.ask_detail_zan!!.setImageResource(R.mipmap.zan_gray)
        }

        //赞的数量
        holder.ask_detail_zan_num!!.text=myDetailAsk.zan
        update_zan=myDetailAsk.zan!!

        //评论的数量
        holder.ask_detail_comment_num!!.text=myDetailAsk.commentCount
        update_commentCount=myDetailAsk.commentCount!!

        //下方图片 1~N张布局显示
        var photos: ArrayList<String>?= ArrayList<String>()
        var photos_thum:ArrayList<String>?=ArrayList<String>()
        photos=myDetailAsk.image//将服务器传过来的网址给到photos
        photos_thum=myDetailAsk.thumbnailImage
        if(photos!=null&&photos.size>0){
            holder.detail_multiImageView!!.visibility=View.VISIBLE
            holder.detail_multiImageView!!.setList(photos_thum)
            holder.detail_multiImageView!!.setOnItemClickListener { view, position ->
                // 因为单张图片时，图片实际大小是自适应的，imageLoader缓存时是按测量尺寸缓存的
                ImagePagerActivity.imageSize = ImageSize(view.measuredWidth, view.measuredHeight)
                ImagePagerActivity.startImagePagerActivity(this@AskDetail, photos!!, position)
            }
        }else{
            holder.detail_multiImageView!!.visibility=View.GONE
        }

        //点赞监听
        holder.ask_detail_zan!!.setOnClickListener{
            if(mPresenter!=null){
                if(token.equals("")){
                    //提示登录
                    val intent = Intent(this@AskDetail, LoginMain::class.java)
                    this@AskDetail.startActivity(intent)
                }else if(!NetUtil.checkNetWork(this@AskDetail)){
                    Toasty.info(this@AskDetail,Constant.NONETWORK).show()
                }else if(bundle!=null&&bundle!!.getString("isZan").equals("1")){
                    Toasty.info(this@AskDetail,"亲，您已经赞过了").show()
                }else{
                   /* bundle!!.putString("isZan","1")*/
                    update_isZan="1"
                    holder.ask_detail_zan!!.setImageResource(R.mipmap.zan_blue)
                    mPresenter!!.addZan(myDetailAsk.id,token,this@AskDetail)
                }
            }
        }

        //分享点击事件处理
        holder.ask_detail_share_btn!!.setOnClickListener{
            flag_isPop=1
            showShareDialog(myDetailAsk.title!!,myDetailAsk.content!!,myDetailAsk.id!!,myDetailAsk.link!!+"?id=${myDetailAsk.id}")
        }

        var isAdopt=false
        //有采纳的评论
        if(null!=myDetailAsk.adviseComment){
            //处理采纳的评论
            isAdopt=true
            icon_adopt=myDetailAsk.adviseComment!!.icon!!
            getIcon(icon_adopt,holder.ask_detail_adopt_comment_icon!!)

            //点击了头像
            holder.ask_detail_adopt_comment_icon!!.setOnClickListener{

                val intent:Intent=Intent(this@AskDetail, IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",myDetailAsk.adviseComment!!.publisherID)
                bundle.putString("icon",myDetailAsk.adviseComment!!.icon)
                bundle.putString("sex",myDetailAsk.adviseComment!!.gender)
                bundle.putString("nickname",myDetailAsk.adviseComment!!.nickname)
                intent.putExtras(bundle)
                this@AskDetail.startActivity(intent)

            }

            //昵称
            holder.ask_detail_comment_nick_tv!!.text=myDetailAsk.adviseComment!!.nickname
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder.ask_detail_comment_nick_tv!!.measure(spec,spec)
            var nickwidth= holder.ask_detail_comment_nick_tv!!.measuredWidth
            LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nickname为：${myDetailAsk.adviseComment!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的137.0f为：${DensityUtil.dip2px(this@AskDetail,300.0f)} ")
            var nicklength=myDetailAsk.adviseComment!!.nickname!!.length
            while(nickwidth>DensityUtil.dip2px(this@AskDetail,137.0f)){
                nicklength-=2
                val name_display=myDetailAsk.adviseComment!!.nickname!!.substring(0,nicklength)+"..."
                holder.ask_detail_comment_nick_tv!!.text=name_display
                var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
                holder.ask_detail_comment_nick_tv!!.measure(spec,spec)
                nickwidth= holder.ask_detail_comment_nick_tv!!.measuredWidth
                LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")

            }

            //性别
            if(myDetailAsk.adviseComment!!.gender!!.equals("")){
                holder.ask_detail_comment_sex_tv!!.visibility=View.GONE
            }else if(myDetailAsk.adviseComment!!.gender!!.equals("女")){
                holder.ask_detail_comment_sex_tv!!.text="♀"
                holder.ask_detail_comment_sex_tv!!.setTextColor(this@AskDetail.resources.getColor(R.color.ask_red))
                holder.ask_detail_comment_sex_tv!!.visibility=View.VISIBLE
            }else if(myDetailAsk.adviseComment!!.gender!!.equals("男")){
                holder.ask_detail_comment_sex_tv!!.text="♂"
                holder.ask_detail_comment_sex_tv!!.setTextColor(this@AskDetail.resources.getColor(R.color.headline))
                holder.ask_detail_comment_sex_tv!!.visibility=View.VISIBLE
            }

            //时间
            val time = myDetailAsk.adviseComment!!.createTime!!.substring(2, 16).replace("T", " ")
            holder.ask_detail_comment_adopt_time_tv!!.text=time

            //内容
            val adopt_content=myDetailAsk.adviseComment!!.content!!
            holder.ask_detail_comment_adopt_content_tv!!.text=adopt_content

            holder.ask_detail_adopt_comment_ll!!.visibility=View.VISIBLE
        }else{
            isAdopt=false
            holder.ask_detail_adopt_comment_ll!!.visibility=View.GONE
        }

        //将帖子详情显示
        holder.ask_detail_list_ll!!.visibility=View.VISIBLE

        //点击空白处回复楼主
        holder.ask_detail_click2comment!!.setOnClickListener{
            val read = this@AskDetail.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token= read.getString("token", "")
            if(token.equals("")){
                val intent = Intent(this@AskDetail, LoginMain::class.java)
                this@AskDetail.startActivity(intent)
            }else if(!NetUtil.checkNetWork(this@AskDetail)){
                Toasty.info(this@AskDetail,Constant.NONETWORK).show()
            }else{
                val comment2host= CommentConfig()
                comment2host.askID=myDetailAsk.id!!
                comment2host.commentReceiverID=myDetailAsk.publisherID!!
                comment2host.commentType=CommentConfig.Type.PUBLIC
                comment2host.token=token
                comment2host.hint="@楼主："
                comment2host.commentPosition=0
                mPresenter!!.showEditTextBody(comment2host)
            }
        }

        holder.ask_detail_comment_btn!!.setOnClickListener{
            if(token.equals("")){
                val intent = Intent(this@AskDetail, LoginMain::class.java)
                this@AskDetail.startActivity(intent)
            }else if(!NetUtil.checkNetWork(this@AskDetail)){
                Toasty.info(this@AskDetail,Constant.NONETWORK).show()
            }else{
                val comment2host= CommentConfig()
                comment2host.askID=myDetailAsk.id!!
                comment2host.commentReceiverID=myDetailAsk.publisherID!!
                comment2host.commentType=CommentConfig.Type.PUBLIC
                comment2host.token=token
                comment2host.hint="@楼主："
                comment2host.commentPosition=0
                mPresenter!!.showEditTextBody(comment2host)
            }
        }

        holder.ask_detail_sendBtn!!.setOnClickListener{
            if(mPresenter!=null){
                val content=holder.detail_commentEt!!.text.toString().trim{it<=' '}
                if(TextUtils.isEmpty(content)){
                    Toasty.info(this@AskDetail,"评论内容不能为空喔...").show()
                    return@setOnClickListener
                }
                loading2master=Loading_view(this,R.style.CustomDialog)
                loading2master!!.show()
                holder.ask_detail_springView!!.header.onStartAnim()
                mCommentConfig!!.content=content
                mPresenter!!.addComment(this@AskDetail,content,mCommentConfig)
            }
            hideEditTextBody()
        }

        if( holder.detail_commentEt!!!=null)
           makeATextWatch(holder.detail_commentEt!!)

        //点赞监听
        holder.ask_detail_zan_btn!!.setOnClickListener{
            if(mPresenter!=null){
                if(token.equals("")){
                    //提示登录
                    val intent = Intent(this@AskDetail, LoginMain::class.java)
                    this@AskDetail.startActivity(intent)
                }else if(!NetUtil.checkNetWork(this@AskDetail)){
                    Toasty.info(this@AskDetail,Constant.NONETWORK).show()
                }else if(bundle!=null&&bundle!!.getString("isZan").equals("1")){
                    Toasty.info(this@AskDetail,"亲，您已经赞过了").show()
                }else{
                    /*bundle!!.putString("isZan","1")*/
                    holder.ask_detail_zan!!.setImageResource(R.mipmap.zan_blue)
                    mPresenter!!.addZan(myDetailAsk.id,token,this@AskDetail)
                }
            }
        }

        var isAdoptID=""
        if(isAdopt){
            isAdoptID=myDetailAsk.adviseComment!!.id!!
        }
        getCommentFromServer(myDetailAsk.publisherID!!,isAdopt,isAdoptID!!,pageNow, holder.ask_detail_springView!!)//更新评论


    }

    //测量scrollview的偏移量
    private fun getScrollviewOffset(commentConfig: CommentConfig?):Int{
        if(commentConfig==null) return 0
        val dm = DisplayMetrics()
        val wm = this@AskDetail.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(dm)
        mScreenHeight=dm.heightPixels
        val r= Rect()
        holder.ask_detail_sv!!.getWindowVisibleDisplayFrame(r)
        val statusBarH=getStatusBarHeight()
        if(r.top!=statusBarH){
            r.top=statusBarH
        }
        mCurrentKeyBoardH=mScreenHeight-(r.bottom-r.top)
        mEditTextBodyHeight=holder.ask_detail_editTextVodyLl!!.height

        LogUtils.d_debugprint(Constant.Ask_TAG,"屏幕的高度："+mScreenHeight+"\n当前键盘的高度："+mCurrentKeyBoardH+"输入框的高度："+mEditTextBodyHeight)
        var scrollviewOffset=mScreenHeight-mCurrentKeyBoardH-mEditTextBodyHeight-getStatusBarHeight()
        if(commentConfig.commentType==CommentConfig.Type.REPLY){
            //回复评论的情况
            //scrollviewOffset=scrollviewOffset+mSelectCommentItemOffset
            LogUtils.d_debugprint(Constant.Ask_TAG,"评论+帖子的高度："+mSelectCommentItemOffset+"\n可视空间的高度："+scrollviewOffset)
            scrollviewOffset=mSelectCommentItemOffset-scrollviewOffset

        }else{
            scrollviewOffset=0
        }
        return scrollviewOffset
    }

    fun makeATextWatch(edit:EditText) {
        class CustomTextWatcher: TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (holder.detail_commentEt!!.text.toString().trim{it<=' '}.length>0) {
                    holder.ask_detail_sendBtn!!.isEnabled=true
                    holder.ask_detail_sendBtn!!.setBackgroundResource(R.drawable.button_shape)
                } else  {
                    holder.ask_detail_sendBtn!!.isEnabled=false
                    holder.ask_detail_sendBtn!!.setBackgroundResource(R.drawable.button_shape_unavailable)
                }
            }
        }
        edit!!.addTextChangedListener(CustomTextWatcher())
    }

    //获取状态栏的高度
    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    //键盘弹出时，计算评论和帖子的高度，让键盘处于正确的位置
    private fun measureAskItemHighAndCommentItemOffset(commentConfig: CommentConfig?){
        if(commentConfig==null) return
        val selectAskItem:View=holder.ask_detail_rl!!
        if(selectAskItem!=null){
            mSelectAskItemH=selectAskItem.height
        }
        if(commentConfig.commentType==CommentConfig.Type.REPLY){//回复某一个人
            val commentLv=holder.ask_detail_commentList!!
            if(commentLv!=null){
                LogUtils.d_debugprint(Constant.Ask_TAG,"选择评论位置为："+commentConfig.commentPosition)
                val selectCommentItem:View=commentLv.getChildAt(commentConfig.commentPosition)
                if(selectCommentItem!=null){
                    //选择的commentItem距选择的CircleItem底部的距离
                    mSelectCommentItemOffset = 0
                    if(commentConfig.commentPosition>=0){
                         var currentTop=selectCommentItem.top
                         var currentBottom=selectCommentItem.bottom
                         var currentHight=selectCommentItem.height
                         var parentTop=holder.ask_comment_all_ll!!.top
                         var parentBottom=holder.ask_comment_all_ll!!.bottom
                         var parentHight=holder.ask_comment_all_ll!!.height

                         mSelectCommentItemOffset=currentBottom+parentTop+DensityUtil.dip2px(this@AskDetail,120.0f)

                        //LogUtils.d_debugprint(Constant.Ask_TAG,"选择的评论的top :"+currentTop+"\n选择评论的bottom："+currentBottom+"\n选择评论的hight"+currentHight
                         //       +"\n评论布局的top："+parentTop+"\n评论布局的bottom："+parentBottom+"\n评论布局的height"+parentHight+"\n偏移量为："+mSelectCommentItemOffset)



                    }
                }
            }
        }
    }

    fun showDialog(id:String){
        val contentView:View= LayoutInflater.from(this@AskDetail).inflate(R.layout.ask_three_point_dialog,null)
        hanleDialog(contentView,id)
        popWindow= CustomPopWindow.PopupWindowBuilder(this@AskDetail)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow!!.showAtLocation(title_right_imageview_btn, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(this@AskDetail,260.0f))
    }

    private fun hanleDialog(contentView:View,id: String){
        ask_three_favorite=contentView.findViewById(R.id.ask_three_favorite) as TextView
        ask_three_report=contentView.findViewById(R.id.ask_three_report) as TextView
        ask_three_cancle=contentView.findViewById(R.id.ask_three_cancle) as TextView
        val ask_three_shielding=contentView.findViewById(R.id.ask_three_shielding) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_three_favorite -> make_favorite(id)
                    R.id.ask_three_report   -> make_report(id)
                    R.id.ask_three_shielding-> make_shielding(id)
                    R.id.ask_three_cancle   -> popWindow!!.dissmiss()
                }
            }
        }
        ask_three_shielding!!.setOnClickListener(listener)
        ask_three_favorite!!.setOnClickListener(listener)
        ask_three_report!!.setOnClickListener(listener)
        ask_three_cancle!!.setOnClickListener(listener)
    }

    private fun make_shielding(id:String){
        //添加到屏蔽的帖子全局文件
        val read = this@AskDetail.getSharedPreferences(Constant.ShareFile_BlackAsk, Context.MODE_PRIVATE)
        val blacklist = read.getString("blackask", "[]")

        val blacklist_array=ArrayList<String>()
        val jsonArray= JSONArray(blacklist)
        if(jsonArray.length()>0){
            for(i in 0..jsonArray.length()-1){
                blacklist_array.add(jsonArray.getString(i))
            }
        }
        blacklist_array.add(id)
        val jsonArray_new= JSONArray()
        for(i in blacklist_array.indices){
            jsonArray_new.put(blacklist_array[i])
        }

        val editor: SharedPreferences.Editor=this@AskDetail.getSharedPreferences(Constant.ShareFile_BlackAsk, Context.MODE_PRIVATE).edit()
        editor.putString("blackask",jsonArray_new.toString())
        val read2 = this@AskDetail.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val userID= read2.getString("id", "")
        editor.putString("userID",userID)
        editor.commit()

        LogUtils.d_debugprint(Constant.Ask_TAG,"\n黑名单帖子id：原来的json为：$blacklist\n现在的json为：${jsonArray_new.toString()} \n当前用户id为：$userID")

        val intent:Intent=Intent(this@AskDetail, NavigationMainActivity::class.java)
        intent.putExtra("fragid",2)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)

    }

    private fun make_favorite(id:String){
        val token=get_token()
        //开始收藏=====请求服务器
        if(token!=""){

            val pDialog = SweetAlertDialog(this@AskDetail, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true)
            pDialog.show()

            AskMainRequest.makeFavoriteFromServer(this@AskDetail, Constant.Ask_TAG, Constant.Ask_Favorite, id, token, pDialog)
        }
    }

    private fun make_report(id:String){
        val token=get_token()
        if(token!=""){
            val contentView:View=LayoutInflater.from(this@AskDetail).inflate(R.layout.ask_five_report_dialog,null)
            hanle_report_Dialog(contentView,id)
            popWindow_report= CustomPopWindow.PopupWindowBuilder(this@AskDetail)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_three_anim)
                    .create()
            popWindow_report!!.showAtLocation(title_right_imageview_btn,Gravity.CENTER_HORIZONTAL,0,DensityUtil.dip2px(this@AskDetail,215.0f))

        }
    }

    private fun hanle_report_Dialog(contentView:View,id: String){
        ask_report_five1=contentView.findViewById(R.id.ask_five_1) as TextView
        ask_report_five2=contentView.findViewById(R.id.ask_five_2) as TextView
        ask_report_five3=contentView.findViewById(R.id.ask_five_3) as TextView
        ask_report_five4=contentView.findViewById(R.id.ask_five_4) as TextView
        ask_report_cancle=contentView.findViewById(R.id.ask_five_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow_report!=null){
                    popWindow_report!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_five_1 -> make_report(id,"欺诈骗钱")
                    R.id.ask_five_2 -> make_report(id,"色情暴力")
                    R.id.ask_five_3 -> make_report(id,"广告骚扰")
                    R.id.ask_five_4 -> make_report_other(id,"")
                    R.id.ask_five_cancle  -> popWindow_report!!.dissmiss()
                }
            }
        }
        ask_report_five1!!.setOnClickListener(listener)
        ask_report_five2!!.setOnClickListener(listener)
        ask_report_five3!!.setOnClickListener(listener)
        ask_report_five4!!.setOnClickListener(listener)
        ask_report_cancle!!.setOnClickListener(listener)
    }

    private fun make_report(id:String,reason:String){
        val token=get_token()
        //开始举报=====请求服务器
        if(token!=""){

            val pDialog = SweetAlertDialog(this@AskDetail, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true)
            pDialog.show()
            AskMainRequest.makeInformFromServer(this@AskDetail, Constant.Ask_TAG, Constant.Inform_Ask, id, token, reason, pDialog)

        }
    }

    private fun make_report_other(id:String,reson:String){
        val token=get_token()
        //开始收藏=====请求服务器
        if(token!=""){
            val intent = Intent(this@AskDetail, AskReport::class.java)
            val bundle= Bundle()
            bundle.putString("id",id)
            bundle.putString("token",token)
            intent.putExtras(bundle)
            this@AskDetail.startActivity(intent)
        }
    }

    private fun get_token():String{
        val read = this@AskDetail.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(this@AskDetail, LoginMain::class.java)
            this@AskDetail.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            if (keyCode == KeyEvent.KEYCODE_BACK && event!!.repeatCount == 0) {
                if (holder.ask_detail_editTextVodyLl != null && holder.ask_detail_editTextVodyLl!!.visibility == View.VISIBLE) {
                    holder.ask_detail_editTextVodyLl!!.visibility=View.GONE
                    return true
                }else{
                    /*val intent:Intent=Intent(this@AskDetail, NavigationMainActivity::class.java)
                    intent.putExtra("fragid",2)
                    startActivity(intent)
                    overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)*/

                    val editor: SharedPreferences.Editor=getSharedPreferences("update_ask", Context.MODE_APPEND).edit()
                    editor.putString("isZan",update_isZan)
                    editor.putString("zan",update_zan)
                    editor.putString("commentCount",update_commentCount)
                    editor.putString("isDeal",update_isDeal)
                    editor.commit()

                    if(flag_isPop==0){
                        finish()
                        overridePendingTransition(0,R.anim.slide_right_out)
                    }else{
                        finish()
                    }
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    fun showShareDialog(title:String,content:String,id:String,link:String){
        val contentView:View=LayoutInflater.from(this@AskDetail).inflate(R.layout.ask_share,null)
        hanleShareDialog(contentView,id,title,content,link)//这个函数还没有写
        popWindow_share= CustomPopWindow.PopupWindowBuilder(this@AskDetail)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_share_anim)
                .create()
        popWindow_share!!.showAtLocation(holder.ask_detail_share,Gravity.CENTER,0,0)

    }

    private fun hanleShareDialog(contentView: View,id: String,title: String,content: String,link:String){
        ask_share_qq=contentView.findViewById(R.id.ask_share_qq) as RelativeLayout
        ask_share_wechat=contentView.findViewById(R.id.ask_share_wechat) as RelativeLayout
        ask_share_qq_zone=contentView.findViewById(R.id.ask_share_qq_zone) as RelativeLayout
        ask_share_wechat_friends=contentView.findViewById(R.id.ask_share_wechat_friends) as RelativeLayout
        ask_share_copy=contentView.findViewById(R.id.ask_share_copy) as RelativeLayout


        ask_share_qq!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id, QQ.Name,"QQFriend")
        }

        ask_share_qq_zone!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id, QZone.Name,"QQZone")
        }

        ask_share_wechat!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id, Wechat.Name,"weChatFriend")
        }

        ask_share_wechat_friends!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id, WechatMoments.Name,"weChatCircle")
        }

        ask_share_copy!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            val cm=this@AskDetail.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val mClipData= ClipData.newPlainText("Label",link+"?id="+id)
            cm.primaryClip=mClipData
            Toasty.info(this@AskDetail,"亲，复制成功，赶快分享一下吧~").show()
        }

    }

    private fun shareAsk(title:String,content:String,link:String,id:String,shareType:String,shareWay:String){
        JEventUtils.onCountEvent(this@AskDetail,Constant.Event_AskShare)//分享帖子内容统计

        val shareParams= ShareParams()
        shareParams.shareType= Platform.SHARE_WEBPAGE
        var display_content=content
        if(content.length>30){
            display_content=content.substring(0,30)+"..."
        }
        shareParams.text=display_content
        shareParams.title=title
        if(shareType.equals(WechatMoments.Name)){
            shareParams.title=title
        }
        shareParams.url=link
        shareParams.imagePath=MainApplication.ImagePath

        val read = this@AskDetail.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        share_token = read.getString("token", "")
        share_id=id
        share_shareWay=shareWay

        JShareInterface.share(shareType, shareParams, mPlatActionListener)
    }

    private val mPlatActionListener = object : PlatActionListener {
        override fun onComplete(platform: Platform, action: Int, data: HashMap<String, Any>) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子详情分享成功！！！")
            Toasty.info(this@AskDetail,"亲，分享成功！").show()
            AskMainRequest.makeShareToServer(Constant.Ask_TAG,Constant.URL_Share_Ask,this@AskDetail,share_id,share_shareWay,share_token)
        }

        override fun onError(platform: Platform, action: Int, errorCode: Int, error: Throwable) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子详情分享失败！！！")
            Toasty.info(this@AskDetail,"亲，帖子分享失败！").show()
        }

        override fun onCancel(platform: Platform, action: Int) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子详情分享取消！！！")
            Toasty.info(this@AskDetail,"亲，您已取消分享！").show()
        }
    }

    private fun getIcon(icon:String,image:ImageView) {
        if (icon !== "") {
            ImageLoader.getInstance().displayImage(icon!!, image)
        }
    }

    inner class ViewHolder{
        var flag_isLoading=false
        var detail_icon_iv:ImageView?=null
        var ask_detail_left_type:Button?=null
        var ask_detail_title_tv:TextView?=null
        var ask_detail_deal_tv:TextView?=null
        var ask_detail_nick_tv:TextView?=null
        var ask_detail_sex_tv:TextView?=null
        var ask_detail_time:TextView?=null
        var ask_detail_reward_tv:TextView?=null
        var ask_detail_reward_num_tv:TextView?=null
        var ask_detail_reward_tv2:TextView?=null
        var ask_detail_urgent_iv:ImageView?=null
        var ask_detail_urgent_tv:TextView?=null
        var ask_detail_content:TextView?=null
        var ask_detail_share:ImageView?=null
        var ask_detail_share_btn:Button?=null
        var ask_detail_zan_btn:Button?=null
        var ask_detail_zan:ImageView?=null
        var ask_detail_comment_btn:Button?=null
        var ask_detail_zan_num:TextView?=null
        var ask_detail_comment_num:TextView?=null
        var ask_detail_comment:ImageView?=null
        var detail_multiImageView: MultiImageView?=null
        var ask_detail_adopt_comment_ll:LinearLayout?=null
        var ask_detail_adopt_comment_icon:ImageView?=null
        var ask_detail_comment_nick_tv:TextView?=null
        var ask_detail_comment_sex_tv:TextView?=null
        var ask_detail_comment_adopt_tv:TextView?=null
        var ask_detail_comment_adopt_time_tv:TextView?=null
        var ask_detail_comment_adopt_content_tv:TextView?=null
        var ask_detail_commentEt: EditText?=null
        var ask_detail_sendBtn:Button?=null
        var ask_detail_list_ll:LinearLayout?=null
        var ask_detail_commentList:CommentListView?=null
        var ask_comment_all_ll:LinearLayout?=null
        var commentAdapter:CommentAdapter?=null
        var ask_detail_sv:ScrollView?=null
        var ask_detail_springView:SpringView?=null
        var ask_detail_click2comment:LinearLayout?=null
        var ask_detail_editTextVodyLl:LinearLayout?=null
        var ask_detail_rl:RelativeLayout?=null
        var ask_detail_comment_ll:LinearLayout?=null
        var ask_detail_list_layout:LinearLayout?=null
        var ask_outter_rl:RelativeLayout?=null
        var detail_commentEt:EditText?=null
        var ask_detail_title_rl:RelativeLayout?=null
    }
    
    fun initHolder(){
        holder.ask_detail_title_rl=findViewById(R.id.ask_detail_title_rl) as RelativeLayout
        holder.ask_detail_rl=findViewById(R.id.ask_detail_rl) as RelativeLayout
        holder.detail_icon_iv=findViewById(R.id.ask_detail_icon) as ImageView
        holder.ask_detail_left_type=findViewById(R.id.ask_detail_left_type) as Button
        holder.ask_detail_title_tv=findViewById(R.id.ask_detail_title_tv) as TextView
        holder.ask_detail_deal_tv=findViewById(R.id.ask_detail_deal_tv) as TextView
        holder.ask_detail_nick_tv=findViewById(R.id.ask_detail_nick_tv) as TextView
        holder.ask_detail_sex_tv=findViewById(R.id.ask_detail_sex_tv) as TextView
        holder.ask_detail_time=findViewById(R.id.ask_detail_time) as TextView
        holder.ask_detail_reward_tv=findViewById(R.id.ask_detail_reward_tv) as TextView
        holder.ask_detail_reward_num_tv=findViewById(R.id.ask_detail_reward_num_tv) as TextView
        holder.ask_detail_reward_tv2=findViewById(R.id.ask_detail_reward_tv2) as TextView
        holder.ask_detail_urgent_iv=findViewById(R.id.ask_detail_urgent_iv) as ImageView
        holder.ask_detail_urgent_tv=findViewById(R.id.ask_detail_urgent_tv) as TextView
        holder.ask_detail_content=findViewById(R.id.ask_detail_content) as TextView
        holder.ask_detail_share=findViewById(R.id.ask_detail_share) as ImageView
        holder.ask_detail_share_btn=findViewById(R.id.ask_detail_share_btn) as Button
        holder.ask_detail_zan_btn=findViewById(R.id.ask_detail_zan_btn) as Button
        holder.ask_detail_comment_btn=findViewById(R.id.ask_detail_comment_btn) as Button
        holder.ask_detail_zan=findViewById(R.id.ask_detail_zan) as ImageView
        holder.ask_detail_zan_num=findViewById(R.id.ask_detail_zan_num_tv) as TextView
        holder.ask_detail_comment=findViewById(R.id.ask_detail_comment) as ImageView
        holder.ask_detail_comment_num=findViewById(R.id.ask_detail_comment_num_tv) as TextView
        holder.detail_multiImageView=findViewById(R.id.ask_detail_multi_iv) as MultiImageView
        holder.ask_detail_adopt_comment_ll=findViewById(R.id.ask_detail_adopt_comment_ll) as LinearLayout
        holder.ask_detail_adopt_comment_icon=findViewById(R.id.ask_detail_adopt_comment_icon) as ImageView
        holder.ask_detail_comment_nick_tv=findViewById(R.id.ask_detail_comment_nick_tv) as TextView
        holder.ask_detail_comment_sex_tv=findViewById(R.id.ask_detail_comment_sex_tv) as TextView
        holder.ask_detail_comment_adopt_tv=findViewById(R.id.ask_detail_adopt_content_tv) as TextView
        holder.ask_detail_comment_adopt_time_tv=findViewById(R.id.ask_detail_adopt_time_tv) as TextView
        holder.ask_detail_comment_adopt_content_tv=findViewById(R.id.ask_detail_adopt_content_tv) as TextView
        holder.ask_detail_editTextVodyLl=findViewById(R.id.detail_editTextBodyLl) as LinearLayout
        holder.detail_commentEt=findViewById(R.id.detail_commentEt) as EditText
        holder.ask_detail_sendBtn=findViewById(R.id.detail_sendIv) as Button
        holder.ask_detail_list_ll=findViewById(R.id.ask_detail_list_layout) as LinearLayout
        holder.ask_detail_list_ll!!.visibility=View.GONE
        holder.ask_detail_commentList=findViewById(R.id.ask_detail_commentList) as CommentListView
        holder.ask_comment_all_ll=findViewById(R.id.ask_comment_all_ll) as LinearLayout
        holder.commentAdapter=CommentAdapter(this@AskDetail)//处理评论列表,还没有填充数据进去
        holder.ask_detail_commentList!!.setAdapter(holder.commentAdapter)//产生必要联系
        holder.ask_detail_click2comment=findViewById(R.id.ask_detail_click2comment_ll) as LinearLayout
        holder.ask_detail_editTextVodyLl=findViewById(R.id.detail_editTextBodyLl) as LinearLayout
        holder.ask_detail_commentEt=findViewById(R.id.detail_commentEt) as EditText
        holder.ask_detail_sv=findViewById(R.id.ask_detail_sv) as ScrollView
        holder.ask_detail_springView=findViewById(R.id.ask_detail_springView) as SpringView
        holder.ask_detail_springView!!.header=DefaultHeader(this@AskDetail)
        holder.ask_detail_springView!!.footer= DefaultFooter(this@AskDetail)
        holder.ask_detail_comment_ll=findViewById(R.id.ask_detail_comment_ll) as LinearLayout
        holder.ask_detail_list_layout=findViewById(R.id.ask_detail_list_layout) as LinearLayout
        holder.ask_outter_rl=findViewById(R.id.ask_outter_rl) as RelativeLayout
        

        holder.ask_detail_springView!!.setListener(object:SpringView.OnFreshListener{

            override fun onRefresh() {
                if(!holder.flag_isLoading){

                    holder.flag_isLoading=true
                    holder.ask_detail_springView!!.isEnable=false


                    Handler().postDelayed({
                        pageNow=0
                        getDetailFromServer(pageNow)

                    },100)
                }

            }

            override fun onLoadmore() {
                if(!holder.flag_isLoading){

                    holder.flag_isLoading=true
                    holder.ask_detail_springView!!.isEnable=false


                    Handler().postDelayed({
                        //getCommentFromServer(pageNow++)
                        getDetailFromServer(++pageNow)
                       // holder.commentAdapter!!.notifyDataSetChanged()

                    },100)
                }
            }
        })
    }

    private fun getDetailFromServer(pageNow:Int){
        if(bundle!=null){
            AskDetailRequest.getAskDetailServer(this@AskDetail,this@AskDetail,Constant.Ask_TAG,Constant.Ask_Detail,bundle!!.getString("id"),pageNow,holder!!.ask_detail_springView!!,object: DetailRequestListener {
              override fun loadSuccess(myDetailAsk: MyDetailAsk,pageNow:Int) {
                    updateView(myDetailAsk,pageNow)
                   /* holder.flag_isBottomLoading=false
                    holder.flag_isUpLoading=false*/
                }
            })
           // getAskDetailServer(this@AskDetail,Constant.Ask_TAG,Constant.Ask_Detail,bundle!!.getString("id"),pageNow,holder!!.ask_detail_springView!!)
        }
    }

    private fun getCommentFromServer(askPublishID:String,isAdopt:Boolean,isAdoptID:String,pageNow:Int,springView:SpringView){
        if(bundle!=null){
            AskDetailRequest.getCommentFromServer(askPublishID,isAdopt,isAdoptID,this@AskDetail,Constant.Ask_TAG,Constant.Ask_List_Comment,bundle!!.getString("id"),pageNow,pageSize,orderBy,order,listItems,holder.commentAdapter!!,holder.ask_detail_springView!!,mPresenter!!,object:IDataRequestListener2String{
                override fun loadSuccess(reponse_string: String?) {
                    holder.flag_isLoading=false
                    holder.ask_detail_springView!!.isEnable=true
                    if(springView.header!=null){
                        springView.header.onFinishAnim()
                    }
                    if(springView.footer!=null){
                        springView.footer.onFinishAnim()
                    }
                    if(loading2master!=null){
                        loading2master!!.dismiss()
                    }
                    if(loading2user!=null){
                        loading2user!!.dismiss()
                    }
                    loading_first!!.cancel()//加载动画结束
                }
            })
        }
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(this@AskDetail)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }

}
