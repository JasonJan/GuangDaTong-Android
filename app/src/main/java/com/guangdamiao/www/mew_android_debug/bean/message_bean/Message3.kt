package com.guangdamiao.www.mew_android_debug.bean.message_bean

import java.io.Serializable


class Message3 : Serializable {

    var id: String? = null
    var icon:String?=null
    var nickname:String?=null
    var senderID:String?=null
    var createTime:String?=null
    var detail: String? = null
    var type: String? = null
    var gender: String? = null
    var isRead:String?=""
   
    
    constructor():super()
    constructor(isRead:String,id:String,senderID:String,detail:String,type: String,nickname:String,gender:String,createTime:String,icon:String){
        this.id=id
        this.isRead=isRead
        this.senderID=senderID
        this.detail=detail
        this.type=type
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.gender=gender
    }

    override fun toString(): String {
        return "Message3[id=$id,\nsenderID=$senderID,\ncreateTime=$createTime,\nicon=$icon,\nnickname=$nickname,\ngender=$gender]"
    }
}
